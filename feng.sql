/*
Navicat MySQL Data Transfer

Source Server         : lalala
Source Server Version : 50709
Source Host           : localhost:3306
Source Database       : feng

Target Server Type    : MYSQL
Target Server Version : 50709
File Encoding         : 65001

Date: 2016-10-11 17:15:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `name` char(20) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `city` varchar(50) NOT NULL,
  `dist` varchar(50) NOT NULL,
  `addr` varchar(100) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of address
-- ----------------------------

-- ----------------------------
-- Table structure for admusers
-- ----------------------------
DROP TABLE IF EXISTS `admusers`;
CREATE TABLE `admusers` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `uname` char(20) NOT NULL,
  `upwd` char(32) NOT NULL,
  `sex` enum('w','m','x') DEFAULT 'x',
  `uface` varchar(50) DEFAULT 'normal.jpg',
  `birth` int(10) unsigned DEFAULT '0',
  `tel` varchar(15) DEFAULT NULL,
  `addr` varchar(100) DEFAULT NULL,
  `auth` tinyint(3) unsigned DEFAULT '0',
  `email` varchar(30) DEFAULT NULL,
  `regtime` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admusers
-- ----------------------------

-- ----------------------------
-- Table structure for adpic
-- ----------------------------
DROP TABLE IF EXISTS `adpic`;
CREATE TABLE `adpic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `adid` int(11) NOT NULL,
  `adtitle` varchar(20) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of adpic
-- ----------------------------

-- ----------------------------
-- Table structure for advertlist
-- ----------------------------
DROP TABLE IF EXISTS `advertlist`;
CREATE TABLE `advertlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `bbsid` int(11) NOT NULL,
  `bbstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of advertlist
-- ----------------------------

-- ----------------------------
-- Table structure for articlelist
-- ----------------------------
DROP TABLE IF EXISTS `articlelist`;
CREATE TABLE `articlelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `artid` int(11) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of articlelist
-- ----------------------------

-- ----------------------------
-- Table structure for bbs_publish
-- ----------------------------
DROP TABLE IF EXISTS `bbs_publish`;
CREATE TABLE `bbs_publish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `bbsid` int(11) NOT NULL,
  `bbstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `bpic` varchar(100) DEFAULT NULL,
  `bvideo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bbs_publish
-- ----------------------------

-- ----------------------------
-- Table structure for bbs_review
-- ----------------------------
DROP TABLE IF EXISTS `bbs_review`;
CREATE TABLE `bbs_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `bbsid` int(11) NOT NULL,
  `bbstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `bpic` varchar(100) DEFAULT NULL,
  `bvideo` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bbs_review
-- ----------------------------

-- ----------------------------
-- Table structure for bbscomment
-- ----------------------------
DROP TABLE IF EXISTS `bbscomment`;
CREATE TABLE `bbscomment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `bbsid` int(11) NOT NULL,
  `bbstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bbscomment
-- ----------------------------

-- ----------------------------
-- Table structure for bbslist
-- ----------------------------
DROP TABLE IF EXISTS `bbslist`;
CREATE TABLE `bbslist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `bbsid` int(11) NOT NULL,
  `bbstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bbslist
-- ----------------------------

-- ----------------------------
-- Table structure for bbspic
-- ----------------------------
DROP TABLE IF EXISTS `bbspic`;
CREATE TABLE `bbspic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `bbsid` int(11) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bbspic
-- ----------------------------

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `adid` int(11) NOT NULL,
  `adtitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of feedback
-- ----------------------------

-- ----------------------------
-- Table structure for news_publish
-- ----------------------------
DROP TABLE IF EXISTS `news_publish`;
CREATE TABLE `news_publish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `newsid` int(11) NOT NULL,
  `newstitle` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `npic` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news_publish
-- ----------------------------

-- ----------------------------
-- Table structure for news_review
-- ----------------------------
DROP TABLE IF EXISTS `news_review`;
CREATE TABLE `news_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `newsid` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `npic` varchar(100) DEFAULT 'normal.jpg',
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news_review
-- ----------------------------

-- ----------------------------
-- Table structure for newscomment
-- ----------------------------
DROP TABLE IF EXISTS `newscomment`;
CREATE TABLE `newscomment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `newsid` int(11) NOT NULL,
  `newstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newscomment
-- ----------------------------

-- ----------------------------
-- Table structure for newslist
-- ----------------------------
DROP TABLE IF EXISTS `newslist`;
CREATE TABLE `newslist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `newsid` int(11) NOT NULL,
  `newstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newslist
-- ----------------------------

-- ----------------------------
-- Table structure for newspic
-- ----------------------------
DROP TABLE IF EXISTS `newspic`;
CREATE TABLE `newspic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `newsid` int(11) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newspic
-- ----------------------------

-- ----------------------------
-- Table structure for systembase
-- ----------------------------
DROP TABLE IF EXISTS `systembase`;
CREATE TABLE `systembase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webname` char(20) NOT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `dataid` int(11) NOT NULL,
  `descrb` varchar(100) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `footinf` varchar(255) DEFAULT NULL,
  `footnum` varchar(100) DEFAULT NULL,
  `des` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of systembase
-- ----------------------------

-- ----------------------------
-- Table structure for systemdata
-- ----------------------------
DROP TABLE IF EXISTS `systemdata`;
CREATE TABLE `systemdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `dataid` int(11) NOT NULL,
  `datatitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of systemdata
-- ----------------------------

-- ----------------------------
-- Table structure for systemshielding
-- ----------------------------
DROP TABLE IF EXISTS `systemshielding`;
CREATE TABLE `systemshielding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of systemshielding
-- ----------------------------

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type` (
  `tid` int(10) NOT NULL AUTO_INCREMENT,
  `tname` varchar(120) NOT NULL,
  `pid` int(10) unsigned DEFAULT '0',
  `path` varchar(120) DEFAULT '0-',
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of type
-- ----------------------------

-- ----------------------------
-- Table structure for usefullinks_list
-- ----------------------------
DROP TABLE IF EXISTS `usefullinks_list`;
CREATE TABLE `usefullinks_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `linkid` int(11) NOT NULL,
  `linktitle` varchar(20) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usefullinks_list
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `uname` char(20) NOT NULL,
  `upwd` char(32) NOT NULL,
  `sex` enum('w','m','x') DEFAULT 'x',
  `uface` varchar(50) DEFAULT 'normal.jpg',
  `tel` varchar(15) DEFAULT NULL,
  `addr` varchar(100) DEFAULT NULL,
  `auth` tinyint(3) unsigned DEFAULT '0',
  `email` varchar(30) DEFAULT NULL,
  `lv` varchar(3) DEFAULT NULL,
  `regtime` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------

-- ----------------------------
-- Table structure for viewpoint_review
-- ----------------------------
DROP TABLE IF EXISTS `viewpoint_review`;
CREATE TABLE `viewpoint_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `detailid` int(11) NOT NULL,
  `goodsid` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of viewpoint_review
-- ----------------------------

-- ----------------------------
-- Table structure for viewpointpic
-- ----------------------------
DROP TABLE IF EXISTS `viewpointpic`;
CREATE TABLE `viewpointpic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `viewpointid` int(11) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of viewpointpic
-- ----------------------------

-- ----------------------------
-- Table structure for vp_publish
-- ----------------------------
DROP TABLE IF EXISTS `vp_publish`;
CREATE TABLE `vp_publish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `newsid` int(11) NOT NULL,
  `newstitle` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `npic` varchar(100) DEFAULT 'normal.jpg',
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vp_publish
-- ----------------------------

-- ----------------------------
-- Table structure for vpcomment
-- ----------------------------
DROP TABLE IF EXISTS `vpcomment`;
CREATE TABLE `vpcomment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `vpid` int(11) NOT NULL,
  `vptitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vpcomment
-- ----------------------------
