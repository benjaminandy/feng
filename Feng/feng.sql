/*
Navicat MySQL Data Transfer

Source Server         : lalala
Source Server Version : 50709
Source Host           : localhost:3306
Source Database       : feng

Target Server Type    : MYSQL
Target Server Version : 50709
File Encoding         : 65001

Date: 2016-10-18 15:33:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `name` char(20) NOT NULL,
  `tel` varchar(15) NOT NULL,
  `city` varchar(50) NOT NULL,
  `dist` varchar(50) NOT NULL,
  `addr` varchar(100) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of address
-- ----------------------------

-- ----------------------------
-- Table structure for admusers
-- ----------------------------
DROP TABLE IF EXISTS `admusers`;
CREATE TABLE `admusers` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `uname` char(20) NOT NULL,
  `upwd` char(32) NOT NULL,
  `sex` enum('男','女','保密') DEFAULT '男',
  `uface` varchar(50) DEFAULT 'normal.jpg',
  `birth` varchar(255) DEFAULT NULL,
  `addr` varchar(100) DEFAULT NULL,
  `role` enum('超级管理员','普通管理员','编辑','实习员工') DEFAULT '普通管理员' COMMENT '角色管理',
  `email` varchar(30) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `regtime` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admusers
-- ----------------------------
INSERT INTO `admusers` VALUES ('1', 'dada', '123', '男', '03_avatar_middle.jpg', '520', '山口山1', '超级管理员', '', '', '2016');
INSERT INTO `admusers` VALUES ('2', 'admin', '123', '男', '24_avatar_middle.jpg', '314', '山口山2', '超级管理员', '1@qq.com', '随风而动,随刃而行', '2016');
INSERT INTO `admusers` VALUES ('5', 'lalala', '123', '保密', 'mzl.cover.175x175-75.jpg', '214', '山口山3', '超级管理员', '3@qq.com', '爱是一道光,如此美妙', '2016');

-- ----------------------------
-- Table structure for adpic
-- ----------------------------
DROP TABLE IF EXISTS `adpic`;
CREATE TABLE `adpic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `adid` int(11) NOT NULL,
  `adtitle` varchar(20) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of adpic
-- ----------------------------
INSERT INTO `adpic` VALUES ('1', '1', '首页广告1', '1', '兔兔助手', null, null, null);

-- ----------------------------
-- Table structure for adpic2
-- ----------------------------
DROP TABLE IF EXISTS `adpic2`;
CREATE TABLE `adpic2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `adid` int(11) NOT NULL,
  `adtitle` varchar(20) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of adpic2
-- ----------------------------
INSERT INTO `adpic2` VALUES ('1', '1', '首页广告2', '1', '游戏推广', '', null, null);

-- ----------------------------
-- Table structure for adpic3
-- ----------------------------
DROP TABLE IF EXISTS `adpic3`;
CREATE TABLE `adpic3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `adid` int(11) NOT NULL,
  `adtitle` varchar(20) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of adpic3
-- ----------------------------
INSERT INTO `adpic3` VALUES ('1', '1', '首页广告3', '1', 'IOS软件推荐', null, null, null);

-- ----------------------------
-- Table structure for adpic4
-- ----------------------------
DROP TABLE IF EXISTS `adpic4`;
CREATE TABLE `adpic4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `adid` int(11) NOT NULL,
  `adtitle` varchar(20) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of adpic4
-- ----------------------------
INSERT INTO `adpic4` VALUES ('1', '1', '首页广告4', '1', '手机浏览器推荐', null, null, null);

-- ----------------------------
-- Table structure for adpic5
-- ----------------------------
DROP TABLE IF EXISTS `adpic5`;
CREATE TABLE `adpic5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `adid` int(11) NOT NULL,
  `adtitle` varchar(20) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of adpic5
-- ----------------------------
INSERT INTO `adpic5` VALUES ('1', '1', '首页轮播1', '1', null, null, null, null);
INSERT INTO `adpic5` VALUES ('2', '2', '首页轮播2', '2', null, null, null, null);
INSERT INTO `adpic5` VALUES ('3', '3', '首页轮播3', '3', null, null, null, null);
INSERT INTO `adpic5` VALUES ('4', '4', '首页轮播4', '4', null, null, null, null);
INSERT INTO `adpic5` VALUES ('5', '5', '首页轮播5', '5', null, null, null, null);

-- ----------------------------
-- Table structure for adpic6
-- ----------------------------
DROP TABLE IF EXISTS `adpic6`;
CREATE TABLE `adpic6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `adid` int(11) NOT NULL,
  `adtitle` varchar(20) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of adpic6
-- ----------------------------

-- ----------------------------
-- Table structure for advertlist
-- ----------------------------
DROP TABLE IF EXISTS `advertlist`;
CREATE TABLE `advertlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uname` char(20) NOT NULL,
  `bbsid` int(11) NOT NULL,
  `bbstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of advertlist
-- ----------------------------
INSERT INTO `advertlist` VALUES ('1', '大力出奇迹', '1', '广告图片1', '广告图片1的简介', 'img201609211102240.png', null, '2016-10-14 00:00:00');
INSERT INTO `advertlist` VALUES ('2', '大力出奇迹2', '2', '广告图片2', '广告图片2的简介', 'img201609251739180.jpg', null, '2016-10-14 14:17:05');
INSERT INTO `advertlist` VALUES ('3', '大力出奇迹3', '3', '广告图片3', '广告图片3的简介', '695559a0img201607281227230_112__82.jpg', null, '2016-10-14 14:17:05');
INSERT INTO `advertlist` VALUES ('10', '热舞热污染', '4', '广告图片4', '广告图片4的简介', 'bdddb4daimg201610042251260_499__246.gif', null, '2016-10-18 00:00:00');

-- ----------------------------
-- Table structure for articlelist
-- ----------------------------
DROP TABLE IF EXISTS `articlelist`;
CREATE TABLE `articlelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `artid` int(11) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of articlelist
-- ----------------------------

-- ----------------------------
-- Table structure for bbs_publish
-- ----------------------------
DROP TABLE IF EXISTS `bbs_publish`;
CREATE TABLE `bbs_publish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `bbsid` int(11) NOT NULL,
  `bbstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `bpic` varchar(100) DEFAULT NULL,
  `bvideo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bbs_publish
-- ----------------------------

-- ----------------------------
-- Table structure for bbscomment
-- ----------------------------
DROP TABLE IF EXISTS `bbscomment`;
CREATE TABLE `bbscomment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `bbsid` int(11) NOT NULL,
  `bbstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bbscomment
-- ----------------------------
INSERT INTO `bbscomment` VALUES ('1', '1', 'user', '1', '骆驼坐骑好还是羊驼坐骑好', '今天升到20级了,打算买一个坐骑,不知道是骆驼后期好还是羊驼好,还是比较喜欢羊驼这样可爱的.', '20161018');

-- ----------------------------
-- Table structure for bbslist
-- ----------------------------
DROP TABLE IF EXISTS `bbslist`;
CREATE TABLE `bbslist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `uname` char(20) NOT NULL,
  `bbsid` int(11) DEFAULT NULL,
  `bbstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bbslist
-- ----------------------------
INSERT INTO `bbslist` VALUES ('5', null, 'admin', null, 'sogou2', '3、长按候选词搜索\r\n和主动搜索类似，但省去了输入“关键词”的步骤，用户只需要长按候选词就能在主动搜索栏中输入该词进行搜索，结果和主动搜索呈现的结果一样。\r\n官网地址：http://shouji.sogou.com/\r\n敲黑板，重点来了！！！\r\n万事俱备，现在就可以体验快速分享功能，并参加搜狗输入法官方的“秀分享赢iPhone7“的活动啦~', null, null, '2016');
INSERT INTO `bbslist` VALUES ('4', null, 'admin', null, '搜狗输入法“快速分享”功能上线', '搜狗输入法特别为爱分享的你推出“快速分享”功能，无需APP切换，不打扰当下节奏，只要有输入的地方就可以轻松分享所需信息。\r\n快速分享的三种使用方式\r\n1、智能唤起\r\n通过情景感知智能呈现。比如当输入 “想吃麦当劳”等字样时，词汇候选区会出现带有“快速”符号的关键词。点击关键词出现和麦当劳相关的信息。点击卡片右下侧分享按钮即可将信息分享出去。\r\n2、主动搜索\r\n通过直接点击 “快速”按钮，展开搜索栏，输入法关', null, null, '2016');
INSERT INTO `bbslist` VALUES ('6', null, 'admin', null, 'sougou3', '活动时间：2016年9月23日—10月7日\r\n下载最新版本的搜狗手机输入法，上传【使用快速分享的聊天截图，内容包含分享餐厅，团购，音乐，地图，百科中的任一种】，并说出你最新欢或会经常使用的分享类型以及理由即可参与踩楼，赢取包含iPhone7在内的超值大奖！具体步骤如下：\r\n1、下载最新版版本的搜狗手机输入法\r\n2、截取使用”快速分享”任意一种分享的聊天截图并附上威锋ID水印，内容包含分享餐厅，团购，音乐', null, null, '2016');
INSERT INTO `bbslist` VALUES ('7', null, 'admin', null, 'sougou4', 'dhsaiudhsadhudhadiuhiudsa', null, null, '2016');

-- ----------------------------
-- Table structure for bbspic
-- ----------------------------
DROP TABLE IF EXISTS `bbspic`;
CREATE TABLE `bbspic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uname` char(20) NOT NULL,
  `bbsid` int(11) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bbspic
-- ----------------------------
INSERT INTO `bbspic` VALUES ('1', 'admin', '1', '论坛图片1', '论坛图片摘要1', '0919b26dimg201607281225140_112__82.jpg', null, '2016-10-18 00:00:00');
INSERT INTO `bbspic` VALUES ('2', 'admin', '2', '论坛图片2', '论坛图片摘要2', 'ce95cd3bimg201609192123140_248__246.jpg', null, '2016-10-14 00:00:00');
INSERT INTO `bbspic` VALUES ('3', 'admin', '3', '论坛图片3', '论坛图片摘要3', 'c28dfedcimg201607281229300_112__82.jpg', null, '2016-10-14 00:00:00');
INSERT INTO `bbspic` VALUES ('4', 'admin', '4', '论坛图片4', '论坛图片摘要4', 'img201607281228110.jpg', null, '2016-10-14 00:00:00');
INSERT INTO `bbspic` VALUES ('5', 'admin', '5', '论坛图片5', '论坛图片宣传5', 'img201608230943390.jpg', null, '2016-10-18 00:00:00');
INSERT INTO `bbspic` VALUES ('9', 'admin', '6', '论坛图片6', '论坛图片宣传6', 'img201609051039000.jpg', null, '2016-10-18 00:00:00');
INSERT INTO `bbspic` VALUES ('10', '大厦', '10', '论坛图片7', '论坛图片宣传7', 'article_97125_640x320.jpg', null, '2016-10-18 00:00:00');

-- ----------------------------
-- Table structure for bbsreview
-- ----------------------------
DROP TABLE IF EXISTS `bbsreview`;
CREATE TABLE `bbsreview` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `bbsid` int(11) NOT NULL,
  `bbstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `bpic` varchar(100) DEFAULT NULL,
  `bvideo` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bbsreview
-- ----------------------------

-- ----------------------------
-- Table structure for carousel
-- ----------------------------
DROP TABLE IF EXISTS `carousel`;
CREATE TABLE `carousel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pictitle` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `addtime` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of carousel
-- ----------------------------
INSERT INTO `carousel` VALUES ('33', 'watchOS 3体验评价：来自用户的核心之变', 'watchOS 3 在不断变化更新，适应用户使用设备的方式——让用户决定什么是好的，让用户来决定个人智能穿戴设备该如何发展。', 'img201609231555470.png', '2016-10-17');
INSERT INTO `carousel` VALUES ('34', '威锋网每周应用推荐 - 2016年第三十七期', '如果你是一位体育竞技爱好者，本周热门APP《NBA 2K17》再合适不过，也许你喜欢边看电影边互动，来自T社的《Batman（蝙蝠侠:故事版）》是个不错的选择。', 'img201609251739180.jpg', '2016-10-17');
INSERT INTO `carousel` VALUES ('35', '跟随保罗乔治脚步 一起来细数本周新作看点', '本周上架的新游质量十分之高，相信在这些精品游戏的加持之下，“床的封印”也会增添几分威力。', 'img201609231243140.jpg', '2016-10-17');
INSERT INTO `carousel` VALUES ('36', '你升级 iOS 10 系统了吗？来看看它的新特性', '对于这个号称苹果 iOS 史上最大的一次革新，你们有什么想说的吗？', 'img201609211146420.jpg', '2016-10-17');
INSERT INTO `carousel` VALUES ('37', 'A 系为首！苹果顶尖的“芯片帝国”已初现雏形', '今天苹果的成就已不仅限于 A 系芯片，更像是一个令行业敬畏强大的芯片厂商。', 'img201609202326340.png', '2016-10-17');

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `adid` int(11) NOT NULL,
  `adtitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of feedback
-- ----------------------------

-- ----------------------------
-- Table structure for news_publish
-- ----------------------------
DROP TABLE IF EXISTS `news_publish`;
CREATE TABLE `news_publish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `newsid` int(11) NOT NULL,
  `newstitle` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `npic` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news_publish
-- ----------------------------

-- ----------------------------
-- Table structure for newscomment
-- ----------------------------
DROP TABLE IF EXISTS `newscomment`;
CREATE TABLE `newscomment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` char(20) NOT NULL,
  `newsid` int(11) NOT NULL,
  `newstitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newscomment
-- ----------------------------

-- ----------------------------
-- Table structure for newslist
-- ----------------------------
DROP TABLE IF EXISTS `newslist`;
CREATE TABLE `newslist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `writer` char(20) NOT NULL,
  `type` enum('z','h') DEFAULT 'z' COMMENT '专题资讯和行业头条',
  `newstitle` varchar(255) DEFAULT NULL,
  `simcontent` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `state` enum('c','y') DEFAULT 'y' COMMENT '草稿 已发布',
  `addtime` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newslist
-- ----------------------------
INSERT INTO `newslist` VALUES ('26', 'Jack', 'z', 'iPhone拍的照片可以这么惊艳！', '你按下快门的时候，一切的感触，思想，情绪，心思，都会定格在这一刹那。美，与你同在，未曾离开。', '123', null, 'y', '2016-10-17');
INSERT INTO `newslist` VALUES ('27', 'Jack', 'z', '不喜欢AirPods? 这里还有几款不错的替代品', '当中既有Lightning耳机，也有无线耳机，可以满足不同消费者的喜好需求。', '123', null, 'y', '2016-10-17');
INSERT INTO `newslist` VALUES ('28', 'Jack', 'z', '三星受挫 但似乎并不会让苹果占到太多便宜', '如果用户弃用三星手机转用iPhone，他们必须使用一个全新的操作系统，放弃此前已经购买的内容和应用。', '123', null, 'y', '2016-10-17');
INSERT INTO `newslist` VALUES ('29', 'Jack', 'z', '探秘苹果 IBM 共赢合作 苹果在下一盘大棋?', '对许多客户而言，IBM看起来甚至像是一个苹果产品的零售商。', '123', null, 'y', '2016-10-17');
INSERT INTO `newslist` VALUES ('30', 'Jack', 'z', '两项 Apple Watch 新专利曝光：表带可变色', '苹果刚刚推出了 Apple Watch Series 2 智能手表，但是最新曝光的两项专利表明了这家科技公司又有了新的想法。', '123', null, 'y', '2016-10-17');
INSERT INTO `newslist` VALUES ('31', 'Jack', 'z', '5年过去了 你觉得Siri比得上Google Now吗?', '尽管 Siri 实现了非常大的突破，但仍有不少用户认为苹果的语音助手还是在追赶谷歌的 Google Now。', '', null, 'y', '2016-10-17');

-- ----------------------------
-- Table structure for newspic
-- ----------------------------
DROP TABLE IF EXISTS `newspic`;
CREATE TABLE `newspic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uploader` char(20) NOT NULL,
  `newsid` int(11) NOT NULL,
  `picname` varchar(20) DEFAULT NULL,
  `pic` blob,
  `addtime` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newspic
-- ----------------------------
INSERT INTO `newspic` VALUES ('7', 'dsadasd', '2', 'NEWS2', 0x696D673230313630373238313232383131302E6A7067, '2016-10-18');
INSERT INTO `newspic` VALUES ('6', 'admin', '0', 'NEWS1', '', '2016-10-18');
INSERT INTO `newspic` VALUES ('8', 'EQWEQW', '3', 'NEWS3', 0x3866366234653162696D673230313630373238313232353333305F3131325F5F38322E6A7067, '2016-10-18');
INSERT INTO `newspic` VALUES ('9', 'WQEQE', '4', 'NEWS4', 0x3134313034396E6172636870726961756170666B31722E6A7067, '2016-10-18');
INSERT INTO `newspic` VALUES ('10', 'dsadA', '5', 'NEWS5', 0x696D673230313630393231313130323234302E706E67, '2016-10-18');

-- ----------------------------
-- Table structure for newsreview
-- ----------------------------
DROP TABLE IF EXISTS `newsreview`;
CREATE TABLE `newsreview` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `newsid` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `npic` varchar(100) DEFAULT 'normal.jpg',
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newsreview
-- ----------------------------

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES ('3', '游戏中心讨论区是Feng综合网专为手机游戏粉丝打造的游戏生活互动交流版块，为保持良好的发帖与讨论环境，特此规定本版相关版规：', '2016-10-16 03:09:40');
INSERT INTO `notice` VALUES ('4', '1. 本版讨论范围是市面上各类iOS/Mac OS单机或者网络游戏相关内容，请勿发表无关帖子内容，错版发贴将被管理员移至相关版块。', '2016-10-16 03:10:02');
INSERT INTO `notice` VALUES ('5', '2. 禁止出现涉及网游金钱交易、卖号，推广游戏广告及链接和涉及用户隐私，政治题材，黄赌毒甚至是肆意谩骂的内容出现。违者按规定进行删帖-禁言-封号等相关处罚。', '2016-10-16 03:10:11');
INSERT INTO `notice` VALUES ('6', '3. 禁止纯表情，纯标点，乱码无意义回复/顶帖，禁止标题无意义符号加长。禁止屠版。违者按版规进行删帖-禁言-封号等相关处罚。', '2016-10-16 03:10:26');
INSERT INTO `notice` VALUES ('7', '4. 禁止标题党，谣言，误导，冒名发布原创贴，转载请注明出处。', '2016-10-16 03:10:39');
INSERT INTO `notice` VALUES ('8', '5. 维护版块氛围靠每一个用户。出现违规者秉持尽可能多的警告少处罚原则进行，共同促进游戏中心板块的和谐发展。对管理操作有疑问的，请 PM 当事版主协商处理，协商无果的请去 站务与公告区申诉。', '2016-10-16 03:10:57');
INSERT INTO `notice` VALUES ('9', '对于违反规定的会员，视情节严重程度，版主有权采取 协助修改、扣分、压帖、删除、限期禁言、永久禁言、封禁 IP 等管理手段处理。', '2016-10-16 03:11:46');
INSERT INTO `notice` VALUES ('10', '对于积极参与讨论，对版块作出突出贡献的会员，版主有权采取 加分、加亮、加精 等管理手段奖励。', '2016-10-16 03:11:55');
INSERT INTO `notice` VALUES ('11', '请会员常常关注本条例，及时自行清理违规内容。以上条款即日生效。', '2016-10-16 03:12:15');

-- ----------------------------
-- Table structure for systembase
-- ----------------------------
DROP TABLE IF EXISTS `systembase`;
CREATE TABLE `systembase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webname` char(20) NOT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `dataid` int(11) NOT NULL,
  `descrb` varchar(100) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `footinf` varchar(255) DEFAULT NULL,
  `footnum` varchar(100) DEFAULT NULL,
  `des` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of systembase
-- ----------------------------

-- ----------------------------
-- Table structure for systemdata
-- ----------------------------
DROP TABLE IF EXISTS `systemdata`;
CREATE TABLE `systemdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `dataid` int(11) NOT NULL,
  `datatitle` varchar(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of systemdata
-- ----------------------------

-- ----------------------------
-- Table structure for systemshielding
-- ----------------------------
DROP TABLE IF EXISTS `systemshielding`;
CREATE TABLE `systemshielding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of systemshielding
-- ----------------------------

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type` (
  `id` int(30) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of type
-- ----------------------------
INSERT INTO `type` VALUES ('1', '首页');
INSERT INTO `type` VALUES ('2', '新闻');
INSERT INTO `type` VALUES ('3', '论坛');

-- ----------------------------
-- Table structure for usefullinkslist
-- ----------------------------
DROP TABLE IF EXISTS `usefullinkslist`;
CREATE TABLE `usefullinkslist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `linkname` varchar(20) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `addtime` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usefullinkslist
-- ----------------------------
INSERT INTO `usefullinkslist` VALUES ('2', 'hao123导航', 'http://zhidao.baidu.com/link?url=kvqaBBGPOqhFKgc7Ia1W5BefSIeRIxRPNhWzxk-honRdldUgqpIuIUiV-FuaIiZB7GkS85FjD21Xnnroc2Bk_9Gk5rVixkb5cm2bGFkKIlG123', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('3', '网易科技', '123123', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('4', '和讯科技', '123', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('5', 'TechWeb', '123', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('6', 'TomPDA智能手机网', '123', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('7', 'IT之家', '123', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('8', 'dospy智能手机', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('9', '当乐手机游戏', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('10', '亿邦动力网', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('11', '威锋商城', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('12', 'PChome下载', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('13', '杭州房产网', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('14', '站长之家', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('15', '科技讯', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('16', '安卓网', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('17', '宝软网', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('18', '手机QQ浏览器', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('20', '老虎游戏', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('21', '搜狐IT', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('22', 'WP8论坛', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('23', '淘米视频', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('24', '人人游戏', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('25', '安卓网', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('26', '电子发烧友', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('27', 'MAXPDA智能手机论坛', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('28', '114啦网址导航', '', '2016-10-17');
INSERT INTO `usefullinkslist` VALUES ('29', '雷锋网', '', '2016-10-17');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uname` char(20) NOT NULL,
  `age` int(10) DEFAULT NULL,
  `sex` enum('0','1','2') DEFAULT '1',
  `uface` varchar(50) DEFAULT '09_avatar_middle.jpg',
  `password` varchar(255) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` enum('已启用','已停用') DEFAULT '已启用',
  `email` varchar(30) DEFAULT NULL,
  `lv` varchar(3) DEFAULT '1',
  `regtime` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('7', 'user', '17', '2', '24_avatar_middle.jpg', '123', '江苏', '已启用', '2@qq.com', '1', '1476557872');
INSERT INTO `users` VALUES ('8', 'BB', '3', '1', '09_avatar_middle.jpg', '123', '江苏', '已启用', '2@qq.com', '1', '1476594352');
INSERT INTO `users` VALUES ('9', 'admin', null, '2', '09_avatar_middle.jpg', 'qwe123', null, '已启用', '496697515@qq.com', '1', null);
INSERT INTO `users` VALUES ('10', '叫爸爸', null, '0', '26_avatar_middle.jpg', '123', null, '已启用', '222@qq.com', '1', null);
INSERT INTO `users` VALUES ('22', 'CC', null, '0', '24_avatar_middle.jpg', '4b4b5eef361fa38b0d31078b74499957', null, '已启用', '11@qq.com', '1', null);
INSERT INTO `users` VALUES ('21', 'AA', null, '0', '09_avatar_middle.jpg', '65bb86549756830caa529e032f829eb2', null, '已启用', 'qe@q.com', '1', null);
INSERT INTO `users` VALUES ('20', 'timo', null, '2', '09_avatar_middle.jpg', '3fd5c518759b2c820c6102ae5629578a', null, '已启用', '2@q.com', '1', null);
INSERT INTO `users` VALUES ('18', 'lalala', null, '0', '26_avatar_middle.jpg', '200820e3227815ed1756a6b531e7e0d2', null, '已启用', 'qe@q.com', '1', null);
INSERT INTO `users` VALUES ('23', 'dd', null, '0', '24_avatar_middle.jpg', '65bb86549756830caa529e032f829eb2', null, '已启用', '2@q.com', '1', null);
INSERT INTO `users` VALUES ('24', 'lala', null, '0', '09_avatar_middle.jpg', '65bb86549756830caa529e032f829eb2', null, '已启用', '496697515@qq.com', '1', null);
