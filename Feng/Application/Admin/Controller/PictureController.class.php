<?php
namespace Admin\Controller;
// use Think\Controller;

class PictureController extends AdminController {
    public function Advertpictureadd(){
        $this->display();
    }
    public function insert()
    {
        // echo 111;die;
        // var_dump($_POST);die;
        $data=M('bbspic');
        $data->create();
        $data->regtime=time();
        if($data->add()>0){
            $this->success('添加成功',U('Picture/Bbspicturelist'));
        }else{
            $this->error('添加失败');
        }
    }
    public function insert2()
    {
        // echo 111;die;
        // var_dump($_POST);die;
        $data=M('advertlist');
        $data->create();
        $data->regtime=time();
        if($data->add()>0){
            $this->success('添加成功',U('Picture/Advertpicturelist'));
        }else{
            $this->error('添加失败');
        }
    }
    public function Advertpicturelist(){
        $data=M('advertlist')->select();
        $this->assign('pic',$data);
        $this->display('Advertpicturelist');
    }
    public function Articlepictureadd(){
        $this->display();
    }
    public function Articlepicturelist(){
        $this->display();
    }    
    public function Bbspictureadd(){
        $this->display();
    }
    public function Bbspicturelist(){
        $data=M('bbspic')->select();
        $this->assign('pic',$data);
        $this->display('Bbspicturelist');
    }   
    //-------------新闻图片--------------
 
    public function Newspicturelist(){
        $data=M('newspic')->select();
        $this->assign('list',$data);
        $this->display('Newspicturelist');
    }

    //---------添加-----------
    public function newspictureadd(){
        $this->display("Newspictureadd");
    }
    public function newspictureinsert()
    {
        $data=M('Newspic');
        $data->create();
        $data->regtime=time();
        if($data->add()>0){
            $this->success('添加成功',U('Newspicturelist'));
        }else{
            $this->error('添加失败');
        }
    }

    //-------------修改----------------
    public function newspictureedit()
    {
        $id=I('get.id/d');
        $data = M('newspic')->find($id);
        $this->assign('data',$data);
        $this->display('Picture/Newspictureedit');
    }
    public function newspictureupdate()
    {
        M('newspic')->create();
        if(M('newspic')->save()>0){
            $this->success('修改成功',U('Picture/Newspicturelist'));
        }else{
            $this->error('修改失败');
        }
    }

    //-------------删除-------------
     public  function newspicturedel()
    {
        $id=I('get.id/d');
        if(M('newspic')->delete($id)>0){
        $this->success('删除成功',U('Picture/Newspicturelist'));
        }else{

            $this->error('删除失败');

        }
    }
	//论坛图片修改开始
	public function Bbspictureedit()
    {
        $id=I('get.id/d');

        $bbspic=M('bbspic')->select($id);
        $this->assign('bbspic',$bbspic);

        $this->display('Picture/Bbspictureedit');
    }
    public function edit()
    {
        $notice=M('bbspic')->create();
        
        if(M('bbspic')->save()>0){

            $this->success('修改成功',U('Picture/Bbspicturelist'));
        }else{

            $this->error('修改失败');
        }
    }
    public  function del()
    {
        $id=I('get.id/d');
        if(M('bbspic')->delete($id)>0){
        $this->success('删除成功',U('Picture/Bbspicturelist'));
        }else{

            $this->error('删除失败');

        }
    }
	//广告图片修改开始
	public function Advertpictureedit()
    {
        $id=I('get.id/d');

        $advertlist=M('advertlist')->select($id);
        $this->assign('advertlist',$advertlist);

        $this->display('Picture/Advertpictureedit');
    }
    public function edit2()
    {
        $notice=M('advertlist')->create();
        
        if(M('advertlist')->save()>0){

            $this->success('修改成功',U('Picture/Advertpicturelist'));
        }else{

            $this->error('修改失败');
        }
    }
    public  function del2()
    {
        $id=I('get.id/d');
        if(M('advertlist')->delete($id)>0){
        $this->success('删除成功',U('Picture/Advertpicturelist'));
        }else{

            $this->error('删除失败');

        }
    
    }
    //--------------轮播图-------------
    public function carousellist(){
        $data=M('carousel')->select();
        $this->assign('list',$data);
        $this->display('Picture/Carousellist');
    }

    //---------添加-----------
    public function carouseladd(){
        $this->display("Picture/Carouseladd");
    }
    public function carouselinsert()
    {
        $data=M('carousel');
        $data->create();
        $data->regtime=time();
        if($data->add()>0){
            $this->success('添加成功',U('Picture/Carousellist'));
        }else{
            $this->error('添加失败');
        }
    }

    //-------------修改----------------
    public function carouseledit()
    {
        $id=I('get.id/d');
        $data = M('carousel')->find($id);
        $this->assign('data',$data);
        $this->display('Picture/Carouseledit');
    }
    public function carouselupdate()
    {
        M('carousel')->create();
        if(M('carousel')->save()>0){
            $this->success('修改成功',U('Picture/Carousellist'));
        }else{
            $this->error('修改失败');
        }
    }

    //-------------删除-------------
     public  function carouseldel()
    {
        $id=I('get.id/d');
        if(M('carousel')->delete($id)>0){
        $this->success('删除成功',U('Picture/Carousellist'));
        }else{

            $this->error('删除失败');

        }
    }

}