<?php
namespace Admin\Controller;
// use Think\Controller;

class MemberController extends AdminController {
    public function Memberadd(){
        $this->display('Memberadd');
    }
    public function insert()
    {
        // echo 111;die;
        // var_dump($_POST);die;
        $data=M('users');
        $data->create();
        $data->regtime=time();
        if($data->add()>0){
            $this->success('添加成功',U('Member/Memberlist'));
        }else{
            $this->error('添加失败');
        }
    }
    public function Memberlevel(){
        $data=M('users')->select();
        $this->assign('list',$data);
        $this->display('Memberlevel');
    }
    public function Memberlist(){
        $data=M('users')->select();
        $this->assign('list',$data);
        $this->display('Memberlist');    
    }
    public function Memberrecordbrowse(){
        $this->display('Memberrecordbrowse');
    }
    public function Memberrecorddownload(){
        $this->display('Memberrecorddownload');
    }
    public function Memberrecordshare(){
        $this->display('Memberrecordshare');
    }
     public function Membershow(){
        $this->display('Membershow');
    }
	//会员修改开始
	public function Memberedit()
    {
        $id=I('get.id/d');

        $users=M('users')->select($id);
        $this->assign('users',$users);
		
        $this->display('Member/Memberedit');

    }
    public function edit()
    {
        // var_dump($_POST);die;
        $users=M('users')->create();
        if(M('users')->save()>0){

            $this->success('修改成功',U('Member/Memberlist'));
        }else{

            $this->error('修改失败');
        }
    }
  
    public  function del()
    {
        $id=I('get.id/d');
        if(M('users')->delete($id)>0){
        $this->success('删除成功',U('Member/Memberlist'));
        }else{

            $this->error('删除失败');

        }
    
    }
  
}