<?php
    namespace Admin\Controller;
    use Think\Controller;

    class UserController extends BaseController
    {
        public function index(){

    
            $data=M('user')->select();          
            $this->assign('list',$data);
            $this->display();
        
            
        }

        public function update()
        {   
            // var_dump($_POST);die;            
             if (empty($_POST)) {
            $this->redirect('请填写数据!', U('User/index'));
            exit;
            }   
            $user = M('user');
            // var_dump($user->create());die;
            // if(($user->create()).u_photo==null){

            // }
            if($user->create()){
                $pname  = $this->upload();
                if($pname){
                    $this->error('上传失败');
                }
                $user->u_photo = $pname;
                // var_dump($_POST);die;
                //echo 111;die;
                //var_dump($user->save());
                // echo $user->getLastSql();die;
                if($user->save()>0){
                    $this->success('修改成功',U('index'));
                }else{
                    $this->error('修改失败');
                }
            }else{
                    // echo 111;die;
                    $this->error($data->getError());
                }
        }

        public function doajax(){
            $name=I('get.name');
        
            // echo 1111;die;
            $data=M('user')->where(array('u_name'=>$name))->select();
            if(empty($data)){
                $this->ajaxReturn(1);
            }
        }
        public function add(){

            $this->display();
        }

        public function insert()
        {
            $data=D('user');
            $data->create();
                
            if($data->create()){
            $pname  = $this->upload();
            if(!$pname){
                $this->error('上传失败');
            }
                $data->u_photo = $pname;
                
                // echo $data->getLastSql();die;
                $result=$data->add();
                if($result>0){
                    $this->success('添加成功',U('index'));
                }else{
                    $this->error('添加失败');
                }
            }else{
                    // echo 111;die;
                    $this->error($data->getError());
                }

        }
            public function upload(){
                $upload=new \Think\Upload();
                
                $upload->maxSize=3145728;//设置大小
                /*$upload->maxWidth=30;
                $upload->maxHeight=30;*/
                $upload->rootPath = './Public';
                $upload->autoSub=false;
                $upload->exts=array('jpg','png','gif','jpeg','bmp');//图片格式
                $upload->savePath='./Uploads/';//路劲
                $info=$upload->upload();
                
                if($info){
                    return $info['u_photo']['savename'];
                }else{

                    return ;
                }
            }
            public function del()
            {
                if(empty($_GET['id'])){
                    $this->redirect('Admin/User/index');
                    exit;
                }
                $id=I('get.id/d');

                if(M('user')->delete($id)>0){
                    $this->redirect('Admin/User/index');
                }else{
                    $this->error('删除失败');
                }
            }
            public function doInfo($id){
                $list=M('user')->find($id);
                $this->ajaxReturn($list);
                $this->display('User/index');
            }
            public function dostatus()
            {
                $id=I('get.id/d');
                // echo $id;die;
                $list=M('user');
                // echo $id;die;
                $data['u_status']=1;
                // var_dump($data);die;
                if($rst=$list->where(array('u_id'=>$id))->save($data)>0){
                    $this->redirect('Admin/user/index');
                }else{
                    $this->error('该用户还在冻结中ing....');
                }
                
            }
            public function jiestatus()
            {
                $id=I('get.id/d');
                $list=M('user');
                // echo $id;die;
                $data['u_status']=0;
                // var_dump($data);die;
                if($rst=$list->where(array('u_id'=>$id))->save($data)>0){
                    $this->redirect('Admin/user/index');
                }else{
                    $this->error('该用户不在冻结中');
                }
            }
        
    }

    


?>