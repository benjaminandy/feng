<?php
namespace Admin\Controller;
// use Think\Controller;

class CommentController extends AdminController {
    public function Feedbacklist(){
        $this->display('Feedbacklist');
    }
	public function Bbscommentlist(){
        $data=M('Bbscomment')->select();
        $this->assign('list',$data);
        $this->display('Bbscommentlist');
    }
    public function Bbscommentadd(){
    	$data=M('Bbscommentlist');
        $data->create();
        $data->regtime=time();
        if($data->add()>0){
            $this->success('添加成功',U('Bbscommentlist'));
        }else{
            $this->error('添加失败');
        }
    }
    public function Bbscommentedit(){
    	$id=I('get.id/d');
    	$bbscomment=M('bbscomment')->select($id);
    	$this->assign('bbscomment',$bbscomment);
    	$this->display('Comment/Bbscommentedit');
    }
    public function Bbscommentdel(){
    	$id=I('get.id/d');
        if(M('bbscomment')->delete($id)>0){
        $this->success('删除成功',U('Comment/Bbscommentlist'));
        }else{
            $this->error('删除失败');
        }
    }
//论坛文章修改开始
	public function Bbsedit()
    {
        $id=I('get.id/d');
        $bbslist=M('bbslist')->select($id);
        $this->assign('bbslist',$bbslist);
        $this->display('Articlelist/Bbsedit');
    }
	public function edit2()
    {
        $bbslist=M('bbslist')->create();
        if(M('bbslist')->save()>0){

            $this->success('修改成功',U('Articlelist/Bbslist'));
        }else{

            $this->error('修改失败');
        }
    } 
    public  function del2()
    {
        $id=I('get.id/d');
        if(M('bbslist')->delete($id)>0){
        $this->success('删除成功',U('Articlelist/Bbslist'));
        }else{
            $this->error('删除失败');
        }
 
    }
}