<?php
namespace Admin\Controller;
// use Think\Controller;

class SystemController extends AdminController {
    public function Systembase(){
        $this->display('Systembase');
    }
    public function Systemcategory(){
        $this->display('Systemcategory');
    }
    public function Systemcategoryadd(){
        $this->display('Systemcategoryadd');
    }
    public function Systemdata(){
        $this->display('Systemdata');
    }
    public function Systemlog(){
        $this->display('Systemlog');
    }
    public function Systemshielding(){
        $this->display('Systemshielding');
    }

}