<?php
namespace Admin\Controller;
// use Think\Controller;

class ArticlelistController extends AdminController {
    public function Advertadd(){
        $this->display('Advertadd');
    }
     public function insert()
    {
        // echo 111;die;
        // var_dump($_POST);die;
        $data=M('bbslist');
        $data->create();
        $data->regtime=time();
        if($data->add()>0){
            $this->success('添加成功',U('Articlelist/Bbslist'));
        }else{
            $this->error('添加失败');
        }
    }
    public function insert2()
    {
        // echo 111;die;
        // var_dump($_POST);die;
        $data=M('notice');
        $data->create();
        $data->regtime=time();
        if($data->add()>0){
            $this->success('添加成功',U('Articlelist/Advertlist'));
        }else{
            $this->error('添加失败');
        }
    }
    // 公告列表开始
    public function Advertlist(){
        $data=M('notice')->select();
        $this->assign('list',$data);
        $this->display('Advertlist');
    }
    // 公告列表结束
    public function Articleadd(){
        
        $this->display('Articleadd');
    }
    public function Articlelist(){
        $this->display('Articlelist');
    }
    public function Bbsadd(){
        $this->display('Bbsadd');
    }
    public function Bbslist(){
        $data=M('bbslist')->select();
        $this->assign('list',$data);
        $this->display('Bbslist');
    }
    //-----------------新闻列表----------------
    public function Newslist(){
        $data = M('newslist')->select();
        $this->assign('title','新闻列表');
        $this->assign('list',$data);
        $this->display('Newslist');
    }

    //删除
    public function newsdel()
    {
        // 判断有无传参
        if (empty($_GET['id'])) {
            $this->redirect('Admin/Articlelist/Newslist');
            exit;
        }
        //接收参数
        // $id = $_GET['id'];//  不建议!!!
        $id = I('get.id/d');
        // echo $id;exit;
        if (M('newslist')->delete($id) > 0) {
            $this->success('恭喜您,删除成功!', U('Newslist'));
        } else {
            $this->error('删除失败!');
        }
    }

    //显示添加页
    public function Newsadd(){
    $this->display('Newsadd');
    }

    //添加
    public function newsinsert()
    {
        $data=M('newslist');
        $data->create();
        $data->regtime=time();
        if($data->add()>0){
            $this->success('添加成功',U('newslist'));
        }else{
            $this->error('添加失败');
        }
    }
 

     //编辑页
    public function newsedit($id)
    {
        //接收参数
        $id = I('get.id/d');
        //查找数据
        $data = M('newslist')->find($id);
        $this->assign('title','用户编辑');
        $this->assign('data',$data);
        $this->display('Articlelist/newsedit');
    }

    //执行修改
    public function newsupdate()
    {
        if (empty($_POST)) {
            $this->redirect('请填写数据!', U('newslist'));
            exit;
        }
        //生成数据对象
        M('newslist')->create();

        if (M('newslist')->save() > 0) {
            $this->success('修改成功',U('newslist'));
        } else {
            $this->error('修改失败');
        }
    }

// -------------------友情链接---------------
    public function Usefullinkslist(){
        $data = M('Usefullinkslist')->select();
        $this->assign('list',$data);
        $this->display('Usefullinkslist');
    }

    //显示友情链接添加页
    public function Usefullinksadd(){
        $this->display('Usefullinksadd');
    }

    //添加
    public function linksinsert()
    {
        $data=M('Usefullinkslist');
        $data->create();
        $data->regtime=time();
        if($data->add()>0){
            $this->success('添加成功',U('Usefullinkslist'));
        }else{
            $this->error('添加失败');
        }
    }

    //删除友情链接
    public function linksdel()
    {
        // 判断有无传参
        if (empty($_GET['id'])) {
            $this->redirect('Usefullinkslist');
            exit;
        }
        //接收参数
        // $id = $_GET['id'];//  不建议!!!
        $id = I('get.id/d');
        // echo $id;exit;
        if (M('Usefullinkslist')->delete($id) > 0) {
            $this->success('恭喜您,删除成功!', U('Usefullinkslist'));
        } else {
            $this->error('删除失败!');
        }
    }

    //编辑链接页
    public function linksedit($id)
    {
        //接收参数
        $id = I('get.id/d');
        //查找数据
        $data = M('Usefullinkslist')->find($id);
        $this->assign('data',$data);
        $this->display('usefullinksedit');
    }

    //执行修改
    public function linksupdate()
    {
        if (empty($_POST)) {
            $this->redirect('请填写数据!', U('newslist'));
            exit;
        }
        //生成数据对象
        M('Usefullinkslist')->create();

        if (M('Usefullinkslist')->save() > 0) {
            $this->success('修改成功',U('Usefullinkslist'));
        } else {
            $this->error('修改失败');
        }
    }
	//公告修改开始
    public function Advertedit()
    {
        $id=I('get.id/d');
        $notice=M('notice')->select($id);
        $this->assign('notice',$notice);
        $this->display('Articlelist/Advertedit');

    }
	public function edit()
    {
        $notice=M('notice')->create();        
        if(M('notice')->save()>0){
            $this->success('修改成功',U('Articlelist/Advertlist'));
        }else{
            $this->error('修改失败');
        }
    } 
    public  function del()
    {
        $id=I('get.id/d');
        if(M('notice')->delete($id)>0){
        $this->success('删除成功',U('Articlelist/Advertlist'));
        }else{
            $this->error('删除失败');
        }
    }
	//论坛文章修改开始
	public function Bbsedit()
    {
        $id=I('get.id/d');
        $bbslist=M('bbslist')->select($id);
        $this->assign('bbslist',$bbslist);
        $this->display('Articlelist/Bbsedit');
    }
	public function edit2()
    {
        $bbslist=M('bbslist')->create();
        if(M('bbslist')->save()>0){

            $this->success('修改成功',U('Articlelist/Bbslist'));
        }else{

            $this->error('修改失败');
        }
    } 
    public  function del2()
    {
        $id=I('get.id/d');
        if(M('bbslist')->delete($id)>0){
        $this->success('删除成功',U('Articlelist/Bbslist'));
        }else{
            $this->error('删除失败');
        }
 
    }
	 


}