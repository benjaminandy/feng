<?php
namespace Admin\Controller;
use Think\Controller;

class LoginController extends Controller
 {
    public function demo()
    {
        $this->display('Login/demo');
    }
    public function dologin()
    {
    	$username = I('post.uname');
		// var_dump($username);die;
		$password = I('post.upwd');
		$user = M('admusers');
		$data = $user->where(array('uname'=>$username))->find();
		// echo $data['upwd'].'<br>';
		// echo $password;exit;
		if (!$data) {
			$this->error('用户名不存在！');
			exit;
		}
		//验证密码
		if ($data['upwd'] !=$password) {
			$this->error('密码不正确');
			exit;
		}
		//把用户信息添加到session
		$_SESSION['adminuser'] = $data;
		// var_dump($data);die;
		// var_dump($_SESSION['admin_user']);die;
		//跳转到首页
		$this->redirect('Index/index');			
		

    }
    public function logout()
	{
		//清空session
		unset($_SESSION['adminuser']);
		//跳转
		$this->redirect('Index/index');
	}
}
?>