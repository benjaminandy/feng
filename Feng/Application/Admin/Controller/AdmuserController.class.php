<?php
namespace Admin\Controller;
// use Think\Controller;

class AdmuserController extends AdminController {
    public function Adminadd(){
        $this->display('Adminadd');
    }
    public function insert()
    {
        // var_dump($_POST);die;
        $data=M('admusers');
        $data->create();
        if($data->add()>0){
            $this->success('添加成功',U('Admuser/Adminlist'));
        }else{
            $this->error('添加失败');
        }
    }
    public function Adminlist(){
        $data=M('admusers')->select();
        $this->assign('list',$data);
        $this->display('Adminlist');
    }
    public function Adminpermission(){
        $this->display('Adminpermission');
    }
    public function Adminpermissionadd(){
        $this->display('Adminpermissionadd');
    }
    public function Adminrole(){
        $this->display('Adminrole');
    }
    public function Adminroleadd(){
        $this->display('Adminroleadd');
    }
	//管理员修改开始
	public function Adminedit()
    {
        $id=I('get.id/d');
		// echo $id;die;
        $admusers=M('admusers')->select($id);
        // var_dump($notice);die;
		$this->assign('admusers',$admusers);
        $this->display('Adminedit');

    }
    public function edit()
    {
        $users=M('admusers')->create();
        // var_dump($users);die;
        if(M('admusers')->save()>0){

            $this->success('修改成功',U('Admuser/Adminlist'));
        }else{

            $this->error('修改失败');
        }
    }
    
  
    public  function del()
    {
        $id=I('get.id/d');
        if(M('admusers')->delete($id)>0){
        $this->success('删除成功',U('Admuser/Adminlist'));
        }else{

            $this->error('删除失败');

        }
    
    }

    //权限修改开始
    public function Adminpermissionedit()
    {
        $id=I('get.id/d');
        // echo $id;die;
        $admusers=M('admusers')->select($id);
        // var_dump($notice);die;
        $this->assign('admusers',$admusers);
        $this->display('Adminpermissionedit');

    }
    public function edit2()
    {
        $users=M('admusers')->create();
        // var_dump($users);die;
        if(M('admusers')->save()>0){

            $this->success('修改成功',U('Admuser/Adminpermission'));
        }else{

            $this->error('修改失败');
        }
    }
    
  
    public  function del2()
    {
        $id=I('get.id/d');
        if(M('admusers')->delete($id)>0){
        $this->success('删除成功',U('Admuser/Adminpermission'));
        }else{

            $this->error('删除失败');

        }
    
    }

}