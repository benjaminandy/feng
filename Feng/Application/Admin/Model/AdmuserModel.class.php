<?php
namespace Admin\Model;
use Think\Model;

class AdmuserModel extends Model
{
	protected $fields=array('s_name','s_password','s_photo','s_sex');
	protected $pk = 's_id';
	protected $_validate = array(
		array('s_name','','用户名已经存在！',0,'unique',1),
		array('s_repassword','s_password','两次密码不一致',0,'confirm'),
		);
	protected $_auto = array(
		array('s_password','md5',3,'function'),
		);
}


