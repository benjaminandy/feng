<?php 
//命名空间
namespace Home\Controller;
//导入类
use Think\Controller;

//定义User 控制器
class UserController extends Controller
{
    //index
    public function index()
    {
        // 加载模版
        // $this->display();
        // $this->display('User/edit');
        // $this->display('User:edit');
        
        // $this->display('Admin/User/edit');//NO
        // $this->display('Admin@User/index');
        
        //T()  获取模版地址,
        // echo T('Admin/User/edit').'<br>';
        // echo T('Admin@User/index').'<br>';
        // $this->display('./Application/Admin/View/User/index.html');
        
        // $this->assign('title','TP3');
        $this->title = 'tp0000003';
        $this->abc = 'tp0000003';
        $this->display('User/index');
    }
}



