<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){
        $data=M('Usefullinkslist')->select();
        $car=M('carousel')->select();
		$this->assign('piclist',$car);
       	$this->assign('list',$data);
       	$this->display('Index/Index');
    }
}