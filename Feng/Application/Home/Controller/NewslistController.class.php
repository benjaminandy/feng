<?php
namespace Home\Controller;
use Think\Controller;
class NewslistController extends Controller {
    public function index(){
        $data=M('newslist')->select();
       	$this->assign('list',$data);
       	$this->display('Newslist/Index');
    }
}