<?php
namespace Home\Controller;
use Think\Controller;
class NewsController extends Controller {
    public function index(){
        $data=M('newslist')->select();
        $car=M('carousel')->select();
       	$this->assign('list',$data);
       	$this->assign('piclist',$car);
       	$this->display('News/Index');
    }
}