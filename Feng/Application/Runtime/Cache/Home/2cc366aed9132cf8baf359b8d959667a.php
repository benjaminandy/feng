<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="/favicon.ico" type="/web3/Public/image/x-icon">
<!-- 给网站添加一个icon图标 -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="apple-itunes-app" content="app-id=981437434" />

<link rel="alternate" type="application/rss+xml" title="Feng新闻RSS" href="">

<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/common.css" />
<script type="text/javascript" src="/web3/Public/js/js_index/jquery-1.7.min.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_index/common.js"></script>

</head>
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/style.css" />
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/feng_index.css" />
<script type="text/javascript" src="/web3/Public/js/js_index/index.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_index/jquery.marquee.min.js"></script>
<body>
<!-- Global Topbar -->
<div class="global_topbar_wrap">
	<div id="global_topbar" class="global_topbar">
		<div class="wrap inner">
			<div class="services" id="global_topbar_services">
				<ul>
				<li class="current"><a href="#"><i class="weiphone"></i>Feng综合网</a></li>				
				<li><a href=""><i class="wegame"></i>F新闻</a></li>						
				<li><a href=""><i class="fengbuy"></i>F论坛</a></li>
					
				<li><a href=""><i class="fengbuy"></i>建设中</a></li>
				
				<li><a href=""><i class="money"></i>扫码点击</a></li>								
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>
			<div class="links" id="globar_topbar_links">
				<ul>
					<li><a href="" target="_blank">首页</a></li>
					<li><a href="" target="_blank">Feng新闻</a></li>
					<li><a href="" target="_blank">Feng论坛</a></li>
				</ul>
				<div class="focus"><em></em></div>
			</div>
			
			<div class="client" id="global_topbar_client">
				<ul>
					<li class="current"><a href="#"><i></i>手机APP</a></li>					
					<div class="codeBox">
						<h3>扫一扫下载APP</h3>
						<b><img src="/web3/Public/picture/fengcode.jpg" width="94" height="94" alt="Feng二维码" /></b>
					</div>
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>

<!-- 弹框开始 -->
			<div class="user_menu" id="global_user_menu">
				<ul>
					<?php if($_SESSION['username']!= null): ?><li class="face">
						<img src="/web3/Public/picture/<?php echo ($_SESSION['username']['uface']); ?>" height="30" width="30" style="margin-top:-5px;">
						</li>
						<li class="face">
							<b style="font-size:15px"><?php echo ($_SESSION['username']['uname']); ?></b>
						</li>
						<li>
							<a class="ref" href="<?php echo U('Action/stop?model=index&controller=index');?>" title="注销">
								<i class="logout"></i>
								<span class="label">注销</span>
							</a>
						</li>
				        <?php else: ?>
							<li><a data-toggle="modal" href="#login-modal"><span>登录</span></a></li>
							<li><a data-toggle="modal" href="#signup-modal"><span>立即注册</span></a></li><?php endif; ?>
					<!-- <li><a data-toggle="modal" href="#forgetform"><span>找回密码</span></a></li> -->
				</ul>				
				<ul style="display:none;">
					<li class="face">
						<img src="/web3/Public/picture/<?php echo ($_SESSION['username']['uface']); ?>" height="30" width="30" style="margin-top:-5px;">
					</li>
					<li class="face">
						<b style="font-size:15px"><?php echo ($_SESSION['username']['uname']); ?></b>
					</li>
					<li>
						<a class="ref" href="<?php echo U('Action/stop?model=index&controller=index');?>" title="注销">
							<i class="logout"></i>
							<span class="label">注销</span>
						</a>
					</li>
				</ul>
			
			</div>
			<script>
			    function select(){
			        var username = document.form1.uname.value;
			        var userpass = document.form1.password.value;
			        var temp;
			        $.ajax({
				        type:'POST',
				        url:"<?php echo U('Action/select');?>",
				        async:false,
          				data:{name:username,pass:userpass},
          				success:function(data){
          					console.log(data);
          					if(data == 1){
					              alert('用户名错误');
					              temp = 1;
					              
					            } else if(data == 2){
					              alert('密码错误');
					              temp = 2;
					          }
          				}
			    	})

			    	if(temp == 1 || temp == 2 ){
					          return false;
					        }
			    }
			</script>
<!-- 弹框结束 -->

		<!-- <script type="text/javascript" src="/web3/Public/js/js_index/jquery.autocomplete.min.ajax.js"></script>
		<div class="searchBtn">
			<span id="quickSearchBtn"></span>
			<div class="headerSearch" id="headerSearch">
				<div class="searchBoxtop">
					<form id="searchform_top" action="http://s.feng.com/search.php" method="get" >
						<input id="top_keyword" name="srchtxt" type="text" value="搜索其实很简单^ ^">
						<input type="hidden" name="srchmod" value="all">
					</form>	
				</div>
			</div>
		</div>	 -->
		</div>
	</div>
</div>

<!-- 弹框start -->
<div class="modal" id="login-modal">
	<a class="close" data-dismiss="modal">×</a>
	<h1>登录</h1>
	
	<!-- 登录开始 -->
	<div class="contact" >
		<form action="<?php echo U('Index/index');?>" name="form1" onsubmit="return select()" method="POST">
			<ul>
				<li>
					<label>用户名：</label>
					<input type="text" name="uname" placeholder="请输入用户名" id='yourname' onblur="checkname()" value="" required/><span class="tips" id="divname2">长度1~12个字符</span>
				</li>

				<li>
					<label>密码：</label>
					<input type="password" name="password" placeholder="请输入您的密码" onblur="checkpsd5()" required/><span class="tips" id="divpassword0">密码必须由字母和数字组成</span>
				</li>
			</ul>
			<b class="btn"><input type="submit" value="登录"/>
			<input type="reset" value="取消"/></b>
		</form>
	</div>
</div>

<div class="modal" id="signup-modal">
	<a class="close" data-dismiss="modal">×</a>
	<h1>注册</h1>
	<div class="contact" >
	
	<!-- 注册开始 -->
	
	<form name="form2" action="<?php echo U('Action/add');?>" method="post">
		<ul>
			<li>
				<label>用户名：</label>
				<input type="text" name="uname" placeholder="请输入用户名"  onblur="checkna()" value="" /><span class="tips" id="divname">长度3~12个含数字和字母字符</span>
			</li>

			<li>
				<label>性别：</label>
				<input type="radio" name="sex" id="1" value="1">男&nbsp;&nbsp;
				<input type="radio" name="sex" id="2" value="2">女&nbsp;&nbsp;
				<input type="radio" name="sex" id="0" value="0">保密
			</li>

			<li>
				<label>密码：</label>
				<input type="password" name="password" placeholder="请输入您的密码" onblur="checkpsd1()" value="" required/><span class="tips" id="divpassword1">密码必须由字母和数字组成</span>
			</li>

			<li>
				<label>确认密码：</label>
				<input type="password" name="yourpass2" placeholder="请再次输入您的密码" onBlur="checkpsd2()" value="" required/><span class="tips" id="divpassword2">两次密码需要相同</span>
			</li>

			<li>
				<label>电子邮箱：</label>
				<input type="text" name="email" placeholder="请输入您的邮箱" onBlur="checkmail()" required/><span class="tips" id="divmail">请输入您的邮箱地址</span>
			</li>

			<li>
				<label>手机号：</label>
				<input type="text" name="yourphone" placeholder="请输入您的手机联系方式" onBlur="checkphone()" required/><span class="tips" id="divphone">以便帐号丢失后找回</span>
			</li>

			<li>
		      	<div id="distpicker5">
		      	<label>请选择所在地:</label>
			        <div class="form-group">
			          <select class="form-control" id="province10">
				          <option value="" data-code="">—— 省 ——</option>
				          <option value="北京市" data-code="110000">北京市</option>
				          <option value="天津市" data-code="120000">天津市</option>
				          <option value="河北省" data-code="130000">河北省</option>
				          <option value="山西省" data-code="140000">山西省</option>
				          <option value="内蒙古自治区" data-code="150000">内蒙古自治区</option>
				          <option value="辽宁省" data-code="210000">辽宁省</option>
				          <option value="吉林省" data-code="220000">吉林省</option>
				          <option value="黑龙江省" data-code="230000">黑龙江省</option>
				          <option value="上海市" data-code="310000">上海市</option>
				          <option value="江苏省" data-code="320000">江苏省</option>
				          <option value="浙江省" data-code="330000">浙江省</option>
				          <option value="安徽省" data-code="340000">安徽省</option>
				          <option value="福建省" data-code="350000">福建省</option>
				          <option value="江西省" data-code="360000">江西省</option>
				          <option value="山东省" data-code="370000">山东省</option>
				          <option value="河南省" data-code="410000">河南省</option>
				          <option value="湖北省" data-code="420000">湖北省</option>
				          <option value="湖南省" data-code="430000">湖南省</option>
				          <option value="广东省" data-code="440000">广东省</option>
				          <option value="广西壮族自治区" data-code="450000">广西壮族自治区</option>
				          <option value="海南省" data-code="460000">海南省</option>
				          <option value="重庆市" data-code="500000">重庆市</option>
				          <option value="四川省" data-code="510000">四川省</option>
				          <option value="贵州省" data-code="520000">贵州省</option>
				          <option value="云南省" data-code="530000">云南省</option>
				          <option value="西藏自治区" data-code="540000">西藏自治区</option>
				          <option value="陕西省" data-code="610000">陕西省</option>
				          <option value="甘肃省" data-code="620000">甘肃省</option>
				          <option value="青海省" data-code="630000">青海省</option>
				          <option value="宁夏回族自治区" data-code="640000">宁夏回族自治区</option>
				          <option value="新疆维吾尔自治区" data-code="650000">新疆维吾尔自治区</option>
				          <option value="台湾省" data-code="710000">台湾省</option>
				          <option value="香港特别行政区" data-code="810000">香港特别行政区</option>
				          <option value="澳门特别行政区" data-code="820000">澳门特别行政区</option>
			          </select>				    
			          <select class="form-control" id="city10">
			          	<option value="" data-code="">—— 市 ——</option>
			          </select>
			          <select class="form-control" id="district10">
			          	<option value="" data-code="">—— 区 ——</option>
			          </select>
			          <div>
			          <br/>
			          <label>请输入具体地址:</label>
			          <input type="text" name="address" placeholder="请输入具体地址" onblur="checkaddress()" required/><span class="tips" id="divaddress">长度1~30个字符</span>
			          </div>
			    </div>
		    </li>
		</ul>
			<b class="btn"><input type="submit" value="提交"/>
			<input type="reset" value="取消"/></b>
	</form>
	</div>
</div>
<script type="text/javascript">
//验证登录用户名
 	function checkname(){
		var na=document.form1.uname.value;
		// console.log(na);
	  	if( na.length <1 || na.length >12)  
  		{  	
  			divname.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
  		     
  		}else{  
  		    divname.innerHTML='<font class="tips_true">输入正确</font>';
  		   
  		}  
  	}

//验证登录密码 
	function checkpsd(){    
		var psd1=document.form1.password.value;  
		var flagZM=false ;
		var flagSZ=false ; 
		var flagQT=false ;
		console.log(psd1);
		if(psd1.length<6 || psd1.length>12){  
			alert(121); 
			divpassword1.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
		}else{   
			for(i=0;i < psd1.length;i++)   
				{    
					if((psd1.charAt(i) >= 'A' && psd1.charAt(i)<='Z') || (psd1.charAt(i)>='a' && psd1.charAt(i)<='z')) 
					{   
						flagZM=true;
					}
					else if(psd1.charAt(i)>='0' && psd1.charAt(i)<='9')    
					{ 
						flagSZ=true;
					}else    
					{ 
						flagQT=true;
					}   
				}   
				if(!flagZM||!flagSZ||flagQT){
				divpassword1.innerHTML='<font class="tips_false">密码必须是字母数字的组合</font>'; 
				 
				}else{
					
				divpassword1.innerHTML='<font class="tips_true">输入正确</font>';
				 
				}  
			 
			}	
	}

//验证注册用户名
 	function checkna(){
		var na=document.form2.uname.value;
		// console.log(na);
	  	if( na.length <1 || na.length >12)  
  		{  	
  			divname.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
  		     
  		}else{  
  		    divname.innerHTML='<font class="tips_true">输入正确</font>';
  		   
  		}  
  	}

//验证注册密码 
	function checkpsd1(){    
		var psd1=document.form2.password.value;  
		var flagZM=false ;
		var flagSZ=false ; 
		var flagQT=false ;
		console.log(psd1);
		if(psd1.length<3 || psd1.length>12){   
			divpassword1.innerHTML='<font class="tips_false">长度必须3~12个字符</font>';
		}else{   
			for(i=0;i < psd1.length;i++)   
				{    
					if((psd1.charAt(i) >= 'A' && psd1.charAt(i)<='Z') || (psd1.charAt(i)>='a' && psd1.charAt(i)<='z')) 
					{   
						flagZM=true;
					}
					else if(psd1.charAt(i)>='0' && psd1.charAt(i)<='9')    
					{ 
						flagSZ=true;
					}else    
					{ 
						flagQT=true;
					}   
				}   
				if(!flagZM||!flagSZ||flagQT){
				divpassword1.innerHTML='<font class="tips_false">密码必须是字母数字的组合</font>'; 
				 
				}else{
					
				divpassword1.innerHTML='<font class="tips_true">输入正确</font>';
				 
				}  
			 
			}	
	}

//验证注册确认密码 
	function checkpsd2(){ 

		if(document.form2.yourpass2.value!=document.form2.password.value || document.form2.yourpass2.value=='') { 
		     divpassword2.innerHTML='<font class="tips_false">您两次输入的密码不一样</font>';
		} else { 
		     divpassword2.innerHTML='<font class="tips_true">输入正确</font>';
		}
	}

//验证注册邮箱		
	function checkmail(){
		var apos=document.form2.email.value.indexOf("@");
		var dotpos=document.form2.email.value.lastIndexOf(".");
		if (apos<1||dotpos-apos<2) 
		  {
		  	divmail.innerHTML='<font class="tips_false">输入错误</font>' ;
		  }
		else {
			divmail.innerHTML='<font class="tips_true">输入正确</font>' ;
		}
	}

//验证注册地址		
	function checkaddress(){
		var address=document.form2.address.value;
	  	if( address.length <1 || address.length >30){ 
	  		divaddress.innerHTML='<font class="tips_false">长度必须1~30个字符</font>';  		     
  		}else{  
  		    divaddress.innerHTML='<font class="tips_true">输入正确</font>';  		   
  		}  
  	}
</script>
<script src="/web3/Public/js/js_index_headclick/jquery.min.js"></script>
<script src="/web3/Public/js/js_index_headclick/distpicker.data.js"></script>
<script src="/web3/Public/js/js_index_headclick/distpicker.js"></script>
<script src="/web3/Public/js/js_index_headclick/main.js"></script>
<div class="modal" id="forgetform">
	<a class="close" data-dismiss="modal">×</a>
	<h1>忘记密码</h1>
	<form class="forgot-form" method="post" action="">
		<input name="email" value="" placeholder="注册邮箱：">
		<div class="clearfix"></div>
		<input type="submit" name="type" class="forgot button-blue" value="发送重设密码邮件">
	</form>
</div>
<script type="text/javascript" src="/web3/Public/js/js_index_headclick/modal.js"></script>
<!-- 弹框end -->


<!-- 继承开始 -->

<title> Feng论坛 -  Feng论坛 - Feng网</title>
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_Bbs/style_4_common.css" /><link rel="stylesheet" type="text/css" href="/web3/Public/css/css_Bbs/style_4_forum_index.css">    
<script src="/web3/Public/js/js_Bbs/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">var STYLEID = '4', STATIC_DIR = '/bbs_v4/', STATICURL = 'static/', IMGDIR = 'static/image/common', VERHASH = 'Ymk', charset = 'utf-8', discuz_uid = '0', cookiepre = 'xIka_2132_', cookiedomain = '', cookiepath = '/', showusercard = '0', attackevasive = '0', disallowfloat = '', creditnotice = '', defaultstyle = '', REPORTURL = 'aHR0cDovL2Jicy5mZW5nLmNvbS9mb3J1bS5waHA/Z2lkPTQ4Ng==', SITEURL = 'http://bbs.feng.com/', JSPATH = 'static/js/', DYNAMICURL = '';
    jQuery.noConflict().ajaxSetup({ cache: true });
</script>
    
<link rel="apple-touch-icon" href="./source/plugin/feng_mobile/template//web3/Public/images/images_Bbs/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76" href="./source/plugin/feng_mobile/template//web3/Public/images/images_Bbs/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120" href="./source/plugin/feng_mobile/template//web3/Public/images/images_Bbs/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152" href="./source/plugin/feng_mobile/template//web3/Public/images/images_Bbs/touch-icon-ipad-retina.png">
<link rel="apple-touch-icon-precomposed" href="./source/plugin/feng_mobile/template//web3/Public/images/images_Bbs/touch-icon-ipad-retina.png">
<link rel="archives" title="Feng论坛" href="http://bbs.feng.com/archiver/">
<script src="/web3/Public/js/js_Bbs/forum.js" type="text/javascript"></script>

<!-- 备份格式 -->
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_Bbs/style_4_common.css" /><link rel="stylesheet" type="text/css" href="/web3/Public/css/css_Bbs/style_4_forum_index.css">    
<script src="/web3/Public/js/js_Bbs/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">var STYLEID = '4', STATIC_DIR = '/bbs_v4/', STATICURL = 'static/', IMGDIR = 'static/image/common', VERHASH = 'Ymk', charset = 'utf-8', discuz_uid = '0', cookiepre = 'xIka_2132_', cookiedomain = '', cookiepath = '/', showusercard = '0', attackevasive = '0', disallowfloat = '', creditnotice = '', defaultstyle = '', REPORTURL = 'aHR0cDovL2Jicy5mZW5nLmNvbS9mb3J1bS5waHA/Z2lkPTQ4Ng==', SITEURL = 'http://bbs.feng.com/', JSPATH = 'static/js/', DYNAMICURL = '';
    jQuery.noConflict().ajaxSetup({ cache: true });
</script>

<!-- Quick Services -->
<div class="wrap quick_services">
    <ul class="user_links">
        <li class="my"><a href="#"><i class="user"><b></b></i>我的威锋</a></li>
        <li><a href="#"><i class="subject"><b></b></i>主题</a></li>
        <li><a href="#"><i class="reply"><b></b></i>回复</a></li>
        <li><a href="#"><i class="fav"><b></b></i>收藏</a></li>
        <li><a href="#"><i class="newscommon"><b></b></i>评论</a></li>
        <li><a href="#"><i class="doing"><b></b></i>动态</a></li>
        <li class="hide"><a href="#"><i class="mark"><b></b></i>书签</a></li>
    </ul>
    <ul class="links">
        <li><a href="#">最新热门</a></li>
        <li><a href="#">精华区</a></li> 
    </ul>
</div>
            	
<style type="text/css">
    #bdcs{ 
        width:366px;height:30px;
    }
    .bdcs-container .bdcs-search-form-input{ 
        width:310px;display:inline-block; background:#efefef; border:1px solid #efefef; border-top-color:#fff; border-radius:4px; box-shadow:0 2px 2px rgba(0,0,0,.09); padding:0 26px; height:28px; vertical-align:top;
    }
    .google_sch{ 
        position:relative; height:30px;
    }
    .google_sch .zoom{
        left: 37px;z-index: 5;top: 1px;
    }
    .bdcs-clearfix{ 
        height:30px;
    }
    .bdcs-container{ 
        height:30px;
    }
    .bdcs-container .bdcs-search-form-submit,.bdcs-container .bdcs-hot{
        display: none;
    }
    .bdcs-container .bdcs-search-form-input:focus {
        border-color: #1AA0CA;
    }
</style>

<script src="/web3/Public/js/js_Bbs/jquery-migrate-1.1.1.js" type="text/javascript"></script> 
<div id="search_box" class="wrap search_box"> 
    <div class="field_panel curr_owner">
        <div class="type_panel">
            <span class="type">
                <em class="google">百度</em>
                <em class="owner">论坛搜索</em>
            </span>
        </div>
        <div class="sch_panel google_sch"><i class="zoom"></i>
            <form  method="get" id="scbar_form2" autocomplete="off" action="http://www.baidu.com"  target="_blank" >
                <ul>
                <li class="txt_field"><span class="txt"><i class="zoom"></i><input type="text" name="srchtxt" id="scbar_txt" placeholder=" 百度一下,你就知道! "><i class="clear hide"></i></span></li>
                </ul>
            </form>
        </div>
        <div class="sch_panel owner_sch">
            <form  method="get" id="scbar_form" autocomplete="off" action="http://s.feng.com/search.php"  target="_blank" >
                <ul>
                    <li class="txt_field">
                        <span class="txt"><i class="zoom"></i><input type="text" name="srchtxt" id="scbar_txt" placeholder="搜索其实很简单！ (^_^)" /><i class="clear hide"></i></span>
                    </li>
                    <li class="sch_type">
                        <div class="type_list">
                            <span class="current">论坛</span>
                            <span data-type="thread">论坛</span>
                            <span data-type="news">新闻</span>
                            <span data-type="lab">观点</span>
                            <span data-type="user">作者</span>
                            <i class="arrow"></i><i class="line"></i>
                        </div> 
                    </li>
                    <li class="btns"><button type="submit">搜索</button></li>
                </ul>
                <input type="hidden" name="srchmod" id="scbar_mod" value="thread">
                <input type="hidden" name="formhash" value="4235f087">
                <input type="hidden" name="srchtype" value="title">
                <input type="hidden" name="srhfid" value="486">
                <input type="hidden" name="srhlocality" value="forum::index">
            </form>
        </div>
    </div>
</div>

<!-- Container -->
<div id="wp" class="wp">
<div id="pt" class="bbs_info crumbs">
<div class="crumbs_inner"><a href="./" class="home" title="首页">威锋论坛</a><em>&raquo;</em><a href="forum.php">论坛</a><em>&rsaquo;</em> 游戏中心</div>
</div>

<div class="wrap fl_row">
    <div class="fl_tb">
        <div class="inner">
            <div id="forum_rules_511" class="forum_rules" style="">
                <div class="s_title"><h3><i class="sound"></i>版块公告</h3><i class="f_l"></i><i class="f_r"></i></div>
                <div class="inner">
                <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$n): $mod = ($i % 2 );++$i;?><strong><?php echo ($n["content"]); ?></strong><br /><?php endforeach; endif; else: echo "" ;endif; ?>
                </div>
            </div>
            <ul style="margin-top: 8px;">
                <li>
                    <div><a href='#' target='_blank' style="position:relative;">
                        <div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div>
                        <img src='/web3/Public/picture/picture_Bbs/view.php' border='0' alt=''></a>
                    </div>
                </li>
                <li>
                    <div><a href='#' target='_blank' style="position:relative;">
                        <div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div>
                        <img src='/web3/Public/picture/picture_Bbs/view.php' border='0' alt=''></a>
                    </div>
                </li>
                <li>
                    <div><a href='#' target='_blank' style="position:relative;">
                        <div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div>
                        <img src='/web3/Public/picture/picture_Bbs/view.php' border='0' alt=''></a>
                    </div>
                </li>
                <li>
                    <div>
                        <a href='#' target='_blank' style="position:relative;">
                        <div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div>
                        <img src='/web3/Public/picture/picture_Bbs/view.php' border='0' alt=''></a>
                    </div>
                </li>
                <li class="fix"></li>
            </ul>
            <ul>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li class="fix"></li>
            </ul>
        </div>
    </div>
</div>
<div id="ct" class="wp cl re_ct">
    <div class="mn">
        <div class="fl bm">
            <div class="bm bmw  cl">
                <div class="bm_h cl">
                    <span class="o hide" onclick="category_486;" title="收起/展开"></span>
                    <span class="y">分区版主: <a href="#" class="notabs" c="1">威游_GM</a>, <a href="#" class="notabs" c="1">威游客服</a>, <a href="#" class="notabs" c="1">威游GM</a>, <a href="#" class="notabs" c="1">威游RX</a></span><h2><a href="<?php echo U('Bbslist/index');?>" style="">游戏中心</a></h2>
                </div>
                <div id="category_486" class="bm_c" style="">
                <table cellspacing="0" cellpadding="0" class="fl_tb">                     
                <tr>
                    <td class="fl_icn"  style="width: 72px;">
                        <a href="#"><img src="/web3/Public/picture/picture_Bbs/common_511_icon.png" align="left" alt="" /></a>
                    </td>
                    <td>
                    <h2><a href="<?php echo U('Bbslist/index');?>">游戏中心</a></h2>
                    <p class="xg2">爱好游戏的你怎能错过！单机or网游在这里可以畅所欲言，攻略、试玩or锋友对战在这里应有尽有。</p>
                    <p>版主: <span class="xi2"><a href="#" class="notabs" c="1"><strong>枫hua</strong></a></span></p></td>
                    <td class="fl_i">
                        <span class="xi2">8355</span><span class="line">/</span><span class="xg1"><span title="184704">18万</span></span>
                    </td>
                    <td class="fl_by">
                        <div class="last_post">
                            <a href="#" class="user_pic">
                                <i class="mask"></i><img src="/web3/Public/picture/picture_Bbs/10_avatar_small.jpg" height="42" width="42" alt="三马俩骑" />
                            </a>
                            <span class="reply">
                                <span>Re:</span>
                                <a href="#" class="xi2">求个NBA2K.16</a>
                            </span>
                            <cite>by <a href="#">三马俩骑</a>, <span title="2016-9-20 21:44">昨天&nbsp;21:44</span></cite>   
                        </div>
                    </td>
                </tr>
                <tr class="fl_row">
                    <td class="fl_icn"  style="width: 72px;">
                        <a href="#"><img src="/web3/Public/picture/picture_Bbs/common_620_icon.png" align="left"></a>
                    </td>
                    <td>
                        <h2><a href="<?php echo U('Bbslist/index');?>">Pokemon Go</a></h2>
                        <p class="xg2">作为任天堂公司旗下的超人气IP作品，《精灵宝可梦》在全球范围内拥有无数的粉丝与拥趸，而今年任天堂推出的《精灵宝可梦GO》可以说是移动平台中的最受关注的游戏作品之一。在威锋论坛大家可以一起来交流讨论这款游戏，晒出你的精灵，寻找志同道合的锋友！</p>
                    </td>
                    <td class="fl_i">
                        <span class="xi2">2664</span><span class="line">/</span><span class="xg1"><span title="26567">2万</span></span>
                    </td>
                    <td class="fl_by">
                        <div class="last_post">
                            <a href="#" class="user_pic">
                                <i class="mask"></i>
                                <img src="/web3/Public/picture/picture_Bbs/46_avatar_small.jpg" height="42" width="42" alt="雷霆神团">
                            </a>
                            <span class="reply"><span>Re:</span>
                            <a href="#" class="xi2">【请教！旧金山的精灵座标刷不出 ...</a></span><cite>by <a href="#">雷霆神团</a>, <span title="2016-9-20 20:18">昨天&nbsp;20:18</span></cite>
                        </div>
                    </td>
                </tr>
                <tr class="fl_row">
                </tr>
                </table>
                </div>
            </div>
        </div>          
    </div>
<script language='JavaScript' type='text/javascript' src='/web3/Public/js/js_Bbs/x.js'></script>
<script language='JavaScript' type='text/javascript'>
   if (!document.phpAds_used) document.phpAds_used = ',';
   phpAds_random = new String (Math.random()); phpAds_random = phpAds_random.substring(2,11);
   
   document.write ("<" + "script language='JavaScript' type='text/javascript' src='");
   document.write ("http://yes1.feng.com/js.php?n=" + phpAds_random);
   document.write ("&amp;what=zone:151");
   document.write ("&amp;exclude=" + document.phpAds_used);
   if (document.referrer)
      document.write ("&amp;referer=" + escape(document.referrer));
   document.write ("'><" + "/script>");
</script>
<noscript>
    <a href='#' target='_blank'><img src='/web3/Public/picture/picture_Bbs/view.php' border='0' alt=''></a>
</noscript> 
</div>

<!-- Quick Links -->
<div class="quick_links_wrap">
    <div id="quick_links" class="quick_links"><a href="#" id="return_top" class="return_top"><span>返回顶部</span></a></div>
</div>
<script type="text/javascript">

//搜索
jQuery(function($){
    var 
    shell = $('#search_box'),
    showInfo = window.showDialog || alert;

//切换
var typePanel = shell.find('.field_panel').eq(0);
shell.delegate('.type_panel em', 'click', function(e){
    var currClassName = typePanel[0].className, cName = this.className;
    if(currClassName.indexOf(cName) < 0){
        typePanel[0].className = 'field_panel curr_' + cName;
        shell.find('.' + cName + '_sch input[type=text]').eq(0).focus();
        setcookie('use_google_sch', cName === 'google' ? 1 : 0, cName === 'google' ? 86400 * 30 : -1);
    }
});

//Clear & submit
shell.delegate('form', 'submit', function(e){
    var field = $(this).find('input[type=text]');
    if(field.val() == ''){
        showInfo('请输入搜索关键字！');
        return false;
    }
})
.delegate('input[type=text]', 'keyup', function(){
    var self = $(this), clearBtn = self.data('clearBtn');
    if(!clearBtn){
        clearBtn = self.next('.clear');
        self.data('clearBtn', clearBtn);
        clearBtn.data('target_field', self);
    }
clearBtn[this.value != '' ? 'show' : 'hide']();
})
.delegate('i.clear', 'click', function(){
    var field = $(this).data('target_field');
    if(field){
        this.style.display = '';
        field.val('').focus();
    }
});

//类别
shell.delegate('.type_list', 'mouseenter', function(){
    $(this).addClass('active');
})
.delegate('.type_list', 'mouseleave', function(){
    $(this).removeClass('active');
})
.delegate('.type_list span', 'click', function(){
    var panel = $(this.parentNode).removeClass('active');

    if(this.className.indexOf('current') < 0){
        panel.find('span.current').html(this.innerHTML);

        var 
        field = $('#scbar_mod').val(this.getAttribute('data-type')),
        form = field[0] && field[0].form;
        form && $('input[type=text]', form).focus();
    }
});


});
</script>


<!-- 继承结束 -->

 
<div class="wrap footer">
	<div class="links">
		<ul>
			<li><a href="#" target="_blank">关于我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">联系我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">About Us</a></li>
		</ul>
	</div>
	<div class="copyright">
		<p>Copyright 2016-2017 © zhuruicheng. All rights reserved 保留所有权利</p>
	</div>
</div>


</div>
<noscript><a href='#' target='_blank'><img src='http://yes1.feng.com/view.php?what=zone:189&amp;n=afbf8627' border='0' alt=''></a></noscript>

</body>
</html>