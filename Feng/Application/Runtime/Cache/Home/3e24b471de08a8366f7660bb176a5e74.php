<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="/favicon.ico" type="/web3/Public/image/x-icon">
<!-- 给网站添加一个icon图标 -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="apple-itunes-app" content="app-id=981437434" />

<link rel="alternate" type="application/rss+xml" title="Feng新闻RSS" href="">

<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/common.css" />
<script type="text/javascript" src="/web3/Public/js/js_index/jquery-1.7.min.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_index/common.js"></script>

</head>
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/style.css" />
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/feng_index.css" />
<script type="text/javascript" src="/web3/Public/js/js_index/index.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_index/jquery.marquee.min.js"></script>
<body>
<!-- Global Topbar -->
<div class="global_topbar_wrap">
	<div id="global_topbar" class="global_topbar">
		<div class="wrap inner">
			<div class="services" id="global_topbar_services">
				<ul>
				<li class="current"><a href="<?php echo U('Index/index');?>"><i class="weiphone"></i>Feng综合网</a></li>				
				<li><a href="<?php echo U('News/index');?>"><i class="wegame"></i>F新闻</a></li>						
				<li><a href="<?php echo U('Bbs/index');?>"><i class="fengbuy"></i>F论坛</a></li>
					
				<li><a href="#"><i class="fengbuy"></i>建设中</a></li>
				
				<li><a href="<?php echo U('Index/index');?>"><i class="money"></i>扫码点击</a></li>								
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>
			<div class="links" id="globar_topbar_links">
				<ul>
					<li><a href="<?php echo U('Index/index');?>" target="_blank">首页</a></li>
					<li><a href="<?php echo U('News/index');?>" target="_blank">Feng新闻</a></li>
					<li><a href="<?php echo U('Bbs/index');?>" target="_blank">Feng论坛</a></li>
					<li><a href="" target="_blank">开辟中</a></li>
				</ul>
				<div class="focus"><em></em></div>
			</div>
			
			<div class="client" id="global_topbar_client">
				<ul>
					<li class="current"><a href="#"><i></i>手机APP</a></li>					
					<div class="codeBox">
						<h3>扫一扫下载APP</h3>
						<b><img src="/web3/Public/picture/fengcode.jpg" width="94" height="94" alt="Feng二维码" /></b>
					</div>
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>

<!-- 弹框开始 -->
			<div class="user_menu" id="global_user_menu">
			<?php if($_SESSION['username']['uname']== '' ): ?><ul>
					<li><a data-toggle="modal" href="#login-modal"><span>登录</span></a></li>
					<li><a data-toggle="modal" href="#signup-modal"><span>立即注册</span></a></li>
					<li><a data-toggle="modal" href="#forgetform"><span>找回密码</span></a></li>
				</ul>
				<?php else: ?>
				<ul style="display:block;">
					<li class="face"><a href="#"><img src="/web3/Public/Picture/<?php echo ($_SESSION['username']['uface']); ?>" height="30px" width="30px"  style="margin-top:-5px;"/></a></li>
					<li>
						<b style="font-size:15px"><?php echo ($_SESSION['username']['uname']); ?></b>
					</li>
					<li>
						<a class="ref" href="<?php echo U('Index/logout');?>" title="注销">
							<i class="logout"></i>
							<span class="label">注销</span>
						</a>
					</li>
				</ul><?php endif; ?>
			</div>
<!-- 弹框结束 -->

		<script type="text/javascript" src="/web3/Public/js/js_index/jquery.autocomplete.min.ajax.js"></script>
		<div class="searchBtn">
			<span id="quickSearchBtn"></span>
			<div class="headerSearch" id="headerSearch">
				<div class="searchBoxtop">
					<form id="searchform_top" action="http://s.feng.com/search.php" method="get" >
						<input id="top_keyword" name="srchtxt" type="text" value="搜索其实很简单^ ^">
						<input type="hidden" name="srchmod" value="all">
					</form>	
				</div>
			</div>
		</div>	
		</div>
	</div>
</div>

<!-- 弹框start -->

<!-- 登录开始 -->
<div class="modal" id="login-modal">
	<a class="close" data-dismiss="modal">×</a>
	<h1>登录</h1>
	<div class="contact" >
	
		<form action="<?php echo U('Index/doSelect');?>" name="login" method="post">
			<ul>
				<li>
					<label>用户名：</label>
					<input type="text" name="yourname" placeholder="请输入用户名" id='yourname' onblur="checkname()" required/><span class="tips" id="divname2">长度1~12个字符</span>
				</li>

				<li>
					<label>密码：</label>
					<input type="password" name="yourpass" placeholder="请输入您的密码" onblur="checkpsd1()" required/><span class="tips" id="divpassword1">密码必须由字母和数字组成</span>
				</li>
			</ul>
			<b class="btn"><input type="submit" value="登录"/>
			<input type="reset" value="取消"/></b>
		</form>

	</div>
</div>

   <script>

      function select(){
      	var username = document.login.yourname.value;
        var userpass = document.login.yourpass.value;
      	var temp;
      	$.ajax({
      		type:'POST',
      		url:"<?php echo U('Action/select');?>",
      		async:false,
      		data:{name:yourname,pass:yourpass},
      	   	
      		success:function(data){
      			if(data == 1){
      				alert('用户名错误');
      				temp = 1;
      				
      			} else if(data == 2){
      				alert('密码错误');
      				temp = 2;  

      			}else if(data == 3){
              		alert('账号已被冻结!')
      				temp = 3;
      				
      			}
      		}
      	})

      	if(temp == 1 || temp == 2 || temp == 3){
      		return false;
      	} else{
          var code = document.deng.codetwo.value;
          var vo = null;
            $.ajax({
              url:"<?php echo U('Action/doAjax');?>",
              data:"info="+code,
              async: false,
              success:function(data){

                if(data != true){
                  shuaxin('verco1');
                  alert('验证码错误');
                  vo = 0;
                } else {

                 	vo = 1;
                }           
              },
            })
            if(vo != 1) {
              return false;
            }else{
              return true;
            }           
      	}
      
      	
      }
      	//关闭窗口	
          var close = document.getElementById('close');
          var back = document.getElementById('login');
          close.onclick = function(){
              back.style.display = 'none';
          }
         function dengru (){
             back.style.display = 'block';          
          }
       </script>
<!-- 登录结束 -->

<!-- 注册开始 -->
<div class="modal" id="signup-modal">
	<a class="close" data-dismiss="modal">×</a>
	<h1>注册</h1>
	<div class="contact" >

	<form action="<?php echo U('Action/add');?>" name="register" method="post">
		<ul>
			<li>
				<label>用户名：</label>
				<input type="text" name="uname" placeholder="请输入用户名"  onblur="checkname()" required/><span class="tips" id="divname">长度1~12个字符</span>
			</li>

			<li>
				<label>性别：</label>
				<input type="radio" name="sex" id="1" value="1">男&nbsp;&nbsp;
				<input type="radio" name="sex" id="2" value="2">女&nbsp;&nbsp;
				<input type="radio" name="sex" id="0" value="0">保密
			</li>

			<li>
				<label>密码：</label>
				<input type="password" name="password" placeholder="请输入您的密码" onblur="checkpsd1()" required/><span class="tips" id="divpasswords1">密码必须由字母和数字组成</span>
			</li>

			<li>
				<label>确认密码：</label>
				<input type="password" name="repassword" placeholder="请再次输入您的密码" onblur="checkpsd2()" required/><span class="tips" id="divpassword2">两次密码需要相同</span>
			</li>

			<li>
				<label>电子邮箱：</label>
				<input type="text" name="email" placeholder="请输入您的邮箱" onblur="checkmail()" required/><span class="tips" id="divmail">请输入您的邮箱地址</span>
			</li>

			<li>
		      	<div id="distpicker5">
		      	<label>请选择所在地:</label>
			        <div class="form-group">
			          <select class="form-control" id="province10">
				          <option value="" data-code="">—— 省 ——</option>
				          <option value="北京市" data-code="110000">北京市</option>
				          <option value="天津市" data-code="120000">天津市</option>
				          <option value="河北省" data-code="130000">河北省</option>
				          <option value="山西省" data-code="140000">山西省</option>
				          <option value="内蒙古自治区" data-code="150000">内蒙古自治区</option>
				          <option value="辽宁省" data-code="210000">辽宁省</option>
				          <option value="吉林省" data-code="220000">吉林省</option>
				          <option value="黑龙江省" data-code="230000">黑龙江省</option>
				          <option value="上海市" data-code="310000">上海市</option>
				          <option value="江苏省" data-code="320000">江苏省</option>
				          <option value="浙江省" data-code="330000">浙江省</option>
				          <option value="安徽省" data-code="340000">安徽省</option>
				          <option value="福建省" data-code="350000">福建省</option>
				          <option value="江西省" data-code="360000">江西省</option>
				          <option value="山东省" data-code="370000">山东省</option>
				          <option value="河南省" data-code="410000">河南省</option>
				          <option value="湖北省" data-code="420000">湖北省</option>
				          <option value="湖南省" data-code="430000">湖南省</option>
				          <option value="广东省" data-code="440000">广东省</option>
				          <option value="广西壮族自治区" data-code="450000">广西壮族自治区</option>
				          <option value="海南省" data-code="460000">海南省</option>
				          <option value="重庆市" data-code="500000">重庆市</option>
				          <option value="四川省" data-code="510000">四川省</option>
				          <option value="贵州省" data-code="520000">贵州省</option>
				          <option value="云南省" data-code="530000">云南省</option>
				          <option value="西藏自治区" data-code="540000">西藏自治区</option>
				          <option value="陕西省" data-code="610000">陕西省</option>
				          <option value="甘肃省" data-code="620000">甘肃省</option>
				          <option value="青海省" data-code="630000">青海省</option>
				          <option value="宁夏回族自治区" data-code="640000">宁夏回族自治区</option>
				          <option value="新疆维吾尔自治区" data-code="650000">新疆维吾尔自治区</option>
				          <option value="台湾省" data-code="710000">台湾省</option>
				          <option value="香港特别行政区" data-code="810000">香港特别行政区</option>
				          <option value="澳门特别行政区" data-code="820000">澳门特别行政区</option>
			          </select>				    
			          <select class="form-control" id="city10">
			          	<option value="" data-code="">—— 市 ——</option>
			          </select>
			          <select class="form-control" id="district10">
			          	<option value="" data-code="">—— 区 ——</option>
			          </select>
			          <div>
			          <br/>
			          <label>请输入具体地址:</label>
			          <input type="text" name="youraddress" placeholder="请输入具体地址" onblur="checkaddress()" required/><span class="tips" id="divaddress">长度1~30个字符</span>
			          </div>
			    </div>
		    </li>
		</ul>
			<b class="btn"><input type="submit" value="提交"/>
			<input type="reset" value="取消"/></b>
	</form>

	</div>
</div>
<script type="text/javascript">
//验证注册用户名
 	function checkname(){
		na=document.register.uname.value;
	  	if( na.length <1 || na.length >12)  
  		{  	
  			divname.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
  		     
  		}else{  
  		    divname.innerHTML='<font class="tips_true">输入正确</font>'; 
  		}  
  	}

//验证注册密码 
	function checkpsd1(){    
		var psd1=document.register.password.value;
		var flagZM=false ;
		var flagSZ=false ; 
		var flagQT=false ;
		if(password.length<6 || repassword.length>12){   
		console.log(password.length);  
			divpasswords1.innerHTML='<font class="tips_false">长度必须大于6位并小于12位</font>';
		}else
			{   
			  for(i=0;i < password.length;i++)   
				{    
					if((psd1.charAt(i) >= 'A' && password.charAt(i)<='Z') || (psd1.charAt(i)>='a' && psd1.charAt(i)<='z')) 
					{   
						flagZM=true;
					}
					else if(password.charAt(i)>='0' && password.charAt(i)<='9')    
					{ 
						flagSZ=true;
					}else    
					{ 
						flagQT=true;
					}   
				}   
				if(!flagZM||!flagSZ||flagQT){
				divpassword1.innerHTML='<font class="tips_false">密码必须是字母数字的组合</font>'; 
				 
				}else{
					
				divpassword1.innerHTML='<font class="tips_true">输入正确</font>';
				 
				}  
			 
			}	
	}

//验证注册确认密码 
	function checkpsd2(){ 
		if(document.register.password.value!=document.register.repassword.value) { 
		     divpassword2.innerHTML='<font class="tips_false">您两次输入的密码不一样</font>';
		} else { 
		     divpassword2.innerHTML='<font class="tips_true">输入正确</font>';
		}
	}

//验证注册邮箱		
	function checkmail(){
		apos=document.register.email.value.indexOf("@");
		dotpos=document.register.email.value.lastIndexOf(".");
		if (apos<1||dotpos-apos<2) 
		  {
		  	divmail.innerHTML='<font class="tips_false">输入错误</font>' ;
		  }
		else {
			divmail.innerHTML='<font class="tips_true">输入正确</font>' ;
		}
	}

//验证注册地址		
	function checkaddress(){
		address=document.register.youraddress.value;
	  	if( address.length <1 || address.length >30){ 
	  	divaddress.innerHTML='<font class="tips_false">长度必须1~30个字符</font>';  		     
  		}else{  
  		    divaddress.innerHTML='<font class="tips_true">输入正确</font>';  		   
  		}  
  	}
</script>
<script src="/web3/Public/js/js_index_headclick/jquery.min.js"></script>
<script src="/web3/Public/js/js_index_headclick/distpicker.data.js"></script>
<script src="/web3/Public/js/js_index_headclick/distpicker.js"></script>
<script src="/web3/Public/js/js_index_headclick/main.js"></script>
<div class="modal" id="forgetform">
	<a class="close" data-dismiss="modal">×</a>
	<h1>忘记密码</h1>
	<form class="forgot-form" method="post" action="">
		<input name="email" value="" placeholder="注册邮箱：">
		<div class="clearfix"></div>
		<input type="submit" name="type" class="forgot button-blue" value="发送重设密码邮件">
	</form>
</div>
<script type="text/javascript" src="/web3/Public/js/js_index_headclick/modal.js"></script>
<!-- 弹框end -->
<!-- 注册结束 -->

<!-- 继承开始 -->


<title>威锋网新闻频道 24小时不间断的新闻_威锋网</title>
<meta name="description" content="Feng网是最早建立的关于iPhone的专题网站,威锋论坛一直是人气最旺的iPhone社区">
<meta name="keywords" content="iPhone4s,iPhone4,iPhone5,iPad,iPad2,iPhone,3G iPhone,iPhone中文网,iPhone 越狱,iPhone 3G,iPad,iPhone" />

<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_news/common.css" />

<script type="text/javascript" src="/web3/Public/js/js_news/common.js"></script>

<script type="text/javascript">
(function(){
if( document.referrer && (
		document.referrer.match(/bbs\.feng\.com\/mobile\-.+\.html/i) ||
		document.referrer.match(/bbs\.feng\.com\/plugin\.php/i)
	) ){
	return ds.setCookie('view_origin', 1, 1);
}
if( navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i) && ds.getCookie('view_origin') != 1 ){
	location.replace('http://bbs.feng.com/mobile-news-app-1.html');
}
})();
</script>

</head><link rel="stylesheet" type="text/css" href="/web3/Public/css/css_news/feng.css" />
<script type="text/javascript" src="/web3/Public/js/js_news/addele.js"></script><body>
<!-- Global Topbar -->


<script type="text/javascript">
$("#top_keyword").autocomplete('http://s.feng.com/api/autocomplateAjax.php',{
		max: 12,
		minChars: 1, 
		width: 306,
		scrollHeight: 300,
		matchContains: true,
		autoFill: false,
		dataType: 'jsonp', 
		extraParams: {
			searchtype: 'thread' 		//应用类型
		},			 
		parse: function(rdata){	
			var rows = [];
			for(var i=0; i<rdata.length; i++){
				rows[i] = { 
					data:rdata, 
					value:rdata[i], 
					result:rdata[i]
				}; 
			}
			return rows;	
		}
	}).result(function(event, data, formatted) {
		$("#searchform_top").submit();
	});
</script>  		</div>
	</div>
</div>
<script type="text/javascript" src="/web3/Public/js/js_news/topbarnav.js"></script><script type="text/javascript">
!(function(){var setUser=function(){return this.userInfo=!1,this.callback=[],this};setUser.prototype={setUserInfo:function(a){return this.userInfo=a,this.run()},run:function(){var a=this;return $.each(this.callback,function(b,c){c.call(a,a.userInfo)}),this},addCallback:function(a){return this.callback.push(a),this.run()}},$.extend({setUser:new setUser});})();
$.getScript("http://www.feng.com/images_v4/js/topbar.js",function(){return $.getScript("http://bbs.feng.com/newsheader.php?r="+(new Date().getTime()));});
(function(){
var ref="";
$("#global_user_menu .ref").each(function(){
	var o=$(this);o.attr("href",o.attr("href")+ref);
});
})();
$("#navigator li:eq()").addClass("current");
</script><div class="headTopBox" style="height:130px">
    <div class="wrap headTopBoxCon">
        <a href="http://news.feng.com" title="威锋网"><i class="logo"></i><i class="news_ico"></i></a>
<div class="searchBox">
	<form id="searchform" action="http://s.feng.com/search.php" method="get" >
	<input id="g_keyword" name="srchtxt" type="text" value="搜索其实很简单^ ^">
	<i></i>
	<input type="hidden" name="srchmod" value="all">
	</form>
</div>
<script type="text/javascript">
$("#g_keyword").autocomplete('http://s.feng.com/api/autocomplateAjax.php',{
		max: 12,
		minChars: 1, 
		width: 306,
		scrollHeight: 300,
		matchContains: true,
		autoFill: false,
		dataType: 'jsonp', 
		extraParams: {
			searchtype: 'thread' 		//应用类型
		},			 
		parse: function(rdata){	
			var rows = [];
			for(var i=0; i<rdata.length; i++){
				rows[i] = { 
					data:rdata, 
					value:rdata[i], 
					result:rdata[i]
				}; 
			}
			return rows;	
		}
	}).result(function(event, data, formatted) {
		$("#searchform").submit();
	});
</script>                   
    </div>
	<div class="top_nav wrap">
		<ul style="margin-top:10px">
			<li class=" first"><a href="" ><span>全部</span></a></li>
			<li class=><a href="<?php echo U('News/index');?>" ><span>新闻</span></a></li>
			<li><a href="<?php echo U('Bbs/index');?>" ><span>论坛</span></a></li>			
			<li><a href="<?php echo U('Viewpoint/index');?>" ><span>观点</span></a></li>
			<li><a href="<?php echo U('Newslist/index');?>" ><span>专题</span></a></li>
			<li><a href="" ><span>新手</span></a></li>
			<li><a href="" ><span>评测</span></a></li>
			<li><a href="http://www.weand.com/"></i>Android</a></li>
			<li class="last"><a href="http://www.wper.com/"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Windows Phone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
		</ul>
	</div>
</div>
<div class="main wrap clearfix">
<!--hot_spots-->
	<div class="news_left">
		<div class="hot_spots">        
			<div id="new_spots" class="clearfix">
				<div class="wrap">
					<div class="newsbanner">
						<ul class="sliderL newstargetO">
							<li>
								<div class="banner_title" style="visibility: visible;">
									<h1>watchOS 3体验评价：来自用户的核心之变</a></h1>
									<p>watchOS 3 在不断变化更新，适应用户使用设备的方式——让用户决定什么是好的，让用户来决定个人智能穿戴设备该如何发展。</p>
								</div>
								<a class="a-img" href="http://www.feng.com/iPhone/news/2016-09-23/WatchOS-3-experience-evaluation-the-change-from-the-core-of-the-user_657781.shtml" target="_blank" style="background-image:url(/web3/Public/images/img201609231555470.png);">
								<img src="/web3/Public/picture/place.png" longdesc="/web3/Public/images/img201609231555470.png" width="654" alt="watchOS 3体验评价：来自用户的核心之变">
								</a>
							</li>
							<li>
								<div class="banner_title">
									<h1>威锋网每周应用推荐 - 2016年第三十七期</a></h1>
									<p>如果你是一位体育竞技爱好者，本周热门APP《NBA 2K17》再合适不过，也许你喜欢边看电影边互动，来自T社的《Batman（蝙蝠侠:故事版）》是个不错的选择。</p>
								</div>
								<a class="a-img" href="http://game.feng.com/gamenew/topicread/2016-09-25/high_frequency_on_reserve_in_advance_as_recommended_in_the_weekly_special_purchases_for_the_spring_festival_application_204.shtml" target="_blank" style="background-image:url(/web3/Public/images/img201609251739180.jpg);">
								<img src="/web3/Public/picture/place.png" longdesc="/web3/Public/images/img201609251739180.jpg" width="654" alt="威锋网每周应用推荐 - 2016年第三十七期">
								</a>
							</li>
							<li>
								<div class="banner_title">
									<h1>跟随保罗乔治脚步 一起来细数本周新作看点</a></h1>
									<p>本周上架的新游质量十分之高，相信在这些精品游戏的加持之下，“床的封印”也会增添几分威力。</p>
								</div>
								<a class="a-img" href="http://game.feng.com/gamenew/infoDetail/2016-09-23/follow_paul_george_steps_together_around_this_week_new_aspect_97189.shtml" target="_blank" style="background-image:url(/web3/Public/images/img201609231243140.jpg);">
								<img src="/web3/Public/picture/place.png" longdesc="/web3/Public/images/img201609231243140.jpg" width="654" alt="跟随保罗乔治脚步 一起来细数本周新作看点">
								</a>
							</li>
							<li>
								<div class="banner_title">
									<h1>你升级 iOS 10 系统了吗？来看看它的新特性</a></h1>
									<p>对于这个号称苹果 iOS 史上最大的一次革新，你们有什么想说的吗？</p>
								</div>
								<a class="a-img" href="http://www.feng.com/Topic/iOS10_TOP30.shtml" target="_blank" style="background-image:url(/web3/Public/images/img201609211146420.jpg);">
								<img src="/web3/Public/picture/place.png" longdesc="/web3/Public/images/img201609211146420.jpg" width="654" alt="你升级 iOS 10 系统了吗？来看看它的新特性">
								</a>
							</li>
							<li>
								<div class="banner_title">
									<h1>A 系为首！苹果顶尖的“芯片帝国”已初现雏形</a></h1>
									<p>今天苹果的成就已不仅限于 A 系芯片，更像是一个令行业敬畏强大的芯片厂商。</p>
								</div>
								<a class="a-img" href="http://www.feng.com/Story/Headed-by-A-department-_657466.shtml" target="_blank" style="background-image:url(/web3/Public/images/img201609202326340.png);">
								<img src="/web3/Public/picture/place.png" longdesc="/web3/Public/images/img201609202326340.png" width="654" alt="A 系为首！苹果顶尖的“芯片帝国”已初现雏形">
								</a>
							</li>
						</ul>
					<div class="controller">
						<div class="triggers">
							<a href="javascript:;" class="current">0</a>
							<a href="javascript:;">1</a>
							<a href="javascript:;">2</a>
							<a href="javascript:;">3</a>
							<a href="javascript:;">4</a>
						</div>
						<a href="javascript:;" class="prev"><span>&#171;</span></a>
                        <a href="javascript:;" class="next"><span>&#187;</span></a>
                    </div>
					</div>
				</div>
			</div>
			<div class="hot_news" id="data_list">
				<ul>
					<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$n): $mod = ($i % 2 );++$i;?><li>
						<div class="news_content newsOne">						
							<h1 class="news_title"><a href="" target="_blank">
							<strong><?php echo ($n["newstitle"]); ?></strong></a> </h1>
							<div class="abs"><strong><?php echo ($n["simcontent"]); ?></strong></div>
							<div class="news_users">
								<span class="time dujia"><strong><?php echo ($n["addtime"]); ?></strong></span>
							</div>
						</div>
						

						<div class="newsOne_img">
							<a href="http://www.feng.com/iPhone/news/2016-09-25/Speakers-Mexico-s-first-apple-store-grand-opening_657918.shtml" target="_blank"><img src="/web3/Public/picture/33784887img201609252203540_214__142.jpg" height="142" width="214" alt="开疆拓土：墨西哥首家苹果零售店盛大开业"/></a>
						</div>
						
					</li><?php endforeach; endif; else: echo "" ;endif; ?>
				
		
<li><div class="ad_650 clearfix"></div></li>       
				</ul>
				<div class="loading" id="loading" style="display:none"><i></i>加载更多</div>
				<div class="list_pager"><div id="new_page_list" class="new_page_list"></div></div>
			</div>
		</div>
	</div>
	<div class="news_right">	               
<div class="hotTopic_news">	
	<h2 class="title">热门<em>话题</em></h2>
        <a href="http://www.feng.com/Story/If-this-is-the-fifth-generation-Apple-TV-it-must-have-been-like-this_657859.shtml" class="img">
        <img src="/web3/Public/picture/b7ed15e9img201609241230390_305__219.png" alt="如果今年有第五代Apple TV 它一定是这样的" />
		<div class="border_arrow"></div>
        </a>
        <ul>
            <li>
                <a href="http://www.feng.com/Story/If-this-is-the-fifth-generation-Apple-TV-it-must-have-been-like-this_657859.shtml"><div class="nper_num">
                    <span class="num">646</span>
                    <p>期</p>
                </div>
                <div class='nper_title'>
                    <h3>如果今年有第五代Apple TV 它一定是这样的</h3>
                    <p class="content">2015 年发布的第四代 Apple TV 可以说带来了这个产品线自诞生以来最大的变革。那么，它今年还有没有机...</p>
                </div></a>
            </li>
            <li>
			<a href="http://www.feng.com/Story/The-iPhone-7-poking-fun-at-nine-Feng-met-several-friends_657693.shtml">
			<div class="nper_num">
            <span class="num">645</span>
            <p>期</p>
        </div>
        <div class='nper_title'>
            <h3>iPhone 7系九大被吐槽问题 锋友们碰到几个?</h3>
            <p class="content">每一年新一代 iPhone 的发布，随着新机发售，总会有诸多果粉发现存在诸多问题。一些的确是官方承认的缺...</p>
        </div></a>
    </li>
	<li>
        <a href="http://www.feng.com/Story/And-McLaren-combination-seems-to-be-great-But-apple-doesn-t-need-it_657679.shtml"><div class="nper_num">
            <span class="num">644</span>
            <p>期</p>
        </div>
        <div class='nper_title'>
            <h3>和迈凯伦强强联合好像很棒 但苹果不需要它</h3>
            <p class="content">苹果加上迈凯伦会发生怎么样的化学反应？人们的遐想很快就遭到了来自现实的迎头一击。我们希望看到最棒...</p>
        </div></a>
    </li>
</ul>             
    </div>
 	</div>
</div>
<!--返回顶部-->
<a href="javascript:;" class="goTopBtn" title="返回顶部">返回顶部</a>
<!--footer-->
 

<script src='js/c.php' language='JavaScript'></script>
<script type="text/javascript">
(function(){
try{
	var bp = document.createElement('script');
	bp.src = '//push.zhanzhang.baidu.com/push.js';
	var s = document.getElementsByTagName("script")[0];
	s.parentNode.insertBefore(bp, s);
	jQuery(function($){
		//顶部导航
		$('#globar_topbar_links').scrollNav();
		//返回顶部
		$('.goTopBtn').goBackTop();
	    //搜索框效果
	    $('.searchBox').searchAnimate();
	});
}catch(e){}
})();
</script></div>

<script type="text/javascript">
jQuery(function($){
  jQuery(function($){
      //homeBanner
      $('.homebanner').mouseover(function() {
          $('.sliderL_nav.prev').stop().animate({'left':0,'filter':'alpha(opacity=100)', 'opacity': 1},400);
          $('.sliderL_nav.next').stop().animate({'right':0,'filter':'alpha(opacity=100)', 'opacity': 1},400);
      }).mouseout(function() {
          $('.sliderL_nav.prev').stop().animate({'left':-35,'filter':'alpha(opacity=0)', 'opacity': 0},400);
          $('.sliderL_nav.next').stop().animate({'right':-35,'filter':'alpha(opacity=0)', 'opacity': 0},400);
      });;
      //Banner
       $(".targetO").responsiveSlides({
          auto: true,
          pager: true,
          nav: true,
          speed: 500,
          maxwidth: 654
      });
  });
});
jQuery(function($){
	$.listPage({
		createHtml:"news",
		ajaxUrl:"/publish/content.php?id=616370&Custom1=2",
		pageCount:5211	}).scroll();
});

//Banner
jQuery(function($){
    var baseUrl ="http://www.feng.com/images_v4/js/";
    ds.loadScript(baseUrl + 'jquery.FadeList.js', function(){
        var slider = ds.buildSlider('#new_spots', {
            delay: 3000,
            imgLoadFlag: [true],
            imgLoadHandler: function(inx){
                this.items.eq(inx).find('.banner_title').css({'visibility':'visible'});
                var panel = this.items.eq(inx).find('.a-img'), img = panel.find('img'), url = img.attr('longDesc');
                panel.css('backgroundImage', 'url('+ url +')');
                img.attr('src', url);
            }
        }, window.FadeList);
    });
});

jQuery(function($){
   var shell = $('#classifyNav');
   var targetW = shell.find('.classifyBox').width();
   var targetUl = shell.find('.classifyBox ul');
   var len = shell.find('.classifyBox ul li').length;
   var pageNum = Math.ceil(len/9);
   var nextBtn = shell.find('.next-btn');
   var prevBtn = shell.find('.prev-btn');
   var index = 0;
   if(len<10){
        nextBtn.hide();
        prevBtn.hide();
   }else{
        nextBtn.show();
        prevBtn.hide();
   }
   nextBtn.click(function(event) {
       event.preventDefault();
        index += 1;
       if(index == pageNum){  
        index = pageNum; 
        shell.find('.classifyBox').addClass('move-right');    
        prevBtn.show();
        nextBtn.hide()
        targetUl.stop(true, false).animate({ "left": 0}, 400);
       }else{
        targetUl.stop(true, false).animate({ "left": -index * targetW }, 400);
        nextBtn.show();
        prevBtn.hide(); 
       }
        console.log(index);       
   });
   prevBtn.click(function(event) {
       event.preventDefault();
       index -= 1;
        if (index == -1){
         index = 0;
         shell.find('.classifyBox').removeClass('move-right');
        nextBtn.show();
        prevBtn.hide();
         targetUl.stop(true, false).animate({ "left": 0}, 400);
        }else{
         targetUl.stop(true, false).animate({ "left": -index * targetW }, 400);
        prevBtn.show();
        nextBtn.hide()
        }
         console.log(index);
   });
});
</script>


<!-- 继承结束 -->
<div class="footer_wrap clearfix">
	<!--友情广告链接-->
	<div class="friendsLinks clearfix">
		<div class="wrap friendsWrap">			

			<div class="friendsA">
				<a href="#" target="_blank" title="hao123导航">hao123导航</a>
				<a href="#" target="_blank" title="网易科技">网易科技</a>
				<a href="#" target="_blank" title="和讯科技">和讯科技</a>
				<a href="#" target="_blank" title="TechWeb">TechWeb</a>
				<a href="#" target="_blank" title="TomPDA智能手机网">TomPDA智能手机网</a>
				<a href="#" target="_blank" title="IT之家">IT之家</a>
				<a href="#" target="_blank" title="dospy智能手机">dospy智能手机</a>
				<a href="#" target="_blank" title="当乐手机游戏">当乐手机游戏</a>
				<a href="#" target="_blank" title="亿邦动力网">亿邦动力网</a>
				<a href="#" target="_blank" title="威锋商城">威锋商城</a>
				<a href="#" target="_blank" title="PChome下载">PChome下载</a>
				<a href="#" target="_blank" title="杭州房产网">杭州房产网</a>
				<a href="#" target="_blank" title="站长之家">站长之家</a>
				<a href="#" target="_blank" title="科技讯">科技讯</a>
				<a href="#" target="_blank" title="安卓网">安卓网</a>
				<a href="#" target="_blank" title="宝软网">宝软网</a>
				<a href="#" target="_blank" title="手机QQ浏览器">手机QQ浏览器</a>
				<a href="#" target="_blank" title="老虎游戏">老虎游戏</a>
				<a href="#" target="_blank" title="搜狐IT">搜狐IT</a>
				<a href="#" target="_blank" title="WP8论坛">WP8论坛</a>
				<a href="#" target="_blank" title="淘米视频">淘米视频</a>
				<a href="#" target="_blank" title="人人游戏">人人游戏</a>
				<a href="#" target="_blank" title="安卓网">安卓网</a>
				<a href="#" target="_blank" title="电子发烧友">电子发烧友</a>
				<a href="#" target="_blank" title="MAXPDA智能手机论坛">MAXPDA智能手机论坛</a>
				<a href="#" target="_blank" title="114啦网址导航">114啦网址导航</a>
				<a href="#" target="_blank" title="雷锋网">雷锋网</a>
				<a href="#" target="_blank" title="cnBeta">cnBeta</a>
				<a href="#" target="_blank" title="UC浏览器">UC浏览器</a>
				<a href="#" target="_blank" title="91手机">91手机</a>
				<a href="#" target="_blank" title="安锋网">安锋网</a>
				<a href="#" target="_blank" title="斑马手机游戏">斑马手机游戏</a>
				<a href="#" target="_blank" title="彼岸桌面">彼岸桌面</a>
				<a href="#" target="_blank" title="iPhone6 论坛">iPhone6 论坛</a>
				<a href="#" target="_blank" title="威锋游戏">威锋游戏</a>
				<a href="#" target="_blank" title="3DM单机游戏">3DM单机游戏</a>
				<a href="#" target="_blank" title="深圳房产">深圳房产</a>
				<a href="#" target="_blank" title="腾讯手机管家">腾讯手机管家</a>
				<a href="#" target="_blank" title="老虎游戏">老虎游戏</a>
				<a href="#" target="_blank" title="19楼">19楼</a>
				<a href="#" target="_blank" title="智能电视网">智能电视网</a>
				<a href="#" target="_blank" title="龙诀">龙诀</a>
				<a href="#="_blank" title="原创精品源">原创精品源</a>
				<a href="#" target="_blank" title="爱思助手">爱思助手</a>
				<a href="#" target="_blank" title="亿智蘑菇">亿智蘑菇</a>
				<a href="#" target="_blank" title="兔兔助手">兔兔助手</a>
				<a href="#" target="_blank" title="砍柴网">砍柴网</a>
				<a href="#" target="_blank" title="快科技">快科技</a>
				<a href="#" target="_blank" title="驱动中国">驱动中国</a>
				<a href="#" target="_blank" title="品途网">品途网</a>
				<a href="#" target="_blank" title="ZEALER中国">ZEALER中国</a>
				<a href="#" target="_blank" title="爱搞机">爱搞机</a>
				<a href="#" target="_blank" title="申请友情链接"><b>申请友情链接</b></a>
			</div>

		<!--广告位1000*90-->
			<noscript><a href='#' target='_blank'><img src='http://yes1.feng.com/view.php?what=zone:165&amp;n=a3e8502c' border='0' alt=''></a></noscript>
		</div>
		<!--广告位1000*90 end-->
		</div>
	</div>
<!--友情链接 end-->
 
<div class="wrap footer">
	<div class="links">
		<ul>
			<li><a href="#" target="_blank">触屏版</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">关于我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">联系我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">商务合作</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">大事记</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">法律条款</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">About Us</a></li>
		</ul>
	</div>
	<div class="copyright">
		<p>Copyright 2007-2016 © Joyslink Inc. All rights reserved 保留所有权利</p>
	</div>
	
	<div class="safe_info">
		<div class="safe_links">
			<a href="#" class="safe_1" target="_blank" rel="nofollow"><span>深圳网络警察报警平台</span></a>
			<a href="#" class="safe_2" target="_blank"><span>经营性网站备案信息</span></a>
			<a href="#" class="safe_3" target="_blank"><span>公安信息安全网络监察</span></a>
		</div>
		
		<p>增值电信业务经营许可证：<a href="#" target="_blank">粤B2-20130239</a>      <a href="#">粤网文[2014]0683-283号</a>   Powered by Discuz! </p>
	</div>
</div>


</div>
<!-- 仅首页，用于壁纸广告  --> 
<!-- <script language='JavaScript' type='text/javascript' src='/web3/Public/js/js_index/x.js'></script>
<script language='JavaScript' type='text/javascript'>
   if (!document.phpAds_used) document.phpAds_used = ',';
   phpAds_random = new String (Math.random()); phpAds_random = phpAds_random.substring(2,11);
   
   document.write ("<" + "script language='JavaScript' type='text/javascript' src='");
   document.write ("http://yes1.feng.com/js.php?n=" + phpAds_random);
   document.write ("&amp;what=zone:189");
   document.write ("&amp;exclude=" + document.phpAds_used);
   if (document.referrer)
      document.write ("&amp;referer=" + escape(document.referrer));
   document.write ("'><" + "/script>");
</script> -->
<noscript><a href='#' target='_blank'><img src='http://yes1.feng.com/view.php?what=zone:189&amp;n=afbf8627' border='0' alt=''></a></noscript>

</body>
</html>