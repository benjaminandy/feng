<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="/favicon.ico" type="/web3/Public/image/x-icon">
<!-- 给网站添加一个icon图标 -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="apple-itunes-app" content="app-id=981437434" />

<link rel="alternate" type="application/rss+xml" title="Feng新闻RSS" href="">

<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/common.css" />
<script type="text/javascript" src="/web3/Public/js/js_index/jquery-1.7.min.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_index/common.js"></script>

</head>
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/style.css" />
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/feng_index.css" />
<script type="text/javascript" src="/web3/Public/js/js_index/index.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_index/jquery.marquee.min.js"></script>
<body>
<!-- Global Topbar -->
<div class="global_topbar_wrap">
	<div id="global_topbar" class="global_topbar">
		<div class="wrap inner">
			<div class="services" id="global_topbar_services">
				<ul>
				<li class="current"><a href="#"><i class="weiphone"></i>Feng综合网</a></li>				
				<li><a href=""><i class="wegame"></i>F新闻</a></li>						
				<li><a href=""><i class="fengbuy"></i>F论坛</a></li>
					
				<li><a href=""><i class="fengbuy"></i>建设中</a></li>
				
				<li><a href=""><i class="money"></i>扫码点击</a></li>								
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>
			<div class="links" id="globar_topbar_links">
				<ul>
					<li><a href="" target="_blank">首页</a></li>
					<li><a href="" target="_blank">Feng新闻</a></li>
					<li><a href="" target="_blank">Feng论坛</a></li>
				</ul>
				<div class="focus"><em></em></div>
			</div>
			
			<div class="client" id="global_topbar_client">
				<ul>
					<li class="current"><a href="#"><i></i>手机APP</a></li>					
					<div class="codeBox">
						<h3>扫一扫下载APP</h3>
						<b><img src="/web3/Public/picture/fengcode.jpg" width="94" height="94" alt="Feng二维码" /></b>
					</div>
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>

<!-- 弹框开始 -->
			<div class="user_menu" id="global_user_menu">
				<ul>
					<?php if($_SESSION['username']!= null): ?><li class="face">
						<img src="/web3/Public/picture/<?php echo ($_SESSION['username']['uface']); ?>" height="30" width="30" style="margin-top:-5px;">
						</li>
						<li class="face">
							<b style="font-size:15px"><?php echo ($_SESSION['username']['uname']); ?></b>
						</li>
						<li>
							<a class="ref" href="<?php echo U('Action/stop?model=index&controller=index');?>" title="注销">
								<i class="logout"></i>
								<span class="label">注销</span>
							</a>
						</li>
				        <?php else: ?>
							<li><a data-toggle="modal" href="#login-modal"><span>登录</span></a></li>
							<li><a data-toggle="modal" href="#signup-modal"><span>立即注册</span></a></li><?php endif; ?>
					<!-- <li><a data-toggle="modal" href="#forgetform"><span>找回密码</span></a></li> -->
				</ul>				
				<ul style="display:none;">
					<li class="face">
						<img src="/web3/Public/picture/<?php echo ($_SESSION['username']['uface']); ?>" height="30" width="30" style="margin-top:-5px;">
					</li>
					<li class="face">
						<b style="font-size:15px"><?php echo ($_SESSION['username']['uname']); ?></b>
					</li>
					<li>
						<a class="ref" href="<?php echo U('Action/stop?model=index&controller=index');?>" title="注销">
							<i class="logout"></i>
							<span class="label">注销</span>
						</a>
					</li>
				</ul>
			
			</div>
			<script>
			    function select(){
			        var username = document.form1.uname.value;
			        var userpass = document.form1.password.value;
			        var temp;
			        $.ajax({
				        type:'POST',
				        url:"<?php echo U('Action/select');?>",
				        async:false,
          				data:{name:username,pass:userpass},
          				success:function(data){
          					console.log(data);
          					if(data == 1){
					              alert('用户名错误');
					              temp = 1;
					              
					            } else if(data == 2){
					              alert('密码错误');
					              temp = 2;
					          }
          				}
			    	})

			    	if(temp == 1 || temp == 2 ){
					          return false;
					        }
			    }
			</script>
<!-- 弹框结束 -->

		<!-- <script type="text/javascript" src="/web3/Public/js/js_index/jquery.autocomplete.min.ajax.js"></script>
		<div class="searchBtn">
			<span id="quickSearchBtn"></span>
			<div class="headerSearch" id="headerSearch">
				<div class="searchBoxtop">
					<form id="searchform_top" action="http://s.feng.com/search.php" method="get" >
						<input id="top_keyword" name="srchtxt" type="text" value="搜索其实很简单^ ^">
						<input type="hidden" name="srchmod" value="all">
					</form>	
				</div>
			</div>
		</div>	 -->
		</div>
	</div>
</div>

<!-- 弹框start -->
<div class="modal" id="login-modal">
	<a class="close" data-dismiss="modal">×</a>
	<h1>登录</h1>
	
	<!-- 登录开始 -->
	<div class="contact" >
		<form action="<?php echo U('Index/index');?>" name="form1" onsubmit="return select()" method="POST">
			<ul>
				<li>
					<label>用户名：</label>
					<input type="text" name="uname" placeholder="请输入用户名" id='yourname' onblur="checkname()" value="" required/><span class="tips" id="divname2">长度1~12个字符</span>
				</li>

				<li>
					<label>密码：</label>
					<input type="password" name="password" placeholder="请输入您的密码" onblur="checkpsd5()" required/><span class="tips" id="divpassword0">密码必须由字母和数字组成</span>
				</li>
			</ul>
			<b class="btn"><input type="submit" value="登录"/>
			<input type="reset" value="取消"/></b>
		</form>
	</div>
</div>

<div class="modal" id="signup-modal">
	<a class="close" data-dismiss="modal">×</a>
	<h1>注册</h1>
	<div class="contact" >
	
	<!-- 注册开始 -->
	
	<form name="form2" action="<?php echo U('Action/add');?>" method="post">
		<ul>
			<li>
				<label>用户名：</label>
				<input type="text" name="uname" placeholder="请输入用户名"  onblur="checkna()" value="" /><span class="tips" id="divname">长度3~12个含数字和字母字符</span>
			</li>

			<li>
				<label>性别：</label>
				<input type="radio" name="sex" id="1" value="1">男&nbsp;&nbsp;
				<input type="radio" name="sex" id="2" value="2">女&nbsp;&nbsp;
				<input type="radio" name="sex" id="0" value="0">保密
			</li>

			<li>
				<label>密码：</label>
				<input type="password" name="password" placeholder="请输入您的密码" onblur="checkpsd1()" value="" required/><span class="tips" id="divpassword1">密码必须由字母和数字组成</span>
			</li>

			<li>
				<label>确认密码：</label>
				<input type="password" name="yourpass2" placeholder="请再次输入您的密码" onBlur="checkpsd2()" value="" required/><span class="tips" id="divpassword2">两次密码需要相同</span>
			</li>

			<li>
				<label>电子邮箱：</label>
				<input type="text" name="email" placeholder="请输入您的邮箱" onBlur="checkmail()" required/><span class="tips" id="divmail">请输入您的邮箱地址</span>
			</li>

			<li>
				<label>手机号：</label>
				<input type="text" name="yourphone" placeholder="请输入您的手机联系方式" onBlur="checkphone()" required/><span class="tips" id="divphone">以便帐号丢失后找回</span>
			</li>

			<li>
		      	<div id="distpicker5">
		      	<label>请选择所在地:</label>
			        <div class="form-group">
			          <select class="form-control" id="province10">
				          <option value="" data-code="">—— 省 ——</option>
				          <option value="北京市" data-code="110000">北京市</option>
				          <option value="天津市" data-code="120000">天津市</option>
				          <option value="河北省" data-code="130000">河北省</option>
				          <option value="山西省" data-code="140000">山西省</option>
				          <option value="内蒙古自治区" data-code="150000">内蒙古自治区</option>
				          <option value="辽宁省" data-code="210000">辽宁省</option>
				          <option value="吉林省" data-code="220000">吉林省</option>
				          <option value="黑龙江省" data-code="230000">黑龙江省</option>
				          <option value="上海市" data-code="310000">上海市</option>
				          <option value="江苏省" data-code="320000">江苏省</option>
				          <option value="浙江省" data-code="330000">浙江省</option>
				          <option value="安徽省" data-code="340000">安徽省</option>
				          <option value="福建省" data-code="350000">福建省</option>
				          <option value="江西省" data-code="360000">江西省</option>
				          <option value="山东省" data-code="370000">山东省</option>
				          <option value="河南省" data-code="410000">河南省</option>
				          <option value="湖北省" data-code="420000">湖北省</option>
				          <option value="湖南省" data-code="430000">湖南省</option>
				          <option value="广东省" data-code="440000">广东省</option>
				          <option value="广西壮族自治区" data-code="450000">广西壮族自治区</option>
				          <option value="海南省" data-code="460000">海南省</option>
				          <option value="重庆市" data-code="500000">重庆市</option>
				          <option value="四川省" data-code="510000">四川省</option>
				          <option value="贵州省" data-code="520000">贵州省</option>
				          <option value="云南省" data-code="530000">云南省</option>
				          <option value="西藏自治区" data-code="540000">西藏自治区</option>
				          <option value="陕西省" data-code="610000">陕西省</option>
				          <option value="甘肃省" data-code="620000">甘肃省</option>
				          <option value="青海省" data-code="630000">青海省</option>
				          <option value="宁夏回族自治区" data-code="640000">宁夏回族自治区</option>
				          <option value="新疆维吾尔自治区" data-code="650000">新疆维吾尔自治区</option>
				          <option value="台湾省" data-code="710000">台湾省</option>
				          <option value="香港特别行政区" data-code="810000">香港特别行政区</option>
				          <option value="澳门特别行政区" data-code="820000">澳门特别行政区</option>
			          </select>				    
			          <select class="form-control" id="city10">
			          	<option value="" data-code="">—— 市 ——</option>
			          </select>
			          <select class="form-control" id="district10">
			          	<option value="" data-code="">—— 区 ——</option>
			          </select>
			          <div>
			          <br/>
			          <label>请输入具体地址:</label>
			          <input type="text" name="address" placeholder="请输入具体地址" onblur="checkaddress()" required/><span class="tips" id="divaddress">长度1~30个字符</span>
			          </div>
			    </div>
		    </li>
		</ul>
			<b class="btn"><input type="submit" value="提交"/>
			<input type="reset" value="取消"/></b>
	</form>
	</div>
</div>
<script type="text/javascript">
//验证登录用户名
 	function checkname(){
		var na=document.form1.uname.value;
		// console.log(na);
	  	if( na.length <1 || na.length >12)  
  		{  	
  			divname.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
  		     
  		}else{  
  		    divname.innerHTML='<font class="tips_true">输入正确</font>';
  		   
  		}  
  	}

//验证登录密码 
	function checkpsd(){    
		var psd1=document.form1.password.value;  
		var flagZM=false ;
		var flagSZ=false ; 
		var flagQT=false ;
		console.log(psd1);
		if(psd1.length<6 || psd1.length>12){  
			alert(121); 
			divpassword1.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
		}else{   
			for(i=0;i < psd1.length;i++)   
				{    
					if((psd1.charAt(i) >= 'A' && psd1.charAt(i)<='Z') || (psd1.charAt(i)>='a' && psd1.charAt(i)<='z')) 
					{   
						flagZM=true;
					}
					else if(psd1.charAt(i)>='0' && psd1.charAt(i)<='9')    
					{ 
						flagSZ=true;
					}else    
					{ 
						flagQT=true;
					}   
				}   
				if(!flagZM||!flagSZ||flagQT){
				divpassword1.innerHTML='<font class="tips_false">密码必须是字母数字的组合</font>'; 
				 
				}else{
					
				divpassword1.innerHTML='<font class="tips_true">输入正确</font>';
				 
				}  
			 
			}	
	}

//验证注册用户名
 	function checkna(){
		var na=document.form2.uname.value;
		// console.log(na);
	  	if( na.length <1 || na.length >12)  
  		{  	
  			divname.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
  		     
  		}else{  
  		    divname.innerHTML='<font class="tips_true">输入正确</font>';
  		   
  		}  
  	}

//验证注册密码 
	function checkpsd1(){    
		var psd1=document.form2.password.value;  
		var flagZM=false ;
		var flagSZ=false ; 
		var flagQT=false ;
		console.log(psd1);
		if(psd1.length<3 || psd1.length>12){   
			divpassword1.innerHTML='<font class="tips_false">长度必须3~12个字符</font>';
		}else{   
			for(i=0;i < psd1.length;i++)   
				{    
					if((psd1.charAt(i) >= 'A' && psd1.charAt(i)<='Z') || (psd1.charAt(i)>='a' && psd1.charAt(i)<='z')) 
					{   
						flagZM=true;
					}
					else if(psd1.charAt(i)>='0' && psd1.charAt(i)<='9')    
					{ 
						flagSZ=true;
					}else    
					{ 
						flagQT=true;
					}   
				}   
				if(!flagZM||!flagSZ||flagQT){
				divpassword1.innerHTML='<font class="tips_false">密码必须是字母数字的组合</font>'; 
				 
				}else{
					
				divpassword1.innerHTML='<font class="tips_true">输入正确</font>';
				 
				}  
			 
			}	
	}

//验证注册确认密码 
	function checkpsd2(){ 

		if(document.form2.yourpass2.value!=document.form2.password.value || document.form2.yourpass2.value=='') { 
		     divpassword2.innerHTML='<font class="tips_false">您两次输入的密码不一样</font>';
		} else { 
		     divpassword2.innerHTML='<font class="tips_true">输入正确</font>';
		}
	}

//验证注册邮箱		
	function checkmail(){
		var apos=document.form2.email.value.indexOf("@");
		var dotpos=document.form2.email.value.lastIndexOf(".");
		if (apos<1||dotpos-apos<2) 
		  {
		  	divmail.innerHTML='<font class="tips_false">输入错误</font>' ;
		  }
		else {
			divmail.innerHTML='<font class="tips_true">输入正确</font>' ;
		}
	}

//验证注册地址		
	function checkaddress(){
		var address=document.form2.address.value;
	  	if( address.length <1 || address.length >30){ 
	  		divaddress.innerHTML='<font class="tips_false">长度必须1~30个字符</font>';  		     
  		}else{  
  		    divaddress.innerHTML='<font class="tips_true">输入正确</font>';  		   
  		}  
  	}
</script>
<script src="/web3/Public/js/js_index_headclick/jquery.min.js"></script>
<script src="/web3/Public/js/js_index_headclick/distpicker.data.js"></script>
<script src="/web3/Public/js/js_index_headclick/distpicker.js"></script>
<script src="/web3/Public/js/js_index_headclick/main.js"></script>
<div class="modal" id="forgetform">
	<a class="close" data-dismiss="modal">×</a>
	<h1>忘记密码</h1>
	<form class="forgot-form" method="post" action="">
		<input name="email" value="" placeholder="注册邮箱：">
		<div class="clearfix"></div>
		<input type="submit" name="type" class="forgot button-blue" value="发送重设密码邮件">
	</form>
</div>
<script type="text/javascript" src="/web3/Public/js/js_index_headclick/modal.js"></script>
<!-- 弹框end -->


<!-- 继承开始 -->


<title>Feng论坛 - Feng网</title>
 
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_Bbs_list/style_4_common.css">
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_Bbs_list/style_4_forum_forumdisplay.css">    
<script src="/web3/Public/js/js_Bbs_list/jquery.min.js" type="text/javascript"></script>
<script src="/web3/Public/js/js_Bbs_list/common.js" type="text/javascript"></script>      
<link rel="apple-touch-icon" href="/web3/Public/images/images_Bbs_list/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76" href="/web3/Public/images/images_Bbs_list/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120" href="/web3/Public/images/images_Bbs_list/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152" href="/web3/Public/images/images_Bbs_list/touch-icon-ipad-retina.png">
<link rel="apple-touch-icon-precomposed" href="/web3/Public/images/images_Bbs_list/touch-icon-ipad-retina.png">
<script src="/web3/Public/js/js_Bbs/forum.js" type="text/javascript"></script>

<!-- 备份格式 -->
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_Bbs/style_4_common.css" /><link rel="stylesheet" type="text/css" href="/web3/Public/css/css_Bbs/style_4_forum_index.css">    
<script src="/web3/Public/js/js_Bbs/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">var STYLEID = '4', STATIC_DIR = '/bbs_v4/', STATICURL = 'static/', IMGDIR = 'static/image/common', VERHASH = 'Ymk', charset = 'utf-8', discuz_uid = '0', cookiepre = 'xIka_2132_', cookiedomain = '', cookiepath = '/', showusercard = '0', attackevasive = '0', disallowfloat = '', creditnotice = '', defaultstyle = '', REPORTURL = 'aHR0cDovL2Jicy5mZW5nLmNvbS9mb3J1bS5waHA/Z2lkPTQ4Ng==', SITEURL = 'http://bbs.feng.com/', JSPATH = 'static/js/', DYNAMICURL = '';
    jQuery.noConflict().ajaxSetup({ cache: true });
</script>

<!-- Quick Services -->
<div class="wrap quick_services">
	<ul class="user_links">
		<li><a href="#"><i class="subject"><b></b></i>主题</a></li>
		<li><a href="#"><i class="reply"><b></b></i>回复</a></li>
		<li><a href="#"><i class="fav"><b></b></i>收藏</a></li>
		<li><a href="#"><i class="newscommon"><b></b></i>评论</a></li>
		<li><a href="#"><i class="doing"><b></b></i>动态</a></li>
		<li class="hide"><a href="#"><i class="mark"><b></b></i>书签</a></li>
	</ul>
</div>

<!-- Top News top_news[_mini] -->	
<div id="bbs_top_news" class="wrap top_news top_news">
	
<style type="text/css">
    #bdcs{ 
        width:366px;height:30px;
    }
    .bdcs-container .bdcs-search-form-input{ 
        width:310px;display:inline-block; background:#efefef; border:1px solid #efefef; border-top-color:#fff; border-radius:4px; box-shadow:0 2px 2px rgba(0,0,0,.09); padding:0 26px; height:28px; vertical-align:top;
    }
    .google_sch{ 
        position:relative; height:30px;
    }
    .google_sch .zoom{
        left: 37px;z-index: 5;top: 1px;
    }
    .bdcs-clearfix{ 
        height:30px;
    }
    .bdcs-container{ 
        height:30px;
    }
    .bdcs-container .bdcs-search-form-submit,.bdcs-container .bdcs-hot{
        display: none;
    }
    .bdcs-container .bdcs-search-form-input:focus {
        border-color: #1AA0CA;
    }
</style>

<div id="wp" class="wp">
<div id="pt" class="bbs_info crumbs">
<div class="crumbs_inner"><a href="./" class="home" title="首页">Feng论坛</a><em>&raquo;</em><a href="forum.php">游戏中心</a><em>&rsaquo;</em> 帖子区</div>
</div>

	<div class="section_panel">	
		<!-- 天气预报端口开始	 -->
		<div class="wefiler_list" style="width:195px ;height:266px;float:right;background-color:white; font-size:8px">				
				
			<p><b>城市:</b>&nbsp; <?php echo ($obj->Weather->city); ?></p><br/>
			<p><b>天气:</b>&nbsp; <?php echo ($obj->Weather->status1); ?>&nbsp; <b>转</b>&nbsp; <?php echo ($obj->Weather->status2); ?></p><br/>
			<p><b>最高温度:</b>&nbsp; <?php echo ($obj->Weather->temperature1); ?><b>℃</b></p><br/>
			<p><b>最低温度:</b>&nbsp; <?php echo ($obj->Weather->temperature2); ?><b>℃</b></p><br/>
			<p><b>建议着装:</b>&nbsp; <?php echo ($obj->Weather->chy_shuoming); ?></p><br/>

		</div>
		<!-- 天气预报端口结束 -->
		
		<div id="top_news_section" class="section news_section focus_section">				
			<div class="clearfix news_list">	
				<ul>						
					<li class="top">
						<div class="item">				
							<a href="#" class="pic" target="_blank" title="大作频上架 提前储备年货尽在每周应用推荐"><img src="/web3/Public/picture/picture_Bbs_list/special_204_980x350.jpg" height="174" width="408" alt="大作频上架 提前储备年货尽在每周应用推荐"></a>					
							<a href="#" target="_blank" title="游戏专题" class="type"><span>游戏专题</span></a>	
							<a href="#" class="tit" target="_blank" title="大作频上架 提前储备年货尽在每周应用推荐">大作频上架 提前储备年货尽在每周应用推荐</a>						
							<p class="desc">一波传奇巨星相继退役，科比、邓肯的离去，让如今的NBA赛场少了一份当年的滋味，当另...</p>
						</div>						
					</li> 						
					<li class="sub_top">				
						<div class="item">				
							<a href="#" class="pic" target="_blank" title="iPhone 7各种防水奇葩测试：表现实属上乘">
								<img src="/web3/Public/picture/picture_Bbs_list/42fb6d9dimg201609281202100_130__84.png" height="84" width="130" alt="iPhone 7各种防水奇葩测试：表现实属上乘" />
							</a>						
							<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>		
							<a href="#" class="tit" target="_blank" title="iPhone 7各种防水奇葩测试：表现实属上乘">[今日头条]iPhone 7各种防水奇葩测试：表现实属上乘</a>				
							<p class="desc">看了那么多的防水测试，感觉iPhone 7的防水表现还是不错的。</p>
						</div>					
					</li> 						
					<li>							
						<div class="item">				
							<a href="#" class="pic" target="_blank" title="开发者的故事"><img src="/web3/Public/picture/picture_Bbs_list/0732c243img201609281030310_130__84.jpg" height="84" width="130" alt="开发者的故事：做冷门游戏 照样上人生巅峰"></a>								
							<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>	
							<a href="#" class="tit" target="_blank" title="开发者的故事：做冷门游戏 照样上人生巅峰">开发者的故事：做冷门游戏 照样上人生巅峰</a>			
							<p class="desc">既然你又要独立，又想获得不错的商业成功，那么最好的策略，就是去成为那个“唯一”。</p>				
						</div>						
					</li> 						
					<li>							
						<div class="item">				
							<a href="#" class="pic" target="_blank" title="苹果要搞事：召集多名越狱大神在总部见面"><img src="/web3/Public/picture/picture_Bbs_list/93f23961img201609290003220_130__84.jpg" height="84" width="130" alt="苹果要搞事：召集多名越狱大神在总部见面"></a>
							<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>		
							<a href="#" class="tit" target="_blank" title="苹果要搞事：召集多名越狱大神在总部见面">苹果要搞事：召集多名越狱大神在总部见面</a>
							<p class="desc">当然这并不是去工作，但是有没有被招安的可能我们就不清楚了。</p>
						</div>						
					</li> 						
					<li>							
						<div class="item">				
							<a href="#" class="pic" target="_blank" title="iCloud 艳照门后续：第二名黑客也被判入狱"><img src="/web3/Public/picture/picture_Bbs_list/b4707dbeimg201609282349300_130__84.jpg" height="84" width="130" alt="iCloud 艳照门后续：第二名黑客也被判入狱" /></a>		
							<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>		
							<a href="#" class="tit" target="_blank" title="iCloud 艳照门后续：第二名黑客也被判入狱">iCloud 艳照门后续：第二名黑客也被判入狱</a>		
							<p class="desc">这起事件也告诉我们，没事不要在云端放一些尺度太大的照片。</p>
						</div>						
					</li> 						
					<li>							
						<div class="item">				
							<a href="#" class="pic" target="_blank" title="呐 假如库克当 F1 车队经理 到底会发生什么"><img src="/web3/Public/picture/picture_Bbs_list/e9a13ae3img201609282325470_130__84.jpg" height="84" width="130" alt="呐 假如库克当 F1 车队经理 到底会发生什么" /></a>	
							<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>								
							<a href="#" class="tit" target="_blank" title="呐 假如库克当 F1 车队经理 到底会发生什么">呐 假如库克当 F1 车队经理 到底会发生什么</a>				
							<p class="desc">候选名单还包括霍纳，布利尔......当然假如有苹果 F1 车队的话，你们会怎么吐槽这个事...</p>			
						</div>						
					</li> 						
					<li>							
						<div class="item">				
							<a href="#" class="pic" target="_blank" title="苹果欲在英国开设新总部：可容纳1400员工"><img src="/web3/Public/picture/picture_Bbs_list/3be9aa10img201609282258130_130__84.jpg" height="84" width="130" alt="苹果欲在英国开设新总部：可容纳1400员工" /></a>						
							<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>		
							<a href="#" class="tit" target="_blank" title="苹果欲在英国开设新总部：可容纳1400员工">苹果欲在英国开设新总部：可容纳1400员工</a>	<p class="desc">像苹果这样的跨国企业，在不同的重要地区开设总部是很有必要的。</p>	
						</div>						
					</li> 						
					<li>							
						<div class="item">				
							<a href="#" class="pic" target="_blank" title="iOS 10 小教程：如何优先下载某个应用程序"><img src="/web3/Public/picture/picture_Bbs_list/5f0f2199img201609282254100_130__84.jpg" height="84" width="130" alt="iOS 10 小教程：如何优先下载某个应用程序" /></a>		
							<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>		
							<a href="#" class="tit" target="_blank" title="iOS 10 小教程：如何优先下载某个应用程序">iOS 10 小教程：如何优先下载某个应用程序</a>	
							<p class="desc">如果你需要尽快使用的应用也处于“正在更新”状态的话，可以选择让它优先下载，并且暂...</p>			
						</div>						
					</li> 						
					<!-- Fix -->						
					<li class="fix"></li>
				</ul>				
						
			</div>				
			
		</div>		
	</div>		
	<div id="top_game_section" class="section game_section hide">				
		<ul>					
			<li class="top">						
				<div class="item">
					<a href="#" class="pic" target="_blank" title="谨以此游纪念逝去的爱情：世界末日"><img src="/web3/Public/picture/picture_Bbs_list/article_97271_640x320.jpg" height="174" width="408" alt="谨以此游纪念逝去的爱情：世界末日"></a>		
					<a href="#" class="icon" target="_blank" title="谨以此游纪念逝去的爱情：世界末日"><i class="mask"></i><img src="/web3/Public/picture/picture_Bbs_list/mzl.cover.175x175-75.jpg" height="110" width="110" alt="谨以此游纪念逝去的爱情：世界末日"></a>	
					<a href="#" class="tit" target="_blank" title="谨以此游纪念逝去的爱情：世界末日">谨以此游纪念逝去的爱情：世界末日</a>
					<p class="desc">这款作品采用了简单又不失唯美的油画风格，草草几笔就能够将场景描绘到位，不愧是场景...</p>	
				</div>					
			</li>						
			<li>							
				<div class="item">
					<a href="#" class="icon" target="_blank" title="一个穿越的话题也能玩出各种套路：《秘密组织4: 超越时空》"><i class="mask"></i><img src="/web3/Public/picture/picture_Bbs_list/mzl.cover.175x175-75_1.jpg" height="62" width="62" alt="一个穿越的话题也能玩出各种套路：《秘密组织4: 超越时空》" /></a>			
					<a href="#" class="tit" target="_blank" title="一个穿越的话题也能玩出各种套路：《秘密组织4: 超越时空》">一个穿越的话题也能玩出各种套路：《秘密组织4: 超越时空》</a>			
					<p class="desc">此前用《乌鸦森林之谜》、《恐怖传奇》等一系列冒险解谜类作品打响名号的游戏开发商 A...</p>		
				</div>						
			</li> 						
			<li>							
				<div class="item">						
					<a href="#" class="icon" target="_blank" title="四象限法工作法 让你从容且优雅：《象限清单》"><i class="mask"></i><img src="/web3/Public/picture/picture_Bbs_list/mzl.cover.175x175-75_2.jpg" height="62" width="62" alt="四象限法工作法 让你从容且优雅：《象限清单》" /></a>					
					<a href="#" class="tit" target="_blank" title="四象限法工作法 让你从容且优雅：《象限清单》">四象限法工作法 让你从容且优雅：《象限清单》</a>								
					<p class="desc">《象限清单》是一款非常简单的免费待办事项 App，简单到 App 全程只有三个用户操作，...</p>
				</div>			
			</li> 						
			<li>							
				<div class="item">
					<a href="#" class="icon" target="_blank" title="烦躁的情绪无处排解？它或许能帮你科学应对：《心潮减压》"><i class="mask"></i><img src="/web3/Public/picture/picture_Bbs_list/mzl.cover.175x175-75_3.jpg" height="62" width="62" alt="烦躁的情绪无处排解？它或许能帮你科学应对：《心潮减压》" /></a>
					<a href="#" class="tit" target="_blank" title="烦躁的情绪无处排解？它或许能帮你科学应对：《心潮减压》">烦躁的情绪无处排解？它或许能帮你科学应对：《心潮减压》</a>			
					<p class="desc">《心潮减压》能够通过检测你的生理数据，找到更有效的方法帮助你快速减压、调节情绪。</p>	
				</div>						
			</li> 						
				<li>							
					<div class="item">
						<a href="#" class="icon" target="_blank" title="跟着众神一起开疆扩土：《奥林匹斯的崛起》"><i class="mask"></i><img src="/web3/Public/picture/picture_Bbs_list/mzl.cover.175x175-75_4.jpg" height="62" width="62" alt="跟着众神一起开疆扩土：《奥林匹斯的崛起》" /></a>					<a href="#" class="tit" target="_blank" title="跟着众神一起开疆扩土：《奥林匹斯的崛起》">跟着众神一起开疆扩土：《奥林匹斯的崛起》</a>
						<p class="desc">神秘且充满魅力的古希腊神话一直都是游戏开发者喜欢拿来大显身手的优秀题材，这一点从...</p>							
					</div>						
				</li> 						
				<li>							
					<div class="item">					
						<a href="#" class="icon" target="_blank" title="可能是迄今最不同流合污的MMO手游：《剑侠世界》"><i class="mask"></i><img src="/web3/Public/picture/picture_Bbs_list/mzl.cover.175x175-75_5.jpg" height="62" width="62" alt="可能是迄今最不同流合污的MMO手游：《剑侠世界》" /></a>				
						<a href="#" class="tit" target="_blank" title="可能是迄今最不同流合污的MMO手游：《剑侠世界》">可能是迄今最不同流合污的MMO手游：《剑侠世界》</a>				
						<p class="desc">《剑侠世界》这次是西山居与小米合作联运，显然不如在腾讯平台上来得影响力大，但这款...
						</p>							
					</div>						
				</li> 						
				<li>							
					<div class="item">				
						<a href="#" class="icon" target="_blank" title="当音乐不再纯粹 你需要用它来净化耳朵：《Earbits》"><i class="mask"></i><img src="/web3/Public/picture/picture_Bbs_list/mzl.cover.175x175-75_6.jpg" height="62" width="62" alt="当音乐不再纯粹 你需要用它来净化耳朵：《Earbits》" /></a>	
						<a href="#" class="tit" target="_blank" title="当音乐不再纯粹 你需要用它来净化耳朵：《Earbits》">当音乐不再纯粹 你需要用它来净化耳朵：《Earbits》</a>
						<p class="desc">《Earbits》作为一款欧美独立音乐电台，始终坚持以音乐为主。</p>	
					</div>						
				</li> 					
				<!-- Fix -->					
				<li class="fix"></li>				
			</ul>			
		</div>			
		<div id="top_fitting_section" class="section news_section fitting_section hide">			
		<div class="clearfix news_list">
			<ul>						
				<li class="top">
					<div class="item">			
						<a href="#" class="pic" target="_blank" title="JIC：首款给 iPhone 提供录音功能的保护壳"><img src="/web3/Public/picture/picture_Bbs_list/8efdd20eimg201609281214100_368__174.jpg" height="174" width="368" alt="JIC：首款给 iPhone 提供录音功能的保护壳" /></a>	
						<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>
						<a href="#" class="tit" target="_blank" title="JIC：首款给 iPhone 提供录音功能的保护壳">JIC：首款给 iPhone 提供录音功能的保护壳</a>		
						<p class="desc">虽然 iPhone 本身也可以通过应用进行录音，但是很多需要录音的时刻并不会等着我们打开...</p>							
					</div>						
				</li>						
				<li class="sub_top">
					<div class="item">					
						<a href="#" class="pic" target="_blank" title="越来越普及 又多一款智能家居支持HomeKit"><img src="/web3/Public/picture/picture_Bbs_list/197432d9img201609281025110_110__84.jpg" height="84" width="110" alt="越来越普及 又多一款智能家居支持HomeKit" /></a>				
						<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>	
						<a href="#" class="tit" target="_blank" title="越来越普及 又多一款智能家居支持HomeKit">越来越普及 又多一款智能家居支持HomeKit</a>
						<p class="desc">相信随着智能家居渐渐走入更多人的家庭里，肯定会有更多设备支持HomeKit。</p>	
					</div>						
				</li> 						
				<li>							
					<div class="item">
						<a href="#" class="pic" target="_blank" title="贝尔金/Incase发布其首款 Apple Watch 表带"><img src="/web3/Public/picture/picture_Bbs_list/0fba38caimg201609280926590_110__84.png" height="84" width="110" alt="贝尔金/Incase发布其首款 Apple Watch 表带" /></a>					
						<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>								
						<a href="#" class="tit" target="_blank" title="贝尔金/Incase发布其首款 Apple Watch 表带">贝尔金/Incase发布其首款 Apple Watch 表带</a>								
						<p class="desc">随着苹果对Apple Watch进行更新，越来越多的配件生产商也开始带来其首款Apple Watch表...</p>
					</div>						
				</li> 						
				<li>							
					<div class="item">
						<a href="#" class="pic" target="_blank" title="一款 500 美元iPhone配件：来自苹果前员工"><img src="/web3/Public/picture/picture_Bbs_list/6e4552acimg201609272350490_110__84.jpeg" height="84" width="110" alt="一款 500 美元iPhone配件：来自苹果前员工" /></a>						
						<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>		
						<a href="#" class="tit" target="_blank" title="一款 500 美元iPhone配件：来自苹果前员工">一款 500 美元iPhone配件：来自苹果前员工</a>								
						<p class="desc">不知道他们的履历能不能为自己带来额外的收益？</p>
					</div>						
				</li> 						
				<li>							
					<div class="item">
						<a href="#" class="pic" target="_blank" title="把iPhone 7电池保护套浸入水中：后果如何？"><img src="/web3/Public/picture/picture_Bbs_list/f1511028img201609271154380_110__84.jpg" height="84" width="110" alt="把iPhone 7电池保护套浸入水中：后果如何？" /></a>						
						<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>
						<a href="#" class="tit" target="_blank" title="把iPhone 7电池保护套浸入水中：后果如何？">把iPhone 7电池保护套浸入水中：后果如何？</a>		
						<p class="desc">苹果自然是不推荐这样做，不过实际上这款产品还是有一定防水能力。</p>
					</div>					
				</li> 						
				<li>							
					<div class="item">
						<a href="#" class="pic" target="_blank" title="这个复古游戏手柄在卖情怀 关于苹果的情怀"><img src="/web3/Public/picture/picture_Bbs_list/c0bc5a23img201609271022570_110__84.gif" height="84" width="110" alt="这个复古游戏手柄在卖情怀 关于苹果的情怀" /></a>
						<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>
						<a href="#" class="tit" target="_blank" title="这个复古游戏手柄在卖情怀 关于苹果的情怀">这个复古游戏手柄在卖情怀 关于苹果的情怀</a>				
						<p class="desc">好玩又好看，你要不要来一个感受下情怀？</p>		
					</div>						
				</li> 						
				<li>							
					<div class="item">		
						<a href="#" class="pic" target="_blank" title="iPhone专属: 这款VR设备能将所有图像3D化"><img src="/web3/Public/picture/picture_Bbs_list/0de0c0f2img201609262232050_110__84.jpg" height="84" width="110" alt="iPhone专属: 这款VR设备能将所有图像3D化" /></a>					
						<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>
						<a href="#" class="tit" target="_blank" title="iPhone专属: 这款VR设备能将所有图像3D化">iPhone专属: 这款VR设备能将所有图像3D化</a>
						<p class="desc">今天，我们要介绍的是一款号称能够“颠覆虚拟现实市场”、“给屏幕画面带来突破”的虚...</p>							
					</div>						
				</li> 						
				<li>							
					<div class="item">
						<a href="#" class="pic" target="_blank" title="新品: 陶瓷版Apple Watch Series 2开箱视频"><img src="/web3/Public/picture/picture_Bbs_list/5276243cimg201609260855380_110__84.png" height="84" width="110" alt="新品: 陶瓷版Apple Watch Series 2开箱视频" /></a>	
						<a href="#" target="_blank" title="新闻" class="type"><span>新闻</span></a>
						<a href="#" class="tit" target="_blank" title="新品: 陶瓷版Apple Watch Series 2开箱视频">新品: 陶瓷版Apple Watch Series 2开箱视频</a>	
						<p class="desc">陶瓷款亮丽轻盈、坚固耐用，其硬度为不锈钢的4倍以上。</p>
					</div>						
				</li> 						
				<!-- Fix -->						
				<li class="fix"></li>					
			</ul>				
		</div>			
					</div>		
				</div>	
			</div>
			<div class="wrap" style="display: block;background:#fff;box-shadow: 0 1px 3px #BCBCBC;margin: 0 auto 10px;padding: 10px 0 5px;overflow: hidden;text-align: center;">
				<div><a href='#' target='_blank' style="position:relative;">
					<div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div>
					<img src='/web3/Public/picture/picture_Bbs_list/view_1.php' border='0' alt=''></a>
				</div>
				<div style="margin-left: auto;margin-right: auto;width: 960px;">
					<div><a href='#' target='_blank' style="position:relative;">
						<div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div>
						<img src='/web3/Public/picture/picture_Bbs_list/view_2.php' border='0' alt=''></a>
					</div>
				</div>
			</div>

<!-- Container -->
<div id="wp" class="wp">
	<div id="pt" class="bbs_info crumbs">
		<div class="crumbs_inner">
			<a href="./" class="home" title="首页">威锋论坛</a>
			<em>&raquo;</em>
			<a href="#">论坛</a> 
			<em>&rsaquo;</em> 
			<a href="#">游戏中心</a>
			<em>&rsaquo;</em> 
			<a href="#">游戏中心</a>
		</div>
	</div>
	<div class="wrap wea_d_panel_980 a_t">
		<table cellpadding="0" cellspacing="1">
			<tr>
				<td width="50%">
					<a href='#' target='_blank' style="position:relative;"><div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div><img src='/web3/Public/picture/picture_Bbs_list/view_3.php' border='0' alt=''></a>
				</td>
				<td width="50%">
					<a href='#' target='_blank' style="position:relative;">
					<div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div>
					<img src='/web3/Public/picture/picture_Bbs_list/view_4.php' border='0' alt=''>
					</a>
				</td>
			</tr>
		</table>
	</div>

	<div class="boardnav">
		<div id="ct" class="wp cl re_ct"> 
			<div class="mn">
				<div class="bm bml pbn">
					<div class="bm_h cl">
						<span class="o" id="forum_rules_511_trigger" onclick="toggle_collapse('forum_rules_511')" title="收起/展开"></span>
						<span class="y pipe">&nbsp;</span>
						<span class="y"></span>
						<h2><a href="#">游戏中心</a></h2>
					</div>
					<div class="bm_c cl pbn">
						<div class="forum_info">
							<a href="#" class="icon"><img src="/web3/Public/picture/picture_Bbs_list/common_511_icon.png" onerror="this.src='/web3/Public/images/images_Bbs_list/forum_icon_new.png';" height="72" width="72" alt="游戏中心" /></a>
							<div class="forum_funs">
								<ul>
									<li>
										<a href="#" onclick="showWindow(this.id, this.href, 'get', 0);" id="a_favorite"><i class="fav"></i>收藏本版<em id="number_favorite">(<b id="number_favorite_num">5</b>)</em></a>
									</li>
									<li>
										<a href="#" target="_blank" title="RSS">
										<i class="rss"></i>订阅本版</a>
									</li>
								</ul>
							</div>
							<div class="forum_props">
								<div class="count">
									<span>今日:</span>
									<em>0</em>
									<span>主题:</span>
									<em>5</em>
									<span>帖子数:</span>
									<em>7</em>
								</div>
								<div class="desc">爱好游戏的你怎能错过！单机or网游在这里可以畅所欲言，攻略、试玩 or Feng友对战在这里应有尽有。</div>
								<div class="author">版主：
									<a href="#" class="notabs" c="1">Jerry.Won</a>, 
									<a href="#" class="notabs" c="1">枫hua</a>
								</div>
							</div>
						</div>
						<div id="forum_rules_511" class="forum_rules" style="">
							<div class="s_title"><h3><i class="sound"></i>版块公告</h3><i class="f_l"></i><i class="f_r"></i></div>
							<div class="inner">
							<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$n): $mod = ($i % 2 );++$i;?><strong><?php echo ($n["content"]); ?></strong><br /><?php endforeach; endif; else: echo "" ;endif; ?>
							<!-- 1. 本版讨论范围是市面上各类iOS/Mac OS单机或者网络游戏相关内容，<font color="Blue">请勿发表无关帖子内容</font>，错版发贴将被管理员移至相关版块。<br />
							2. 禁止出现涉及网游金钱交易、卖号，推广游戏广告及链接和涉及用户隐私，政治题材，黄赌毒甚至是肆意谩骂的内容出现。违者按规定进行删帖-禁言-封号等相关处罚。<br />
							3. 禁止纯表情，纯标点，乱码无意义回复/顶帖，禁止标题无意义符号加长。禁止屠版。违者按版规进行删帖-禁言-封号等相关处罚。<br />
							4. 禁止标题党，谣言，误导，冒名发布原创贴，转载请注明出处。<br />
							5. 维护版块氛围靠每一个用户。出现违规者秉持尽可能多的警告少处罚原则进行，共同促进游戏中心板块的和谐发展。对管理操作有疑问的，请 PM 当事版主协商处理，协商无果的请去 <strong><a href="#" target="_blank">站务与公告区</a></strong>申诉。<br><br>
							<font color="Blue">对于违反规定的会员，视情节严重程度，版主有权采取 协助修改、扣分、压帖、删除、限期禁言、永久禁言、封禁 IP 等管理手段处理。<br />
							对于积极参与讨论，对版块作出突出贡献的会员，版主有权采取 加分、加亮、加精 等管理手段奖励。<br />
							请会员常常关注本条例，及时自行清理违规内容。以上条款即日生效。<br />
							</font> -->
							</div>
						</div>
				</div>
			</div>

			<div class="mbw bmw fl flg">
				<div class="bm_h cl">
					<span class="o" id="subforum_511_trigger" onclick="toggle_collapse('subforum_511');" title="收起/展开"></span>
					<h2>子版块</h2>
				</div>

				<div id="subforum_511" class="bm_c" style="">
					<table cellspacing="0" cellpadding="0" class="fl_tb">
						<tr>
							<td class="fl_g" width="24.9%">
								<div class="fl_icn_g" style="width: 72px;">
									<a href="#"><img src="/web3/Public/picture/picture_Bbs_list/common_616_icon.png" align="left" alt="" /></a>
								</div>
								<dl style="margin-left: 72px;">
									<dt><a href="#"  style="">暴走部落（刀塔之刃）</a></dt>
									<dd><em>主题: 0</em>, <em>帖数: 0</em></dd>
									<dd>
										<a href="#">最后发表: <span title="2016-10-13 15:13">6&nbsp;天前</span></a>
									</dd>
								</dl>
							</td>
							<td class="fl_g" width="24.9%">
								<div class="fl_icn_g" style="width: 72px;">
								<a href="thread-htm-fid-600.html"><img src="/web3/Public/picture/picture_Bbs_list/common_600_icon.png" align="left" alt="" /></a></div>
								<dl style="margin-left: 72px;">
									<dt><a href="thread-htm-fid-600.html"  style="">逐鹿天下</a></dt>
									<dd><em>主题: 0</em>, <em>帖数: 0</em></dd>
									<dd>
										<a href="#">最后发表: 2016-9-21 22:18</a>
									</dd>
								</dl>
							</td>
							<td class="fl_g" width="24.9%">
								<div class="fl_icn_g" style="width: 72px;">
									<a href="#"><img src="/web3/Public/picture/picture_Bbs_list/common_586_icon.png" align="left" alt="" /></a>
								</div>
								<dl style="margin-left: 72px;">
									<dt><a href="#"  style="">龙诀</a></dt>
									<dd><em>主题: 0</em>, <em>帖数: 0</em></dd>
									<dd>
										<a href="#">最后发表: 2016-9-7 17:11</a>
									</dd>
								</dl>
							</td>
							<td class="fl_g" width="24.9%">
								<div class="fl_icn_g" style="width: 72px;">
									<a href="#"><img src="/web3/Public/picture/picture_Bbs_list/common_616_icon.png"
									align="left" alt="" /></a>
								</div>
								<dl style="margin-left: 72px;">
									<dt><a href="#"  style="">傲世之剑</a></dt>
									<dd><em>主题: 0</em>, <em>帖数: 0</em></dd>
									<dd>
										<a href="#">最后发表: 2016-9-21 16:38</a>
									</dd>
								</dl>
							</td>
						</tr>
						<tr class="fl_row">
							<td class="fl_g" width="24.9%">
								<div class="fl_icn_g" style="width: 72px;">
									<a href="#"><img src="/web3/Public/picture/picture_Bbs_list/common_616_icon.png"
									align="left" alt="" /></a>
								</div>
								<dl style="margin-left: 72px;">
									<dt><a href="#"  style="">推倒三国</a></dt>
									<dd><em>主题: 0</em>, <em>帖数: 0<span title="79189">7万</span></em></dd><dd>
										<a href="#">最后发表: <span title="2016-9-28 09:47">昨天&nbsp;09:47</span></a>
									</dd>
								</dl>
							</td>
							<td class="fl_g" width="24.9%">
								<div class="fl_icn_g" style="width: 72px;">
									<a href="#"><img src="/web3/Public/picture/picture_Bbs_list/common_560_icon.png" align="left" alt="" /></a>
								</div>
								<dl style="margin-left: 72px;">
									<dt><a href="#"  style="">威锋德州扑克</a></dt>
									<dd><em>主题: 0</em>, <em>帖数: 0</em></dd>
									<dd>
										<a href="#">最后发表: <span title="2016-9-28 10:41">昨天&nbsp;10:41</span></a>
									</dd>
								</dl>
							</td>
							<td class="fl_g" width="24.9%">
								<div class="fl_icn_g" style="width: 72px;">
									<a href="#"><img src="/web3/Public/picture/picture_Bbs_list//common_594_icon.png" align="left" alt="" /></a>
								</div>
								<dl style="margin-left: 72px;">
									<dt><a href="#"  style="">虚荣</a></dt>
									<dd><em>主题: 0</em>, <em>帖数: 0</em></dd>
									<dd>
										<a href="#">最后发表: <span title="2016-9-23 15:40">6&nbsp;天前</span></a>
									</dd>
								</dl>
							</td>
							<td>&nbsp;</td>
						</tr></tr>
					</table>
				</div>
			</div>

<div id="pgt" class="bm bw0 pgs cl">
	<div class="pager pager_top">
		<div class="page_list">
			<div class="pg back_pg" ><a href="#">返回论坛</a></div>
				<div class="pg"><strong>1</strong>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#">4</a>
				<a href="#">5</a>
				<a href="#">6</a>
				<a href="#">7</a>
				<a href="#">8</a>
				<a href="#">9</a>
				<a href="#">10</a>
				<label><input type="text" name="custompage" class="px" size="2" title="输入页码，按回车快速跳转" value="1" onkeydown="if(event.keyCode==13) {window.location='forum.php?mod=forumdisplay&fid=511&amp;page='+this.value;; doane(event);}" /><span title="共 10 页"> / 10 页</span></label>
				<a href="#" class="nxt">下一页</a>
			</div>
		</div>
	</div> 

	<div id='gt_forumdisplay_postbutton_top'>
		<script type="text/javascript" src="/web3/Public/js/js_Bbs_list/get.php"></script>
	</div>    
	<script type="text/javascript">
        function move_fast_geetest_before_submit() {
            var livereplysubmit = $('livereplysubmit');
            var geetest = $('gt_forumdisplay_postbutton_top');
            livereplysubmit.parentNode.insertBefore(geetest, livereplysubmit);
        }
        _attachEvent(window, 'load', move_fast_geetest_before_submit);

    </script>
</div>

<div id="threadlist" class="tl bm bmw thread_list">
    <script type='text/javascript' src='/web3/Public/js/js_Bbs_list/extension.php'></script>     
	<div class="th">
		<table cellspacing="0" cellpadding="0">
			<tr>
				<th colspan="2">
					<div class="tf">
						<span id="atarget" onclick="setatarget(1)" class="y" title="在新窗口中打开帖子">新窗</span>
						<a id="filter_special" href="javascript:;" class="showmenu xi2" onclick="showMenu(this.id)">全部主题</a>
						<span class="pipe">|</span>
						<a href="javascript:;" onclick="clearStickThread()" class="xi2" title="显示置顶">显示置顶</a>
						</span>
					</div>
				</th>
				<td class="by">作者</td>
				<td class="num">回复/查看</td>
				<td class="by">最后发表</td>
			</tr>
		</table>
	</div>
	<div class="bm_c">
		<script type="text/javascript">var lasttime = 1475083583;var listcolspan= '5';</script>
		<div id="forumnew" style="display:none"></div>
			<form method="post" autocomplete="off" name="moderate" id="moderate" action="forum.php?mod=topicadmin&amp;action=moderate&amp;fid=511&amp;infloat=yes&amp;nopost=yes">
				<input type="hidden" name="formhash" value="8fd6a6f8" />
				<input type="hidden" name="listextra" value="page%3D1" />
				<table summary="forum_511" cellspacing="0" cellpadding="0" id="threadlisttableid">
					<tbody>
						<tr>
							<td class="icn"><img src="/web3/Public/picture/picture_Bbs_list/ann_icon.png" height="22" width="22" alt="公告" /></td>
							<th><em class="xst">公告: <a href="#" target="_blank"><b><font color="#FF0000">兔兔助手安卓版2.0上线！畅玩精灵宝可梦，还有各种大作！！</font></b></a></em></th>
							<td class="by">
							<cite><a href="home.php?mod=space&amp;uid=1" c="1">Nicholas</a></cite>
							<em>2016-9-23</em>
							</td>
							<td class="num">&nbsp;</td>
							<td class="by">&nbsp;</td>
						</tr>
					</tbody>
                                                                                
					<tbody id="stickthread_10802814">
						<tr>
							<td class="icn">
								<a href="#" title="全局置顶主题 - 新窗口打开" target="_blank">
									<img src="/web3/Public/picture/picture_Bbs_list/pin_3.png" alt="全局置顶" />
								</a>
							</td>
							<th class="common">
								<em><a href="javascript:void(0);" onclick="hideStickThread('10802814')" class="showhide y" title="隐藏置顶帖">隐藏置顶帖</a></em>
								<a href="<?php echo U('Bbsdetail/index');?>" style="font-weight: bold;color: #2B65B7;" onclick="atarget(this)" class="s xst">搜狗输入法“快速分享”功能上线，秀分享赢iPhone7</a>
								<img src="/web3/Public/picture/picture_Bbs_list/rushreply_s.png" alt="抢楼" align="absmiddle" />
								<span id="rushtimer_10802814"> 【还有 <span id="rushtimer_body_10802814"></span> <script language="javascript">settimer(750097, 'rushtimer_body_10802814');</script> 结束  】</span>
								
								<a href="javascript:" id="user_report_10802814" style="display:none;float:right" title="这是垃圾帖"><img src="/web3/Public/picture/picture_Bbs_list/trash_gray.png"></a>
						<script>
							jQuery(function(){
								jQuery('#user_report_10802814').click(function(){
										user_report(10802814, 0);
							});
							jQuery('#stickthread_10802814').hover(
								function () {
									jQuery('#user_report_10802814').stop(false, true).show("slow");
										},
										function() {
										jQuery('#user_report_10802814').stop(false, true).hide("slow");
										}
									);
								});
							</script>
							</th>
							<td class="by">
								<cite>
									<a href="home.php?mod=space&amp;uid=1802899" c="1">搜狗官方</a>                        
								</cite>
								<em><span><span title="2016-9-23">6&nbsp;天前</span></span></em>
							</td>
							<td class="num">
								<a href="#" class="xi2">6789</a><em>173692</em>
							</td>
							<td class="by">
								<cite><a href="#" c="1">美丽的书</a></cite>
								<em><a href="#"><span title="2016-9-29 01:24">2&nbsp;分钟前</span></a></em>
							</td>
						</tr>
					</tbody>                                                                                      
                                                        
<tbody id="normalthread_10813773">
<tr>
<td class="icn">
<a href="read-htm-tid-10813773.html" title="有新回复 - 新窗口打开" target="_blank">
<img src="/web3/Public/picture/picture_Bbs_list/folder_new.png" />
</a>
</td>
<th class="new">
<em>[<a href="forum.php?mod=forumdisplay&fid=511&amp;filter=typeid&amp;typeid=785">攻略</a>]</em> <a href="<?php echo U('Bbsdetail/index');?>" onclick="atarget(this)" class="s xst">《众神国度》自动主线小号多开</a>
<a href="forum.php?mod=redirect&amp;tid=10813773&amp;goto=lastpost#lastpost" class="xi1">New</a>
<a href="javascript:" id="user_report_10813773" style="display:none;float:right" title="这是垃圾帖" ><img src="/web3/Public/picture/picture_Bbs_list/trash_gray.png"></a>
<script>
	jQuery(function(){
		jQuery('#user_report_10813773').click(function(){
			user_report(10813773, 0);
		});
		jQuery('#normalthread_10813773').hover(
			function () {
				jQuery('#user_report_10813773').stop(false, true).show("slow");
			},
			function () {
				jQuery('#user_report_10813773').stop(false, true).hide("slow");
			}
		);
	});
</script>
</th>
<td class="by">
<cite><a href="home.php?mod=space&amp;uid=11536057" c="1">chz3503211</a></cite>
<em><span><span title="2016-9-27">前天&nbsp;15:14</span></span></em>
</td>
<td class="num"><a href="read-htm-tid-10813773.html" class="xi2">0</a><em>170</em></td>
<td class="by">
<cite><a href="home.php?mod=space&username=chz3503211" c="1">chz3503211</a></cite>
<em><a href="forum.php?mod=redirect&tid=10813773&goto=lastpost#lastpost"><span title="2016-9-27 15:14">前天&nbsp;15:14</span></a></em>
</td>
</tr>
</tbody>
                                                                                    
<tbody id="normalthread_10807407">
<tr>
<td class="icn">
<a href="read-htm-tid-10807407.html" title="有新回复 - 新窗口打开" target="_blank">
<img src="/web3/Public/picture/picture_Bbs_list/folder_new.png" />
</a>
</td>
<th class="new">
<em>[<a href="forum.php?mod=forumdisplay&fid=511&amp;filter=typeid&amp;typeid=785">攻略</a>]</em> <a href="<?php echo U('Bbsdetail/index');?>" onclick="atarget(this)" class="s xst">游戏蜂窝最强手游服务 打造最嗨日常体验</a>

<a href="forum.php?mod=redirect&amp;tid=10807407&amp;goto=lastpost#lastpost" class="xi1">New</a>
<a href="javascript:" id="user_report_10807407" style="display:none;float:right" title="这是垃圾帖" ><img src="/web3/Public/picture/picture_Bbs_list/trash_gray.png"></a>
<script>
	jQuery(function(){
		jQuery('#user_report_10807407').click(function(){
			user_report(10807407, 0);
		});
		jQuery('#normalthread_10807407').hover(
			function () {
				jQuery('#user_report_10807407').stop(false, true).show("slow");
			},
			function () {
				jQuery('#user_report_10807407').stop(false, true).hide("slow");
			}
		);
	});
</script>
</th>
<td class="by">
<cite>
<a href="home.php?mod=space&amp;uid=11536057" c="1">chz3503211</a>                                            
                                        </cite>
<em><span><span title="2016-9-25">4&nbsp;天前</span></span></em>
</td>
<td class="num"><a href="read-htm-tid-10807407.html" class="xi2">1</a><em>272</em></td>
<td class="by">
<cite><a href="home.php?mod=space&username=%E6%89%8B%E6%9C%BA%E9%94%8B%E5%8F%8B6d7ylab" c="1">手机锋友6d7ylab</a></cite>
<em><a href="#"><span title="2016-9-26 22:41">3&nbsp;天前</span></a></em>
</td>
</tr>
</tbody>
                            
                                                        
<tbody id="normalthread_10807691">
<tr>
<td class="icn">
<a href="read-htm-tid-10807691.html" title="有新回复 - 新窗口打开" target="_blank">
<img src="/web3/Public/picture/picture_Bbs_list/folder_new.png" />
</a>
</td>
<th class="new">
<em>[<a href="#">分享</a>]</em> <a href="<?php echo U('Bbsdetail/index');?>" onclick="atarget(this)" class="s xst">蜂窝手游辅助体验纯手工问道伏魔日常</a>
<img src="/web3/Public/picture/picture_Bbs_list/image_s.png" alt="attach_img" title="图片附件" align="absmiddle" />
<a href="forum.php?mod=redirect&amp;tid=10807691&amp;goto=lastpost#lastpost" class="xi1">New</a>
<a href="javascript:" id="user_report_10807691" style="display:none;float:right" title="这是垃圾帖" ><img src="/web3/Public/picture/picture_Bbs_list/trash_gray.png"></a>
<script>
	jQuery(function(){
		jQuery('#user_report_10807691').click(function(){
			user_report(10807691, 0);
		});
		jQuery('#normalthread_10807691').hover(
			function () {
				jQuery('#user_report_10807691').stop(false, true).show("slow");
			},
			function () {
				jQuery('#user_report_10807691').stop(false, true).hide("slow");
			}
		);
	});
</script>
</th>
<td class="by">
<cite>
<a href="#" c="1">caiyuwei120</a>                                            
                                        </cite>
<em><span><span title="2016-9-25">4&nbsp;天前</span></span></em>
</td>
<td class="num"><a href="#" class="xi2">1</a><em>357</em></td>
<td class="by">
<cite><a href="#" c="1">手机锋友swq58vn</a></cite>
<em><a href="forum.php?mod=redirect&tid=10807691&goto=lastpost#lastpost"><span title="2016-9-26 21:32">3&nbsp;天前</span></a></em>
</td>
</tr>
</tbody>
                            
                                                        
<tbody id="normalthread_10807697">
<tr>
<td class="icn">
<a href="read-htm-tid-10807697.html" title="有新回复 - 新窗口打开" target="_blank">
<img src="/web3/Public/picture/picture_Bbs_list/folder_new.png" />
</a>
</td>
<th class="new">
<em>[<a href="#">攻略</a>]</em> <a href="<?php echo U('Bbsdetail/index');?>" onclick="atarget(this)" class="s xst">梦幻西游手游辅助 体验纯手工师门打宝抓鬼</a>
<img src="/web3/Public/picture/picture_Bbs_list/image_s.png" alt="attach_img" title="图片附件" align="absmiddle" />
<a href="forum.php?mod=redirect&amp;tid=10807697&amp;goto=lastpost#lastpost" class="xi1">New</a>
<a href="javascript:" id="user_report_10807697" style="display:none;float:right" title="这是垃圾帖" ><img src="/web3/Public/picture/picture_Bbs_list/trash_gray.png"></a>
<script>
jQuery(function(){
	jQuery('#user_report_10807697').click(function(){
		user_report(10807697, 0);
	});
	jQuery('#normalthread_10807697').hover(
		function () {
			jQuery('#user_report_10807697').stop(false, true).show("slow");
		},
		function () {
			jQuery('#user_report_10807697').stop(false, true).hide("slow");
		}
	);
});
</script>
</th>
<td class="by">
<cite><a href="#" c="1">chz3503211</a></cite>
<em><span><span title="2016-9-25">4&nbsp;天前</span></span></em>
</td>
<td class="num"><a href="#" class="xi2">1</a><em>390</em></td>
<td class="by">
<cite><a href="#" c="1">手机锋友swq58vn</a></cite>
<em><a href="#"><span title="2016-9-26 21:32">3&nbsp;天前</span></a></em>
</td>
</tr>
</tbody>                           
                                                        
<tbody id="normalthread_10808389">
<tr>
<td class="icn">
<a href="read-htm-tid-10808389.html" title="有新回复 - 新窗口打开" target="_blank">
<img src="/web3/Public/picture/picture_Bbs_list/folder_new.png" />
</a>
</td>
<th class="new">
<em>[<a href="forum.php?mod=forumdisplay&fid=511&amp;filter=typeid&amp;typeid=577">分享</a>]</em> <a href="<?php echo U('Bbsdetail/index');?>" onclick="atarget(this)" class="s xst">限免不够长？蜂窝助手接力再续十天</a>
<img src="/web3/Public/picture/picture_Bbs_list/image_s.png" alt="attach_img" title="图片附件" align="absmiddle" />
<a href="forum.php?mod=redirect&amp;tid=10808389&amp;goto=lastpost#lastpost" class="xi1">New</a>
<a href="javascript:" id="user_report_10808389" style="display:none;float:right" title="这是垃圾帖" ><img src="/web3/Public/picture/picture_Bbs_list/trash_gray.png"></a>
<script>
	jQuery(function(){
		jQuery('#user_report_10808389').click(function(){
			user_report(10808389, 0);
		});
		jQuery('#normalthread_10808389').hover(
			function () {
				jQuery('#user_report_10808389').stop(false, true).show("slow");
			},
			function () {
				jQuery('#user_report_10808389').stop(false, true).hide("slow");
			}
		);
	});
</script>
</th>
<td class="by">
<cite><a href="home.php?mod=space&amp;uid=11620393" c="1">caiyuwei120</a></cite>
<em><span><span title="2016-9-25">4&nbsp;天前</span></span></em>
</td>
<td class="num"><a href="read-htm-tid-10808389.html" class="xi2">1</a><em>592</em></td>
<td class="by">
<cite><a href="home.php?mod=space&username=%E6%89%8B%E6%9C%BA%E9%94%8B%E5%8F%8Bswq58vn" c="1">手机锋友swq58vn</a></cite>
<em><a href="forum.php?mod=redirect&tid=10808389&goto=lastpost#lastpost"><span title="2016-9-26 21:31">3&nbsp;天前</span></a></em>
</td>
</tr>
</tbody>                          
                                                       
<tbody id="normalthread_10808664">
<tr>
<td class="icn">
<a href="read-htm-tid-10808664.html" title="有新回复 - 新窗口打开" target="_blank">
<img src="/web3/Public/picture/picture_Bbs_list/folder_new.png" />
</a>
</td>
<th class="new">
<em>[<a href="#">试玩</a>]</em> <a href="<?php echo U('Bbsdetail/index');?>" onclick="atarget(this)" class="s xst">微信牛牛机器人财务自动算账</a>
<a href="#" class="xi1">New</a>
<a href="javascript:" id="user_report_10808664" style="display:none;float:right" title="这是垃圾帖" ><img src="/web3/Public/picture/picture_Bbs_list/trash_gray.png"></a>
<script>
	jQuery(function(){
		jQuery('#user_report_10808664').click(function(){
			user_report(10808664, 0);
		});
		jQuery('#normalthread_10808664').hover(
			function () {
				jQuery('#user_report_10808664').stop(false, true).show("slow");
			},
			function () {
				jQuery('#user_report_10808664').stop(false, true).hide("slow");
			}
		);
	});
</script>
</th>
<td class="by">
<cite><a href="#" c="1">手机锋友tf56lwm</a></cite>
<em><span><span title="2016-9-25">4&nbsp;天前</span></span></em>
</td>
<td class="num"><a href="#" class="xi2">1</a><em>412</em></td>
<td class="by">
<cite><a href="#" c="1">手机锋友yav5e8y</a></cite>
<em><a href="#"><span title="2016-9-26 20:35">3&nbsp;天前</span></a></em>
</td>
</tr>
</tbody>
</table><!-- end of table "forum_G[fid]" branch 1/3 -->
</form>
</div>
</div>

<div id="filter_special_menu" class="p_pop" style="display:none">
<ul>
<li><a href="thread-htm-fid-511.html">全部主题</a></li>
<li><a href="#">投票</a></li></ul>
</div>
<div id="filter_reward_menu" class="p_pop" style="display:none" change="#">
<ul>
<li><a href="#">全部悬赏</a></li>
<li><a href="#">进行中</a></li>
<li><a href="#">已解决</a></li></ul>
</div>
<div id="filter_dateline_menu" class="p_pop" style="display:none; _width:320px;">
<ul class="pop_moremenu">
<li>排序: 
<a href="#" >发帖时间</a><span class="pipe">|</span>
<!--<a href="forum.php?mod=forumdisplay&amp;fid=511&amp;filter=reply&amp;orderby=replies" >回复/查看</a><span class="pipe">|</span>
<a href="forum.php?mod=forumdisplay&amp;fid=511&amp;filter=reply&amp;orderby=views" >查看</a>-->
</li>
<li>时间: 
<a href="#" class="xw1">全部时间</a><span class="pipe">|</span>
<a href="#" >一天</a><span class="pipe">|</span>
<a href="#" >两天</a><span class="pipe">|</span>
<a href="#" >一周</a><span class="pipe">|</span>
<a href="#" >一个月</a><span class="pipe">|</span>
<a href="#" >三个月</a>
</li>
</ul>
</div>
<div id="filter_orderby_menu" class="p_pop" style="display:none"> 
<ul>
<li><a href="thread-htm-fid-511.html">默认排序</a></li>
<li><a href="#">发帖时间</a></li>
<li><a href="#">回复/查看</a></li>
<li><a href="#">查看</a></li>
<li><a href="#">最后发表</a></li>
<li><a href="#">热门</a></li>
<ul>
</div>

<div class="bm bw0 pgs cl">
	<div class="pager pager_bottom">
		<div class="page_list">
			<div class="pg back_pg" ><a href="forum.php">返回论坛</a></div>
			<div class="pg"><strong>1</strong><a href="thread-htm-fid-511-page-2.html">2</a>
				<a href="thread-htm-fid-511-page-3.html">3</a>
				<a href="thread-htm-fid-511-page-4.html">4</a>
				<a href="thread-htm-fid-511-page-5.html">5</a>
				<a href="thread-htm-fid-511-page-6.html">6</a>
				<a href="thread-htm-fid-511-page-7.html">7</a>
				<a href="thread-htm-fid-511-page-8.html">8</a>
				<a href="thread-htm-fid-511-page-9.html">9</a>
				<a href="thread-htm-fid-511-page-10.html">10</a>
				<label><input type="text" name="custompage" class="px" size="2" title="输入页码，按回车快速跳转" value="1" onkeydown="if(event.keyCode==13) {window.location='forum.php?mod=forumdisplay&fid=511&amp;page='+this.value;; doane(event);}" />
					<span title="共 1 页"> / 10 页</span>
				</label><a href="thread-htm-fid-511-page-2.html" class="nxt">下一页</a>
			</div>
		</div>
	</div>

</div>
<script type="text/javascript">
	var postminchars = parseInt('6');
	var postmaxchars = parseInt('320000');
	var disablepostctrl = parseInt('0');
	var fid = parseInt('511');
</script>
<div id="f_pst" class="bm">
	<div class="bm_h">
		<h2>快速发帖</h2>
	</div>
	<div class="bm_c">
		<form method="post" autocomplete="off" id="fastpostform" action="" onSubmit="return fastpostvalidate(this)">
			
		<!-- 文本输入开始 -->
			<div id="menu"></div>
			<div id="sample">
				<script type="text/javascript" src="/web3/Public/js/js_text/nicEdit.js"></script>
				<script type="text/javascript">
					bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
				</script>
				<form action="<?php echo U('Comment/Bbscommentedit');?>" method="post" >
				<textarea name="area3" style="width: 800px; height: 200px;"></textarea>
				<br/>
				<input  style="size:30px" type="submit" name="sb" value="提交">
				</form>
			</div>
		<!-- 文本输入结束 -->

		</form>
	</div>
</div>

<!-- Quick Links -->
<div class="quick_links_wrap">
    <div id="quick_links" class="quick_links"><a href="#" id="return_top" class="return_top"><span>返回顶部</span></a></div>
</div>
<script type="text/javascript">

//搜索
jQuery(function($){
    var 
    shell = $('#search_box'),
    showInfo = window.showDialog || alert;

//切换
var typePanel = shell.find('.field_panel').eq(0);
shell.delegate('.type_panel em', 'click', function(e){
    var currClassName = typePanel[0].className, cName = this.className;
    if(currClassName.indexOf(cName) < 0){
        typePanel[0].className = 'field_panel curr_' + cName;
        shell.find('.' + cName + '_sch input[type=text]').eq(0).focus();
        setcookie('use_google_sch', cName === 'google' ? 1 : 0, cName === 'google' ? 86400 * 30 : -1);
    }
});

//Clear & submit
shell.delegate('form', 'submit', function(e){
    var field = $(this).find('input[type=text]');
    if(field.val() == ''){
        showInfo('请输入搜索关键字！');
        return false;
    }
})
.delegate('input[type=text]', 'keyup', function(){
    var self = $(this), clearBtn = self.data('clearBtn');
    if(!clearBtn){
        clearBtn = self.next('.clear');
        self.data('clearBtn', clearBtn);
        clearBtn.data('target_field', self);
    }
clearBtn[this.value != '' ? 'show' : 'hide']();
})
.delegate('i.clear', 'click', function(){
    var field = $(this).data('target_field');
    if(field){
        this.style.display = '';
        field.val('').focus();
    }
});

//类别
shell.delegate('.type_list', 'mouseenter', function(){
    $(this).addClass('active');
})
.delegate('.type_list', 'mouseleave', function(){
    $(this).removeClass('active');
})
.delegate('.type_list span', 'click', function(){
    var panel = $(this.parentNode).removeClass('active');

    if(this.className.indexOf('current') < 0){
        panel.find('span.current').html(this.innerHTML);

        var 
        field = $('#scbar_mod').val(this.getAttribute('data-type')),
        form = field[0] && field[0].form;
        form && $('input[type=text]', form).focus();
    }
});


});


<!-- 继承结束 -->

 
<div class="wrap footer">
	<div class="links">
		<ul>
			<li><a href="#" target="_blank">关于我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">联系我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">About Us</a></li>
		</ul>
	</div>
	<div class="copyright">
		<p>Copyright 2016-2017 © zhuruicheng. All rights reserved 保留所有权利</p>
	</div>
</div>


</div>
<noscript><a href='#' target='_blank'><img src='http://yes1.feng.com/view.php?what=zone:189&amp;n=afbf8627' border='0' alt=''></a></noscript>

</body>
</html>