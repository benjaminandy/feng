<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="/favicon.ico" type="/web3/Public/image/x-icon">
<!-- 给网站添加一个icon图标 -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="apple-itunes-app" content="app-id=981437434" />

<link rel="alternate" type="application/rss+xml" title="Feng新闻RSS" href="">

<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/common.css" />
<script type="text/javascript" src="/web3/Public/js/js_index/jquery-1.7.min.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_index/common.js"></script>

</head>
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/style.css" />
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/feng_index.css" />
<script type="text/javascript" src="/web3/Public/js/js_index/index.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_index/jquery.marquee.min.js"></script>
<body>
<!-- Global Topbar -->
<div class="global_topbar_wrap">
	<div id="global_topbar" class="global_topbar">
		<div class="wrap inner">
			<div class="services" id="global_topbar_services">
				<ul>
				<li class="current"><a href="#"><i class="weiphone"></i>Feng综合网</a></li>				
				<li><a href=""><i class="wegame"></i>F新闻</a></li>						
				<li><a href=""><i class="fengbuy"></i>F论坛</a></li>
					
				<li><a href=""><i class="fengbuy"></i>建设中</a></li>
				
				<li><a href=""><i class="money"></i>扫码点击</a></li>								
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>
			<div class="links" id="globar_topbar_links">
				<ul>
					<li><a href="" target="_blank">首页</a></li>
					<li><a href="" target="_blank">Feng新闻</a></li>
					<li><a href="" target="_blank">Feng论坛</a></li>
				</ul>
				<div class="focus"><em></em></div>
			</div>
			
			<div class="client" id="global_topbar_client">
				<ul>
					<li class="current"><a href="#"><i></i>手机APP</a></li>					
					<div class="codeBox">
						<h3>扫一扫下载APP</h3>
						<b><img src="/web3/Public/picture/fengcode.jpg" width="94" height="94" alt="Feng二维码" /></b>
					</div>
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>

<!-- 弹框开始 -->
			<div class="user_menu" id="global_user_menu">
				<ul>
					<?php if($_SESSION['username']!= null): ?><li class="face">
						<img src="/web3/Public/picture/<?php echo ($_SESSION['username']['uface']); ?>" height="30" width="30" style="margin-top:-5px;">
						</li>
						<li class="face">
							<b style="font-size:15px"><?php echo ($_SESSION['username']['uname']); ?></b>
						</li>
						<li>
							<a class="ref" href="<?php echo U('Action/stop?model=index&controller=index');?>" title="注销">
								<i class="logout"></i>
								<span class="label">注销</span>
							</a>
						</li>
				        <?php else: ?>
							<li><a data-toggle="modal" href="#login-modal"><span>登录</span></a></li>
							<li><a data-toggle="modal" href="#signup-modal"><span>立即注册</span></a></li><?php endif; ?>
					<!-- <li><a data-toggle="modal" href="#forgetform"><span>找回密码</span></a></li> -->
				</ul>				
				<ul style="display:none;">
					<li class="face">
						<img src="/web3/Public/picture/<?php echo ($_SESSION['username']['uface']); ?>" height="30" width="30" style="margin-top:-5px;">
					</li>
					<li class="face">
						<b style="font-size:15px"><?php echo ($_SESSION['username']['uname']); ?></b>
					</li>
					<li>
						<a class="ref" href="<?php echo U('Action/stop?model=index&controller=index');?>" title="注销">
							<i class="logout"></i>
							<span class="label">注销</span>
						</a>
					</li>
				</ul>
			
			</div>
			<script>
			    function select(){
			        var username = document.form1.uname.value;
			        var userpass = document.form1.password.value;
			        var temp;
			        $.ajax({
				        type:'POST',
				        url:"<?php echo U('Action/select');?>",
				        async:false,
          				data:{name:username,pass:userpass},
          				success:function(data){
          					console.log(data);
          					if(data == 1){
					              alert('用户名错误');
					              temp = 1;
					              
					            } else if(data == 2){
					              alert('密码错误');
					              temp = 2;
					          }
          				}
			    	})

			    	if(temp == 1 || temp == 2 ){
					          return false;
					        }
			    }
			</script>
<!-- 弹框结束 -->

		<!-- <script type="text/javascript" src="/web3/Public/js/js_index/jquery.autocomplete.min.ajax.js"></script>
		<div class="searchBtn">
			<span id="quickSearchBtn"></span>
			<div class="headerSearch" id="headerSearch">
				<div class="searchBoxtop">
					<form id="searchform_top" action="http://s.feng.com/search.php" method="get" >
						<input id="top_keyword" name="srchtxt" type="text" value="搜索其实很简单^ ^">
						<input type="hidden" name="srchmod" value="all">
					</form>	
				</div>
			</div>
		</div>	 -->
		</div>
	</div>
</div>

<!-- 弹框start -->
<div class="modal" id="login-modal">
	<a class="close" data-dismiss="modal">×</a>
	<h1>登录</h1>
	
	<!-- 登录开始 -->
	<div class="contact" >
		<form action="<?php echo U('Index/index');?>" name="form1" onsubmit="return select()" method="POST">
			<ul>
				<li>
					<label>用户名：</label>
					<input type="text" name="uname" placeholder="请输入用户名" id='yourname' onblur="checkname()" value="" required/><span class="tips" id="divname2">长度1~12个字符</span>
				</li>

				<li>
					<label>密码：</label>
					<input type="password" name="password" placeholder="请输入您的密码" onblur="checkpsd5()" required/><span class="tips" id="divpassword0">密码必须由字母和数字组成</span>
				</li>
			</ul>
			<b class="btn"><input type="submit" value="登录"/>
			<input type="reset" value="取消"/></b>
		</form>
	</div>
</div>

<div class="modal" id="signup-modal">
	<a class="close" data-dismiss="modal">×</a>
	<h1>注册</h1>
	<div class="contact" >
	
	<!-- 注册开始 -->
	
	<form name="form2" action="<?php echo U('Action/add');?>" method="post">
		<ul>
			<li>
				<label>用户名：</label>
				<input type="text" name="uname" placeholder="请输入用户名"  onblur="checkna()" value="" /><span class="tips" id="divname">长度3~12个含数字和字母字符</span>
			</li>

			<li>
				<label>性别：</label>
				<input type="radio" name="sex" id="1" value="1">男&nbsp;&nbsp;
				<input type="radio" name="sex" id="2" value="2">女&nbsp;&nbsp;
				<input type="radio" name="sex" id="0" value="0">保密
			</li>

			<li>
				<label>密码：</label>
				<input type="password" name="password" placeholder="请输入您的密码" onblur="checkpsd1()" value="" required/><span class="tips" id="divpassword1">密码必须由字母和数字组成</span>
			</li>

			<li>
				<label>确认密码：</label>
				<input type="password" name="yourpass2" placeholder="请再次输入您的密码" onBlur="checkpsd2()" value="" required/><span class="tips" id="divpassword2">两次密码需要相同</span>
			</li>

			<li>
				<label>电子邮箱：</label>
				<input type="text" name="email" placeholder="请输入您的邮箱" onBlur="checkmail()" required/><span class="tips" id="divmail">请输入您的邮箱地址</span>
			</li>

			<li>
				<label>手机号：</label>
				<input type="text" name="yourphone" placeholder="请输入您的手机联系方式" onBlur="checkphone()" required/><span class="tips" id="divphone">以便帐号丢失后找回</span>
			</li>

			<li>
		      	<div id="distpicker5">
		      	<label>请选择所在地:</label>
			        <div class="form-group">
			          <select class="form-control" id="province10">
				          <option value="" data-code="">—— 省 ——</option>
				          <option value="北京市" data-code="110000">北京市</option>
				          <option value="天津市" data-code="120000">天津市</option>
				          <option value="河北省" data-code="130000">河北省</option>
				          <option value="山西省" data-code="140000">山西省</option>
				          <option value="内蒙古自治区" data-code="150000">内蒙古自治区</option>
				          <option value="辽宁省" data-code="210000">辽宁省</option>
				          <option value="吉林省" data-code="220000">吉林省</option>
				          <option value="黑龙江省" data-code="230000">黑龙江省</option>
				          <option value="上海市" data-code="310000">上海市</option>
				          <option value="江苏省" data-code="320000">江苏省</option>
				          <option value="浙江省" data-code="330000">浙江省</option>
				          <option value="安徽省" data-code="340000">安徽省</option>
				          <option value="福建省" data-code="350000">福建省</option>
				          <option value="江西省" data-code="360000">江西省</option>
				          <option value="山东省" data-code="370000">山东省</option>
				          <option value="河南省" data-code="410000">河南省</option>
				          <option value="湖北省" data-code="420000">湖北省</option>
				          <option value="湖南省" data-code="430000">湖南省</option>
				          <option value="广东省" data-code="440000">广东省</option>
				          <option value="广西壮族自治区" data-code="450000">广西壮族自治区</option>
				          <option value="海南省" data-code="460000">海南省</option>
				          <option value="重庆市" data-code="500000">重庆市</option>
				          <option value="四川省" data-code="510000">四川省</option>
				          <option value="贵州省" data-code="520000">贵州省</option>
				          <option value="云南省" data-code="530000">云南省</option>
				          <option value="西藏自治区" data-code="540000">西藏自治区</option>
				          <option value="陕西省" data-code="610000">陕西省</option>
				          <option value="甘肃省" data-code="620000">甘肃省</option>
				          <option value="青海省" data-code="630000">青海省</option>
				          <option value="宁夏回族自治区" data-code="640000">宁夏回族自治区</option>
				          <option value="新疆维吾尔自治区" data-code="650000">新疆维吾尔自治区</option>
				          <option value="台湾省" data-code="710000">台湾省</option>
				          <option value="香港特别行政区" data-code="810000">香港特别行政区</option>
				          <option value="澳门特别行政区" data-code="820000">澳门特别行政区</option>
			          </select>				    
			          <select class="form-control" id="city10">
			          	<option value="" data-code="">—— 市 ——</option>
			          </select>
			          <select class="form-control" id="district10">
			          	<option value="" data-code="">—— 区 ——</option>
			          </select>
			          <div>
			          <br/>
			          <label>请输入具体地址:</label>
			          <input type="text" name="address" placeholder="请输入具体地址" onblur="checkaddress()" required/><span class="tips" id="divaddress">长度1~30个字符</span>
			          </div>
			    </div>
		    </li>
		</ul>
			<b class="btn"><input type="submit" value="提交"/>
			<input type="reset" value="取消"/></b>
	</form>
	</div>
</div>
<script type="text/javascript">
//验证登录用户名
 	function checkname(){
		var na=document.form1.uname.value;
		// console.log(na);
	  	if( na.length <1 || na.length >12)  
  		{  	
  			divname.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
  		     
  		}else{  
  		    divname.innerHTML='<font class="tips_true">输入正确</font>';
  		   
  		}  
  	}

//验证登录密码 
	function checkpsd(){    
		var psd1=document.form1.password.value;  
		var flagZM=false ;
		var flagSZ=false ; 
		var flagQT=false ;
		console.log(psd1);
		if(psd1.length<6 || psd1.length>12){  
			alert(121); 
			divpassword1.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
		}else{   
			for(i=0;i < psd1.length;i++)   
				{    
					if((psd1.charAt(i) >= 'A' && psd1.charAt(i)<='Z') || (psd1.charAt(i)>='a' && psd1.charAt(i)<='z')) 
					{   
						flagZM=true;
					}
					else if(psd1.charAt(i)>='0' && psd1.charAt(i)<='9')    
					{ 
						flagSZ=true;
					}else    
					{ 
						flagQT=true;
					}   
				}   
				if(!flagZM||!flagSZ||flagQT){
				divpassword1.innerHTML='<font class="tips_false">密码必须是字母数字的组合</font>'; 
				 
				}else{
					
				divpassword1.innerHTML='<font class="tips_true">输入正确</font>';
				 
				}  
			 
			}	
	}

//验证注册用户名
 	function checkna(){
		var na=document.form2.uname.value;
		// console.log(na);
	  	if( na.length <1 || na.length >12)  
  		{  	
  			divname.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
  		     
  		}else{  
  		    divname.innerHTML='<font class="tips_true">输入正确</font>';
  		   
  		}  
  	}

//验证注册密码 
	function checkpsd1(){    
		var psd1=document.form2.password.value;  
		var flagZM=false ;
		var flagSZ=false ; 
		var flagQT=false ;
		console.log(psd1);
		if(psd1.length<3 || psd1.length>12){   
			divpassword1.innerHTML='<font class="tips_false">长度必须3~12个字符</font>';
		}else{   
			for(i=0;i < psd1.length;i++)   
				{    
					if((psd1.charAt(i) >= 'A' && psd1.charAt(i)<='Z') || (psd1.charAt(i)>='a' && psd1.charAt(i)<='z')) 
					{   
						flagZM=true;
					}
					else if(psd1.charAt(i)>='0' && psd1.charAt(i)<='9')    
					{ 
						flagSZ=true;
					}else    
					{ 
						flagQT=true;
					}   
				}   
				if(!flagZM||!flagSZ||flagQT){
				divpassword1.innerHTML='<font class="tips_false">密码必须是字母数字的组合</font>'; 
				 
				}else{
					
				divpassword1.innerHTML='<font class="tips_true">输入正确</font>';
				 
				}  
			 
			}	
	}

//验证注册确认密码 
	function checkpsd2(){ 

		if(document.form2.yourpass2.value!=document.form2.password.value || document.form2.yourpass2.value=='') { 
		     divpassword2.innerHTML='<font class="tips_false">您两次输入的密码不一样</font>';
		} else { 
		     divpassword2.innerHTML='<font class="tips_true">输入正确</font>';
		}
	}

//验证注册邮箱		
	function checkmail(){
		var apos=document.form2.email.value.indexOf("@");
		var dotpos=document.form2.email.value.lastIndexOf(".");
		if (apos<1||dotpos-apos<2) 
		  {
		  	divmail.innerHTML='<font class="tips_false">输入错误</font>' ;
		  }
		else {
			divmail.innerHTML='<font class="tips_true">输入正确</font>' ;
		}
	}

//验证注册地址		
	function checkaddress(){
		var address=document.form2.address.value;
	  	if( address.length <1 || address.length >30){ 
	  		divaddress.innerHTML='<font class="tips_false">长度必须1~30个字符</font>';  		     
  		}else{  
  		    divaddress.innerHTML='<font class="tips_true">输入正确</font>';  		   
  		}  
  	}
</script>
<script src="/web3/Public/js/js_index_headclick/jquery.min.js"></script>
<script src="/web3/Public/js/js_index_headclick/distpicker.data.js"></script>
<script src="/web3/Public/js/js_index_headclick/distpicker.js"></script>
<script src="/web3/Public/js/js_index_headclick/main.js"></script>
<div class="modal" id="forgetform">
	<a class="close" data-dismiss="modal">×</a>
	<h1>忘记密码</h1>
	<form class="forgot-form" method="post" action="">
		<input name="email" value="" placeholder="注册邮箱：">
		<div class="clearfix"></div>
		<input type="submit" name="type" class="forgot button-blue" value="发送重设密码邮件">
	</form>
</div>
<script type="text/javascript" src="/web3/Public/js/js_index_headclick/modal.js"></script>
<!-- 弹框end -->


<!-- 继承开始 -->

<title>Feng论坛综合网</title>

<div class="headTopBox headTopBg">
    <div class="wrap headTopBoxCon">
        <div  class="logo">
        	<a href=''><img src='/web3/Public/picture/view.php' border='0' alt=''></a>
		</div>
        
		<div id="navBox" class="navBox clearfix">
            <ul>
                <li class="current"><!-- 加上此样式 即是滑块默认的位置 -->
                   <a href="<?php echo U('Index/index');?>">
                       <i class="home"></i>
                       <span>首页</span>
                   </a>
                </li>
                <li>
                    <a href="<?php echo U('News/index');?>" target="_blank">
                        <i class="news"></i>
                        <span>新闻</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo U('Bbs/index');?>" target="_blank">
                        <i class="bbs"></i>
                        <span>论坛</span>
                    </a>
                </li>
            </ul>
            <div class="focus"></div>
        </div>
    </div>
</div>
<br/>
<div class="ad_90-top wrap clearfix">
	<a href='#' target='_blank' style="position:relative;">
	<div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div>
	<img src='/web3/Public/picture/view_1.php' border='0' alt=''>
	</a>
</div>
<br/>
<div id="hot_spots" class="clearfix">
    <div class="wrap">
        <div class="homebanner">
            <ul class="sliderL targetO">
                
				<?php if(is_array($piclist)): $i = 0; $__LIST__ = $piclist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$p): $mod = ($i % 2 );++$i;?><li>
								<div class="banner_title" style="visibility: visible;">
								</div>
								<a class="a-img" href="" target="_blank" <strong style="background-image:url(/web3/Public/picture/<?php echo ($p["pic"]); ?>);"></strong>
								<img src="/web3/Public/picture/place.png" longdesc="/web3/Public/images/<?php echo ($p["pic"]); ?>" width="654" alt="watchOS 3体验评价：来自用户的核心之变">
								</a>
							</li><?php endforeach; endif; else: echo "" ;endif; ?>	
            </ul>
			<div class="controller">
              
				
				
            <!--    <a href="javascript:;" class="prev"><span>&#171;</span></a>
                <a href="javascript:;" class="next"><span>&#187;</span></a> -->
            </div>
        </div>
        
        <!--apps-->
		<div class="Topapps">
            <div class="topModule">
				<div class="appbox">
					<a href="#">
						<img class="appimg" src="/web3/Public/picture/tutuicon2015.png" height="46" width="46">
					</a>
					<h5>
					<a href="#">兔兔助手-免费下载正版应用</a></h5>
					<div class="downLink">
						<a href="#" target="_blank">免越狱版</a> 
						<span>|</span> 
						<a href="#" target="_blank">越狱版</a> <span>
					</div>
					<span class="er_img"></span>
				</div>
			</div>
            <div class="topModule">
				<div class="appbox">
					<a href="#">
						<img class="appimg" src="/web3/Public/picture/tutuicon2015.png" height="46" width="46">
					</a>
					<h5>
					<a href="#">兔兔助手-免费下载正版应用</a></h5>
					<div class="downLink">
						<a href="#" target="_blank">免越狱版</a> 
						<span>|</span> 
						<a href="#" target="_blank">越狱版</a> <span>
					</div>
					<span class="er_img"></span>
				</div>
			</div> 	
			<div class="topModule">
				<div class="appbox">
					<a href="#">
						<img class="appimg" src="/web3/Public/picture/tutuicon2015.png" height="46" width="46">
					</a>
					<h5>
					<a href="#">兔兔助手-免费下载正版应用</a></h5>
					<div class="downLink">
						<a href="#" target="_blank">免越狱版</a> 
						<span>|</span> 
						<a href="#" target="_blank">越狱版</a> <span>
					</div>
					<span class="er_img"></span>
				</div>
			</div>    
	
            <div class="blank_t">
                <a href="#" target="_blank">F论坛</a>
                <span>|</span>
                 <a href="#" target="_blank">F新闻</a>
            </div>
        </div>
	</div>
</div>


<!--Recommended_news-->
<div id="Recommended_news" class="clearfix">
	<div class="Recommended_news wrap">
		<h1><i>新闻</i><a href="#" hidefocus="true" target="_blank">据说iPhone 7有谜之噪音 它究竟是什么东西</a> </h1>
		<div class="listNewsBox clearfix">
			<ul>
				<li>
					<a href="#" hidefocus="true" target="_blank" title="本周最佳壁纸:期待iPhone 7更好的焦外成像"><i></i>新闻 丨 本周最佳壁纸:期待iPhone 7更好的焦外成像</a>
					<a href="#" hidefocus="true" target="_blank" title="听摄影师谈他在美网时用7 Plus拍摄的经历"><i></i>新闻 丨 听摄影师谈他在美网时用7 Plus拍摄的经历</a>
					<a href="#" hidefocus="true" target="_blank" title="遇到了吗 iPhone7关闭飞行模式后变无服务"><i></i>新闻 丨 遇到了吗 iPhone7关闭飞行模式后变无服务</a>
					<a href="#" hidefocus="true" target="_blank" title="实际使用对比: iPhone 7完爆Galaxy Note 7"><i></i>新闻 丨 实际使用对比: iPhone 7完爆Galaxy Note 7</a>
					<a href="#" hidefocus="true" target="_blank" title="扛着它实在太辛苦了 帮 iMac 找个“车子”吧"><i></i>新闻 丨 扛着它实在太辛苦了 帮 iMac 找个“车子”吧</a>
					<a href="#" hidefocus="true" target="_blank" title="三星S7 Edge和iPhone 7 Plus相机拍摄对比"><i></i>新闻 丨 三星S7 Edge和iPhone 7 Plus相机拍摄对比</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<!--今日导读-->
<div id="todayNews">    
	<div class="todayNews wrap">
		 <h1>
			<div class="titleBox"><i><span>今日导读</span></i></div>
		</h1>
		<div class="todayNewsBox clearfix">
			<!--左边内容-->
			<div class="columnL">
				<div class="wea_d"><a href="#" target="_blank"><img src="/web3/Public/picture/69217929img201609191753150_306__220.jpg" height="220" width="306" alt="威预告:国产手游陷僵 需踏上名为创新的路" /></a></div>
				<h2 class="anewsd_title"><a href="#" target="_blank">威预告:国产手游陷僵 需踏上名为创新的路</a></h2>
				<div class="abs">如今国产游戏越做越好,我们相信只要再给他们一点时间,国产手游也能走向世界,成为受到全球玩家喜爱的游戏。</div>

				<div class="game_list">
					<ul>
						<li class="first"><a href="#" hidefocus="true" title="工信部的VR白皮书 这些内容你都读懂了吗 " target="_blank"><i class="game-icon"></i>工信部的VR白皮书 这些内容你都读懂了吗 </a></li>
						<li><a href="#" hidefocus="true" title="推理游戏《十二个不在场的人》将登双平台" target="_blank"><i class="game-icon"></i>推理游戏《十二个不在场的人》将登双平台</a></li>
						<li><a href="#" hidefocus="true" title="模拟经营《阿斯特克斯和他的朋友们》上架" target="_blank"><i class="game-icon"></i>模拟经营《阿斯特克斯和他的朋友们》上架</a></li>
						<li><a href="#" hidefocus="true" title="拟真航海新作《Sailaway》或将于年内上架" target="_blank"><i class="game-icon"></i>拟真航海新作《Sailaway》或将于年内上架</a></li>
						<li><a href="#" hidefocus="true" title="三部曲齐了《勇者斗恶龙3》 加入繁体中文" target="_blank"><i class="game-icon"></i>三部曲齐了《勇者斗恶龙3》 加入繁体中文</a></li>
	
					</ul>
				</div>
				<div class="ad_500"><a href='#' target='_blank' style="position:relative;">
					<div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div>
						<img src='/web3/Public/picture/view_3.php' border='0' alt=''></a>
					</div>
				</div>
			<!--左边内容 end-->

      	<!--中间内容-->
			<div class="columnMid">
				<div class="tel_list">
					<ul>
						<li><i class="tel-icon"></i><a href="#" target="_blank">本周最佳壁纸:期待iPhone 7更好的焦外成像</a></li>
						<li><i class="tel-icon"></i><a href="#" target="_blank">听摄影师谈他在美网时用7 Plus拍摄的经历</a></li>
						<li><i class="tel-icon"></i><a href="#" target="_blank">遇到了吗 iPhone7关闭飞行模式后变无服务</a></li>
						<li><i class="tel-icon"></i><a href="#" target="_blank">实际使用对比: iPhone 7完爆Galaxy Note 7</a></li>
						<li><i class="apple-icon"></i><a href="#" target="_blank">扛着它实在太辛苦了 帮 iMac 找个“车子”吧</a></li>
					</ul>
				</div>
				<div class="tel_list">
					<ul>
						<li><i class="tel-icon"></i><a href="#" target="_blank">三星S7 Edge和iPhone 7 Plus相机拍摄对比</a></li>
						<li><i class="tel-icon"></i><a href="#" target="_blank">开直升机上天去抛落iPhone 7猜它命运如何</a></li>
						<li><i class="apple-icon"></i><a href="#" target="_blank">二代Apple Watch没革命升级 不如不发布？</a></li>
						<li><i class="apple-icon"></i><a href="#" target="_blank">一张趣图:苹果在全国哪些省份的人气最高？</a></li>
						<li><i class="tel-icon"></i><a href="#" target="_blank">三星新机爆炸不断：竟然是iPhone 7的锅？</a></li>
					</ul>
				</div>
				<div class="tel_list">
					<ul>
						<li><i class="apple-icon"></i><a href="#" target="_blank">网页版Apple Pay上线了 可是你却忽视了它</a></li>
						<li><i class="tel-icon"></i><a href="#" target="_blank">海关提醒：携带iPhone 7入境必须缴税15%</a></li>
						<li><i class="tel-icon"></i><a href="#" target="_blank">花式晒机: 初代iPhone到iPhone 7 Plus都有</a></li>
						<li><i class="apple-icon"></i><a href="#" target="_blank">AirPods对Siri和苹果人工智能会有很大影响</a></li>
						<li><i class="apple-icon"></i><a href="#" target="_blank">美国企业齐来声援苹果：要求欧盟推翻裁决</a></li>
					</ul>
				</div>	

				<div class="projectItem">
					<a href="#" target="_blank"><img src="/web3/Public/picture/img201609051039000.jpg" alt="2016苹果秋季发布会 - 威锋网全程专题报道"/></a>
					<a class="label" href="#" target="_blank"><span>专题</span></a>
					<div class="border_arrow"></div>
				</div>
				<div class="projectList">
					<ul>
						<li>
							<div class="title">
		                        <div class="redLine"></div>
		                        <a class="title" href="#" target="_blank">2016苹果秋季发布会 - 威锋网全程专题报道</a>
		                    </div>

							<div class="keybox">
								<a href="#" target="_blank">苹果</a>
								<a href="#" target="_blank">发布会</a>
								<a href="#" target="_blank">直播</a>
							</div>
							<p>北京时间9月8日凌晨，苹果在美国旧金山比尔·格雷厄姆市政大礼堂召开发布会，iPhone 7/7 Plus、二代 Apple Watch 以及 AirPods 无线耳机纷纷亮相，威...</p>
						</li>
					</ul>
				</div>
				<div class="fengActivity">
					<div class="fAListN">
						<ul>
							<li>
								<a href="#" title="【踩楼赢大奖】拦截骚扰拒做“恐接族”，腾讯手机管家千元京东卡等你拿！" target="_blank">【踩楼赢大奖】拦截骚扰拒做“恐接族”...</a>
							</li>
							<li>
								<a href="#" title="【有奖活动】中秋节和“嫦娥威威”一起玩游戏，赢价值2699元的无屏超级电视" target="_blank">【有奖活动】中秋节和“嫦娥威威”一起...</a>
							</li>
							<li>
								<a href="#" title="【首发即拿iPhone 7】It is the best iPhone！想要吗？PPmoney免费送！" target="_blank">【首发即拿iPhone 7】It is the best i...</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!--中间内容 end-->
			<!--右边内容-->
			<div class="columnR">
				<div class="week_headLine">
					<h2>一周<em>头条</em><a href="" target="_blank">新闻中心</a></h2> 
					<div class="week_lineNews">
						<ul>
							<li>
								<a href="#" title="iPhone 7/ 7 Plus纷纷到货 锋友开箱晒机忙" target="_blank">iPhone 7/ 7 Plus纷纷到货 锋友开箱晒机忙</a>
								<p>咱们论坛的锋友纷纷在收到新机后第一时间发帖分享了...</p>
							</li>     
							<li>
								<a href="#" title="比想象中更有趣！iMessage国区商店体验" target="_blank">比想象中更有趣！iMessage国区商店体验</a>
								<p>iMessage 商店虽然拥有独立的体系，但它的体验仍然...</p>
							</li>     
							<li>
								<a href="#" title="零售店约起 Apple Watch Series 2高清开箱图赏" target="_blank">零售店约起 Apple Watch Series 2高清开箱图赏</a>
								<p>明天（9月16日） Apple Watch Series 2 将发售或...</p>
							</li>     
							<li>
								<a href="#" title="这就是创新：探究A10芯片如此强大的秘密" target="_blank">这就是创新：探究A10芯片如此强大的秘密</a>
								<p>对于很多人来说，今年秋季苹果所发布的新内容中，最...</p>
							</li>     
							<li>
								<a href="#" title="AirPods和Siri黄金组合将揭示你不知道的宏大计划" target="_blank">AirPods和Siri黄金组合将揭示你不知道的宏大计划</a>
								<p>相比起过去的那一些，这一回苹果看上去似乎有些鲁莽...</p>
							</li>     
						</ul>
					</div>
				</div>
				<!--话题-->
        		<div class="topic_view">
					<a href="#" target="_blank"><span class="label">话题</span></a>
					<a href="#" target="_blank"><img src="/web3/Public/picture/a4c22b10img201609191157140_308__219.jpg" alt="据说iPhone 7有谜之噪音 它究竟是什么东西"/></a>                
					<div class="border_arrow"></div>
				</div>
				<div class="topic_list nper">
					<ul>
						<li>
							<a href="#" target="_blank">
							<div class="nper_num">
								<span class="num">639</span>
								<p>期</p>
							</div>
							<div class="nper_title">
								<h1 class="title">据说iPhone 7有谜之噪音 它究竟是什么东西</h1>
								<p class="content">新一代 iPhone 刚刚入手，如果说遇到问题那是谁都不想的，不如说任何一点困扰都足以让人寝食难安。你的 iPhone 7 会发出瘆人...</p>
							</div></a>
						</li>
						<li>
							<a href="#" target="_blank">
							<div class="nper_num">
								<span class="num">638</span>
								<p>期</p>
							</div>
							<div class="nper_title">
								<h1 class="title">比想象中更有趣！iMessage国区商店体验</h1>
								<p class="content">随着 iOS 10 正式版来到，全新的 iMessage 也终于上线了。国内不少人并不看好新 iMessage，毕竟以微信为代表的即时通信平台...</p>
							</div></a>
						</li>
						<li>
							<a href="#" target="_blank">
							<div class="nper_num">
								<span class="num">637</span>
								<p>期</p>
							</div>
							<div class="nper_title">
								<h1 class="title">无线和AR才是最终目标？但库克让你先忍忍</h1>
								<p class="content">iPhone 7 的登台亮相，人们的期待与争论共存。苹果究竟在想些什么？它未来将要向着何处去？大家最想知道的就是这些问题的答...</p>
							</div></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!--右边内容 end-->   
	</div>
</div>

<!--推荐软件&游戏-->
<div id="softwareGames">
  <div class="softwareGames wrap">
		<h1><i><span>推荐软件&游戏</span></i></h1>
		<div class="gameList" id="gameList">
			<div class="innerCon">
				<ul>
					<li>
						<div class="commonGmeeBox newusers" style="display: block;">
							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/1381297.175x175-75.png" height="66" width="66" alt="免越狱玩汉化破解游戏： 兔兔助手正版发布"/>
								</a>
								<h2><a href="#" title="免越狱玩汉化破解游戏： 兔兔助手正版发布" target="_blank">兔兔助手免越狱版</a></h2>
								<p class="abs">一直以来不少用户都在抱怨《兔兔助手》只能...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75.jpg" height="66" width="66" alt="享受惬意的阅读时光：石头阅读"/>
								</a>
								<h2><a href="#" title="享受惬意的阅读时光：石头阅读" target="_blank">石头阅读-找书神器</a></h2>
								<p class="abs">《石头阅读》这款专注移动阅读的 APP 为我...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_1.jpg" height="66" width="66" alt="【中奖名单公布】用十年的时间 换一次惊艳全场：《银河历险记3》"/>
								</a>
								<h2><a href="#" title="【中奖名单公布】用十年的时间 换一次惊艳全场：《银河历险记3》" target="_blank">银河历险记3 (Samorost 3)</a></h2>
								<p class="abs">本周神奇周四，Amanita Design 最新作《银...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_2.jpg" height="66" width="66" alt="我要去成为最强训练师了：《精灵宝可梦GO》先行体验"/>
								</a>
								<h2><a href="#" title="我要去成为最强训练师了：《精灵宝可梦GO》先行体验" target="_blank">精灵宝可梦GO 官方原版</a></h2>
								<p class="abs">《精灵宝可梦GO》此前该作品已经在部分地区...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_3.jpg" height="66" width="66" alt="生死相依 不离不弃：《兄弟：双子传说》">
								</a>
								<h2><a href="#" title="生死相依 不离不弃：《兄弟：双子传说》" target="_blank">兄弟：双子传说</a></h2>
								<p class="abs">《兄弟：双子传说》由 Starbreeze 工作室使...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_4.jpg" height="66" width="66" alt="艺术与匠心 一份朴实又珍贵的礼物：《爷爷的城市》">
								</a>
								<h2><a href="#" title="艺术与匠心 一份朴实又珍贵的礼物：《爷爷的城市》" target="_blank">卢米诺之城</a></h2>
								<p class="abs">在这个电子技术越来越高端的时代，还有没有...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_5.jpg" height="66" width="66" alt="对不起 这不是一个游戏——这是我的战争"/>
								</a>
								<h2><a href="#" title="对不起 这不是一个游戏——这是我的战争" target="_blank">这是我的战争</a></h2>
								<p class="abs">如果你不是原本在 PC 或者主机上就接触过《...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_6.jpg" height="66" width="66" alt="世界太残酷 生存太艰难 臣妾好辛苦：《饥荒》评测">
								</a>
								<h2><a href="#" title="世界太残酷 生存太艰难 臣妾好辛苦：《饥荒》评测" target="_blank">饥荒</a></h2>
								<p class="abs">在《饥荒》之前，这种荒野求生加冒险的游戏...</p>
							</div>
						</div>  
					</li>
					<li>
						<div class="commonGmeeBox newusers" style="display: block;">
							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_7.jpg" height="66" width="66" alt="水上废墟世界的平淡温情：《淹没》">
								</a>
								<h2><a href="#" title="水上废墟世界的平淡温情：《淹没》" target="_blank">淹没</a></h2>
								<p class="abs">动人的剧情，精良的音效，渐渐浮出水面的谜...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_8.jpg" height="66" width="66" alt="花式塔防套路多 前方高能小心翻车：《地牢战争》">
								</a>
								<h2><a href="#" title="花式塔防套路多 前方高能小心翻车：《地牢战争》" target="_blank">地牢战争</a></h2>
								<p class="abs">“塔防”作为PC以及移动端最早的一批游戏类...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_9.jpg" height="66" width="66" alt="当音乐到类俄罗斯方块之后：《音乐方块》">
								</a>
								<h2><a href="#" title="当音乐到类俄罗斯方块之后：《音乐方块》" target="_blank">音乐方块</a></h2>
								<p class="abs">此前在PSP平台上，曾经有一款将《俄罗斯方...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_10.jpg" height="66" width="66" alt="小学生的华丽逆袭 跑酷还能拯救世界：《超能小子》">
								</a>
								<h2><a href="#" title="小学生的华丽逆袭 跑酷还能拯救世界：《超能小子》" target="_blank">Super Powerboy</a></h2>
								<p class="abs">你听说过用跑酷来拯救世界的故事吗？如果没...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_11.jpg" height="66" width="66" alt="好技术好地点和好的装备都不能少：《冰湖》">
								</a>
								<h2><a href="#" title="好技术好地点和好的装备都不能少：《冰湖》" target="_blank">Ice Lakes</a></h2>
								<p class="abs">此前在移动平台当中也曾经出现过许多的与钓...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_12.jpg" height="66" width="66" alt="终有一天，我们将体会到活着的残忍：《天空之山》">
								</a>
								<h2><a href="#" title="终有一天，我们将体会到活着的残忍：《天空之山》" target="_blank">天空之山</a></h2>
								<p class="abs">曾经开发过《风雨世界（The Whispered Worl...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_1.jpg" height="66" width="66" alt="【中奖名单公布】用十年的时间 换一次惊艳全场：《银河历险记3》">
								</a>
								<h2><a href="#" title="【中奖名单公布】用十年的时间 换一次惊艳全场：《银河历险记3》" target="_blank">银河历险记3 (Samorost 3)</a></h2>
								<p class="abs">本周神奇周四，Amanita Design 最新作《银...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_13.jpg" height="66" width="66" alt="夜路漫漫 步步惊心：《乔治：黑暗恐惧》"/>
								</a>
								<h2><a href="#" title="夜路漫漫 步步惊心：《乔治：黑暗恐惧》" target="_blank">乔治:黑暗恐惧</a></h2>
								<p class="abs">Wall West 早些时候曾推出过 3 部作品，但...</p>
							</div>
						</div>  
					</li>
					<li>
						<div class="commonGmeeBox newusers" style="display: block;">
							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/dd7b35b3img201608081308070_66__66.jpeg" height="66" width="66" alt="微软又来跟苹果原生竞争了：《Microsoft Pix》">
								</a>
								<h2><a href="#" title="微软又来跟苹果原生竞争了：《Microsoft Pix》" target="_blank">Microsoft Pix</a></h2>
								<p class="abs">微软出品，帮助人们拍摄出更优质的照片。</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/07addfd2img201608081305120_66__66.jpeg" height="66" width="66" alt="助你一夜好眠：《蜗牛睡眠》">
								</a>
								<h2><a href="#" title="助你一夜好眠：《蜗牛睡眠》" target="_blank">蜗牛睡眠</a></h2>
								<p class="abs">来自国内团队 Seblong 的睡眠追踪记录软件...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/17bf90dcimg201608081302470_66__66.png" height="66" width="66" alt="一份美妙创作 无心插柳而喜上心头：《Symmys 画板》">
								</a>
								<h2><a href="#" title="一份美妙创作 无心插柳而喜上心头：《Symmys 画板》" target="_blank">Symmys 画板</a></h2>
								<p class="abs">它本质是一个画板，你可以用手指在屏幕上绘...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/cf25a91cimg201608031552340_66__66.png" height="66" width="66" alt="优化之后更快更智能：Firefox 火狐浏览器">
								</a>
								<h2><a href="#" title="优化之后更快更智能：Firefox 火狐浏览器" target="_blank">Firefox 火狐浏览器</a></h2>
								<p class="abs">现在iPhone用户使用Firefox浏览器的理由又...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/bf7a2449img201607041704280_66__66.jpeg" height="66" width="66" alt="给你一片小公举专属的梦幻之光：《Lumiè 微光照片》">
								</a>
								<h2><a href="#" title="给你一片小公举专属的梦幻之光：《Lumiè 微光照片》" target="_blank">Lumiè</a></h2>
								<p class="abs">《Lumiè》是一款照片光影编辑应用，风格比...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/bb46986eimg201607041659310_66__66.jpeg" height="66" width="66" alt="这不是一个应用 这是一个宝藏：《听戏》">
								</a>
								<h2><a href="#" title="这不是一个应用 这是一个宝藏：《听戏》" target="_blank">听戏</a></h2>
								<p class="abs">新媒体时代已经彻底改变了传统戏曲的生态，...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/360810e9img201606171015490_66__66.png" height="66" width="66" alt="您身边的跑步健身教练：Runtastic GPS 跑步">
								</a>
								<h2><a href="#" title="您身边的跑步健身教练：Runtastic GPS 跑步" target="_blank">Runtastic GPS 跑步</a></h2>
								<p class="abs">既能够锻炼身体，也能够交友，然而跑步这看...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/598c457dimg201606140957520_66__66.jpeg" height="66" width="66" alt="比实验品更巨大的增强现实野心：《视+》">
								</a>
								<h2><a href="#" title="比实验品更巨大的增强现实野心：《视+》" target="_blank">视+</a></h2>
								<p class="abs">最近到处都在说 AR？那么什么是 AR？</p>
							</div>
						</div>  
					</li>
					<li>
						<div class="commonGmeeBox newusers" style="display: block;">
							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_14.jpg" height="66" width="66" alt="Dark Arcana: 嘉年华 (Full)">
								</a>
								<h2><a href="#" title="Dark Arcana: 嘉年华 (Full)" target="_blank">Dark Arcana: 嘉年华 (Full)</a></h2>
								<p class="abs">在这个令人心跳加速的历险中，面对面挑战恶...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_15.jpg" height="66" width="66" alt="生活规划好帮手：Orderly">
								</a>
								<h2><a href="#" title="生活规划好帮手：Orderly" target="_blank">生活规划好帮手 Orderly</a></h2>
								<p class="abs">相信朋友们在看到《Orderly》的图标时就已...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_16.jpg" height="66" width="66" alt="弓箭游戏：Archer Apple Shooter">								
								</a>
								<h2><a href="#" title="弓箭游戏：Archer Apple Shooter" target="_blank">Archer Apple Shooter - Free bow and arrow games</a></h2>
								<p class="abs">Archer Apple Shooter is a free archery ....</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_17.jpg" height="66" width="66" alt="Stackables - 分层的纹理"/>
								</a>
								<h2><a href="#" title="Stackables - 分层的纹理" target="_blank">Stackables - 分层的纹理，效果，及掩码</a></h2>
								<p class="abs">Stackables 是在同极具才华的摄影家和艺术...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_18.jpg" height="66" width="66" alt="木星日志 - 记录个人日志"/>
								</a>
								<h2><a href="#" title="木星日志 - 记录个人日志" target="_blank">木星日志 - 记录个人日志</a></h2>
								<p class="abs">木星日志是记录个人日记的一种简单有趣的方...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_19.jpg" height="66" width="66" alt="我的猫呢-喵魂设计 柠檬酱出品"/>
								</a>
								<h2><a href="#" title="我的猫呢-喵魂设计 柠檬酱出品" target="_blank">我的猫呢-喵魂设计 柠檬酱出品</a></h2>
								<p class="abs">大家好，我是喵魂。作为一个养猫、爱猫的人...</p>
							</div>
							
							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_20.jpg" height="66" width="66" alt="财务管理：How Long Will I Earn"/>
								</a>
								<h2><a href="#" title="财务管理：How Long Will I Earn" target="_blank">How Long Will I Earn</a></h2>
								<p class="abs">Have lots of items in your wish list and...</p>
							</div>

							<div class="newusers_list">
								<a class="gameImg" href="#" target="_blank">
									<img src="/web3/Public/picture/mzl.cover.175x175-75_21.jpg" height="66" width="66" alt="唱片蹦跑：Record Run"/>
								</a>
								<h2><a href="#" title="唱片蹦跑：Record Run" target="_blank">唱片蹦跑</a></h2>
								<p class="abs">《Record Run》是一款音乐游戏，在游戏中玩...</p>
							</div>
						</div>  
					</li>
				</ul>
			</div>
			<div class="controller">
				<div class="triggers">
					<a href="javascript:;" title="新手必装">新手必装</a>
					<a href="javascript:;" title="推荐游戏">推荐游戏</a>
					<a href="javascript:;" title="推荐软件">推荐软件</a>
					<a href="javascript:;" title="限时免费">限时免费</a>
				</div>
			</div>
		</div>
  </div>
</div>

 <!--广告位招租1000*90-->
<div class="ad_90 wrap clearfix" style="clear:both;">
	<a href='#' target='_blank' style="position:relative;">
		<div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div>
		<img src='/web3/Public/picture/view_4.php' border='0' alt=''>
	</a>
</div>

<!--新闻频道-->
<div id="feng_tecList">
	<div class="feng_tecList wrap">
		<h1>
			<i>新闻频道</i><span>不一样的科技新闻</span>
			<div class="title_right">
				<div class="triggers">
					<a href="<?php echo U('News/index');?>" title="进入频道" class="feng_views" target="_blank">进入频道</a>
				</div>
			</div>
		</h1>
		<div class="picList clearfix">
			<div class="moduleOne">
				<a href="#" target="_blank">
					<div class="mask"></div>
					<span class="label yellowBg" style="background:#f8c100;">科技</span>
					<img src="/web3/Public/picture/3bec0b3eimg201609192302300_499__246.jpg" height="246" width="499" alt="除了飞机和汽车 轮椅其实也能实现自动驾驶" />
					<div class="drift">
						<span class="title">除了飞机和汽车 轮椅其实也能实现自动驾驶</span>
						<div class="abs">来自美国西北大学的康复机器人助理教授 Brenna Argall 和她的团队正在为严重残疾人士开发一款自动驾驶轮椅。</div>
					</div>
				</a>
			</div>

			<div class="moduleTwo midMargin">
				<a href="#" target="_blank">
					<div class="mask"></div>
					<span class="label yellowBg" style="background:#ff0042;">天文</span>
					<img src="/web3/Public/picture/2b8f965eimg201609192146420_249__246.jpg" height="246" width="249" alt="火星上有水时间比之前猜测还要长10亿年" />
					<div class="drift">
						<span class="title">火星上有水时间比之前猜测还要长10亿年</span>
						<div class="abs">这就意味着，火星上可能有微生物生命的时间比我们原本以为的还要长10亿年。</div>
					</div>
				</a>
			</div>

			<div class="moduleTwo">
				<a href="#" target="_blank">
					<div class="mask"></div>
					<span class="label yellowBg" style="background:#007eff;">PC和硬件</span>
					<img src="/web3/Public/picture/5adb50deimg201609192133250_248__246.jpg" height="246" width="248" alt="索尼为VR做最后准备 售价与独家游戏成优势" />
					<div class="drift">
						<span class="title">索尼为VR做最后准备 售价与独家游戏成优势</span>
						<div class="abs">索尼希望PlayStation VR能凭借价格优势在与竞争对手的较量中胜出。</div>
					</div>
				</a>
			</div>

			<div class="moduleTwo">
				<a href="#" target="_blank">
					<div class="mask"></div>
					<span class="label yellowBg" style="background:#00d8ff;">电视</span>
					<img src="/web3/Public/picture/ce95cd3bimg201609192123140_248__246.jpg" height="246" width="248" alt="LG 77寸OLED电视开卖：售价超13万 " />
					<div class="drift">
						<span class="title">LG 77寸OLED电视开卖：售价超13万 </span>
						<div class="abs">这款电视号称开创电视新纪元，分辨率4K，采用OLED像素自发光面板，支持3D、HDR、杜比音效等。</div>
					</div>
				</a>
			</div>

			<div class="moduleTwo midMargin">
				<a href="#" target="_blank">
					<div class="mask"></div>
					<span class="label yellowBg" style="background:#ff0042;">手机</span>
					<img src="/web3/Public/picture/cab4f2fbimg201609191835180_249__246.jpg" height="246" width="249" alt="苹果不理 三星不想玩芯片代工转而重投OLED" />
					<div class="drift">
						<span class="title">苹果不理 三星不想玩芯片代工转而重投OLED</span>
						<div class="abs">作为全球最大的显示面板制造商，三星继续投资到熟悉的领域，由于研发新技术和扩大产能，持续保持强大的竞争优势完全不是问题。</div>
					</div>
				</a>
			</div>

			<div class="moduleOne">
				<a href="#" target="_blank">
					<div class="mask"></div>
					<span class="label yellowBg" style="background:#007eff;">户外</span>
					<img src="/web3/Public/picture/1697d4c4img201609191813100_499__246.jpg" height="246" width="499" alt="勇敢者的游戏 一特技演员乘火箭车飞越峡谷" />
					<div class="drift">
						<span class="title">勇敢者的游戏 一特技演员乘火箭车飞越峡谷</span>
						<div class="abs">Eddie Braun 乘坐火箭车成功飞越爱达荷州的蛇河峡谷。</div>
					</div>
				</a>
			</div>

			<div class="games_bottom">
    			<div class="game_news">
    				<div class="view_news">
    					<div class="news_p">
    						<div class="news_ps">
    							<a href="#" target="_blank"><span class="label" style="background:#f8c100;">PC和硬件</span></a>
    						</div>
    						<h2><a target="_blank" href="http://tech.feng.com/2016-09-19/Small-Titan-X-_657378.shtml">小型Titan X！NV新旗舰GTX 1080 Ti规格泄露</a></h2>
    						<p>GTX 1080 Ti 也将基于 GP102 核心打造，不过流处理单元开启的数量比 Titan X 少了点，大概为 3228 个，比 TITAN X 少 256 个，比 1080 多 768 个，单浮点精度达到 10.8TFLOPS。</p>
    					</div>             
    				</div>
    			</div>

    			<div class="game_news">
    				<div class="view_news">
    					<div class="news_p">
    						<div class="news_ps">
    							<a href="#" target="_blank"><span class="label" style="background:#ff0042;">科技</span></a>
    						</div>
    						<h2><a target="_blank" href="#">你没看错 这是一辆会冲你笑的汽车</a></h2>
    						<p>微笑是人类最好的沟通语言，人类和无人驾驶汽车也是如此。</p>
    					</div>             
    				</div>
    			</div>

    			<div class="game_news">
    				<div class="view_news">
    					<div class="news_p">
    						<div class="news_ps">
    							<a href="#" target="_blank"><span class="label" style="background:#007eff;">科技</span></a>
    						</div>
    						<h2><a target="_blank" href="#">涨知识了 室内攀岩墙还能用来打乒乓</a></h2>
    						<p>见过用菜刀锅铲垃圾铲打乒乓的趣味表演，但要在攀岩墙上来一盘就需要点技术手段了。</p>
    					</div>             
    				</div>
    			</div>

				<div class="game_dongtai">
					<a href="#" target="_blank"><span class="label" style="background-color:#ffd200">访谈</span></a>
					<ul>
						<li><a target="_blank" href="#">超越7待，沃更精彩——深圳联通版iPhone7“亮”彩首发</a></li>
						<li><a target="_blank" href="#">iPhone7今日开售，想买亮黑款又怕刮花？ESR亿色来帮你</a></li>
						<li class="last"><a target="_blank" href="#">iPhone 7 最强 4G LTE深圳地区运营商实测</a></li>
					</ul>
				</div>
			</div>	
		</div>
	</div>
</div>
<!--广告位1000*90-->
<div class="ad_90 wrap clearfix" style="clear:both;">
	<a href='#' target='_blank' style="position:relative;">
		<div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div>
		<img src='/web3/Public/picture/view_6.php' border='0' alt=''>
	</a>
</div>

<!--论坛游戏推荐频道-->
<div id="gameChannel">
    <div class="gameChannel wrap">
        <h1>
	        <i>论坛推荐</i>
	        <span>iPhone、iPad游戏，和我们一起关注吧！</span>
			<div class="title_right">
			    <div class="triggers">
			        <a href="<?php echo U('Bbs/index');?>" title="进入频道" class="feng_views">进入频道</a>
			    </div>
			</div>
		</h1>

<!--game part end-->            
<div class="gameChannel_left">
   	<div class="game_pc">
   	    <div class="game_pcOne">
           	<a class="wea_dimg" href="#" target="_blank"><img src="/web3/Public/picture/article_97128_640x320.jpg" height="260" width="490" alt=""></a>
           	<a class="label view-tip" href="#" target="_blank"><span>资讯</span></a>
            <div class="mask"></div>
	        <div class="drift">
	           	<a href="#" target="_blank" title="工信部的VR白皮书 这些内容你都读懂了吗 ">
		            <p class="game_title">工信部的VR白皮书 这些内容你都读懂了吗 </p>
		            <p class="game_content">VR 依旧是个精品生存的产业，依照目前的趋势来看，未来被淘汰出局的厂商将不是一个小数目。</p>
	            </a>
	        </div>
       </div>
              
        <div class="game_pcTwo game_pcTwoleft">
           	<a class="app-img" href="#" target="_blank">
               	<div class="border_arrow"></div> 
               	<img src="/web3/Public/picture/article_97127_640x320.jpg" height="150" width="244" alt="">
          	</a>
           	<a class="label locale-tip" href="#" target="_blank"><span>评测</span></a>
  
        	<div class="right-location ceping-tip">
               	<span class="ce_num">8.0</span>
               	<span class="ce_num lab">必玩</span>
               	<span class="fav"></span>
           	</div>
                     
           	<div class="pc">
               	<h2><a href="#" target="_blank" title="一部兼具了感性与任性的音乐电台：《MOZIK》">一部兼具了感性与任性的音乐电台：《MOZIK》</a></h2>
               	<p>如果你是一个想要试着发现生活中那些“无心插柳”之处的人，那么本作会给你一种全新的试听体验。</p>
           	</div>            
       	</div>
        <div class="game_pcTwo ">
           	<a class="app-img" href="#" target="_blank">
               	<div class="border_arrow"></div> 
               	<img src="/web3/Public/picture/article_97126_640x320.jpg" height="150" width="244" alt="">
           	</a>
           	<a class="label locale-tip" href="#" target="_blank"><span>评测</span></a>
            <div class="right-location ceping-tip">
               	<span class="ce_num">8.0</span>
               	<span class="ce_num lab">必玩</span>
               	<span class="fav"></span>
           	</div>
                     
           	<div class="pc">
               	<h2><a href="#" target="_blank" title="地下城内多凶险 玩家遗言需小心：《立在地下城的墓标》">地下城内多凶险 玩家遗言需小心：《立在地下城的墓标》</a></h2>
               	<p>《立在地下城的墓标》这款游戏在画风与玩法上巧妙的继承了前作，在保证了游戏核心玩法的基础上，加入了城镇等区域。</p>
           	</div>            
       	</div>
       
   	</div>
   
   	<div class="game_xm">
   	    <div class="game_xmOne game_xmOnetop">
          	<a href="#" class="app-img" target="_blank">
            	<div class="border_arrow"></div>
               	<img src="/web3/Public/picture/article_97125_640x320.jpg" height="150" width="244" alt="">
           	</a>
           	<a class="label view-tip" href="#" target="_blank"><span>资讯</span></a>
             
           	<div class="xm">
               	<h2><a href="#" target="_blank">推理游戏《十二个不在场的人》将登双平台</a></h2>
               	<p>游戏内玩家将化身威严的法官，裁定各种案件，从中分辨出真凶以及无辜者。</p>
           	</div>             
       	</div>

        <div class="game_xmOne ">
           	<a href="#" class="app-img" target="_blank">
            	<div class="border_arrow"></div>
            	<img src="/web3/Public/picture/article_97124_640x320.jpg" height="150" width="244" alt="">
         	</a>
           	<a class="label view-tip" href="#" target="_blank"><span>资讯</span></a>
             
           	<div class="xm">
               	<h2><a href="#" target="_blank">模拟经营《阿斯特克斯和他的朋友们》上架</a></h2>
               	<p>《阿斯特克斯和他的朋友们》是改编自经典漫画《高卢英雄历险记》的一款类 COC 策略游戏，集模拟经营和建造元素。</p>
           	</div>             
       	</div>
                
   	</div>
   	<div class="games_bottom">
   
           <div class="game_news">
           <div class="view_news">
               <div class="news_p">
                   <div class="news_ps">
                       <a class="label hardware-tip" href="#" target="_blank"><span>限时免费</span></a>
                   </div>
                   <h2><a href="#" target="_blank">Dark Arcana: 嘉年华 (Full)</a></h2>
                   <p>小镇上正在举办神秘的嘉年华会，但神秘事件也随之发生，令居民人心惶惶。请你在一切都还来得及之前，找到离奇失踪的年轻女子。在这个令人心跳加速的历险中，面对面挑战恶魔。你将在两个世界中展开调查：一个是你当前居住的世界；另一个则是通过嘉年华会镜厅进入的镜后世界。</p>
               	</div>             
           	</div>
       	</div>
          
   	    <div class="game_news addw">
           	<div class="view_news">
               	<div class="news_p">
                   	<div class="news_ps">
                       	<a class="label view-tip" href="http://game.feng.com/gamenew/evaluate/index.shtml" target="_blank"><span>资讯</span></a>
                   	</div>
                   	<h2><a href="#" target="_blank">拟真航海新作《Sailaway》或将于年内上架</a></h2>
                   	<p>如果能够坐上船来一次远程航海之旅，一定是一个不错的体验。</p>
               	</div>             
           	</div>
       	</div>
        
     	<div class="game_gl game_gltop">
           	<div class="view_gl">
               	<div class="gl_p">
                   	<div class="gl_ps">
                       	<a class="label face-tip" href="#" target="_blank"><span>攻略</span></a>
                    </div>
                   	<h2><a href="#" target="_blank">《银河历险记3》图文+视频攻略（最终回）</a></h2>
               	</div>             
           	</div>
       	</div>
        <div class="game_gl game_gltop">
           	<div class="view_gl">
               	<div class="gl_p">
                   	<div class="gl_ps">
                       	<a class="label face-tip" href="#" target="_blank"><span>攻略</span></a>
                        <p><a href="#" target="_blank"><img src="/web3/Public/picture/mzl.cover.175x175-75_22.jpg" height="17" width="17" alt=""><span>银河历险记3 (</span></a></p>
                   	</div>
                   	<h2><a href="#" target="_blank">《银河历险记3》图文+视频攻略（第四段）</a></h2>
               	</div>             
           	</div>
       	</div>
               
    </div>
</div>
<!--game part end-->            
<div class="gameChannel_right">
	<div class="slide_game_list slider_game_rank" id="slider_game_rank">
	    <div class="title"><h2><em>手游</em>风向标</h2><a target="_blank" href="#" class="more">更多</a></div>
			<div class="inner">
		<ul>
			<li class="active">              
				<div class="mod" style="height:103px;">
	                <h3><span class="red">1</span> <a class="hd" href="http://game.feng.com/gamenew/read/index-id-1467677.shtml" title="魔法门之英雄无敌3 - 高清版">魔法门之英雄无敌3 - 高清版</a></h3>
	                <a class="ranklist-img fl" href="http://game.feng.com/gamenew/read/index-id-1467677.shtml"> 
	                <img src="/web3/Public/picture/mzl.cover.175x175-75_23.jpg" width="66" height="66" alt="魔法门之英雄无敌3 - 高清版"> </a>
	                <div class="intro"> <a href="#" class="intro-category">策略游戏</a> <p>大小：1.69GB</p>
		                <div class="star-empty">
			                <div class="star-hover" style="width:98%"></div>
			            </div>
			        </div>                  
	            </div>
			</li> 
			<li class="">              
				<div class="mod" >
	                <h3><span class="red">2</span> 
	                <a class="hd" href="#" title="火柴人联盟 无限金币版">火柴人联盟 无限金币版</a></h3>
	                <a class="ranklist-img fl" href="#">
	                <img src="/web3/Public/picture/1839443.175x175-75.jpg" width="66" height="66" alt="火柴人联盟 无限金币版"></a>
	                <div class="intro"><a href="#" class="intro-category">动作游戏</a> 
		                <p>大小：59.4MB</p>
		                <div class="star-empty">
		                   	<div class="star-hover" style="width:91%"></div>
		                </div>
	               	</div>                  
	            </div>
			</li> 
			<li class="">              
				<div class="mod" >
	                <h3><span class="red">3</span> 
	                <a class="hd" href="#" title="杀手：狙击 汉化版">杀手：狙击 汉化版</a></h3>
	                <a class="ranklist-img fl" href="#">
	                <img src="/web3/Public/picture/1677883.175x175-75.jpg" width="66" height="66" alt="杀手：狙击 汉化版"></a>
	                <div class="intro"> 
	                   	<a href="#" class="intro-category">动作游戏</a> <p>大小：546.79MB</p>
	                   	<div class="star-empty">
	                   		<div class="star-hover" style="width:100%"></div>
	                   	</div>
	                </div>                  
	            </div>
			</li> 
			<li class="">              
				<div class="mod" >
	                <h3><span >4</span> 
	                <a class="hd" href="#" title="劳拉 GO">劳拉 GO</a></h3>
	                <a class="ranklist-img fl" href="#"> 
	                <img src="/web3/Public/picture/mzl.cover.175x175-75_24.jpg" width="66" height="66" alt="劳拉 GO"></a>
	                <div class="intro">
		                <a href="#" class="intro-category">智力游戏</a> 
		                <p>大小：957.96MB</p>
			                <div class="star-empty">
			                	<div class="star-hover" style="width:90%"></div>
			                </div>
	                </div>                  
	            </div>
			</li> 
			<li class="">              
				<div class="mod" >
                    <h3><span >5</span> 
                    <a class="hd" href="#" title="虚荣">虚荣</a></h3>
                    <a class="ranklist-img fl" href="#">
                    <img src="/web3/Public/picture/1229185.175x175-75.png" width="66" height="66" alt="虚荣"></a>
                    <div class="intro"> 
	                    <a href="#" class="intro-category">动作游戏</a> 
	                    <p>大小：1.02GB</p>
	                    <div class="star-empty">
	                    	<div class="star-hover" style="width:96%"></div>
	                    </div>
                    </div>                  
                </div>
			</li> 
			<li class="">              
				<div class="mod" >
                    <h3><span >6</span><a class="hd" href="#" title="黑白雨夜 (Calvino Noir)">黑白雨夜 (Calvino Noir)</a></h3>
                    <a class="ranklist-img fl" href="#"> 
                    <img src="/web3/Public/picture/mzl.cover.175x175-75_25.jpg" width="66" height="66" alt="黑白雨夜 (Calvino Noir)"></a>
                    <div class="intro"> 
                    	<a href="#" class="intro-category">策略游戏</a> <p>大小：156.51MB</p>
	                    <div class="star-empty">
	                    	<div class="star-hover" style="width:91%"></div>
	                    </div>
                    </div>                  
                </div>
			</li> 
			<li class="">              
				<div class="mod" >
                    <h3><span >7</span> <a class="hd" href="#" title="地牢猎手4 满级剑圣无限金币宝石版">地牢猎手4 满级剑圣无限金币宝石版</a></h3>
                    <a class="ranklist-img fl" href="#">
                    <img src="/web3/Public/picture/1378450.175x175-75.jpg" width="66" height="66" alt="地牢猎手4 满级剑圣无限金币宝石版"></a>
                    <div class="intro"> 
	                    <a href="#" class="intro-category">角色扮演游戏</a><p>大小：1.02GB</p>
	                    <div class="star-empty">
	                    	<div class="star-hover" style="width:100%"></div>
	                    </div>
                    </div>                  
                </div>
			</li> 
			<li class="">              
				<div class="mod" >
                    <h3><span >8</span><a class="hd" href="#" title="火柴人联盟">火柴人联盟</a></h3>
                    <a class="ranklist-img fl" href="#">
                    <img src="/web3/Public/picture/mzl.cover.175x175-75_26.jpg" width="66" height="66" alt="火柴人联盟"> </a>
                    <div class="intro"> 
	                    <a href="#" class="intro-category">动作游戏</a><p>大小：98.31MB</p>
	                    <div class="star-empty">
	                    	<div class="star-hover" style="width:100%"></div>
	                	</div>
                    </div>                  
                </div>
			</li> 
			<li class="">              
				<div class="mod" >
                    <h3><span >9</span> <a class="hd" href="#" title="我的世界">我的世界</a></h3>
                    <a class="ranklist-img fl" href="#">
                    <img src="/web3/Public/picture/mzl.cover.175x175-75_27.jpg" width="66" height="66" alt="我的世界"></a>
                    <div class="intro"> 
                    	<a href="#" class="intro-category">探险游戏</a> <p>大小：126.28MB</p>
	                    <div class="star-empty">
	                    	<div class="star-hover" style="width:80%"></div>
	                    </div>
                    </div>                  
                </div>
			</li> 
			<li class="">              
				<div class="mod" >
                    <h3><span >10</span> <a class="hd" href="#" title="冒险王2">冒险王2</a></h3>
                    <a class="ranklist-img fl" href="#">
                    <img src="/web3/Public/picture/1819054.175x175-75.png" width="66" height="66" alt="冒险王2"> </a>
                    <div class="intro"> 
                    	<a href="#" class="intro-category">角色扮演游戏</a> <p>大小：122 MB</p>
                    	<div class="star-empty">
                    		<div class="star-hover" style="width:100%"></div>
                    	</div>
                    </div>                  
                </div>
			</li> 
		</ul>                                                                                 
	</div>
</div>
<!--game part end-->

<div class="game_active">
    <h2>游戏行业动态</h2>
    <ul>
    	<li><a target="_blank" href="#" title="倒计时10天！GMGC 昆山数娱节十大亮点齐曝光">倒计时10天！GMGC 昆山数娱节十大亮点齐曝光</a></li>
        <li><a target="_blank" href="#" title="上方汇携手二次元女神夏小薇共同打造二次元网综《小...">上方汇携手二次元女神夏小薇共同打造二次元网综《小...</a></li>
        <li><a target="_blank" href="#" title="重磅消息！全球近50家知名厂商齐聚GMGC昆山数娱节...">重磅消息！全球近50家知名厂商齐聚GMGC昆山数娱节...</a></li>
        <li class="last" ><a target="_blank" href="#" title="聚焦海外蓝海 2016TFC移动游戏海外论坛嘉宾曝光">聚焦海外蓝海 2016TFC移动游戏海外论坛嘉宾曝光</a></li>           
    </ul>
</div>
<!--game part end-->			

			</div>
        </div>
    </div>
 <!--广告位招租1000*90-->
<div class="ad_90 wrap clearfix" style="clear:both;">
	<a href='#' target='_blank' style="position:relative;">
		<div style="position: absolute; height: 15px; line-height: 15px; font-size: 12px;background: #C9C; color: #fff; left: 0; bottom: 0;">广告</div>
		<img src='/web3/Public/picture/view_5.php' border='0' alt=''>
	</a>
</div>
<!--原测评-->
<div id="fengCommunity"></div>		
<!--bbs part end-->
  
<!--快捷导航-->
<div class="quick_navigation">
    <ul>
        <li>
            <a class="todayNews" data-target="todayNews" href="javascript:;" target="_blank">
                <i>今日导读</i>
                <span>今日导读</span>
            </a>
        </li>
        <li>
            <a class="gameChannel" data-target="gameChannel" href="javascript:;" target="_blank">
                <i>论坛推荐</i>
                <span>论坛推荐</span>
            </a>
        </li>
        <li>
            <a class="feng_tecList" data-target="feng_tecList" href="javascript:;" target="_blank">
                <i>新闻频道</i>
                <span>新闻频道</span>
            </a>
        </li>
        <li>
            <a class="gobackBtn" href="javascript:;" target="_blank">
                <i>返回顶部</i>
                <span>返回顶部</span>
            </a>
        </li>
    </ul>
</div>


<!-- 继承结束 -->
<div class="footer_wrap clearfix">
	<!--友情广告链接-->
	<div class="friendsLinks clearfix">
		<div class="wrap friendsWrap">
			<h1>
				<em></em>
				友情链接
			</h1>

			<div class="friendsA">
			<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$n): $mod = ($i % 2 );++$i;?><a href="" target="_blank" title=""><strong><?php echo ($n["linkname"]); ?></strong></a><?php endforeach; endif; else: echo "" ;endif; ?>
			</div>

		<!--广告位1000*90-->
			<noscript><a href='#' target='_blank'><img src='http://yes1.feng.com/view.php?what=zone:165&amp;n=a3e8502c' border='0' alt=''></a></noscript>
		</div>
		<!--广告位1000*90 end-->
		</div>
	</div>
<!--友情链接 end-->
 
 
<div class="wrap footer">
	<div class="links">
		<ul>
			<li><a href="#" target="_blank">关于我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">联系我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">About Us</a></li>
		</ul>
	</div>
	<div class="copyright">
		<p>Copyright 2016-2017 © zhuruicheng. All rights reserved 保留所有权利</p>
	</div>
</div>


</div>
<noscript><a href='#' target='_blank'><img src='http://yes1.feng.com/view.php?what=zone:189&amp;n=afbf8627' border='0' alt=''></a></noscript>

</body>
</html>