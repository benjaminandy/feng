<?php if (!defined('THINK_PATH')) exit();?>﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="alternate" type="application/rss+xml" title="锋观点RSS" href="http://www.feng.com/view/rss.xml" />
<title>锋观点_iPhone,,iPad,MAC及苹果系列产品专题站_威锋网</title>
<meta name="description" content="威锋网诞生于2007年1月10日,与美国苹果公司(Apple Inc.)公布iPhone是同一天,是最早建立的关于iPhone的专题网站,威锋论坛一直是人气最旺的iPhone社区,威锋网出品了第一个原创的iPhone中文App,目前网站提供iPhone新闻,论坛,周边配件评测,锋科技,Cydia威锋源,威锋盘Wefiler分享等服务." />
<meta name="keywords" content="iPhone4s,iPhone4,iPhone5,iPad,iPad2,iPhone,3G iPhone,iPhone中文网,iPhone 越狱,iPhone 3G,iPad,iPhone 3GS,iPad2,iPhone手机,说明书,联通iPhone,iPhone Girl,iTunes,Cydia,iPhone模拟器,iPhone价格,iPhone论坛,iPhone新手指南,iPhone教程,iPhone破解,周边,iPhone固件,iPhone上网,iPhone软件,iPhone游戏,电影,主题,壁纸,铃声,下载,威锋原创软件,iPhone,Apple" />
<link rel="stylesheet" type="text/css" href="css/common.css" /><script type="text/javascript" src="js/jquery-1.7.min.js"></script><script type="text/javascript" src="js/common.js"></script>


<script type="text/javascript">
(function(){
if( document.referrer && (
		document.referrer.match(/bbs\.feng\.com\/mobile\-.+\.html/i) ||
		document.referrer.match(/bbs\.feng\.com\/plugin\.php/i)
	) ){
	return ds.setCookie('view_origin', 1, 1);
}
if( navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i) && ds.getCookie('view_origin') != 1 ){
	location.replace('http://bbs.feng.com/mobile-news-app-2.html');
}
})();
</script>
</head><link rel="stylesheet" type="text/css" href="css/feng.css" /><script type="text/javascript" src="js/addele.js"></script><body class="viewBody">
<!-- Global Topbar -->
<div class="global_topbar_wrap">
	<div id="global_topbar" class="global_topbar">
		<div class="wrap inner">
			<div class="services" id="global_topbar_services">
				<ul>
					<li class="current"><a href="http://www.feng.com"><i class="weiphone"></i>威锋网</a></li>								   <li><a href="http://g.feng.com"><i class="wegame"></i>威锋游戏</a></li>														<li><a href="http://www.weand.com/"><i class="weand"></i>安锋网</a></li>										
					<li><a href="http://www.fengbuy.com/"><i class="fengbuy"></i>威锋商城</a></li>										
					<li><a href="http://www.wper.com/"><i class="wper"></i>威智网</a></li>										
					<li><a href="http://money.feng.com"><i class="money"></i>扫码党</a></li>									
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>
			<div class="links" id="globar_topbar_links">
				<ul>
					<li><a href="http://www.feng.com" target="_blank">首页</a></li>
					<li><a href="http://game.feng.com" target="_blank">游戏</a></li>
					<li><a href="http://tech.feng.com" target="_blank">锋科技</a></li>
					<li><a href="http://news.feng.com" target="_blank">新闻</a></li>
					<li><a href="http://lab.feng.com" target="_blank">评测</a></li>
					<li><a href="http://bbs.feng.com" target="_blank">论坛</a></li>
					<li><a href="http://apt.feng.com" target="_blank">威锋源</a></li>
					<li><a href="http://g.feng.com" target="_blank">威锋游戏</a></li>
					<li><a href="http://www.fengbuy.com/" target="_blank">威锋商城</a></li>
					<li><a href="http://live.feng.com" target="_blank">直播</a></li>
				</ul>
				<div class="focus"><em></em></div>
			</div>
			
			<div class="client" id="global_topbar_client">
				<ul>
					<li class="current"><a href="#"><i></i>手机威锋</a></li>					
					<li><a href="http://www.feng.com/app/"><i class="fengClient"></i>威锋客户端</a></li>					
					<div class="codeBox">
						<h3>用微信扫我</h3>
						<b><img src="picture/fengcode.jpg" width="94" height="94" alt="威锋二维码" /></b>
					</div>
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>
			<div class="user_menu" id="global_user_menu">
				<ul>
					<li><a href="http://bbs.feng.com/hack.php?H_name=ssourl&action=ajax&cmd=login&ref=http%3A%2F%2Fwww.feng.com%2Fview%2Findex.shtml" id="user_login"><span>登录</span></a></li>
					<li><a class="ref" href="http://bbs.feng.com/hack.php?H_name=ssourl&action=ajax&cmd=register&ref="><span>立即注册</span></a></li>
					<li><a class="ref" href="http://passport.feng.com/?r=user/lostPwdRequest&ref="><span>找回密码</span></a></li>
				</ul>
				<ul style="display:none;">
					<li class="face"><a href=""><img src="" height="24" width="24" alt="" /></a></li>
					<li><a href="http://bbs.feng.com/home.php?mod=space&do=pm" title="消息"><i class="message"></i><span class="label">消息</span><span class="num"><em></em></span></a></li>
					<li><a href="http://passport.feng.com/index.php?r=site/index" title="设置"><i class="setting"></i><span class="label">设置</span></a></li>
					<li><a class="ref" href="http://bbs.feng.com/hack.php?H_name=ssourl&action=ajax&cmd=logout&ref=" title="注销"><i class="logout"></i><span class="label">注销</span></a></li>
				</ul>
			</div>
<script type="text/javascript" src="js/jquery.autocomplete.min.ajax.js"></script><div class="searchBtn">
				<span id="quickSearchBtn"></span>
				<div class="headerSearch" id="headerSearch">
					<div class="searchBoxtop">
					<form id="searchform_top" action="http://s.feng.com/search.php" method="get" >
						<input id="top_keyword" name="srchtxt" type="text" value="搜索其实很简单^ ^">
						<i></i>
						<input type="hidden" name="srchmod" value="all">
					</form>	
					</div>
				</div>
</div>
<script type="text/javascript">
$("#top_keyword").autocomplete('http://s.feng.com/api/autocomplateAjax.php',{
		max: 12,
		minChars: 1, 
		width: 306,
		scrollHeight: 300,
		matchContains: true,
		autoFill: false,
		dataType: 'jsonp', 
		extraParams: {
			searchtype: 'thread' 		//应用类型
		},			 
		parse: function(rdata){	
			var rows = [];
			for(var i=0; i<rdata.length; i++){
				rows[i] = { 
					data:rdata, 
					value:rdata[i], 
					result:rdata[i]
				}; 
			}
			return rows;	
		}
	}).result(function(event, data, formatted) {
		$("#searchform_top").submit();
	});
</script>  		</div>
	</div>
</div>
<script type="text/javascript" src="js/topbarnav.js"></script><script type="text/javascript">
!(function(){var setUser=function(){return this.userInfo=!1,this.callback=[],this};setUser.prototype={setUserInfo:function(a){return this.userInfo=a,this.run()},run:function(){var a=this;return $.each(this.callback,function(b,c){c.call(a,a.userInfo)}),this},addCallback:function(a){return this.callback.push(a),this.run()}},$.extend({setUser:new setUser});})();
$.getScript("http://www.feng.com/images_v4/js/topbar.js",function(){return $.getScript("http://bbs.feng.com/newsheader.php?r="+(new Date().getTime()));});
(function(){
var ref="";
$("#global_user_menu .ref").each(function(){
	var o=$(this);o.attr("href",o.attr("href")+ref);
});
})();
$("#navigator li:eq()").addClass("current");
</script>
<div class="viewHead clearfix">
	<div class="wrap">
	 		<a href="http://www.feng.com/view"><h1 class="view_logo">锋观点</h1></a>
		<a href="http://www.feng.com/about/News_Delivery_147.shtml" class="submissionBtn">我要投稿</a>
		<div class="searchBox">
	<form id="searchform" action="http://s.feng.com/search.php" method="get" >
	<input id="g_keyword" name="srchtxt" type="text" value="搜索其实很简单^ ^">
	<i></i>
	<input type="hidden" name="srchmod" value="all">
	</form>
</div>
<script type="text/javascript">
$("#g_keyword").autocomplete('http://s.feng.com/api/autocomplateAjax.php',{
		max: 12,
		minChars: 1, 
		width: 306,
		scrollHeight: 300,
		matchContains: true,
		autoFill: false,
		dataType: 'jsonp', 
		extraParams: {
			searchtype: 'thread' 		//应用类型
		},			 
		parse: function(rdata){	
			var rows = [];
			for(var i=0; i<rdata.length; i++){
				rows[i] = { 
					data:rdata, 
					value:rdata[i], 
					result:rdata[i]
				}; 
			}
			return rows;	
		}
	}).result(function(event, data, formatted) {
		$("#searchform").submit();
	});
</script>        	</div>
</div>

<div class="main wrap clearfix">
	<div class="view_left">
		<div id="data_list" class="clearfix images_wall">
			<div id="bg_share_trigger" style="display:none"><TEXTAREA NAME='' ROWS='5' COLS='50'>NULL</TEXTAREA></div>

			<ul class="left">
				<li>
					<div class="category_top"><span><b>应用</b>/ Apps</span><span class="name_date"><em>lemone</em> | 17小时前</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Apps/2016-10-08/A-raw-silk-stockings-triggered-a-wandering-fish_658844.shtml" target="_blank">一条原味丝袜引发了闲鱼的烦恼</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Apps/2016-10-08/A-raw-silk-stockings-triggered-a-wandering-fish_658844.shtml" target="_blank"><img src="picture/img201610081729550.jpg"  alt="一条原味丝袜引发了闲鱼的烦恼" /></a></p>
					<p class="abs">有用户反映在闲鱼App上发现有店主出售原味丝袜、内裤等低俗色情产品。不仅如此，销售页面上还挂着多张店主穿着丝袜的性感照片。</p>
					<div class="operation">
						<div class="ico browse"><i></i>2142</div>
						<a href="http://www.feng.com/view/Apps/2016-10-08/A-raw-silk-stockings-triggered-a-wandering-fish_658844.shtml#comment" target="_blank"><div class="ico msg"><i></i>11</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>快讯</b>/ News</span><span class="name_date"><em>神_谕</em> | 22小时前</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/News/2016-10-08/Feelings-are-no-longer_658828.shtml" target="_blank">情怀不再？锤子T3工信部证件照曝光</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/News/2016-10-08/Feelings-are-no-longer_658828.shtml" target="_blank"><img src="picture/img201610081245190.jpg"  alt="情怀不再？锤子T3工信部证件照曝光" /></a></p>
					<p class="abs">罗永浩创立的锤子科技有限公司，一直以来自诩为最有情怀和人文气息的科技公司，设计上更是标新立异。如今 T3 的证件照曝光，这个情怀看来已经不在了。</p>
					<div class="operation">
						<div class="ico browse"><i></i>2681</div>
						<a href="http://www.feng.com/view/News/2016-10-08/Feelings-are-no-longer_658828.shtml#comment" target="_blank"><div class="ico msg"><i></i>17</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>观点</b>/ Views</span><span class="name_date"><em>神_谕</em> | 10-04 10:25</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Views/2016-10-04/The-promotion-effects-of-seven-tips_658604.shtml" target="_blank">提升拍照效果的七个小技巧</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Views/2016-10-04/The-promotion-effects-of-seven-tips_658604.shtml" target="_blank"><img src="picture/img201610041026420.jpg"  alt="提升拍照效果的七个小技巧" /></a></p>
					<p class="abs">手机的摄像头虽然已经发展得很不错了，但实际成像效果还是没办法媲美数码相机的，不过我们可以通过使用一些拍摄技巧和小工具来提升整体的拍摄效果。</p>
					<div class="operation">
						<div class="ico browse"><i></i>4721</div>
						<a href="http://www.feng.com/view/Views/2016-10-04/The-promotion-effects-of-seven-tips_658604.shtml#comment" target="_blank"><div class="ico msg"><i></i>8</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>观点</b>/ Views</span><span class="name_date"><em>神_谕</em> | 09-30 17:24</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Views/2016-09-30/WeChat-technologies-masking-brush-tool-The-public-big-V-collective-swimming-naked_658407.shtml" target="_blank">微信技术屏蔽刷量工具 公众号大V集体裸泳</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Views/2016-09-30/WeChat-technologies-masking-brush-tool-The-public-big-V-collective-swimming-naked_658407.shtml" target="_blank"><img src="picture/img201609301724540.jpg"  alt="微信技术屏蔽刷量工具 公众号大V集体裸泳" /></a></p>
					<p class="abs">微信公众号大 V 们每篇十万以上的阅读量确实让人惊讶，但是微信团队突然来了一次技术屏蔽，围观群众终于看清楚了，潮水退去，原来大 V 们在裸泳。</p>
					<div class="operation">
						<div class="ico browse"><i></i>7378</div>
						<a href="http://www.feng.com/view/Views/2016-09-30/WeChat-technologies-masking-brush-tool-The-public-big-V-collective-swimming-naked_658407.shtml#comment" target="_blank"><div class="ico msg"><i></i>12</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>快讯</b>/ News</span><span class="name_date"><em>懿慈</em> | 09-29 19:28</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/News/2016-09-29/Millet-TV-lots-of-failures-Rice-noodles-rights-runs-smooth_658350.shtml" target="_blank">小米电视频出故障 米粉维权一波三折</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/News/2016-09-29/Millet-TV-lots-of-failures-Rice-noodles-rights-runs-smooth_658350.shtml" target="_blank"><img src="picture/img201609291928520.jpg"  alt="小米电视频出故障 米粉维权一波三折" /></a></p>
					<p class="abs">一位“米粉”在购买小米电视后频繁出现各种问题，一个多月的时间内更换了三台电视，但还是没有一台符合标准，退货过程一波三折。</p>
					<div class="operation">
						<div class="ico browse"><i></i>7057</div>
						<a href="http://www.feng.com/view/News/2016-09-29/Millet-TV-lots-of-failures-Rice-noodles-rights-runs-smooth_658350.shtml#comment" target="_blank"><div class="ico msg"><i></i>59</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>观点</b>/ Views</span><span class="name_date"><em>lemone</em> | 09-29 17:20</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Views/2016-09-29/Nokia-to-Microsoft-s-damage-Lenovo-is-get-on-the-Moto_658341.shtml" target="_blank">诺基亚带给微软的伤害 联想在Moto身上感受到了</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Views/2016-09-29/Nokia-to-Microsoft-s-damage-Lenovo-is-get-on-the-Moto_658341.shtml" target="_blank"><img src="picture/img201609291721210.jpg"  alt="诺基亚带给微软的伤害 联想在Moto身上感受到了" /></a></p>
					<p class="abs">前两天，联想开启了新一轮裁员，裁掉了摩托罗拉部门近一半员工。原本联想寄希望Moto重振手机业务，如今反而变成了一个大负担。
 </p>
					<div class="operation">
						<div class="ico browse"><i></i>3251</div>
						<a href="http://www.feng.com/view/Views/2016-09-29/Nokia-to-Microsoft-s-damage-Lenovo-is-get-on-the-Moto_658341.shtml#comment" target="_blank"><div class="ico msg"><i></i>40</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>观点</b>/ Views</span><span class="name_date"><em>神_谕</em> | 09-28 18:41</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Views/2016-09-28/Millet-5-s-series-This-is-not-a-fever_658244.shtml" target="_blank">小米 5s 系列发布 这次一点都不发烧</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Views/2016-09-28/Millet-5-s-series-This-is-not-a-fever_658244.shtml" target="_blank"><img src="picture/img201609281841560.png"  alt="小米 5s 系列发布 这次一点都不发烧" /></a></p>
					<p class="abs">每次提起小米总能想到 “发烧” 这个词，这也是雷军给小米手机的定义：为发烧而生。在这次小米 5s 系列的新品发布会上，却感觉小米离发烧越来越远了。</p>
					<div class="operation">
						<div class="ico browse"><i></i>4098</div>
						<a href="http://www.feng.com/view/Views/2016-09-28/Millet-5-s-series-This-is-not-a-fever_658244.shtml#comment" target="_blank"><div class="ico msg"><i></i>56</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>快讯</b>/ News</span><span class="name_date"><em>懿慈</em> | 09-28 07:59</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/News/2016-09-28/Ren-zhengfei-huawei-never-thought-out-the-apple-don-t-too-many-enemies_658169.shtml" target="_blank">任正非：华为从未想过干翻苹果 不要树敌过多</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/News/2016-09-28/Ren-zhengfei-huawei-never-thought-out-the-apple-don-t-too-many-enemies_658169.shtml" target="_blank"><img src="picture/img201609280800050.jpg"  alt="任正非：华为从未想过干翻苹果 不要树敌过多" /></a></p>
					<p class="abs">任正非称，我们从来没有想要干翻苹果，我们为什么要推翻他们，称霸世界将死无葬身之地。我们不要树敌过多，我们要多交朋友。</p>
					<div class="operation">
						<div class="ico browse"><i></i>2794</div>
						<a href="http://www.feng.com/view/News/2016-09-28/Ren-zhengfei-huawei-never-thought-out-the-apple-don-t-too-many-enemies_658169.shtml#comment" target="_blank"><div class="ico msg"><i></i>26</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>观点</b>/ Views</span><span class="name_date"><em>lemone</em> | 09-27 18:02</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Views/2016-09-27/If-no-alipay-and-WeChat-what-to-do_658126.shtml" target="_blank">如果人“没”了  支付宝和微信该怎么处理？</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Views/2016-09-27/If-no-alipay-and-WeChat-what-to-do_658126.shtml" target="_blank"><img src="picture/img201609271803110.jpg"  alt="如果人“没”了  支付宝和微信该怎么处理？" /></a></p>
					<p class="abs">虽然钱不多，买不起房，也买不起车，但如果突然有一天不小心挂掉了，放在支付宝和微信零钱包里的那点钱该怎么办？这其实是一个很严肃的问题。</p>
					<div class="operation">
						<div class="ico browse"><i></i>3139</div>
						<a href="http://www.feng.com/view/Views/2016-09-27/If-no-alipay-and-WeChat-what-to-do_658126.shtml#comment" target="_blank"><div class="ico msg"><i></i>36</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>一周热评</b>/ Hot</span><span class="name_date"><em>lemone</em> | 09-26 16:26</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Hot/2016-09-26/Jia-Yueting-three-magic-weapon-brag-suck-informers-week-buzz_658006.shtml" target="_blank">贾跃亭三大法宝：吹牛、骂街、告密|一周热评  </a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Hot/2016-09-26/Jia-Yueting-three-magic-weapon-brag-suck-informers-week-buzz_658006.shtml" target="_blank"><img src="picture/img201609261627590.jpg"  alt="贾跃亭三大法宝：吹牛、骂街、告密|一周热评  " /></a></p>
					<p class="abs">国行版 Note 7 爆炸为外部因素所致，难道是友商的策划？锤科即将发布T3，但却被曝出亏损达6.5亿。乐视造了第三大电商节，不过数据真实性却受到质疑。</p>
					<div class="operation">
						<div class="ico browse"><i></i>3599</div>
						<a href="http://www.feng.com/view/Hot/2016-09-26/Jia-Yueting-three-magic-weapon-brag-suck-informers-week-buzz_658006.shtml#comment" target="_blank"><div class="ico msg"><i></i>20</div></a>
					</div>
				</li>
			</ul>
			<ul>
				<li>
					<div class="category_top"><span><b>快讯</b>/News</span><span class="name_date"><em> 懿慈</em> | 21小时前</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/News/2016-10-08/Alibaba-to-complaint-or-binary-notorious-markets-list_658837.shtml" target="_blank">阿里巴巴再遭投诉 或将二进“恶名市场”黑名单</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/News/2016-10-08/Alibaba-to-complaint-or-binary-notorious-markets-list_658837.shtml" target="_blank"><img src="picture/img201610081319000.jpg"  alt="阿里巴巴再遭投诉 或将二进“恶名市场”黑名单" /></a></p>
					<p class="abs">据外媒消息，美国服装和鞋履协会呼吁美国贸易代表办公室再次将阿里巴巴及该公司旗下的淘宝等平台归入“恶名市场”黑名单。</p>
					<div class="operation">
						<div class="ico browse"><i></i>1670</div>
						<a href="http://www.feng.com/view/News/2016-10-08/Alibaba-to-complaint-or-binary-notorious-markets-list_658837.shtml#comment" target="_blank"><div class="ico msg"><i></i>13</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>硬件</b>/Hardware</span><span class="name_date"><em> lemone</em> | 10-06 15:35</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Hardware/2016-10-06/Millet-ultrasonic-how-many-fingerprint-identification-technology-is-your-own_658737.shtml" target="_blank">小米的超声波指纹识别有多少技术是自家的？</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Hardware/2016-10-06/Millet-ultrasonic-how-many-fingerprint-identification-technology-is-your-own_658737.shtml" target="_blank"><img src="picture/img201610061537190.jpg"  alt="小米的超声波指纹识别有多少技术是自家的？" /></a></p>
					<p class="abs">超声波指纹识别算得上是一项黑科技，但国产厂商为了抢得先发或出于营销目的，在没有完全调校好的情况下便采用了这一技术，其结果是用户体验大打折扣。</p>
					<div class="operation">
						<div class="ico browse"><i></i>3554</div>
						<a href="http://www.feng.com/view/Hardware/2016-10-06/Millet-ultrasonic-how-many-fingerprint-identification-technology-is-your-own_658737.shtml#comment" target="_blank"><div class="ico msg"><i></i>57</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>锋玩</b>/Play</span><span class="name_date"><em> 懿慈</em> | 10-02 11:57</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Play/2016-10-02/Le-Pro3-evaluation-flagship-real-killer_658509.shtml" target="_blank">乐 Pro3 评测：旗舰杀手是否名副其实？</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Play/2016-10-02/Le-Pro3-evaluation-flagship-real-killer_658509.shtml" target="_blank"><img src="picture/img201610021157300.jpg"  alt="乐 Pro3 评测：旗舰杀手是否名副其实？" /></a></p>
					<p class="abs">乐视推出年度新款旗舰产品——乐 Pro 3，搭载高通 821 处理器，背面采用金属拉丝工艺，配备 4100mAh 电池，起售价 1799 元。</p>
					<div class="operation">
						<div class="ico browse"><i></i>6649</div>
						<a href="http://www.feng.com/view/Play/2016-10-02/Le-Pro3-evaluation-flagship-real-killer_658509.shtml#comment" target="_blank"><div class="ico msg"><i></i>55</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>观点</b>/Views</span><span class="name_date"><em> 懿慈</em> | 09-30 10:47</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Views/2016-09-30/The-blackberry-great-die-grief_658380.shtml" target="_blank">黑莓手机：生得伟大 死得忧伤</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Views/2016-09-30/The-blackberry-great-die-grief_658380.shtml" target="_blank"><img src="picture/img201609301048160.jpg"  alt="黑莓手机：生得伟大 死得忧伤" /></a></p>
					<p class="abs">由于持续亏损，黑莓宣布关闭手机业务，又一个曾经的巨头倒下。内部管理不当，加上外界竞争激烈，黑莓注定将被市场淘汰。</p>
					<div class="operation">
						<div class="ico browse"><i></i>5493</div>
						<a href="http://www.feng.com/view/Views/2016-09-30/The-blackberry-great-die-grief_658380.shtml#comment" target="_blank"><div class="ico msg"><i></i>45</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>观点</b>/Views</span><span class="name_date"><em> 神_谕</em> | 09-29 18:10</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Views/2016-09-29/Pay-treasure-to-boost-social-function-Looking-for-rich-people-around-you_658345.shtml" target="_blank">支付宝开发 “到位” 平台 寻找身边有钱人</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Views/2016-09-29/Pay-treasure-to-boost-social-function-Looking-for-rich-people-around-you_658345.shtml" target="_blank"><img src="picture/img201609291824260.jpg"  alt="支付宝开发 “到位” 平台 寻找身边有钱人" /></a></p>
					<p class="abs">还记得支付宝在去年愚人节玩笑中开发的新功能 “到位” 服务吗？在 9.9.3 版本更新下居然真的实现了这个功能，一键寻找身边有钱人不是梦！</p>
					<div class="operation">
						<div class="ico browse"><i></i>3578</div>
						<a href="http://www.feng.com/view/Views/2016-09-29/Pay-treasure-to-boost-social-function-Looking-for-rich-people-around-you_658345.shtml#comment" target="_blank"><div class="ico msg"><i></i>20</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>快讯</b>/News</span><span class="name_date"><em> 懿慈</em> | 09-28 23:34</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/News/2016-09-28/TCL-pushed-the-high-end-products-mobile-phone-qualcomm-carrying-820-sold-for-3299-yuan_658265.shtml" target="_blank">TCL推高端新品：手机搭载高通820 售价3299元</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/News/2016-09-28/TCL-pushed-the-high-end-products-mobile-phone-qualcomm-carrying-820-sold-for-3299-yuan_658265.shtml" target="_blank"><img src="picture/img201609282335200.jpg"  alt="TCL推高端新品：手机搭载高通820 售价3299元" /></a></p>
					<p class="abs">TCL日前正式对外召开秋季新品发布会，推出高端副品牌“创逸”，并发布了年度旗舰手机 950，搭载高通骁龙 820 处理器，售价 3299 元。</p>
					<div class="operation">
						<div class="ico browse"><i></i>2267</div>
						<a href="http://www.feng.com/view/News/2016-09-28/TCL-pushed-the-high-end-products-mobile-phone-qualcomm-carrying-820-sold-for-3299-yuan_658265.shtml#comment" target="_blank"><div class="ico msg"><i></i>14</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>硬件</b>/Hardware</span><span class="name_date"><em> lemone</em> | 09-28 17:54</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Hardware/2016-09-28/In-front-of-the-big-jiang-Mavic-Pro-All-amateur-unmanned-aerial-vehicle-uav-is-just-a-toy_658240.shtml" target="_blank">在大疆Mavic Pro面前 一切自拍无人机都只是玩具</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Hardware/2016-09-28/In-front-of-the-big-jiang-Mavic-Pro-All-amateur-unmanned-aerial-vehicle-uav-is-just-a-toy_658240.shtml" target="_blank"><img src="picture/img201609281755490.jpg"  alt="在大疆Mavic Pro面前 一切自拍无人机都只是玩具" /></a></p>
					<p class="abs">在将无人机变小这件事上，许多公司都做出了努力。作为行业的先行者，大疆给出的答案是这台Mavic Pro，它可以说是无人机里的性能小怪兽。</p>
					<div class="operation">
						<div class="ico browse"><i></i>3518</div>
						<a href="http://www.feng.com/view/Hardware/2016-09-28/In-front-of-the-big-jiang-Mavic-Pro-All-amateur-unmanned-aerial-vehicle-uav-is-just-a-toy_658240.shtml#comment" target="_blank"><div class="ico msg"><i></i>20</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>观点</b>/Views</span><span class="name_date"><em> 神_谕</em> | 09-27 18:26</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Views/2016-09-27/Who-hurt-the-false-proof-propaganda_658128.shtml" target="_blank">虚假样张宣传到底伤害了谁？</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Views/2016-09-27/Who-hurt-the-false-proof-propaganda_658128.shtml" target="_blank"><img src="picture/img201609271827590.jpg"  alt="虚假样张宣传到底伤害了谁？" /></a></p>
					<p class="abs">在手机或者相机发布会和官网上，我们总是可以看到精美绝伦的官方拍摄样张，但产品到手之后，我们却难以拍摄出高水平的样张，难道又是官方样张作假？</p>
					<div class="operation">
						<div class="ico browse"><i></i>2357</div>
						<a href="http://www.feng.com/view/Views/2016-09-27/Who-hurt-the-false-proof-propaganda_658128.shtml#comment" target="_blank"><div class="ico msg"><i></i>21</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>快讯</b>/News</span><span class="name_date"><em> 神_谕</em> | 09-26 18:40</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/News/2016-09-26/A-hammer-is-about-to-collapse_658013.shtml" target="_blank">锤子即将倒闭？罗永浩辟谣：都是谣传</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/News/2016-09-26/A-hammer-is-about-to-collapse_658013.shtml" target="_blank"><img src="picture/img201609261841230.png"  alt="锤子即将倒闭？罗永浩辟谣：都是谣传" /></a></p>
					<p class="abs">锤子科技近来被曝光了 2016 年上半年的财务情况，一时之间各种锤子 T3 难产、锤子即将倒闭等消息纷纷四起。日前，老罗正式回应了这些谣言。</p>
					<div class="operation">
						<div class="ico browse"><i></i>2158</div>
						<a href="http://www.feng.com/view/News/2016-09-26/A-hammer-is-about-to-collapse_658013.shtml#comment" target="_blank"><div class="ico msg"><i></i>19</div></a>
					</div>
				</li>
				<li>
					<div class="category_top"><span><b>观点</b>/Views</span><span class="name_date"><em> 神_谕</em> | 09-25 11:48</span></div>
					<h2 class="newsTitle"><a href="http://www.feng.com/view/Views/2016-09-25/China-s-video-industry-hope_657905.shtml" target="_blank">中国影像行业的希望？小蚁无反相机分析展望</a></h2>
					<p> <a class="show_img" href="http://www.feng.com/view/Views/2016-09-25/China-s-video-industry-hope_657905.shtml" target="_blank"><img src="picture/img201609251149370.png"  alt="中国影像行业的希望？小蚁无反相机分析展望" /></a></p>
					<p class="abs">说起无反微单相机和单反相机，国内消费者一般会想起索尼、佳能、尼康等国外品牌，小蚁这一次推出的无反相机，可以说正式打破了这个局面。</p>
					<div class="operation">
						<div class="ico browse"><i></i>2521</div>
						<a href="http://www.feng.com/view/Views/2016-09-25/China-s-video-industry-hope_657905.shtml#comment" target="_blank"><div class="ico msg"><i></i>30</div></a>
					</div>
				</li>
			</ul>
			<div class="loading" id="loading" style="display:none"><i></i>加载更多</div>
			<div class="list_pager"><div id="new_page_list" class="new_page_list"></div></div>
			
		</div>
	</div>
	<div class="news_right view_right">
			<div class="category">
			<h3><em>分类</em> / Categories</h3>
			<ul>
				<li  class="current"" ><a href="http://www.feng.com/view" ><i></i>全部 / All （1505）</a></li>
				<li ><a href="http://www.feng.com/view/Views/index.shtml">观点 / Views（981）</a></li>
				<li ><a href="http://www.feng.com/view/Hardware/index.shtml">硬件 / Hardware（261）</a></li>
				<li ><a href="http://www.feng.com/view/Apps/index.shtml">应用 / Apps（106）</a></li>
				<li ><a href="http://www.feng.com/view/Scene/index.shtml">现场 / Scene（10）</a></li>
				<li ><a href="http://www.feng.com/view/Faces/index.shtml">人物 / Faces（29）</a></li>
				<li ><a href="http://www.feng.com/view/News/index.shtml">快讯 / News（36）</a></li>
				<li ><a href="http://www.feng.com/view/Play/index.shtml">锋玩 / Play（8）</a></li>
				<li ><a href="http://www.feng.com/view/Hot/index.shtml">一周热评 / Hot（11）</a></li>
					
			</ul>
			
		</div>    
	
<style>
.recommended_news ul{padding: 10px 20px 0;}
.recommended_news .img img,.hotTopic_news .img img{width:200px;height:120px;}
.hotTopic_news ul li .nper_title .content{display:none;}
</style>	 
<div class="hotTopic_news">
		<h2 class="title">热门<em>话题</em></h2>
            <a href="http://www.feng.com/Story/October-is-a-conference_658833.shtml" class="img">
                <img src="picture/8df3e636img201610081321180_305__219.jpg" alt="十月发布会有戏？盘点Mac或将迎来的新变化" />
				<div class="border_arrow"></div>
            </a>
                
                    <ul>
                        <li>
                            <a href="http://www.feng.com/Story/October-is-a-conference_658833.shtml"><div class="nper_num">
                                <span class="num">673</span>
                                <p>期</p>
                            </div>
                            <div class='nper_title'>
                                <h3>十月发布会有戏？盘点Mac或将迎来的新变化</h3>
                                <p class="content">苹果仍然是一如既往的沉默，但人人都在期待着这个月可能将要到来的新品发布会。届时，我们或许将迎来 M...</p>
                            </div></a>
                        </li>
						                        						<li>
                            <a href="http://www.feng.com/Story/Apple-and-samsung-opened-up-new-field-the-battle-to-play-for-many-years_658818.shtml"><div class="nper_num">
                                <span class="num">672</span>
                                <p>期</p>
                            </div>
                            <div class='nper_title'>
                                <h3>苹果和三星开辟新战场：这一仗要打很多年</h3>
                                <p class="content">眼下最受关注的技术非人工智能莫属，苹果公司的 Siri、谷歌的 Google Assistant、微软的 Cortana 和亚...</p>
                            </div></a>
                        </li>
												<li>
                            <a href="http://www.feng.com/Story/Don-t-ridicule-apple-service-experience-is-expected-to-roll-over_658773.shtml"><div class="nper_num">
                                <span class="num">671</span>
                                <p>期</p>
                            </div>
                            <div class='nper_title'>
                                <h3>不要再吐槽了：苹果服务体验已经有望翻身</h3>
                                <p class="content">科技界如今的趋势是朝着网络化服务化发展，然而那么些年过去了，苹果的云服务却依然没有太大的好转。我...</p>
                            </div></a>
                        </li>
						                    </ul>             
            
        </div><div class="recommended_news">

            <h2 class="title">推荐<em>专题</em></h2>
            <a href="http://www.feng.com/Topic/macOS_Sierra_TOP15.shtml" class="img">
                <img src="picture/ec988b0cimg201609290955100_305__180.png" alt="关于macOS Sierra系统的15个重大更新改进" />
				<div class="border_arrow"></div>
            </a>
            <h2 class="recommended_title"><a href="http://www.feng.com/Topic/macOS_Sierra_TOP15.shtml">关于macOS Sierra系统的15个重大更新改进</a></h2>
            <ul>
			                <li><a href="http://www.feng.com/Topic/watchOS3_TOP20.shtml">关于watchOS 3你需要了解的20项新特性</a></li>
			                <li><a href="http://www.feng.com/Topic/iOS10_TOP30.shtml">关于iOS 10：你需要了解的30个新特性</a></li>
			                <li><a href="http://www.feng.com/Topic/ppmoney.shtml">iPhone7 首发即拿, 抢先体验</a></li>
			                <li><a href="http://www.feng.com/Topic/Apple_201609.shtml">2016苹果秋季发布会 - 威锋网全程专题报道</a></li>
			            </ul>
</div>  
  <div class="hotAttention_news">
            <h2 class="title">热门<em>关注</em></h2>
            <ul>
			                <li><span class="red">1</span><p><a href="http://www.feng.com/iPhone/news/2016-10-05/Google-Pixel-phone-ran-points-was-the-iPhone-off-the-two-blocks_658713.shtml"  title="谷歌Pixel手机跑分竟被iPhone甩开了两条街">谷歌Pixel手机跑分竟被iPhone甩开了两条街</a></p></li>
			                <li><span class="red">2</span><p><a href="http://www.feng.com/iPhone/news/2016-10-03/Hong-Kong-s-chief-executive-apple-store-queue-Want-to-buy-a-bright-black-iPhone-7_658564.shtml"  title="香港特首苹果店内排队 要买亮黑iPhone 7？">香港特首苹果店内排队 要买亮黑iPhone 7？</a></p></li>
			                <li><span class="red">3</span><p><a href="http://www.feng.com/iPhone/news/2016-10-05/IOS-10.1-Beta-2-add-switch-automatically-broadcast-information-effect_658659.shtml"  title="iOS 10.1 Beta 2发布：信息应用相关功能获更新">iOS 10.1 Beta 2发布：信息应用相关功能获更新</a></p></li>
			                <li><span >4</span><p><a href="http://www.feng.com/iPhone/news/2016-10-02/7-iPhone-camera-scratch-resistant-sapphire-performance-degradation-is-why_658516.shtml"  title="iPhone 7 摄像头蓝宝石耐刮性能降低是为何">iPhone 7 摄像头蓝宝石耐刮性能降低是为何</a></p></li>
			                <li><span >5</span><p><a href="http://www.feng.com/iPhone/news/2016-10-07/Apple-push-three-new-the-next-year-you-will-have-what-effect_658786.shtml"  title="苹果明年推三款新iPad 对你会有什么影响？">苹果明年推三款新iPad 对你会有什么影响？</a></p></li>
			                <li><span >6</span><p><a href="http://www.feng.com/iPhone/news/2016-10-04/IOS-wish-list-get-the-iPhone-double-click-the-wake-up-function_658616.shtml"  title="iOS 11愿望清单:让iPhone获得双击唤醒功能">iOS 11愿望清单:让iPhone获得双击唤醒功能</a></p></li>
			                <li><span >7</span><p><a href="http://www.feng.com/view/Hardware/2016-10-06/Millet-ultrasonic-how-many-fingerprint-identification-technology-is-your-own_658737.shtml"  title="小米的超声波指纹识别有多少技术是自家的？">小米的超声波指纹识别有多少技术是自家的？</a></p></li>
			                <li><span >8</span><p><a href="http://www.feng.com/view/Play/2016-10-02/Le-Pro3-evaluation-flagship-real-killer_658509.shtml"  title="乐 Pro3 评测：旗舰杀手是否名副其实？">乐 Pro3 评测：旗舰杀手是否名副其实？</a></p></li>
			            </ul>
        </div>		<!--<div class="riss">
			<h2><b>订阅锋观点</b></h2>
			<p class="hint">输入email即可立即订阅锋观点，了解更多好玩资讯。</p>
			<div class="enterEmail"><input type="text" value="请输入您的email地址" /><a class="btn" href="javascript:;">Go</a></div>
		</div>-->
<div class="weixin" id="qr_weixin">
	<ul>
		<li><p>威锋客户端</p><img src="picture/news4.jpg" height="80" width="80" alt="" /></li>
		<li><p>用微信扫我</p><img src="picture/news5.jpg" height="80" width="80" alt="" /></li>                
	</ul>
</div>	</div>
</div>
<!--返回顶部-->
<a href="javascript:;" class="goTopBtn" title="返回顶部">返回顶部</a>
<div class="footer_wrap clearfix">
 
<div class="wrap footer">
	<div class="links">
		<ul>

			<li><a href="http://bbs.feng.com/mobile-news-app-2.html" target="_blank">触屏版</a></li>
			<li class="line"><span>|</span></li>

			<li><a href="http://www.feng.com/about/" target="_blank">关于我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="http://www.feng.com/about/about/Contact_Us.shtml" target="_blank">联系我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="http://www.feng.com/app/cooperation_media.html" target="_blank">商务合作</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="http://www.feng.com/about/about/Events.shtml" target="_blank">大事记</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="http://www.feng.com/app/egal.html" target="_blank">法律条款</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="http://www.feng.com/app/about_us_english.html" target="_blank">About Us</a></li>
		</ul>
	</div>
	<div class="copyright">
		<p>Copyright 2007-2016 © Joyslink Inc. All rights reserved 保留所有权利</p>
	</div>
	<div class="our_services">
		<div class="title"><h3>威锋旗下产品</h3><i class="f_l"></i><i class="f_r"></i></div>
		<ul>
			<li><a href="http://www.feng.com/app/" target="_blank" rel="nofollow"><i class="icon bbsapp"><b></b></i><i class="circle"></i><span>威锋客户端</span></a></li>
			<li><a href="http://g.feng.com/" target="_blank"><i class="icon wegame"><b></b></i><i class="circle"></i><span>威锋游戏</span></a></li>
			<li><a href="http://www.fengbuy.com/" target="_blank"><i class="icon fengbuy"><b></b></i><i class="circle"></i><span>威锋商城</span></a></li>
			<li><a href="http://apt.feng.com/" target="_blank"><i class="icon cydia"><b></b></i><i class="circle"></i><span>威锋源</span></a></li>
			<li><a href="http://www.wper.com/" target="_blank"><i class="icon wper"><b></b></i><i class="circle"></i><span>威智网</span></a></li>
			<!-- <li><a href="http://itunes.apple.com/us/app/wei-feng-mi-bao/id448274644?l=es&mt=8" target="_blank"><i class="icon wekey"><b></b></i><i class="circle"></i><span>威锋密保</span></a></li> -->
		</ul>
	</div>
	<div class="our_image"><em>Hi~我是威威！</em><img src="picture/weiwei.png" height="184" width="310" alt="威威" class="hide"></div>
	<div class="safe_info">
		<div class="safe_links">
			<a href="http://www.sznet110.gov.cn/" class="safe_1" target="_blank" rel="nofollow"><span>深圳网络警察报警平台</span></a>
			<a href="http://www.miitbeian.gov.cn/" class="safe_2" target="_blank"><span>经营性网站备案信息</span></a>
			<a href="http://www.sznet110.gov.cn/webrecord/innernet/Welcome.jsp?bano=4403101901155" class="safe_3" target="_blank"><span>公安信息安全网络监察</span></a>
			<!-- <a href="javascript:;" onclick="window.location.href='https://ss.knet.cn/verifyseal.dll?sn=2010092800100002461&amp;ct=df&amp;pa=840515'" class="safe_4" id="l4" rel="external nofollow">可信网站</a> -->
			<!-- <a href="http://www.zx110.org/picp/?sn=310100101606" class="safe_5" target="_blank" rel="nofollow"><span>310100101606</span></a> -->
		</div>
		<p><a style="color:#6e6e6e" target="_blank" href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=31011502001229">沪公网安备 31011502001229号</a> 丨 沪ICP备14001835号-1 丨 新三板上市公司威锋科技（836555）</p><!-- 备案号 丨 深公安网监备案号 4403101901155-->
		<p>增值电信业务经营许可证：<a href="http://www.feng.com/about/certificate.shtml" target="_blank">粤B2-20130239</a>      <a href="http://www.feng.com/about/Guangdong_wangwen_20140683-283_No._594914.shtml">粤网文[2014]0683-283号</a>   Powered by Discuz! </p>
	</div>
</div>
<script src='js/c.php' language='JavaScript'></script>
<script type="text/javascript">
(function(){
try{
	var bp = document.createElement('script');
	bp.src = '//push.zhanzhang.baidu.com/push.js';
	var s = document.getElementsByTagName("script")[0];
	s.parentNode.insertBefore(bp, s);
	jQuery(function($){
		//顶部导航
		$('#globar_topbar_links').scrollNav();
		//返回顶部
		$('.goTopBtn').goBackTop();
	    //搜索框效果
	    $('.searchBox').searchAnimate();
	});
}catch(e){}
})();
</script></div>
<script type="text/javascript">

jQuery(function($){
	$.listPage({
		createHtml:"view",
		ajaxUrl:"/publish/content.php?id=616370&Custom1=37",
		pageCount:76,
		item:function(o){
			return o.children("li");
		}
	}).scroll();
});
</script>
</body>
</html>