<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="/favicon.ico" type="/web3/Public/image/x-icon">
<!-- 给网站添加一个icon图标 -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="apple-itunes-app" content="app-id=981437434" />

<link rel="alternate" type="application/rss+xml" title="Feng新闻RSS" href="">

<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/common.css" />
<script type="text/javascript" src="/web3/Public/js/js_index/jquery-1.7.min.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_index/common.js"></script>

</head>
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/style.css" />
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/feng_index.css" />
<script type="text/javascript" src="/web3/Public/js/js_index/index.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_index/jquery.marquee.min.js"></script>
<body>
<!-- Global Topbar -->
<div class="global_topbar_wrap">
	<div id="global_topbar" class="global_topbar">
		<div class="wrap inner">
			<div class="services" id="global_topbar_services">
				<ul>
				<li class="current"><a href="#"><i class="weiphone"></i>Feng综合网</a></li>				
				<li><a href=""><i class="wegame"></i>F新闻</a></li>						
				<li><a href=""><i class="fengbuy"></i>F论坛</a></li>
					
				<li><a href=""><i class="fengbuy"></i>建设中</a></li>
				
				<li><a href=""><i class="money"></i>扫码点击</a></li>								
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>
			<div class="links" id="globar_topbar_links">
				<ul>
					<li><a href="" target="_blank">首页</a></li>
					<li><a href="" target="_blank">Feng新闻</a></li>
					<li><a href="" target="_blank">Feng论坛</a></li>
				</ul>
				<div class="focus"><em></em></div>
			</div>
			
			<div class="client" id="global_topbar_client">
				<ul>
					<li class="current"><a href="#"><i></i>手机APP</a></li>					
					<div class="codeBox">
						<h3>扫一扫下载APP</h3>
						<b><img src="/web3/Public/picture/fengcode.jpg" width="94" height="94" alt="Feng二维码" /></b>
					</div>
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>

<!-- 弹框开始 -->
			<div class="user_menu" id="global_user_menu">
				<ul>
					<?php if($_SESSION['username']!= null): ?><li class="face">
						<img src="/web3/Public/picture/<?php echo ($_SESSION['username']['uface']); ?>" height="30" width="30" style="margin-top:-5px;">
						</li>
						<li class="face">
							<b style="font-size:15px"><?php echo ($_SESSION['username']['uname']); ?></b>
						</li>
						<li>
							<a class="ref" href="<?php echo U('Action/stop?model=index&controller=index');?>" title="注销">
								<i class="logout"></i>
								<span class="label">注销</span>
							</a>
						</li>
				        <?php else: ?>
							<li><a data-toggle="modal" href="#login-modal"><span>登录</span></a></li>
							<li><a data-toggle="modal" href="#signup-modal"><span>立即注册</span></a></li><?php endif; ?>
					<!-- <li><a data-toggle="modal" href="#forgetform"><span>找回密码</span></a></li> -->
				</ul>				
				<ul style="display:none;">
					<li class="face">
						<img src="/web3/Public/picture/<?php echo ($_SESSION['username']['uface']); ?>" height="30" width="30" style="margin-top:-5px;">
					</li>
					<li class="face">
						<b style="font-size:15px"><?php echo ($_SESSION['username']['uname']); ?></b>
					</li>
					<li>
						<a class="ref" href="<?php echo U('Action/stop?model=index&controller=index');?>" title="注销">
							<i class="logout"></i>
							<span class="label">注销</span>
						</a>
					</li>
				</ul>
			
			</div>
			<script>
			    function select(){
			        var username = document.form1.uname.value;
			        var userpass = document.form1.password.value;
			        var temp;
			        $.ajax({
				        type:'POST',
				        url:"<?php echo U('Action/select');?>",
				        async:false,
          				data:{name:username,pass:userpass},
          				success:function(data){
          					console.log(data);
          					if(data == 1){
					              alert('用户名错误');
					              temp = 1;
					              
					            } else if(data == 2){
					              alert('密码错误');
					              temp = 2;
					          }
          				}
			    	})

			    	if(temp == 1 || temp == 2 ){
					          return false;
					        }
			    }
			</script>
<!-- 弹框结束 -->

		<!-- <script type="text/javascript" src="/web3/Public/js/js_index/jquery.autocomplete.min.ajax.js"></script>
		<div class="searchBtn">
			<span id="quickSearchBtn"></span>
			<div class="headerSearch" id="headerSearch">
				<div class="searchBoxtop">
					<form id="searchform_top" action="http://s.feng.com/search.php" method="get" >
						<input id="top_keyword" name="srchtxt" type="text" value="搜索其实很简单^ ^">
						<input type="hidden" name="srchmod" value="all">
					</form>	
				</div>
			</div>
		</div>	 -->
		</div>
	</div>
</div>

<!-- 弹框start -->
<div class="modal" id="login-modal">
	<a class="close" data-dismiss="modal">×</a>
	<h1>登录</h1>
	
	<!-- 登录开始 -->
	<div class="contact" >
		<form action="<?php echo U('Index/index');?>" name="form1" onsubmit="return select()" method="POST">
			<ul>
				<li>
					<label>用户名：</label>
					<input type="text" name="uname" placeholder="请输入用户名" id='yourname' onblur="checkname()" value="" required/><span class="tips" id="divname2">长度1~12个字符</span>
				</li>

				<li>
					<label>密码：</label>
					<input type="password" name="password" placeholder="请输入您的密码" onblur="checkpsd5()" required/><span class="tips" id="divpassword0">密码必须由字母和数字组成</span>
				</li>
			</ul>
			<b class="btn"><input type="submit" value="登录"/>
			<input type="reset" value="取消"/></b>
		</form>
	</div>
</div>

<div class="modal" id="signup-modal">
	<a class="close" data-dismiss="modal">×</a>
	<h1>注册</h1>
	<div class="contact" >
	
	<!-- 注册开始 -->
	
	<form name="form2" action="<?php echo U('Action/add');?>" method="post">
		<ul>
			<li>
				<label>用户名：</label>
				<input type="text" name="uname" placeholder="请输入用户名"  onblur="checkna()" value="" /><span class="tips" id="divname">长度3~12个含数字和字母字符</span>
			</li>

			<li>
				<label>性别：</label>
				<input type="radio" name="sex" id="1" value="1">男&nbsp;&nbsp;
				<input type="radio" name="sex" id="2" value="2">女&nbsp;&nbsp;
				<input type="radio" name="sex" id="0" value="0">保密
			</li>

			<li>
				<label>密码：</label>
				<input type="password" name="password" placeholder="请输入您的密码" onblur="checkpsd1()" value="" required/><span class="tips" id="divpassword1">密码必须由字母和数字组成</span>
			</li>

			<li>
				<label>确认密码：</label>
				<input type="password" name="yourpass2" placeholder="请再次输入您的密码" onBlur="checkpsd2()" value="" required/><span class="tips" id="divpassword2">两次密码需要相同</span>
			</li>

			<li>
				<label>电子邮箱：</label>
				<input type="text" name="email" placeholder="请输入您的邮箱" onBlur="checkmail()" required/><span class="tips" id="divmail">请输入您的邮箱地址</span>
			</li>

			<li>
				<label>手机号：</label>
				<input type="text" name="yourphone" placeholder="请输入您的手机联系方式" onBlur="checkphone()" required/><span class="tips" id="divphone">以便帐号丢失后找回</span>
			</li>

			<li>
		      	<div id="distpicker5">
		      	<label>请选择所在地:</label>
			        <div class="form-group">
			          <select class="form-control" id="province10">
				          <option value="" data-code="">—— 省 ——</option>
				          <option value="北京市" data-code="110000">北京市</option>
				          <option value="天津市" data-code="120000">天津市</option>
				          <option value="河北省" data-code="130000">河北省</option>
				          <option value="山西省" data-code="140000">山西省</option>
				          <option value="内蒙古自治区" data-code="150000">内蒙古自治区</option>
				          <option value="辽宁省" data-code="210000">辽宁省</option>
				          <option value="吉林省" data-code="220000">吉林省</option>
				          <option value="黑龙江省" data-code="230000">黑龙江省</option>
				          <option value="上海市" data-code="310000">上海市</option>
				          <option value="江苏省" data-code="320000">江苏省</option>
				          <option value="浙江省" data-code="330000">浙江省</option>
				          <option value="安徽省" data-code="340000">安徽省</option>
				          <option value="福建省" data-code="350000">福建省</option>
				          <option value="江西省" data-code="360000">江西省</option>
				          <option value="山东省" data-code="370000">山东省</option>
				          <option value="河南省" data-code="410000">河南省</option>
				          <option value="湖北省" data-code="420000">湖北省</option>
				          <option value="湖南省" data-code="430000">湖南省</option>
				          <option value="广东省" data-code="440000">广东省</option>
				          <option value="广西壮族自治区" data-code="450000">广西壮族自治区</option>
				          <option value="海南省" data-code="460000">海南省</option>
				          <option value="重庆市" data-code="500000">重庆市</option>
				          <option value="四川省" data-code="510000">四川省</option>
				          <option value="贵州省" data-code="520000">贵州省</option>
				          <option value="云南省" data-code="530000">云南省</option>
				          <option value="西藏自治区" data-code="540000">西藏自治区</option>
				          <option value="陕西省" data-code="610000">陕西省</option>
				          <option value="甘肃省" data-code="620000">甘肃省</option>
				          <option value="青海省" data-code="630000">青海省</option>
				          <option value="宁夏回族自治区" data-code="640000">宁夏回族自治区</option>
				          <option value="新疆维吾尔自治区" data-code="650000">新疆维吾尔自治区</option>
				          <option value="台湾省" data-code="710000">台湾省</option>
				          <option value="香港特别行政区" data-code="810000">香港特别行政区</option>
				          <option value="澳门特别行政区" data-code="820000">澳门特别行政区</option>
			          </select>				    
			          <select class="form-control" id="city10">
			          	<option value="" data-code="">—— 市 ——</option>
			          </select>
			          <select class="form-control" id="district10">
			          	<option value="" data-code="">—— 区 ——</option>
			          </select>
			          <div>
			          <br/>
			          <label>请输入具体地址:</label>
			          <input type="text" name="address" placeholder="请输入具体地址" onblur="checkaddress()" required/><span class="tips" id="divaddress">长度1~30个字符</span>
			          </div>
			    </div>
		    </li>
		</ul>
			<b class="btn"><input type="submit" value="提交"/>
			<input type="reset" value="取消"/></b>
	</form>
	</div>
</div>
<script type="text/javascript">
//验证登录用户名
 	function checkname(){
		var na=document.form1.uname.value;
		// console.log(na);
	  	if( na.length <1 || na.length >12)  
  		{  	
  			divname.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
  		     
  		}else{  
  		    divname.innerHTML='<font class="tips_true">输入正确</font>';
  		   
  		}  
  	}

//验证登录密码 
	function checkpsd(){    
		var psd1=document.form1.password.value;  
		var flagZM=false ;
		var flagSZ=false ; 
		var flagQT=false ;
		console.log(psd1);
		if(psd1.length<6 || psd1.length>12){  
			alert(121); 
			divpassword1.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
		}else{   
			for(i=0;i < psd1.length;i++)   
				{    
					if((psd1.charAt(i) >= 'A' && psd1.charAt(i)<='Z') || (psd1.charAt(i)>='a' && psd1.charAt(i)<='z')) 
					{   
						flagZM=true;
					}
					else if(psd1.charAt(i)>='0' && psd1.charAt(i)<='9')    
					{ 
						flagSZ=true;
					}else    
					{ 
						flagQT=true;
					}   
				}   
				if(!flagZM||!flagSZ||flagQT){
				divpassword1.innerHTML='<font class="tips_false">密码必须是字母数字的组合</font>'; 
				 
				}else{
					
				divpassword1.innerHTML='<font class="tips_true">输入正确</font>';
				 
				}  
			 
			}	
	}

//验证注册用户名
 	function checkna(){
		var na=document.form2.uname.value;
		// console.log(na);
	  	if( na.length <1 || na.length >12)  
  		{  	
  			divname.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
  		     
  		}else{  
  		    divname.innerHTML='<font class="tips_true">输入正确</font>';
  		   
  		}  
  	}

//验证注册密码 
	function checkpsd1(){    
		var psd1=document.form2.password.value;  
		var flagZM=false ;
		var flagSZ=false ; 
		var flagQT=false ;
		console.log(psd1);
		if(psd1.length<3 || psd1.length>12){   
			divpassword1.innerHTML='<font class="tips_false">长度必须3~12个字符</font>';
		}else{   
			for(i=0;i < psd1.length;i++)   
				{    
					if((psd1.charAt(i) >= 'A' && psd1.charAt(i)<='Z') || (psd1.charAt(i)>='a' && psd1.charAt(i)<='z')) 
					{   
						flagZM=true;
					}
					else if(psd1.charAt(i)>='0' && psd1.charAt(i)<='9')    
					{ 
						flagSZ=true;
					}else    
					{ 
						flagQT=true;
					}   
				}   
				if(!flagZM||!flagSZ||flagQT){
				divpassword1.innerHTML='<font class="tips_false">密码必须是字母数字的组合</font>'; 
				 
				}else{
					
				divpassword1.innerHTML='<font class="tips_true">输入正确</font>';
				 
				}  
			 
			}	
	}

//验证注册确认密码 
	function checkpsd2(){ 

		if(document.form2.yourpass2.value!=document.form2.password.value || document.form2.yourpass2.value=='') { 
		     divpassword2.innerHTML='<font class="tips_false">您两次输入的密码不一样</font>';
		} else { 
		     divpassword2.innerHTML='<font class="tips_true">输入正确</font>';
		}
	}

//验证注册邮箱		
	function checkmail(){
		var apos=document.form2.email.value.indexOf("@");
		var dotpos=document.form2.email.value.lastIndexOf(".");
		if (apos<1||dotpos-apos<2) 
		  {
		  	divmail.innerHTML='<font class="tips_false">输入错误</font>' ;
		  }
		else {
			divmail.innerHTML='<font class="tips_true">输入正确</font>' ;
		}
	}

//验证注册地址		
	function checkaddress(){
		var address=document.form2.address.value;
	  	if( address.length <1 || address.length >30){ 
	  		divaddress.innerHTML='<font class="tips_false">长度必须1~30个字符</font>';  		     
  		}else{  
  		    divaddress.innerHTML='<font class="tips_true">输入正确</font>';  		   
  		}  
  	}
</script>
<script src="/web3/Public/js/js_index_headclick/jquery.min.js"></script>
<script src="/web3/Public/js/js_index_headclick/distpicker.data.js"></script>
<script src="/web3/Public/js/js_index_headclick/distpicker.js"></script>
<script src="/web3/Public/js/js_index_headclick/main.js"></script>
<div class="modal" id="forgetform">
	<a class="close" data-dismiss="modal">×</a>
	<h1>忘记密码</h1>
	<form class="forgot-form" method="post" action="">
		<input name="email" value="" placeholder="注册邮箱：">
		<div class="clearfix"></div>
		<input type="submit" name="type" class="forgot button-blue" value="发送重设密码邮件">
	</form>
</div>
<script type="text/javascript" src="/web3/Public/js/js_index_headclick/modal.js"></script>
<!-- 弹框end -->


<!-- 继承开始 -->


<link rel="alternate" type="application/rss+xml" title="威锋网新闻RSS" href="http://www.feng.com/rss.xml" />
<title>专题_iPhone,,iPad,MAC及苹果系列产品专题站_威锋网</title>
<meta name="keywords" content="iPhone4s,iPhone4,iPhone5,iPad,iPad2,iPhone,3G iPhone,iPhone中文网">

<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_news/List/common.css" />

<script type="text/javascript" src="/web3/Public/js/js_news/List/common.js"></script>

<script type="text/javascript">
(function(){
if( document.referrer && (
		document.referrer.match(/bbs\.feng\.com\/mobile\-.+\.html/i) ||
		document.referrer.match(/bbs\.feng\.com\/plugin\.php/i)
	) ){
	return ds.setCookie('view_origin', 1, 1);
}
if( navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i) && ds.getCookie('view_origin') != 1 ){
	location.replace('http://bbs.feng.com/mobile-news-app-10.html');
}
})();
</script>
</head><link rel="stylesheet" type="text/css" href="/web3/Public/css/css_news/List/feng.css" />
<script type="text/javascript" src="/web3/Public/js/js_news/List/jquery.easing-1.3.min.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_news/List/addele.js"></script><body>

<!-- Global Topbar -->

<script type="text/javascript">
$("#top_keyword").autocomplete('http://s.feng.com/api/autocomplateAjax.php',{
		max: 12,
		minChars: 1, 
		width: 306,
		scrollHeight: 300,
		matchContains: true,
		autoFill: false,
		dataType: 'jsonp', 
		extraParams: {
			searchtype: 'thread' 		//应用类型
		},			 
		parse: function(rdata){	
			var rows = [];
			for(var i=0; i<rdata.length; i++){
				rows[i] = { 
					data:rdata, 
					value:rdata[i], 
					result:rdata[i]
				}; 
			}
			return rows;	
		}
	}).result(function(event, data, formatted) {
		$("#searchform_top").submit();
	});
</script>  		</div>
	</div>
</div>
<script type="text/javascript" src="/web3/Public/js/js_news/List/topbarnav.js"></script><script type="text/javascript">
!(function(){var setUser=function(){return this.userInfo=!1,this.callback=[],this};setUser.prototype={setUserInfo:function(a){return this.userInfo=a,this.run()},run:function(){var a=this;return $.each(this.callback,function(b,c){c.call(a,a.userInfo)}),this},addCallback:function(a){return this.callback.push(a),this.run()}},$.extend({setUser:new setUser});})();
$.getScript("http://www.feng.com/images_v4/js/topbar.js",function(){return $.getScript("http://bbs.feng.com/newsheader.php?r="+(new Date().getTime()));});
(function(){
var ref="";
$("#global_user_menu .ref").each(function(){
	var o=$(this);o.attr("href",o.attr("href")+ref);
});
})();
$("#navigator li:eq()").addClass("current");
</script>
<div class="headTopBox" style="height:100px">
    <div class="wrap headTopBoxCon">
        
        <a href="http://www.feng.com" title="威锋网"><i class="logo"></i><i class="news_ico"></i><span>专题</span></a>
     <div class="searchBox">
	<form id="searchform" action="http://s.feng.com/search.php" method="get" >
	<input id="g_keyword" name="srchtxt" type="text" value="搜索其实很简单^ ^">
	<i></i>
	<input type="hidden" name="srchmod" value="all">
	</form>
</div>
<script type="text/javascript">
$("#g_keyword").autocomplete('http://s.feng.com/api/autocomplateAjax.php',{
		max: 12,
		minChars: 1, 
		width: 306,
		scrollHeight: 300,
		matchContains: true,
		autoFill: false,
		dataType: 'jsonp', 
		extraParams: {
			searchtype: 'thread' 		//应用类型
		},			 
		parse: function(rdata){	
			var rows = [];
			for(var i=0; i<rdata.length; i++){
				rows[i] = { 
					data:rdata, 
					value:rdata[i], 
					result:rdata[i]
				}; 
			}
			return rows;	
		}
	}).result(function(event, data, formatted) {
		$("#searchform").submit();
	});
</script>           
    </div>    
</div>

<div class="main wrap clearfix">
<!--hot_spots-->
    <div class="special_left">
        <div class="special_content" id="data_list">
		
            <ul>
				<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$n): $mod = ($i % 2 );++$i;?><li class="special_list_img">

                    <a href="<?php echo U('Newsdetail/index');?>" target="_blank">
						<img src="/web3/Public/picture/img201607292329270.jpg" alt="关于iOS 10：你需要了解的30个新特性" />
					</a>
                    <div class="special_des">
                        <p class="time"><strong><?php echo ($n["addtime"]); ?></strong></p>
                        <h1><a href="<?php echo U('Newsdetail/index');?>" target="_blank"><strong><?php echo ($n["newstitle"]); ?></strong></a></h1>
                        <p class="txt"><i></i><strong><?php echo ($n["simcontent"]); ?></strong><a href="<?php echo U('Newsdetail/index');?>" target="_blank">【详细】</a></p>
                    </div>
                </li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>







          <!--      <li class="special_list_img">
                    <a href="http://www.feng.com/Topic/ppmoney.shtml" target="_blank">
						<img src="/web3/Public/picture/img201609192257380.jpg" alt="iPhone7 首发即拿, 抢先体验" />
					</a>
                    <div class="special_des">
                        <p class="time">2016.09.19</p>
                        <h1><a href="http://www.feng.com/Topic/ppmoney.shtml" target="_blank">iPhone7 首发即拿, 抢先体验</a></h1>
                        <p class="txt"><i></i>iPhone7 ! iPhone7 ! iPhone7 !三重礼震撼来袭, 投资再小,壕礼不少, 尽在PP理财！<a href="http://www.feng.com/Topic/ppmoney.shtml" target="_blank">【详细】</a></p>
                        <div class="keybox">

                            <a href="http://www.feng.com/Topic/ppmoney.shtml" target="_blank">PPmoney</a>
                            <a href="http://www.feng.com/Topic/ppmoney.shtml" target="_blank">推广</a>
                        </div>
                    </div>
                </li>
                <li class="special_list_img">
                    <a href="http://www.feng.com/Topic/Apple_201609.shtml" target="_blank">
						<img src="/web3/Public/picture/img201609051038550.jpg" alt="2016苹果秋季发布会 - 威锋网全程专题报道" />
					</a>
                    <div class="special_des">
                        <p class="time">2016.09.08</p>
                        <h1><a href="http://www.feng.com/Topic/Apple_201609.shtml" target="_blank">2016苹果秋季发布会 - 威锋网全程专题报道</a></h1>
                        <p class="txt"><i></i>北京时间9月8日凌晨，苹果在美国旧金山比尔·格雷厄姆市政大礼堂召开发布会，iPhone 7/7 Plus、二代 Apple Watch 以及 AirPods 无线耳机纷纷亮相，威锋网全程直播报道。<a href="http://www.feng.com/Topic/Apple_201609.shtml" target="_blank">【详细】</a></p>
                        <div class="keybox">
                            <a href="http://www.feng.com/Topic/Apple_201609.shtml" target="_blank">苹果</a>
                            <a href="http://www.feng.com/Topic/Apple_201609.shtml" target="_blank">发布会</a>
                            <a href="http://www.feng.com/Topic/Apple_201609.shtml" target="_blank">直播</a>
                        </div>
                    </div>
                </li>
                <li class="special_list_img">
                    <a href="http://www.feng.com/Topic/appleyearbook/2016-08-12/The-iPhone-7-selection_654338.shtml" target="_blank">
						<img src="/web3/Public/picture/img201608301033020.png" alt="iPhone 7传闻精选：不能错过的“或”“曝”“传”" />
					</a>
                    <div class="special_des">
                        <p class="time">2016.09.01</p>
                        <h1><a href="http://www.feng.com/Topic/appleyearbook/2016-08-12/The-iPhone-7-selection_654338.shtml" target="_blank">iPhone 7传闻精选：不能错过的“或”“曝”“传”</a></h1>
                        <p class="txt"><i></i>新一代iPhone的传闻就像一套连续剧，从大致的轮廓到逐渐明朗的外形、及愈发详尽的配置，逐渐地你会发现它离我们越来越近。<a href="http://www.feng.com/Topic/appleyearbook/2016-08-12/The-iPhone-7-selection_654338.shtml" target="_blank">【详细】</a></p>
                        <div class="keybox">
                            <a href="http://www.feng.com/Topic/appleyearbook/2016-08-12/The-iPhone-7-selection_654338.shtml" target="_blank">iPhone 7</a>
                            <a href="http://www.feng.com/Topic/appleyearbook/2016-08-12/The-iPhone-7-selection_654338.shtml" target="_blank">传闻</a>
                            <a href="http://www.feng.com/Topic/appleyearbook/2016-08-12/The-iPhone-7-selection_654338.shtml" target="_blank">苹果年鉴</a>
                        </div>
                    </div>
                </li>
                <li class="special_list_img">
                    <a href="http://www.feng.com/Topic/ESR.shtml" target="_blank">
						<img src="/web3/Public/picture/img201609011719070.jpg" alt="ESR亿色-iPhone7绝配CP" />
					</a>
                    <div class="special_des">
                        <p class="time">2016.09.01</p>
                        <h1><a href="http://www.feng.com/Topic/ESR.shtml" target="_blank">ESR亿色-iPhone7绝配CP</a></h1>
                        <p class="txt"><i></i>万事俱备，只等疯来。iPhone7还没面世，ESR亿色就已经等不及了。观赏iPhone7绝配CP大片，赢iPhone7！<a href="http://www.feng.com/Topic/ESR.shtml" target="_blank">【详细】</a></p>
                        <div class="keybox">
                            <a href="http://www.feng.com/Topic/ESR.shtml" target="_blank">亿色</a>
                            <a href="http://www.feng.com/Topic/ESR.shtml" target="_blank">iPhone 7</a>
                            <a href="http://www.feng.com/Topic/ESR.shtml" target="_blank">保护壳</a>
                        </div>
                    </div>
                </li>
                <li class="special_list_img">
                    <a href="http://www.feng.com/Topic/1MORE.shtml" target="_blank">
						<img src="/web3/Public/picture/img201608291109440.jpg" alt="1MORE万魔耳机新品发布会" />
					</a>
                    <div class="special_des">
                        <p class="time">2016.08.29</p>
                        <h1><a href="http://www.feng.com/Topic/1MORE.shtml" target="_blank">1MORE万魔耳机新品发布会</a></h1>
                        <p class="txt"><i></i>这个周末来深圳现场，看4次格莱美录音大师Luca，与众发烧友展开的“大师对话”！一起探索音乐的奥秘。威锋直播9月3日敬请期待！<a href="http://www.feng.com/Topic/1MORE.shtml" target="_blank">【详细】</a></p>
                        <div class="keybox">
                            <a href="http://www.feng.com/Topic/1MORE.shtml" target="_blank">1MORE</a>
                            <a href="http://www.feng.com/Topic/1MORE.shtml" target="_blank">万魔耳机</a>
                            <a href="http://www.feng.com/Topic/1MORE.shtml" target="_blank">发布会</a>
                        </div>
                    </div>
                </li>
                <li class="special_list_img">
                    <a href="http://www.feng.com/gallery/2016-08-18/V-sting-of-beast-Explore-the-apple-retail-world-trade-center_654816.shtml" target="_blank">
						<img src="/web3/Public/picture/img201608230943390.jpg" alt="探秘苹果纽约世贸中心零售店" />
					</a>
                    <div class="special_des">
                        <p class="time">2016.08.23</p>
                        <h1><a href="http://www.feng.com/gallery/2016-08-18/V-sting-of-beast-Explore-the-apple-retail-world-trade-center_654816.shtml" target="_blank">探秘苹果纽约世贸中心零售店</a></h1>
                        <p class="txt"><i></i>苹果纽约世贸中心零售店值得你一看，因为这或许是苹果有史以来最时尚，概念设计最抽象的一家零售店。<a href="http://www.feng.com/gallery/2016-08-18/V-sting-of-beast-Explore-the-apple-retail-world-trade-center_654816.shtml" target="_blank">【详细】</a></p>
                        <div class="keybox">
                            <a href="http://www.feng.com/gallery/2016-08-18/V-sting-of-beast-Explore-the-apple-retail-world-trade-center_654816.shtml" target="_blank">苹果</a>
                            <a href="http://www.feng.com/gallery/2016-08-18/V-sting-of-beast-Explore-the-apple-retail-world-trade-center_654816.shtml" target="_blank">零售店</a>
                            <a href="http://www.feng.com/gallery/2016-08-18/V-sting-of-beast-Explore-the-apple-retail-world-trade-center_654816.shtml" target="_blank">图集</a>
                            <a href="http://www.feng.com/gallery/2016-08-18/V-sting-of-beast-Explore-the-apple-retail-world-trade-center_654816.shtml" target="_blank">Apple Store</a>
                        </div>
                    </div>
                </li>
                <li class="special_list_img">
                    <a href="http://www.feng.com/gallery/2016-08-06/The-iPhone-7-very-ugly_653799.shtml" target="_blank">
						<img src="/web3/Public/picture/img201608061637190.png" alt="iPhone 7很难看？其实看着看着也就习惯了" />
					</a>
                    <div class="special_des">
                        <p class="time">2016.08.06</p>
                        <h1><a href="http://www.feng.com/gallery/2016-08-06/The-iPhone-7-very-ugly_653799.shtml" target="_blank">iPhone 7很难看？其实看着看着也就习惯了</a></h1>
                        <p class="txt"><i></i>也许你会说苹果一个设计用了三年，但别忘了在其它领域，一个设计的使用可能会是上百年。<a href="http://www.feng.com/gallery/2016-08-06/The-iPhone-7-very-ugly_653799.shtml" target="_blank">【详细】</a></p>
                        <div class="keybox">
                            <a href="http://www.feng.com/gallery/2016-08-06/The-iPhone-7-very-ugly_653799.shtml" target="_blank">新闻回顾</a>
                            <a href="http://www.feng.com/gallery/2016-08-06/The-iPhone-7-very-ugly_653799.shtml" target="_blank">图集</a>
                            <a href="http://www.feng.com/gallery/2016-08-06/The-iPhone-7-very-ugly_653799.shtml" target="_blank">每周新闻回顾</a>
                        </div>
                    </div>
                </li>
                <li class="special_list_img">
                    <a href="http://www.feng.com/Topic/ChinaJoy2016.shtml" target="_blank">
						<img src="/web3/Public/picture/img201607282137370.jpg" alt="ChinaJoy2016威锋网专题报道" />
					</a>
                    <div class="special_des">
                        <p class="time">2016.07.28</p>
                        <h1><a href="http://www.feng.com/Topic/ChinaJoy2016.shtml" target="_blank">ChinaJoy2016威锋网专题报道</a></h1>
                        <p class="txt"><i></i>一年一度的Chinajoy开幕了，据悉本次将有30多个国家的知名软硬件及游戏等内容参展，作品数量超过3500部，观众人数预计超过27万人次...<a href="http://www.feng.com/Topic/ChinaJoy2016.shtml" target="_blank">【详细】</a></p>
                        <div class="keybox">
                            <a href="http://www.feng.com/Topic/ChinaJoy2016.shtml" target="_blank">ChinaJoy</a>
                            <a href="http://www.feng.com/Topic/ChinaJoy2016.shtml" target="_blank">ChinaJoy2016</a>
                        </div>
                    </div>
                </li>
                <li class="special_list_img">
                    <a href="http://www.feng.com/gallery/2016-07-28/The-original-iPhone-photos-can-be-so-amazing_652943.shtml" target="_blank">
						<img src="/web3/Public/picture/img201607292329270.jpg" alt="iPhone拍的照片可以这么惊艳" />
					</a>
                    <div class="special_des">
                        <p class="time">2016.07.28</p>
                        <h1><a href="<?php echo U('Newsdetail/index');?>" target="_blank">iPhone拍的照片可以这么惊艳</a></h1>
                        <p class="txt"><i></i>你按下快门的时候，一切的感触，思想，情绪，心思，都会定格在这一刹那。美，与你同在，未曾离开。<a href="http://www.feng.com/gallery/2016-07-28/The-original-iPhone-photos-can-be-so-amazing_652943.shtml" target="_blank">【详细】</a></p>
                        <div class="keybox">
                            <a href="http://www.feng.com/gallery/2016-07-28/The-original-iPhone-photos-can-be-so-amazing_652943.shtml" target="_blank">iPhone</a>
                            <a href="http://www.feng.com/gallery/2016-07-28/The-original-iPhone-photos-can-be-so-amazing_652943.shtml" target="_blank">照片</a>
                            <a href="http://www.feng.com/gallery/2016-07-28/The-original-iPhone-photos-can-be-so-amazing_652943.shtml" target="_blank">摄影大赛</a>
                        </div>
                    </div>
                </li>
            </ul>-->
			<div class="loading" id="loading" style="display:none"><i></i>加载更多</div>
			<div class="list_pager"><div id="new_page_list" class="new_page_list"></div></div>
        </div>
    </div>
    <div class="news_right">           
		<div class="recommended_news">
            <h2 class="title">推荐<em>专题</em></h2>
            <a href="http://www.feng.com/Topic/iOS10_TOP30.shtml" class="img">
                <img src="/web3/Public/picture/504a1098img201609211102240_305__180.png" alt="关于iOS 10：你需要了解的30个新特性" />
				<div class="border_arrow"></div>
            </a>
            <h2 class="recommended_title"><a href="http://www.feng.com/Topic/iOS10_TOP30.shtml">关于iOS 10：你需要了解的30个新特性</a></h2>
            <ul>
                <li><a href="http://www.feng.com/Topic/ppmoney.shtml">iPhone7 首发即拿, 抢先体验</a></li>
                <li><a href="http://www.feng.com/Topic/Apple_201609.shtml">2016苹果秋季发布会 - 威锋网全程专题报道</a></li>
                <li><a href="http://www.feng.com/Topic/appleyearbook/2016-08-12/The-iPhone-7-selection_654338.shtml">iPhone 7传闻精选：不能错过的“或”“曝”“传”</a></li>
                <li><a href="http://www.feng.com/Topic/ESR.shtml">ESR亿色-iPhone7绝配CP</a></li>
            </ul> 
</div>    
</div>
</div>

<!--返回顶部-->
<a href="javascript:;" class="goTopBtn" title="返回顶部">返回顶部</a>
<!--footer-->

<script src='js/c.php' language='JavaScript'></script>
<script type="text/javascript">
(function(){
try{
	var bp = document.createElement('script');
	bp.src = '//push.zhanzhang.baidu.com/push.js';
	var s = document.getElementsByTagName("script")[0];
	s.parentNode.insertBefore(bp, s);
	jQuery(function($){
		//顶部导航
		$('#globar_topbar_links').scrollNav();
		//返回顶部
		$('.goTopBtn').goBackTop();
	    //搜索框效果
	    $('.searchBox').searchAnimate();
	});
}catch(e){}
})();
</script></div>
<script type="text/javascript">

jQuery(function($){
	$.listPage({
		createHtml:"special",
		ajaxUrl:"/publish/content.php?id=616370&Custom1=229",
		pageCount:119	}).scroll();
});
</script>


 
<div class="wrap footer">
	<div class="links">
		<ul>
			<li><a href="#" target="_blank">关于我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">联系我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">About Us</a></li>
		</ul>
	</div>
	<div class="copyright">
		<p>Copyright 2016-2017 © zhuruicheng. All rights reserved 保留所有权利</p>
	</div>
</div>


</div>
<noscript><a href='#' target='_blank'><img src='http://yes1.feng.com/view.php?what=zone:189&amp;n=afbf8627' border='0' alt=''></a></noscript>

</body>
</html>