<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="/favicon.ico" type="/web3/Public/image/x-icon">
<!-- 给网站添加一个icon图标 -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="apple-itunes-app" content="app-id=981437434" />

<link rel="alternate" type="application/rss+xml" title="Feng新闻RSS" href="">

<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/common.css" />
<script type="text/javascript" src="/web3/Public/js/js_index/jquery-1.7.min.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_index/common.js"></script>

</head>
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/style.css" />
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_index/feng_index.css" />
<script type="text/javascript" src="/web3/Public/js/js_index/index.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_index/jquery.marquee.min.js"></script>
<body>
<!-- Global Topbar -->
<div class="global_topbar_wrap">
	<div id="global_topbar" class="global_topbar">
		<div class="wrap inner">
			<div class="services" id="global_topbar_services">
				<ul>
				<li class="current"><a href="#"><i class="weiphone"></i>Feng综合网</a></li>				
				<li><a href=""><i class="wegame"></i>F新闻</a></li>						
				<li><a href=""><i class="fengbuy"></i>F论坛</a></li>
					
				<li><a href=""><i class="fengbuy"></i>建设中</a></li>
				
				<li><a href=""><i class="money"></i>扫码点击</a></li>								
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>
			<div class="links" id="globar_topbar_links">
				<ul>
					<li><a href="" target="_blank">首页</a></li>
					<li><a href="" target="_blank">Feng新闻</a></li>
					<li><a href="" target="_blank">Feng论坛</a></li>
				</ul>
				<div class="focus"><em></em></div>
			</div>
			
			<div class="client" id="global_topbar_client">
				<ul>
					<li class="current"><a href="#"><i></i>手机APP</a></li>					
					<div class="codeBox">
						<h3>扫一扫下载APP</h3>
						<b><img src="/web3/Public/picture/fengcode.jpg" width="94" height="94" alt="Feng二维码" /></b>
					</div>
				</ul>
				<a href="javascript:;" class="expand"><span>展开</span></a>
			</div>

<!-- 弹框开始 -->
			<div class="user_menu" id="global_user_menu">
				<ul>
					<?php if($_SESSION['username']!= null): ?><li class="face">
						<img src="/web3/Public/picture/<?php echo ($_SESSION['username']['uface']); ?>" height="30" width="30" style="margin-top:-5px;">
						</li>
						<li class="face">
							<b style="font-size:15px"><?php echo ($_SESSION['username']['uname']); ?></b>
						</li>
						<li>
							<a class="ref" href="<?php echo U('Action/stop?model=index&controller=index');?>" title="注销">
								<i class="logout"></i>
								<span class="label">注销</span>
							</a>
						</li>
				        <?php else: ?>
							<li><a data-toggle="modal" href="#login-modal"><span>登录</span></a></li>
							<li><a data-toggle="modal" href="#signup-modal"><span>立即注册</span></a></li><?php endif; ?>
					<!-- <li><a data-toggle="modal" href="#forgetform"><span>找回密码</span></a></li> -->
				</ul>				
				<ul style="display:none;">
					<li class="face">
						<img src="/web3/Public/picture/<?php echo ($_SESSION['username']['uface']); ?>" height="30" width="30" style="margin-top:-5px;">
					</li>
					<li class="face">
						<b style="font-size:15px"><?php echo ($_SESSION['username']['uname']); ?></b>
					</li>
					<li>
						<a class="ref" href="<?php echo U('Action/stop?model=index&controller=index');?>" title="注销">
							<i class="logout"></i>
							<span class="label">注销</span>
						</a>
					</li>
				</ul>
			
			</div>
			<script>
			    function select(){
			        var username = document.form1.uname.value;
			        var userpass = document.form1.password.value;
			        var temp;
			        $.ajax({
				        type:'POST',
				        url:"<?php echo U('Action/select');?>",
				        async:false,
          				data:{name:username,pass:userpass},
          				success:function(data){
          					console.log(data);
          					if(data == 1){
					              alert('用户名错误');
					              temp = 1;
					              
					            } else if(data == 2){
					              alert('密码错误');
					              temp = 2;
					          }
          				}
			    	})

			    	if(temp == 1 || temp == 2 ){
					          return false;
					        }
			    }
			</script>
<!-- 弹框结束 -->

		<!-- <script type="text/javascript" src="/web3/Public/js/js_index/jquery.autocomplete.min.ajax.js"></script>
		<div class="searchBtn">
			<span id="quickSearchBtn"></span>
			<div class="headerSearch" id="headerSearch">
				<div class="searchBoxtop">
					<form id="searchform_top" action="http://s.feng.com/search.php" method="get" >
						<input id="top_keyword" name="srchtxt" type="text" value="搜索其实很简单^ ^">
						<input type="hidden" name="srchmod" value="all">
					</form>	
				</div>
			</div>
		</div>	 -->
		</div>
	</div>
</div>

<!-- 弹框start -->
<div class="modal" id="login-modal">
	<a class="close" data-dismiss="modal">×</a>
	<h1>登录</h1>
	
	<!-- 登录开始 -->
	<div class="contact" >
		<form action="<?php echo U('Index/index');?>" name="form1" onsubmit="return select()" method="POST">
			<ul>
				<li>
					<label>用户名：</label>
					<input type="text" name="uname" placeholder="请输入用户名" id='yourname' onblur="checkname()" value="" required/><span class="tips" id="divname2">长度1~12个字符</span>
				</li>

				<li>
					<label>密码：</label>
					<input type="password" name="password" placeholder="请输入您的密码" onblur="checkpsd5()" required/><span class="tips" id="divpassword0">密码必须由字母和数字组成</span>
				</li>
			</ul>
			<b class="btn"><input type="submit" value="登录"/>
			<input type="reset" value="取消"/></b>
		</form>
	</div>
</div>

<div class="modal" id="signup-modal">
	<a class="close" data-dismiss="modal">×</a>
	<h1>注册</h1>
	<div class="contact" >
	
	<!-- 注册开始 -->
	
	<form name="form2" action="<?php echo U('Action/add');?>" method="post">
		<ul>
			<li>
				<label>用户名：</label>
				<input type="text" name="uname" placeholder="请输入用户名"  onblur="checkna()" value="" /><span class="tips" id="divname">长度3~12个含数字和字母字符</span>
			</li>

			<li>
				<label>性别：</label>
				<input type="radio" name="sex" id="1" value="1">男&nbsp;&nbsp;
				<input type="radio" name="sex" id="2" value="2">女&nbsp;&nbsp;
				<input type="radio" name="sex" id="0" value="0">保密
			</li>

			<li>
				<label>密码：</label>
				<input type="password" name="password" placeholder="请输入您的密码" onblur="checkpsd1()" value="" required/><span class="tips" id="divpassword1">密码必须由字母和数字组成</span>
			</li>

			<li>
				<label>确认密码：</label>
				<input type="password" name="yourpass2" placeholder="请再次输入您的密码" onBlur="checkpsd2()" value="" required/><span class="tips" id="divpassword2">两次密码需要相同</span>
			</li>

			<li>
				<label>电子邮箱：</label>
				<input type="text" name="email" placeholder="请输入您的邮箱" onBlur="checkmail()" required/><span class="tips" id="divmail">请输入您的邮箱地址</span>
			</li>

			<li>
				<label>手机号：</label>
				<input type="text" name="yourphone" placeholder="请输入您的手机联系方式" onBlur="checkphone()" required/><span class="tips" id="divphone">以便帐号丢失后找回</span>
			</li>

			<li>
		      	<div id="distpicker5">
		      	<label>请选择所在地:</label>
			        <div class="form-group">
			          <select class="form-control" id="province10">
				          <option value="" data-code="">—— 省 ——</option>
				          <option value="北京市" data-code="110000">北京市</option>
				          <option value="天津市" data-code="120000">天津市</option>
				          <option value="河北省" data-code="130000">河北省</option>
				          <option value="山西省" data-code="140000">山西省</option>
				          <option value="内蒙古自治区" data-code="150000">内蒙古自治区</option>
				          <option value="辽宁省" data-code="210000">辽宁省</option>
				          <option value="吉林省" data-code="220000">吉林省</option>
				          <option value="黑龙江省" data-code="230000">黑龙江省</option>
				          <option value="上海市" data-code="310000">上海市</option>
				          <option value="江苏省" data-code="320000">江苏省</option>
				          <option value="浙江省" data-code="330000">浙江省</option>
				          <option value="安徽省" data-code="340000">安徽省</option>
				          <option value="福建省" data-code="350000">福建省</option>
				          <option value="江西省" data-code="360000">江西省</option>
				          <option value="山东省" data-code="370000">山东省</option>
				          <option value="河南省" data-code="410000">河南省</option>
				          <option value="湖北省" data-code="420000">湖北省</option>
				          <option value="湖南省" data-code="430000">湖南省</option>
				          <option value="广东省" data-code="440000">广东省</option>
				          <option value="广西壮族自治区" data-code="450000">广西壮族自治区</option>
				          <option value="海南省" data-code="460000">海南省</option>
				          <option value="重庆市" data-code="500000">重庆市</option>
				          <option value="四川省" data-code="510000">四川省</option>
				          <option value="贵州省" data-code="520000">贵州省</option>
				          <option value="云南省" data-code="530000">云南省</option>
				          <option value="西藏自治区" data-code="540000">西藏自治区</option>
				          <option value="陕西省" data-code="610000">陕西省</option>
				          <option value="甘肃省" data-code="620000">甘肃省</option>
				          <option value="青海省" data-code="630000">青海省</option>
				          <option value="宁夏回族自治区" data-code="640000">宁夏回族自治区</option>
				          <option value="新疆维吾尔自治区" data-code="650000">新疆维吾尔自治区</option>
				          <option value="台湾省" data-code="710000">台湾省</option>
				          <option value="香港特别行政区" data-code="810000">香港特别行政区</option>
				          <option value="澳门特别行政区" data-code="820000">澳门特别行政区</option>
			          </select>				    
			          <select class="form-control" id="city10">
			          	<option value="" data-code="">—— 市 ——</option>
			          </select>
			          <select class="form-control" id="district10">
			          	<option value="" data-code="">—— 区 ——</option>
			          </select>
			          <div>
			          <br/>
			          <label>请输入具体地址:</label>
			          <input type="text" name="address" placeholder="请输入具体地址" onblur="checkaddress()" required/><span class="tips" id="divaddress">长度1~30个字符</span>
			          </div>
			    </div>
		    </li>
		</ul>
			<b class="btn"><input type="submit" value="提交"/>
			<input type="reset" value="取消"/></b>
	</form>
	</div>
</div>
<script type="text/javascript">
//验证登录用户名
 	function checkname(){
		var na=document.form1.uname.value;
		// console.log(na);
	  	if( na.length <1 || na.length >12)  
  		{  	
  			divname.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
  		     
  		}else{  
  		    divname.innerHTML='<font class="tips_true">输入正确</font>';
  		   
  		}  
  	}

//验证登录密码 
	function checkpsd(){    
		var psd1=document.form1.password.value;  
		var flagZM=false ;
		var flagSZ=false ; 
		var flagQT=false ;
		console.log(psd1);
		if(psd1.length<6 || psd1.length>12){  
			alert(121); 
			divpassword1.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
		}else{   
			for(i=0;i < psd1.length;i++)   
				{    
					if((psd1.charAt(i) >= 'A' && psd1.charAt(i)<='Z') || (psd1.charAt(i)>='a' && psd1.charAt(i)<='z')) 
					{   
						flagZM=true;
					}
					else if(psd1.charAt(i)>='0' && psd1.charAt(i)<='9')    
					{ 
						flagSZ=true;
					}else    
					{ 
						flagQT=true;
					}   
				}   
				if(!flagZM||!flagSZ||flagQT){
				divpassword1.innerHTML='<font class="tips_false">密码必须是字母数字的组合</font>'; 
				 
				}else{
					
				divpassword1.innerHTML='<font class="tips_true">输入正确</font>';
				 
				}  
			 
			}	
	}

//验证注册用户名
 	function checkna(){
		var na=document.form2.uname.value;
		// console.log(na);
	  	if( na.length <1 || na.length >12)  
  		{  	
  			divname.innerHTML='<font class="tips_false">长度必须1~12个字符</font>';
  		     
  		}else{  
  		    divname.innerHTML='<font class="tips_true">输入正确</font>';
  		   
  		}  
  	}

//验证注册密码 
	function checkpsd1(){    
		var psd1=document.form2.password.value;  
		var flagZM=false ;
		var flagSZ=false ; 
		var flagQT=false ;
		console.log(psd1);
		if(psd1.length<3 || psd1.length>12){   
			divpassword1.innerHTML='<font class="tips_false">长度必须3~12个字符</font>';
		}else{   
			for(i=0;i < psd1.length;i++)   
				{    
					if((psd1.charAt(i) >= 'A' && psd1.charAt(i)<='Z') || (psd1.charAt(i)>='a' && psd1.charAt(i)<='z')) 
					{   
						flagZM=true;
					}
					else if(psd1.charAt(i)>='0' && psd1.charAt(i)<='9')    
					{ 
						flagSZ=true;
					}else    
					{ 
						flagQT=true;
					}   
				}   
				if(!flagZM||!flagSZ||flagQT){
				divpassword1.innerHTML='<font class="tips_false">密码必须是字母数字的组合</font>'; 
				 
				}else{
					
				divpassword1.innerHTML='<font class="tips_true">输入正确</font>';
				 
				}  
			 
			}	
	}

//验证注册确认密码 
	function checkpsd2(){ 

		if(document.form2.yourpass2.value!=document.form2.password.value || document.form2.yourpass2.value=='') { 
		     divpassword2.innerHTML='<font class="tips_false">您两次输入的密码不一样</font>';
		} else { 
		     divpassword2.innerHTML='<font class="tips_true">输入正确</font>';
		}
	}

//验证注册邮箱		
	function checkmail(){
		var apos=document.form2.email.value.indexOf("@");
		var dotpos=document.form2.email.value.lastIndexOf(".");
		if (apos<1||dotpos-apos<2) 
		  {
		  	divmail.innerHTML='<font class="tips_false">输入错误</font>' ;
		  }
		else {
			divmail.innerHTML='<font class="tips_true">输入正确</font>' ;
		}
	}

//验证注册地址		
	function checkaddress(){
		var address=document.form2.address.value;
	  	if( address.length <1 || address.length >30){ 
	  		divaddress.innerHTML='<font class="tips_false">长度必须1~30个字符</font>';  		     
  		}else{  
  		    divaddress.innerHTML='<font class="tips_true">输入正确</font>';  		   
  		}  
  	}
</script>
<script src="/web3/Public/js/js_index_headclick/jquery.min.js"></script>
<script src="/web3/Public/js/js_index_headclick/distpicker.data.js"></script>
<script src="/web3/Public/js/js_index_headclick/distpicker.js"></script>
<script src="/web3/Public/js/js_index_headclick/main.js"></script>
<div class="modal" id="forgetform">
	<a class="close" data-dismiss="modal">×</a>
	<h1>忘记密码</h1>
	<form class="forgot-form" method="post" action="">
		<input name="email" value="" placeholder="注册邮箱：">
		<div class="clearfix"></div>
		<input type="submit" name="type" class="forgot button-blue" value="发送重设密码邮件">
	</form>
</div>
<script type="text/javascript" src="/web3/Public/js/js_index_headclick/modal.js"></script>
<!-- 弹框end -->


<!-- 继承开始 -->



<title>原来iPhone拍的照片可以这么惊艳 iPhone,摄影,作品,照片 _Feng网</title>
<meta name="description" content="每一张照片，都是时空的缩影，时代的背景，当你按下快门的时候，一切的感触，思想，情绪，心思，都会定格在这一刹那。 " />
<meta name="keywords" content="iPhone,摄影,作品,照片" />
<meta name="cmsver" content="20160912" />
<link rel="stylesheet" type="text/css" href="/web3/Public/css/css_news/detail/common.css" />

<script type="text/javascript" src="/web3/Public/js/js_news/detail/common.js"></script>

<script type="text/javascript">
(function(){
if( document.referrer && (
		document.referrer.match(/bbs\.feng\.com\/mobile\-.+\.html/i) ||
		document.referrer.match(/bbs\.feng\.com\/plugin\.php/i)
	) ){
	return ds.setCookie('view_origin', 1, 1);
}
if( navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i) && ds.getCookie('view_origin') != 1 ){
	location.replace('http://bbs.feng.com/mobile-news-read-0-652943.html');
}
})();
</script>
</head><link rel="stylesheet" type="text/css" href="/web3/Public/css/css_news/detail/feng.css" /><link rel="stylesheet" type="text/css" href="/web3/Public/css/css_news/detail/201508tuji_base.css" /><script type="text/javascript" src="/web3/Public/js/js_news/detail/jquery.autofixed.js"></script><script type="text/javascript" src="/web3/Public/js/js_news/detail/tuji.js"></script>
<body>
<!-- Global Topbar -->

<script type="text/javascript">
$("#top_keyword").autocomplete('http://s.feng.com/api/autocomplateAjax.php',{
		max: 12,
		minChars: 1, 
		width: 306,
		scrollHeight: 300,
		matchContains: true,
		autoFill: false,
		dataType: 'jsonp', 
		extraParams: {
			searchtype: 'thread' 		//应用类型
		},			 
		parse: function(rdata){	
			var rows = [];
			for(var i=0; i<rdata.length; i++){
				rows[i] = { 
					data:rdata, 
					value:rdata[i], 
					result:rdata[i]
				}; 
			}
			return rows;	
		}
	}).result(function(event, data, formatted) {
		$("#searchform_top").submit();
	});
</script>  		</div>
	</div>
</div>
<script type="text/javascript" src="/web3/Public/js/js_news/detail/topbarnav.js"></script><script type="text/javascript">
!(function(){var setUser=function(){return this.userInfo=!1,this.callback=[],this};setUser.prototype={setUserInfo:function(a){return this.userInfo=a,this.run()},run:function(){var a=this;return $.each(this.callback,function(b,c){c.call(a,a.userInfo)}),this},addCallback:function(a){return this.callback.push(a),this.run()}},$.extend({setUser:new setUser});})();
$.getScript("http://www.feng.com/images_v4/js/topbar.js",function(){return $.getScript("http://bbs.feng.com/newsheader.php?r="+(new Date().getTime()));});
(function(){
var ref="";
$("#global_user_menu .ref").each(function(){
	var o=$(this);o.attr("href",o.attr("href")+ref);
});
})();
$("#navigator li:eq()").addClass("current");
</script>
<div class="headTopBox" style="height:100px">
	<div class="wrap headTopBoxCon">
			<a href="http://www.feng.com" title="Feng网"><i class="logo"></i><i class="gallery_ico"></i></a>
			<div class="soruce">
				<a href="http://www.feng.com/index.shtml"> Feng网-首页 </a>				<span class="crumbs_line">&gt;</span><a href="http://www.feng.com/gallery/index.shtml"> 图集 </a>				<span class="crumbs_line">&gt;</span> 原来iPhone拍的照片可以这么惊艳 			</div>
<div class="searchBox">
	<form id="searchform" action="http://s.feng.com/search.php" method="get" >
	<input id="g_keyword" name="srchtxt" type="text" value="搜索其实很简单^ ^">
	<i></i>
	<input type="hidden" name="srchmod" value="all">
	</form>
</div>
<script type="text/javascript">
$("#g_keyword").autocomplete('http://s.feng.com/api/autocomplateAjax.php',{
		max: 12,
		minChars: 1, 
		width: 306,
		scrollHeight: 300,
		matchContains: true,
		autoFill: false,
		dataType: 'jsonp', 
		extraParams: {
			searchtype: 'thread' 		//应用类型
		},			 
		parse: function(rdata){	
			var rows = [];
			for(var i=0; i<rdata.length; i++){
				rows[i] = { 
					data:rdata, 
					value:rdata[i], 
					result:rdata[i]
				}; 
			}
			return rows;	
		}
	}).result(function(event, data, formatted) {
		$("#searchform").submit();
	});
</script>           
		</div>
</div>
<div class="wrap">
	<h1 class="tujiTitle">原来iPhone拍的照片可以这么惊艳</h1>
</div>
<div class="props wrap clearfix" style="margin: 16px auto 20px;">
	<p class="leftInfo">
		<i class="ico dateIco"></i><span class="information article_time">2016-07-29 22:22:14</span>

		<a href="http://bbs.feng.com/u.php?uid=2908717" target="_blank"><i class="ico authorIco"></i><span class="information">bqx911bqx</span></a>           
		<i class="ico fromIco"></i><span class="information">Feng网</span>
	</p>
	<ul class="rightInfo" style="float:left;">
		<li><a href="#comments"><span class="ico cmtNum"></span><span id="comment_num"></span></a></li>
		<li><a href="javascript:;"><span class="ico viewNum"></span><span id="hit_num"></span></a></li>
	</ul>
	<div style="float:right;">支持键盘 ←左 右→ 翻页</div>
</div>
<script type="text/javascript">$.getScript("http://www.feng.com/publish/counter.php?o=display_counter&Id=652943&callback=%24%28%22%23hit_num%22%29.text");</script>
<div class="wrap">
	<div class="bigPicBox">
		<ul>
	<li><img src="/web3/Public/picture/img201607281257240.png" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281215480.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281223320.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281223420.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281224530.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281225040.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281225140.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281225230.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281225330.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281226120.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281226230.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281226310.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281226390.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281226490.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281233230.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281227080.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281227150.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281227230.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281227340.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281227420.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281227490.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281229300.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281228110.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
	<li><img src="/web3/Public/picture/img201607281228240.jpg" alt="原来iPhone拍的照片可以这么惊艳"/></li>
		</ul>
		<a href="javascript:;" class="prevABox"><span class="prevBtn"><i></i></span></a>
		<a href="javascript:;" class="nextABox"><span class="nextBtn"><i></i></span></a>
	</div>
	<div class="imgTitle">
	</div>
	<div class="smallPicBox">
		<a href="javascript:;" class="smallPrevBtn" title="上一张"><i></i></a>
		<div class="smallPicUlBox">
			<ul>
				<li class="current">
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/e5a32440img201607281257240_112__82.png" alt="图1"/>
					<div class="desc">
						<span class="num-title"><em>1</em>/24</span>
						<p>　　由 IPPAWARDS 网站举办的 iPhone Photography Awards 摄影大赛今年刚好步入了第十个年头，一如既往的，今年的比赛吸引了来自全球 139 个国家和地区的 iPhone 用户参与，在举办方设置的 19 个参赛组别中，参赛者共上传了数以千计的精彩作品！<br /><br />　　今年 19 个类别的一、二、三等奖分别授予了来自各个国家和地区的摄影师，这其中包括澳大利亚、巴西、中国、智利、法国、香港、印度、意大利、菲律宾、波兰、葡萄牙、新加坡、西班牙、瑞士、瑞典、台湾、阿联酋、英国和美国。在 61 个奖项内，中国参赛者包揽了全部奖项中的 27 个，其中香港和台湾各得 2 个，在数字上充分反映 iPhone 在大中华地区的重要性。<br /><br />　　IPPAWARDS 诞生于 2007 年，自从 iPhone 首先为全球用户带来灵感、激励和互动以来，它一直都为 iPhone 摄影师提供了创意。今年 iPhone Photography Awards 年度最佳照片桂冠由来自中国的摄影师牛思远（Siyuan Niu）一举摘得，其他获奖作品也涵盖了我们日常生活的方方面面。欣赏到一张张令人心动的照片，我们才发现，原来每天陪伴左右的 iPhone 如此强大，不需要昂贵专业的器材你也能记录身边的美丽瞬间。<br /><br />　　正如法国雕塑名家奥古斯特·罗丹所说：“世界中从不缺少美，而是缺少发现美的眼睛。”美丽的事物往往是转瞬即逝的，而 iPhone 恰能成为我们另一双能留住这些美丽画面的眼睛。当你按下快门的时候，一切的感触，思想，情绪，心思，都会定格在这一刹那。美，与你同在，未曾离开。<a href="http://www.feng.com/iPhone/news/2016-07-08/_651300.shtml" target="_blank" style="color:red">『阅读详情』</a></p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/faf8e1e5img201607281215480_112__82.jpg" alt="图2"/>
					<div class="desc">
						<span class="num-title"><em>2</em>/24</span>
						<p>年度最佳／作品︰Man and the Eagle／摄影师︰Siyuan Niu（中国）<br /><br />英勇智慧的柯尔克孜人生活在新疆南部的山脉中，世代与鹰为伴。他们把鹰当作自己的孩子，并常年驯服雄鹰来帮自己捕猎。</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/2d6875c2img201607281223320_112__82.jpg" alt="图3"/>
					<div class="desc">
						<span class="num-title"><em>3</em>/24</span>
						<p>年度摄影师第一名／作品︰Modern Cathedrals／摄影师︰Patryk Kuleta（波兰）<br /><br />自波兰华沙的 Patryk Kuleta 就用 iPhone 5 为我们带来自己的作品，这也让 Kuleta 获得了年度摄影师第一名的荣誉。“我的大部分照片都拍摄自华沙，一张拍摄自斯特拉斯堡。我当时的想法是让照片更加写意，结合我的平面设计和绘画背景，来以完全不同的方式展现建筑风格。”</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/7d5349f0img201607281223420_112__82.jpg" alt="图4"/>
					<div class="desc">
						<span class="num-title"><em>4</em>/24</span>
						<p>年度摄影第二名／作品：She Bends with the Wind／摄影师：Robin Robertis(美国)<br /><br />那不屈不挠的野草地，黄昏的寂寞的天空，还有拿着雨伞舞动的红衣少女，追求艺术的道路都是孤独，寂寞难熬的，但是只要心有所属不放弃，世间万物都会成为你的观众，都会为了你的舞姿而喝彩，而这又是一副人与自然完美融合的画卷，你心中可曾划过千年之恋里的意境？美国加州的摄影师 Robin Robertis 利用 iPhone 6 拍摄的照片拿到了年度摄影师第二名。</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/e7e5dbcbimg201607281224530_112__82.jpg" alt="图5"/>
					<div class="desc">
						<span class="num-title"><em>5</em>/24</span>
						<p>年度摄影第三名／作品：Wonderland／摄影师：Carolyn Mara Borlenghi(美国)<br /><br /> “这张照片是我为 Instagram 的 #WHPwonderland 主题所拍摄的系列照片中的一部分。每个周末当有主题项目出现时，我都会试着寻找灵感，而圣诞节前这个周末的主题是仙境 (Wonderland)。为了配合这个主题，我带着儿子，戴上驯鹿面具去了海边。”——摄影师 Carolyn Mara Borlenghi</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/a56641c4img201607281225040_112__82.jpg" alt="图6"/>
					<div class="desc">
						<span class="num-title"><em>6</em>/24</span>
						<p>抽象组第一名／摄影师︰Jiayu Ma（美国）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/0919b26dimg201607281225140_112__82.jpg" alt="图7"/>
					<div class="desc">
						<span class="num-title"><em>7</em>/24</span>
						<p>儿童组第一名／摄影师︰KK（中国）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/f4049251img201607281225230_112__82.jpg" alt="图8"/>
					<div class="desc">
						<span class="num-title"><em>8</em>/24</span>
						<p>建筑组第一名／摄影师：Jian Wang（中国）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/8f6b4e1bimg201607281225330_112__82.jpg" alt="图9"/>
					<div class="desc">
						<span class="num-title"><em>9</em>/24</span>
						<p>动物组第一名／摄影师：Erica Wu(美国)</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/365e8b94img201607281226120_112__82.jpg" alt="图10"/>
					<div class="desc">
						<span class="num-title"><em>10</em>/24</span>
						<p>生活风格组第一名／摄影师：Yuki Cheung（中国香港）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/a82f8a89img201607281226230_112__82.jpg" alt="图11"/>
					<div class="desc">
						<span class="num-title"><em>11</em>/24</span>
						<p>风景组第一名／摄影师︰Vasco Galhardo Simoes（葡萄牙）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/ce01c238img201607281226310_112__82.jpg" alt="图12"/>
					<div class="desc">
						<span class="num-title"><em>12</em>/24</span>
						<p>花卉组第一名／摄影师︰Lone Bjørn（瑞士）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/289efe34img201607281226390_112__82.jpg" alt="图13"/>
					<div class="desc">
						<span class="num-title"><em>13</em>/24</span>
						<p>食物组第一名／摄影师：Andrew Montgomery（英国）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/03f1c30aimg201607281226490_112__82.jpg" alt="图14"/>
					<div class="desc">
						<span class="num-title"><em>14</em>/24</span>
						<p>全景照片组第一名／摄影师：Glenn Homann（澳大利亚）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/7ad53ccbimg201607281233230_112__82.jpg" alt="图15"/>
					<div class="desc">
						<span class="num-title"><em>15</em>/24</span>
						<p>其他组第一名／摄影师：Kevin Casey（澳大利亚）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/49308f61img201607281227080_112__82.jpg" alt="图16"/>
					<div class="desc">
						<span class="num-title"><em>16</em>/24</span>
						<p>自然组第一名／摄影师︰Junfeng Wang（中国）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/d9b28d5aimg201607281227150_112__82.jpg" alt="图17"/>
					<div class="desc">
						<span class="num-title"><em>17</em>/24</span>
						<p>新闻组第一名／摄影师︰Loulou d'Aki（瑞典）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/695559a0img201607281227230_112__82.jpg" alt="图18"/>
					<div class="desc">
						<span class="num-title"><em>18</em>/24</span>
						<p>人物组第一名／摄影师︰Xia Zhenkai（中国）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/50c225e8img201607281227340_112__82.jpg" alt="图19"/>
					<div class="desc">
						<span class="num-title"><em>19</em>/24</span>
						<p>静物组第一名／摄影师︰Wen Qi（中国）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/dd8ff8abimg201607281227420_112__82.jpg" alt="图20"/>
					<div class="desc">
						<span class="num-title"><em>20</em>/24</span>
						<p>人像组第一名／摄影师︰Elaine Taylor（英国）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/727875dfimg201607281227490_112__82.jpg" alt="图21"/>
					<div class="desc">
						<span class="num-title"><em>21</em>/24</span>
						<p>季节组第一名／摄影师︰Valencia Tom（美国）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/c28dfedcimg201607281229300_112__82.jpg" alt="图22"/>
					<div class="desc">
						<span class="num-title"><em>22</em>/24</span>
						<p>日落组第一名／摄影师：Nicky Ryan（澳大利亚）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/31fc4252img201607281228110_112__82.jpg" alt="图23"/>
					<div class="desc">
						<span class="num-title"><em>23</em>/24</span>
						<p>旅行组第一名／摄影师︰Fugen Xiao（中国）</p>
					</div>
				</li>
				<li>
					<div class="mask"><em></em></div>
					<img src="/web3/Public/picture/4765cf20img201607281228240_112__82.jpg" alt="图24"/>
					<div class="desc">
						<span class="num-title"><em>24</em>/24</span>
						<p>树木组第一／摄影师：Victor Kintanar(菲律宾)</p>
					</div>
				</li>
			</ul>
		</div>
		<a href="javascript:;" class="smallNextBtn" title="下一张"><i></i></a>			
	</div>	
	<div class="tujicomment"><div class="comments" id="comments">
	<div class="topBar">
		<span class="title">锋友跟帖</span>
		<div class="totalbar">
			<div class="partIn"><b></b><em>人参与</em></div>
			<div class="threat"><b></b><em>人跟帖</em></div>
		</div>
	</div>
	<div class="catery-msgbox clearfix">
		<h2 class="hot"></h2>
		<div class="catery-tabNav">
			<a href="#comments_hot" title="热度">热度</a>
			<a href="#comments_new" title="时间">时间</a>
		</div>
	</div>
	<div id="comments_hot" class="commt_panel" style="dislay:none"></div>
	<div id="comments_new" class="commt_panel" style="dislay:none"></div>
	<div id="comments_none" class="commt_panel" style="dislay:none"><center>现在还没有评论，请发表第一个评论吧！</center></div>
	<div class="loading"><i></i>正在加载评论</div>
</div></div>
</div>
<div style="width:100%" id="commentSite"></div>
<!--弹出用户刚才留言-->
<div class="comUpPop" id="comUpPop">
	<div class="con">
		<a href="javascript:;" class="closeBtn" title="关闭">关闭</a>
		<a class="userHeader" href=""><img src="" height="40" width="40"/></a>
		<h2><span class="name"></span><span class=time></span></h2>
		<p></p>
	</div>
</div>

<!-- 评论框 -->
<div class="fixedBottomBox" id="bottomComments">
	<div class="wrap commentItem">
		<div class="msgArea">
			<a class="header userHeader" href=""><img src="" height="40" width="40"></a>
			<div class="textarea">
				<textarea tabindex="1" placeholder="立即参与评论..."></textarea>
				<div class="bottombar">
					<div class="nameDiv">
						<div class="animateBox">
							
						</div>
					</div>
					<a class="oKBtn" href="javascript:;" tabindex="4"></a>
					<span class="chartNum"><i></i>字</span>
				</div>
				<div class="bind-sina-qq"></div>
			</div>
		</div>
		<div class="shareModule">
			<div class="bdsharebuttonbox bdshare-button-style0-16" data-bd-bind="1428378996752">
				<a href="javascript:;" class="bds_weixin share_wx" data-cmd="weixin" title="分享到微信"></a>
				<a href="javascript:;" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a>
				<a href="javascript:;" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a>
				<a href="javascript:;" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博"></a>
				<a href="javascript:;" class="bds_renren" data-cmd="renren" title="分享到人人网"></a>
			</div>
			<div class="label">分享到</div>
		</div>
	</div>
</div>
<div class="con" id="replyComments" style="display:none">
	<i class="triangle"><em></em></i>
	<div class="writeBox">
		<a class="userHeader" href=""><img src="" height="30" width="30"/></a>
		<div class="textarea">
			<textarea tabindex="1"></textarea>
			<div class="bottombar">
				<div class="nameDiv">
					<div class="animateBox">
						<div class="oneModule">
							<a class="fengLoginBtn" href="javascript:;" title="威锋账号登录">威锋账号登录</a>
							<input type="text" placeholder="锋友名" class="defaultName" tabindex="2"/>
							<input type="text" placeholder="验证码" class="verification-code" size="4" tabindex="3"/>
							<a class="verificationimg" href="javascript:;"><img src="" height="30" width="60"></a>
						</div>
						
					</div>
				</div>
				<a class="oKBtn" href="javascript:;" tabindex="4"></a>
				<span class="chartNum"><i></i>字</span>
			</div>
			<div class="bind-sina-qq"></div>
		</div>
	</div>
</div>
<div class="msgPopHint"></div>
<script type="text/javascript" src="/web3/Public/js/js_news/detail/twemoji.js"></script>
<script type="text/javascript" src="/web3/Public/js/js_news/detail/common_1.js"></script><script type="text/javascript">
$(document).ready(function(){
	$.getScript('http://www.feng.com/images_v4/js/comment_v4.js').done(function(){
		$.startComment(652943, 0, 0, {'sina':false,'tencent':false});
	});
});
window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdPic":"","bdStyle":"0","bdSize":"16"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];
</script>

<!--footer-->

<script src='js/c.php' language='JavaScript'></script>
<script type="text/javascript">
(function(){
try{
	var bp = document.createElement('script');
	bp.src = '//push.zhanzhang.baidu.com/push.js';
	var s = document.getElementsByTagName("script")[0];
	s.parentNode.insertBefore(bp, s);
	jQuery(function($){
		//顶部导航
		$('#globar_topbar_links').scrollNav();
		//返回顶部
		$('.goTopBtn').goBackTop();
	    //搜索框效果
	    $('.searchBox').searchAnimate();
	});
}catch(e){}
})();
</script></div>


 
<div class="wrap footer">
	<div class="links">
		<ul>
			<li><a href="#" target="_blank">关于我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">联系我们</a></li>
			<li class="line"><span>|</span></li>
			<li><a href="#" target="_blank">About Us</a></li>
		</ul>
	</div>
	<div class="copyright">
		<p>Copyright 2016-2017 © zhuruicheng. All rights reserved 保留所有权利</p>
	</div>
</div>


</div>
<noscript><a href='#' target='_blank'><img src='http://yes1.feng.com/view.php?what=zone:189&amp;n=afbf8627' border='0' alt=''></a></noscript>

</body>
</html>