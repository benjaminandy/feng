// JavaScript Document
$(document).ready(function(e) {
//globar_topbar
	jQuery(function($){
		var 
		linkTimer,
		linkPanel = $('#globar_topbar_links'),
		linkFocusStyle = linkPanel.find('.focus').prop('style');
		if(linkFocusStyle){
			linkPanel.delegate('li a', 'mouseenter', function(){
				var self = $(this);
				clearTimeout(linkTimer);
				
				linkFocusStyle.left = self.position().left + 'px';
				linkFocusStyle.width = self.innerWidth() + 'px';
			})
			.bind('mouseleave', function(){
				clearTimeout(linkTimer);
				linkTimer = setTimeout(function(){
					linkFocusStyle.left = '';
				}, 240);
			});
		}
	});
});