//ds.base
;(function(global, document, undefined){
	var 
	rblock = /\{([^\}]*)\}/g,
	ds = global.ds = {
		noop: function(){},
		//Object
		mix: function(target, source, cover){
			if(typeof source !== 'object'){
				cover = source;
				source = target;
				target = this;
			}
			for(var k in source){
				if(cover || target[k] === undefined){
					target[k] = source[k];
				}
			}
			return target;
		},
		//String
		mixStr: function(sStr){
			var args = Array.prototype.slice.call(arguments, 1);
			return String(sStr).replace(rblock, function(a, i){
				return args[i] != null ? args[i] : a;
			});
		},
		trim: function(str){
			return String(str).replace(/^\s*/, '').replace(/\s*$/, '');
		},
		//Number
		getRndNum: function(max, min){
			min = isFinite(min) ? min : 0;
			return Math.random() * ((isFinite(max) ? max : 1000) - min) + min;
		},
		//BOM
		scrollTo: (function(){
			var 
			duration = 480,
			view = $(global),
			setTop = function(top){ global.scrollTo(0, top);},
			fxEase = function(t){return (t*=2)<1?.5*t*t:.5*(1-(--t)*(t-2));};
			return function(top, callback){
				top = Math.max(0, ~~top);
				var 
				tMark = new Date(),
				currTop = view.scrollTop(),
				height = top - currTop,
				fx = function(){
					var tMap = new Date() - tMark;
					if(tMap >= duration){
						setTop(top);
						return (callback || ds.noop).call(ds, top);
					}
					setTop(currTop + height * fxEase(tMap/duration));
					setTimeout(fx, 16);
				};
				fx();
			};
		})(),
		//DOM
		loadScriptCache: {},
		loadScript: function(url, callback, args){
			var cache = this.loadScriptCache[url];
			if(!cache){
				cache = this.loadScriptCache[url] = {
					callbacks: [],
					url: url
				};

				var 
				firstScript = document.getElementsByTagName('script')[0],
				script = document.createElement('script');
				if(typeof args === 'object'){
					for(var k in args){
						script[k] = args[k];
					}
				}
				script.src = url;
				script.onload = script.onreadystatechange = 
				script.onerror = function(){
					if(/undefined|loaded|complete/.test(this.readyState)){
						script = script.onreadystatechange = 
						script.onload = script.onerror = null;
						cache.loaded = true;
						
						for(var i=0,len=cache.callbacks.length; i<len; i++){
							cache.callbacks[i].call(null, url);
						}
						cache.callbacks = [];
					}
				};
				firstScript.parentNode.insertBefore(script, firstScript);
			}

			if(!cache.loaded){
				if(typeof callback === 'function'){
					cache.callbacks.push(callback);
				}
			}
			else{
				(callback || ds.noop).call(null, url);
			}
			return this;
		},
		requestAnimationFrame: (function(){
			var handler = global.requestAnimationFrame || global.webkitRequestAnimationFrame || 
				global.mozRequestAnimationFrame || global.msRequestAnimationFrame || 
				global.oRequestAnimationFrame || function(callback){
					return global.setTimeout(callback, 1000 / 60);
				};
			return function(callback){
				return handler(callback);
			};
		})(),
		animate: (function(){
			var 
			easeOut = function(pos){ return (Math.pow((pos - 1), 3) + 1);};
			getCurrCSS = global.getComputedStyle ? function(elem, name){
				return global.getComputedStyle(elem, null)[name];
			} : function(elem, name){
				return elem.currentStyle[name];
			};
			return function(elem, name, to, duration, callback, easing){
				var 
				from = parseFloat(getCurrCSS(elem, name)) || 0,
				style = elem.style,
				tMark = new Date(),
				size = 0;
				function fx(){
					var elapsed = +new Date() - tMark;
					if(elapsed >= duration){
						style[name] = to + 'px';
						return (callback || ds.noop).call(elem);
					}
					style[name] = (from + size * easing(elapsed/duration)) + 'px';
					ds.requestAnimationFrame(fx);
				};
				to = parseFloat(to) || 0;
				duration = ~~duration || 200;
				easing = easing || easeOut;
				size = to - from;
				fx();
				return this;
			};
		})(),
		//Cookies
		getCookie: function(name){
			var ret = new RegExp('(?:^|[^;])' + name + '=([^;]+)').exec(document.cookie);
			return ret ? decodeURIComponent(ret[1]) : '';
		},
		setCookie: function(name, value, expir){
			var cookie = name + '=' + encodeURIComponent(value);
			if(expir !== void 0){
				var now = new Date();
				now.setDate(now.getDate() + ~~expir);
				cookie += '; expires=' + now.toGMTString();
			}
			document.cookie = cookie;
		},
		//Hacker
		transitionSupport: (function(){
			var 
			name = '',
			prefixs = ['', 'webkit', 'moz', 'ms', 'o'],
			docStyle = (document.documentElement || document.body).style;
			for(var i=0,len=prefixs.length; i<len; i++){
				name = prefixs[i] + (prefixs[i]!=='' ? 'Transition' : 'transition');
				if(name in docStyle){
					return {
						propName: name,
						prefix: prefixs[i]
					};
				}
			}
			return null;
		})(),
		isIE6: !-[1,] && !global.XMLHttpRequest
	};
})(this, this.document);

//CSS3 Support
;(function(){
	var 
	root = document.documentElement,
	className = root.className,
	classList = [];
	if(className){
		classList.push(className);
	}
	if(ds.transitionSupport){
		classList.push('transition');
	}
	if(ds.transformSupport){
		classList.push('transform');
	}
	root.className = classList.join(' ');
})();

/**
* ds.buildSlider
*/
;(function(global, document, $){
	var sliderList = [];
	//buildSlider
	ds.buildSlider = function(shell, ops, buildHandler){
		if(!(shell = $(shell)).length){ return; }

		ops = ops || {};
		var 
		triggers = shell.find('.triggers a'),
		_ops = {
			duration: 400,
			unitSize: 880,
			imgLoadFlag: null,
			imgLoadHandler: ds.noop,
			onplay: function(inx, prevInx){
				if(ops.imgLoadFlag && !ops.imgLoadFlag[inx]){
					ops.imgLoadHandler.call(this, inx, prevInx);
					ops.imgLoadFlag[inx] = true;
				}

				prevInx > -1 && triggers.eq(prevInx).removeClass('current');
				triggers.eq(inx).addClass('current');
			}
		};
		for(var k in _ops){
			if(typeof ops[k] === 'undefined'){
				ops[k] = _ops[k];
			}
		}
		if(!ops.shell){
			ops.shell = shell.find('ul').eq(0);
			if(typeof ops.length !== 'number'){
				ops.length = ops.shell.find('li').length;
			}
		}

		var timer, slider = new (buildHandler || Slider)(ops);
		shell.delegate('a.prev,a.next', 'click', function(e){
			e.preventDefault();

			clearTimeout(timer);
			slider[this.className.indexOf('next')>-1 ? 'next' : 'prev']();
		})
		.hover(function(){
			slider.mouseEnter = true;
			slider.stopAuto();
		}, function(){
			slider.mouseEnter = false;
			slider.autoPlay();
		});
		triggers.bind('click', function(e){
			e.preventDefault();
		})
		.hover(function(){
			clearTimeout(timer);
			var self = this;
			timer = setTimeout(function(){
				slider.play(triggers.index(self));
			}, 160);
		}, function(){
			clearTimeout(timer);
		});

		sliderList.push(slider);
		slider.shellWrap = shell;
		
		//Fix IE6 hover
		if(ds.isIE6){
			var prevAndNext = shell.find('a.prev,a.next');
			shell.hover(function(){
				prevAndNext.show();
			}, function(){
				prevAndNext.hide();
			});
		}
		return slider;
	};

	//scroll
	var 
	scrollTimer,
	view = $(global),
	checkScroll = function(){
		var 
		viewHeight = view.height(),
		scrollTop = view.scrollTop();
		//checkSlider in viewport
		$.each(sliderList, function(){
			var 
			offsetTop = this.shellWrap.offset().top,
			height = this.shellWrap.height();
			if( !this.mouseEnter && ( (offsetTop >= scrollTop && offsetTop <= scrollTop+viewHeight)
				|| (offsetTop+height >= scrollTop && offsetTop+height <= scrollTop+viewHeight) )
			){
				this.autoPlay();
			}
			else{
				this.stopAuto();
			}
		});
	},
	scrollHandler = function(){
		clearTimeout(scrollTimer);
		scrollTimer = setTimeout(checkScroll, 128);
	};
	view.bind('scroll resize', scrollHandler);
	scrollHandler();
})(window, document, jQuery);


;(function(){
	//global_topbar 下划线跟随鼠标移动
	$.fn.extend({
		scrollNav : function(options){
			var defaults = {
			
			};
			var options = $.extend(defaults,options);
			var linkTimer,
			linkPanel = $(this),
			linkFocusStyle = linkPanel.find('.focus').prop('style');
			if(linkFocusStyle){
				linkPanel.delegate('li a', 'mouseenter', function(){
					var self = $(this);
					clearTimeout(linkTimer);
					linkFocusStyle.left = self.position().left + 'px';
					linkFocusStyle.width = self.innerWidth() + 'px';
				})
				.bind('mouseleave', function(){
					clearTimeout(linkTimer);
					linkTimer = setTimeout(function(){
						linkFocusStyle.left = '';
					}, 240);
				});
			}
		}
	});
})(jQuery);

;(function(){
	//返回顶部
	$.fn.extend({
		goBackTop:function(options){
			var defaults = {
					topLeval:200
				};
			var options = $.extend(defaults,options),
				$This = $(this);
			$(window).scroll(function(){
				var scrollOffsetVal = $(window).scrollTop();
				if(scrollOffsetVal>options.topLeval){
					$This.css({'filter':'alpha(opacity=100)', 'opacity': 1});					
				}else{
					$('.goTopBtn').css({'filter':'alpha(opacity=0)', 'opacity': 0});
				}
			});
			$This.bind('click',function(event){
				event.preventDefault();
				ds.scrollTo(0);
			});
		}
	});
})(jQuery);

;(function(){
	//搜索框动画
    $.fn.extend({
        searchAnimate:function(options){
            var defaults = {

            };
            var options = $.extend(defaults,options),
                $This = $(this);
            $This.find('input').focus(function(){
				var inputWidth = $(this).width() - 35;
                $(this).val('').css({
                    'text-align':'left',
                    'padding-left':'40px',
					'width':inputWidth
                });
                $(this).next().animate({
                    left:'14px'
                    },300);
                }).blur(function(){
					var inputWidth = $(this).width() + 35;
                    $(this).val('搜索其实很简单^ ^').css({
                        'text-align':'center',
                        'padding-left':'5px',
						'width':inputWidth
                });
                $(this).next().animate({
                    left:'72px'
                },300);
            });                
        }
    });    
})(jQuery);

jQuery(function($){
	//顶部快捷搜索
	var timer;
	$('#quickSearchBtn').bind('click',function(){
		clearTimeout(timer);
		$('#headerSearch').stop().animate({'height':80},400);
	});
	$('#quickSearchBtn').mouseout(function(){
		timer = setTimeout(function(){
			$('#headerSearch').stop().animate({'height':0},400);
		},600);
	});
	$('.headerSearch').mouseover(function(){
		clearTimeout(timer);
		$('#headerSearch').stop().animate({'height':80},400);	
	});
	$('.headerSearch').mouseout(function(){
		$('#headerSearch').stop().animate({'height':0},400);
	});
});

jQuery(function($){
	//搜索框效果
	$('.searchBoxtop').searchAnimate();
});	


jQuery(function($){
	//阅读页面二维码微信固定
	if ($('#qr_weixin').length == 0) {return;};
	var qrOffsetTop = $('#qr_weixin').offset().top;
	$(window).scroll(function(){
		var windowOffsetTop = $(this).scrollTop();
		if( windowOffsetTop>=qrOffsetTop ){
			$('#qr_weixin').addClass('qr_fixed');
		}else{
			$('#qr_weixin').removeClass('qr_fixed');
		}
	});
});


jQuery(function($){
	//阅读页面二维码微信固定
	if ($('#qr_weixinB').length == 0) {return;};
	var sqrOffsetTop = $('#qr_weixinB').offset().top;
	$(window).scroll(function(){
		var swindowOffsetTop = $(this).scrollTop();
		if( swindowOffsetTop>=sqrOffsetTop ){
			$('#qr_weixinB').addClass('qr_fixedB');
		}else{
			$('#qr_weixinB').removeClass('qr_fixedB');
		}
	});
});

jQuery(function($){
	//阅读页面二维码微信固定
	if ($('#qr_weixinC').length == 0) {return;};
	var sqrOffsetTop = $('#qr_weixinC').offset().top;
	$(window).scroll(function(){
		var swindowOffsetTop = $(this).scrollTop();
		if( swindowOffsetTop>=sqrOffsetTop ){
			$('#qr_weixinC').addClass('qr_fixedC');
		}else{
			$('#qr_weixinC').removeClass('qr_fixedC');
		}
	});
});

/**
* jquery.slider.base.js
* create: 2011.12.13
* update: 2012.03.28
* admin@laoshu133.com
*/
;(function(global,document,$,undefined){var noop=function(){},slider=global.Slider=function(ops){this.init(ops||{});};slider.prototype={constructor:slider,_ops:{length:3,shell:null,auto:true,cName:'left',duration:200,unitSize:500,delay:5000,easing:'swing',onbeforeplay:noop,onplay:noop},init:function(ops){var self=this,_ops=this._ops;for(var k in _ops){if(typeof ops[k]==='undefined'){ops[k]=_ops[k];}}this.shell=$(ops.shell);this.ops=ops;this.index=-1;this.play(0);if(ops.auto){this.shell.bind('mouseenter.slider',function(){self.stopAuto();}).bind('mouseleave.slider',function(){self.autoPlay();});}},play:function(inx){var self=this,ops=this.ops;inx=~~(inx===void 0?this.index+1:inx)%ops.length;inx=inx<0?ops.length+inx:inx;if(inx===this.index||ops.onbeforeplay.call(this,inx,this.index)===false){return this;}var aniOps={};aniOps[ops.cName]=-inx*ops.unitSize;this.shell.stop().animate(aniOps,ops.duration,ops.easing,function(){ops.onplay.call(self,inx,self.index);self.index=inx;if(ops.auto){self.autoPlay();}});clearTimeout(this.timer);return this;},next:function(){return this.play();},prev:function(){return this.play(this.index-1);},autoPlay:function(){var self=this;this.ops.auto=true;clearTimeout(this.timer);this.timer=setTimeout(function(){self.play();},this.ops.delay);},stopAuto:function(){clearTimeout(this.timer);this.ops.auto=false;}};})(window,document,jQuery);

/**
焦点图片播放
*/
;(function(d,D,v){d.fn.responsiveSlides=function(h){var b=d.extend({auto:!0,speed:1E3,timeout:4E3,pager:!1,nav:!1,random:!1,pause:!1,pauseControls:!1,prevText:"Previous",nextText:"Next",maxwidth:"",controls:"",namespace:"sliderL",before:function(){},after:function(){}},h);return this.each(function(){v++;var e=d(this),n,p,i,k,l,m=0,f=e.children(),w=f.size(),q=parseFloat(b.speed),x=parseFloat(b.timeout),r=parseFloat(b.maxwidth),c=b.namespace,g=c+v,y=c+"_nav "+g+"_nav",s=c+"_here",j=g+"_on",z=g+"_s",o=d("<ul class='"+c+"_tabs "+g+"_tabs' />"),A={"float":"left",position:"relative"},E={"float":"none",position:"absolute"},t=function(a){b.before();f.stop().fadeOut(q,function(){d(this).removeClass(j).css(E)}).eq(a).fadeIn(q,function(){d(this).addClass(j).css(A);b.after();m=a})};b.random&&(f.sort(function(){return Math.round(Math.random())-0.5}),e.empty().append(f));f.each(function(a){this.id=z+a});e.addClass(c+" "+g);h&&h.maxwidth&&e.css("max-width",r);f.hide().eq(0).addClass(j).css(A).show();if(1<f.size()){if(x<q+100)return;if(b.pager){var u=[];f.each(function(a){a=a+1;u=u+("<li><a href='javascript:;' class='"+z+a+"'>"+a+"</a></li>")});o.append(u);l=o.find("a");h.controls?d(b.controls).append(o):e.after(o);n=function(a){l.closest("li").removeClass(s).eq(a).addClass(s)}}b.auto&&(p=function(){k=setInterval(function(){var a=m+1<w?m+1:0;b.pager&&n(a);t(a)},x)},p());i=function(){if(b.auto){clearInterval(k);p()}};b.pause&&e.hover(function(){clearInterval(k)},function(){i()});b.pager&&(l.bind("click",function(a){a.preventDefault();b.pauseControls||i();a=l.index(this);if(!(m===a||d("."+j+":animated").length)){n(a);t(a)}}).eq(0).closest("li").addClass(s),b.pauseControls&&l.hover(function(){clearInterval(k)},function(){i()}));if(b.nav){c="<a href='#' class='"+y+" prev'>"+b.prevText+"</a><a href='#' class='"+y+" next'>"+b.nextText+"</a>";h.controls?d(b.controls).append(c):e.after(c);var c=d("."+g+"_nav"),B=d("."+g+"_nav.prev");c.bind("click",function(a){a.preventDefault();if(!d("."+j+":animated").length){var c=f.index(d("."+j)),a=c-1,c=c+1<w?m+1:0;t(d(this)[0]===B[0]?a:c);b.pager&&n(d(this)[0]===B[0]?a:c);b.pauseControls||i()}});b.pauseControls&&c.hover(function(){clearInterval(k)},function(){i()})}}if("undefined"===typeof document.body.style.maxWidth&&h.maxwidth){var C=function(){e.css("width","100%");e.width()>r&&e.css("width",r)};C();d(D).bind("resize",function(){C()})}})}})(jQuery,this,0);


//page bigads
;(function($){
	var
	shell, style,
	head = document.head || $('head')[0],
	_ops = {
		count: 0,
		active: false,
		startTime: '2014-03-12 0:0',
		endTime: '2199-12-12 0:0',
		imgUrl: 'about:blank',
		linkUrl: './',
		customCSS: ''
	},
	linkTmpl = '<a href="{linkUrl}" class="link" target="_blank"><img src="{imgUrl}" /></a>';
	function parseDate(dateStr){
		var date = new Date(0), rdate = /^(\d{4})\-(\d+)-(\d+)\s(\d+):(\d+)/;
		if(rdate.test(dateStr)){
			date.setYear(RegExp.$1);
			date.setMonth(RegExp.$2-1);
			date.setDate(RegExp.$3);
			date.setHours(RegExp.$4);
			date.setMinutes(RegExp.$5);
		}
		return date;
	}
	ds.mix({
		fillPageAd: function(ops){
			ops = ds.mix(ops || {}, _ops);

			var now = +new Date();
			if(!ops.active || now < +parseDate(ops.startTime) || now > +parseDate(ops.endTime)){ return; }

			//css
			if(!style){
				style = document.createElement('style');
				style.type = 'text/css';
				head.insertBefore(style, head.firstChild);
			}
			var cssArr = [];
			if(!ops.isLightColor){
				cssArr.push('.page_bigad .top_news .section li,.page_bigad .bbs_notices,.page_bigad .bbs_treasure li{box-shadow:none;}');
			}
			if(ops.customCSS){
				cssArr.push(ops.customCSS);
			}
			if(style.styleSheet){
				style.styleSheet.cssText = cssArr.join('');
			}
			else{
				style.innerHTML = cssArr.join('');
			}

			//html
			if(!shell){
				shell = document.createElement('div');
				shell.className = 'page_bigad_shell';
				document.body.insertBefore(shell, document.body.firstChild);
			}
			var html = linkTmpl.replace('{linkUrl}', ops.linkUrl).replace('{imgUrl}', ops.imgUrl);
			shell.innerHTML = html;

			$(document.documentElement).addClass('page_bigad');
		}
	});
})(jQuery);

/*
格式化输出的时间
*/
jQuery(function($){
$.extend({
relativeTime : (function(){
	var relativeTime = function(){
		this.time = 0;
		this.now = 0;
		this.text = '';
		this.diff = 0;
		arguments.length && this.setTime(arguments[0], arguments[1]);
		return this;
	};
	relativeTime.prototype = {
		toString : function(){
			return this.text;
		},
		transTime : function(time){
			if( typeof time == 'object' ){
				return time;
			}
			var ret = new Date();
			if( /^\d+$/.test(time) ){
				if( (time + '').length < 12 ){
					time *= 1000;
				}
				ret.setTime(time);
			}else{
				var part = time.split(' '),
				mat = part[0].match(/^(\d+)\-(\d+)\-(\d+)$/);
				if( mat ){
					ret.setFullYear(mat[1]);
					ret.setMonth(mat[2] - 1);
					ret.setDate(mat[3]);
				}
				if( mat = part[1].match(/^(\d+)\:(\d+)\:(\d+)$/) ){
					ret.setHours(mat[1]);
					ret.setMinutes(mat[2]);
					ret.setSeconds(mat[3]);
				}
			}
			return ret;
		},
		strPad : function(value, length){
			length = length || 2;
			value += '';
			while( value.length < length ){
				value = '0' + value;
			}
			return value;
		},
		setTime : function(time, now){
			time = this.time = this.transTime(time);
			return now === true ? time : (now === false ? this : this.setNow(now));
		},
		setNow : function(now){
			now = this.now = now ? this.transTime(now) : new Date();
			var time = this.time, timeValue = time.getTime(),
			diff = this.diff = (now.getTime() - timeValue) / 1000;
			if( diff <= 3600 ){
				if( diff <= 60 ){
					return this.setText('刚刚');
				}else{
					return this.setText(parseInt(diff / 60) + '分钟前');
				}
			}else if( diff <= 86400 ){
				return this.setText(parseInt(diff / 3600) + '小时前');
			}else{
				var yesterday = parseInt(now.getTime() / 1000),
				yesterdayValue,
				ret = this.strPad(time.getHours()) + ':' + this.strPad(time.getMinutes());
				yesterday -= now.getHours() * 3600 + time.getMinutes() * 60 + time.getSeconds() + 86400;
				yesterday = this.transTime(yesterday);
				yesterdayValue = yesterday.getTime();
				if( timeValue >= yesterdayValue ){
					return this.setText('昨天 ' + ret);
				}else if( timeValue >= yesterdayValue - 86400 * 1000 ){
					return this.setText('前天 ' + ret);
				}else{
					var month = time.getMonth() + 1,
					date = time.getDate();
					ret = this.strPad(month) + '-' + this.strPad(date) + ' ' + ret;
					yesterday.setMonth(0);
					yesterday.setDate(0);
					if( timeValue < yesterday.getTime() ){
						ret = time.getFullYear() + '-' + ret;
					}
					return this.setText(ret);
				}
			}
		},
		setText : function(text){
			this.text = text || '';
			return this;
		}
	}
	return function(time, now){
		return new relativeTime().setTime(time, now);
	}
})()
});
});