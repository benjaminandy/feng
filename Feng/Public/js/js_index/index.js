// JavaScript Document

    jQuery(function($){
        //homeBanner
        $('.homebanner').mouseover(function() {
            $('.sliderL_nav.prev').stop().animate({'left':5,'filter':'alpha(opacity=100)', 'opacity': 1},400);
            $('.sliderL_nav.next').stop().animate({'right':5,'filter':'alpha(opacity=100)', 'opacity': 1},400);
        })
        $('.homebanner').mouseout(function() {
            $('.sliderL_nav.prev').stop().animate({'left':-35,'filter':'alpha(opacity=0)', 'opacity': 0},400);
            $('.sliderL_nav.next').stop().animate({'right':-35,'filter':'alpha(opacity=0)', 'opacity': 0},400);
        });
        //Banner
		$(".targetO").responsiveSlides({
			auto: true,
			pager: true,
			nav: true,
			speed: 500,
			maxwidth: 692
        });
    });

	jQuery(function($){
		//顶部导航
		$('#globar_topbar_links').scrollNav();
		//导航栏
		$('#navBox').scrollNav();
	});	
	
	jQuery(function($){
		//推荐软件与游戏
		ds.buildSlider('#gameList', {
       	 length: Math.ceil($('#gameList li').length/1),
       	 unitSize: 1000,
       	 duration: 500
    	});
	});
	
    jQuery(function($){
		//热门游戏(推荐)
		var liArray = $('.slide_game_list li');
		var liLength = liArray.length;
		if( liLength < 0)
		return;	
		liArray.each(function(){
			$(this).mouseover(function(event) {
				liArray.find('.mod').stop().animate({'height':31},400);
				liArray.removeClass('active');
				$(this).find('.mod').stop().animate({'height':103},400);
				$(this).addClass('active');
			});
		});
	});

	jQuery(function($){
		//游戏频道(论坛)
		$('.gameChannel .gameChannel_left .drift').mouseover(function(event) {
			$(this).find('.game_title').stop().animate({'top':105},400);
			$(this).find('.game_content').stop().animate({'top':154},400);
		}).mouseout(function(event) {
			$(this).find('.game_title').stop().animate({'top':205},400);
			$(this).find('.game_content').stop().animate({'top':266},400);
		});
	});
	
	jQuery(function($){
		//锋新闻动画封装
		(function(){
			$.fn.extend({
				linearTop:function(options){
					var defaults={
					
					}
					$(this).mouseover(function(){
						$(this).find('.abs').stop().animate({'height':60,'marginBottom':20},300);
					}).mouseout(function(){
						$(this).find('.abs').stop().animate({'height':0,'marginBottom':0},100);
					});
				}
			});		
		})(jQuery);		
	});
	
	jQuery(function($){
		//快捷导航		
		var shortcutNav = $(".quick_navigation");	
		var offsetDefault = 200; //设置偏移值
		var moduleArr =[
			$('#todayNews').offset().top - offsetDefault,//今日导读
			$('#gameChannel').offset().top - offsetDefault,//游戏频道(论坛列表页)
			$('#feng_tecList').offset().top - offsetDefault,//锋科技
			$('#fengCommunity').offset().top - offsetDefault//社区(论坛)
		];
				
		$(window).scroll(function(){
			var topVal = $(this).scrollTop();
			if( topVal > 200){
				shortcutNav.css({"opacity":"1", "transform":"scale(1)", "-moz-transform":"scale(1)", "-ms-transform":"scale(1)", "-webkit-transform":"scale(1)", "-moz-transition":"all .32s linear", "-ms-transition":"all .32s linear", "-webkit-transition":"all .32s linear", "transition":"all .32s linear"});					
				//快捷高亮显示
				var curBtn = shortcutNav.find('.todayNews');//默认新闻导读
				if( moduleArr[0]<=topVal&&topVal<moduleArr[1] ){
					//此时页面是新闻导读区域
					curBtn = shortcutNav.find('.todayNews');
				}else if( moduleArr[1]<=topVal&&topVal<moduleArr[2] ){
					//此时页面是锋观点区域
					curBtn = shortcutNav.find('.fengView');
				}else if( moduleArr[2]<=topVal&&topVal<moduleArr[3] ){
					//此时页面是游戏频道区域
					curBtn = shortcutNav.find('.gameChannel');
				}else if( moduleArr[3]<=topVal&&topVal<moduleArr[4] ){
					//此时页面是锋科技区域
					curBtn = shortcutNav.find('.feng_tecList');
				}else if( moduleArr[4]<=topVal ){
					//此时页面是社区区域
					curBtn = shortcutNav.find('.community');
				}
				//判断是否已经有激活的按钮
				if(!curBtn.hasClass('active')){
					shortcutNav.find('li>a').removeClass('active');
					curBtn.addClass('active');
				}
				
			}else{
			shortcutNav.css({"opacity":"0", "transform":"scale(0)", "-moz-transform":"scale(0)", "-ms-transform":"scale(0)", "-webkit-transform":"scale(0)", "-moz-transition":"all .32s linear", "-ms-transition":"all .32s linear", "-webkit-transition":"all .32s linear", "transition":"all .32s linear"});
			}
		});
		//点击快捷键按钮
		shortcutNav.delegate('a','click',function(event){
			event.preventDefault();
			var targetId = $(this).attr('data-target');
			if( $(this).hasClass('gobackBtn')){
				ds.scrollTo(0);
			}else{
				var topValNum = $('#'+targetId).offset().top;
				ds.scrollTo(topValNum);
			}	
		});
	});
	/*
	//留言冒泡 封装
    (function(){
        $.fn.extend({
            msgUpPop:function(options){
                var defaults = {

                };
                var options = $.extend(defaults,options);
                var targetO = $(this),
                    targetHeight = 21;
                    function popMsgBox(targetDiv,contents){
                        var flag = false,
                            innerHtmlCode = $('<span class="msg_bar">'+ contents +'</span>'),
                            xRandom = parseInt(Math.random()*(targetDiv.width() - 125)),//left,随机
                            yRandom = parseInt(Math.random()*(targetDiv.height() - 21));//top,随机
                            console.log(targetDiv.find('.msg_bar').length);
                            if(targetDiv.find('.msg_bar').length>=3){
                                targetDiv.find('.msg_bar:eq(0)').remove();
                            }

                        targetDiv.find('.msg_bar').each(function(){
                            var topVal = $(this).position().top;
                            if((yRandom>=topVal-targetHeight)&&(yRandom<=topVal+targetHeight)){
                                flag = true;
                                return false;
                            }
                        });
                        if(!flag){
                            innerHtmlCode.css({'left':xRandom,'top':yRandom,'display':'none'});
                            targetO.append(innerHtmlCode).find('.msg_bar:last').fadeIn(500);
                        }else{
                                popMsgBox(targetO,contents);
                        }
                    }
                var msgData = ['我爱威锋网','威锋商城不错','威锋游戏好玩','我爱威锋','你个逗逼我不说你了！'];//测试数据用
                targetO.mouseenter(function(){          
                    var index = 0;
                    //  ajax 取得最新的评论信息 2S 更新一次
                    var timer = setInterval(function(){
                        popMsgBox(targetO,msgData[index]);
                        index++;
                        if(index===msgData.length){
                            index=0;
                        }
                    },2000);
                    targetO.data('timer', timer);                   
                });
            
                targetO.mouseleave(function(){
                    clearInterval(targetO.data('timer'));
                    targetO.find('.msg_bar').stop().animate({'opacity':0,'filter':'alpha(opacity=0)'},400,function(){
                        $(this).remove();
                    });
                });
            }
        });
    })(jQuery);
    $('.todayNews .columnL .wea_d').msgUpPop();
    $('.todayNews .columnMid .projectItem').msgUpPop();
    $('.todayNews .columnR .topic_view').msgUpPop();
    */

    jQuery(function($){
    	//首页banner右侧二维码动画
    	$('.Topapps .er_img').each(function(){
    		$(this).mouseover(function(event) {
    			$(this).parents('.topModule').find('.app_f_location').stop().animate({'right':-80,'opacity':1,'zIndex':1},400);
    		}).mouseout(function(event) {
    			$(this).parents('.topModule').find('.app_f_location').stop().animate({'right':0,'opacity':0,'zIndex':-1},400);
    		});
    	});

    	$('.Topapps .bottomModule .app').each(function(){
    		$(this).mousemove(function(event) {
    			$(this).find('.app_t_location').stop().animate({'opacity':1,'zIndex':1},200);
    		}).mouseout(function(event) {
    			$(this).find('.app_t_location').stop().animate({'opacity':0,'zIndex':-1},200);
    		});
    	});
    });


