﻿!(function(){
//加载更多对象
var topicObj = function(opt){
	this.data = undefined;
	this.htmlString = {};
	this.working = false;
	var def = {
		//需要加载的元素的父元素
		parent:$("#data_list>ul"),
		//需要加载的单位元素项目
		item:false,
		//翻页按钮对象
		pager:$("#new_page_list"),
		//显示加载元素
		loading:$("#loading"),
		//AJAX地址
		ajaxUrl:"",
		//需要获取第几页的数据
		pageNum:1,
		//总页数
		pageCount:1,
		//字符串拼接函数
		createHtml:undefined,
		//scroll次数
		scrollCount:0,
		//scroll上限
		maxScroll:3
	};
	this.option = $.extend({},def,opt);
	if( typeof this.option.createHtml == "string" ){
		this.option.createHtml = createHtmlFuncs[this.option.createHtml];
	}
	var self=this;
  this.option.pager.delegate("a", "click", function(){
		if (!self.working) {
			$('html,body').animate({
				scrollTop: self.option.parent.offset().top - 100
			}, 300);
			self.option.parent.empty();
			self.option.pageNum = $(this).data("page");
			self.getData();
			//清空加载次数
			self.option.scrollCount = 0;
		}
	});
	if( this.option.item ){
		var htmlString = this.htmlString[1] = [],
		item = this.option.item;
		this.option.parent.each(function(i){
			item($(this).data("key",i)).each(function(k){
				var html = $(this).prop("outerHTML");
				i ? htmlString.splice(k * 2 + 1, 0, html): htmlString.push(html);
			});
		});
	}else{
		this.htmlString[1] = this.option.parent.html();
	}
	this.changePager();
}
topicObj.prototype = {
	getData:function(page){
		page = typeof page == "undefined" ? this.option.pageNum : page;
		var ret = true;
		if( this.htmlString[page] ){
			this.option.pageNum = page;
			this.addElement();
	    this.changePager();
		}else if( !this.working ){
			if( page > this.option.pageCount ){
				ret = false;
			}else{
				this.working = true;
				this.option.loading.show();
			  var self=this;
			  $.ajax({
			      url:this.option.ajaxUrl,
			      type:"POST",
			      data:{
			        page:page - 1
			      },
			      dataType:"json",
			      success:function(result){
			      	self.working = false;
							self.option.loading.hide();
			        self.data = result;
			        self.checkJson();
			      },
			      error:function(){
							self.option.loading.html("<i></i>加载异常，请刷新后重试");
			      }
			  });
			}
		}else{
			ret = false;
		}
		return ret;
	},
	checkJson:function(){
	    var jsonObj = this.data;
	    var objType = typeof jsonObj == "object" && jsonObj.status == "success" && jsonObj.dataList.length != 0;
	    if (objType == false) {
	        this.falseResult()
	    }else{
	        this.trueResult(jsonObj);
	    }
	},
	falseResult:function(){
	},
	trueResult:function(){
	    this.option.pageNum = this.data.curr_page;
	    this.option.pageCount = this.data.page_total;
	    this.joinData();
	    this.addElement();
	    this.changePager();
	},
	joinData:function(){
		this.htmlString[this.option.pageNum] = this.option.createHtml.call(this, this.data);
	},
	addElement:function(){
		var htmlString = this.htmlString[this.option.pageNum];
		if( typeof htmlString == "object" ){
			var parent = this.option.parent;
			if( parent.length > 1 ){
				var left = parent.eq(0),
				right = parent.eq(1),
				i = 0,
				addItem = function(){
					var html = htmlString[i];
					if( html ){
						(left.height() > right.height() ? right : left).append(html);
						i ++;
						setTimeout(addItem, 500);
					}
				};
				addItem();
			}else{
				$.each(htmlString, function(i, html){
					parent.append(html);
				});
			}
		}else{
	    this.option.parent.append(htmlString);
	  }
	},
	changePager:function(){
	  var now_page = parseInt(this.option.pageNum);
	  var page_total = parseInt(this.option.pageCount);
	  var small_page = (now_page - 2 < 1)?1:(now_page - 2);
	  var big_page = (now_page + 2) > page_total?page_total:(now_page + 2);
	  var html = '';
	  //上一页
	  if (now_page == 1) {
	      html = '<a href="javascript:;" class="prev_btn disabled" title="上一页" data-page="1"><span>上一页<i class="bg_png"></i></span></a>';
	  }else{
	      html = '<a href="javascript:;" class="prev_btn" title="上一页" data-page="'+(now_page-1)+'"><span>上一页<i class="bg_png"></i></span></a>';
	  }
	  if (page_total > 6) {
	      if (now_page < 4) {
	          for (var i = 1; i <= 5; i++) {
	              i == now_page ? (html = html + '<a href="javascript:;" class="current" title="第'+i+'页" data-page="'+i+'"> <span> '+i+' </span> </a>'):(html = html + '<a href="javascript:;" title="第'+i+'页" data-page="'+i+'"> <span> '+i+' </span> </a>');
	          }
	          html = html + '<span class="more"> <span> ... </span> </span><a href="javascript:;" title="第'+page_total+'页" data-page="'+page_total+'"> <span> '+page_total+' </span> </a>'
	      }else if(now_page > page_total - 2){
	          html = html+'<a href="javascript:;" title="第1页" data-page="1"> <span> 1 </span> </a><span class="more"> <span> ... </span> </span>';
	          for (var i = page_total - 4; i <= page_total; i++) {
	              i == now_page ? (html = html + '<a href="javascript:;" class="current" title="第'+i+'页" data-page="'+i+'"> <span> '+i+' </span> </a>'):(html = html + '<a href="javascript:;" title="第'+i+'页" data-page="'+i+'"> <span> '+i+' </span> </a>');
	          }
	      }else{
	          html = html + '<a href="javascript:;" title="第1页" data-page="1"> <span> 1 </span> </a><span class="more"> <span> ... </span> </span>';
	          for (var i = small_page; i <= big_page; i++) {
	              i == now_page ? (html = html + '<a href="javascript:;" class="current" title="第'+i+'页" data-page="'+i+'"> <span> '+i+' </span> </a>'):(html = html + '<a href="javascript:;" title="第'+i+'页" data-page="'+i+'"> <span> '+i+' </span> </a>');
	          }
	          html = html + '<span class="more"> <span> ... </span> </span><a href="javascript:;" title="第'+page_total+'页" data-page="'+page_total+'"> <span> '+page_total+' </span> </a>'
	      }
	  }else{
	      html = now_page > 1?'<a href="javascript:;" class="prev_btn" title="上一页" data-page="'+(now_page-1)+'"> <span> <i class="bg_png"> </i> 上一页 </span></a>':'<a href="javascript:;" class="prev_btn disabled" title="上一页" data-page="1"><span>上一页<i class="bg_png"></i></span></a>'
	      for (var i = 1; i <= page_total; i++) {
	          i == now_page ? (html = html + '<a href="javascript:;" class="current" title="第'+i+'页" data-page="'+i+'"> <span> '+i+' </span> </a>'):(html = html + '<a href="javascript:;" title="第'+i+'页" data-page="'+i+'"> <span> '+i+' </span> </a>');
	      }
	  }
	  //下一页
	  if (now_page == page_total) {
	      html = html + '<a href="javascript:;" class="next_btn disabled" title="下一页" data-page="'+now_page+'"> <span> 下一页 <i class="bg_png"> </i> </span></a>';
	  }else{
	      html = html + '<a href="javascript:;" class="next_btn" title="下一页" data-page="'+(now_page+1)+'"> <span> 下一页 <i class="bg_png"> </i> </span></a>';
	  }
	  this.option.pager.html(html)
	},
	scroll:function(){
		var self = this;
		$(window).scroll(function(){
			var nowScrollTop = $(document).scrollTop(),
			documentHeight = $(document).height(),
			windowHeight = $(window).height(),
			footerHeight = 331,
			result = documentHeight - nowScrollTop - windowHeight <= footerHeight;
			if (result && self.option.scrollCount < self.option.maxScroll) {
				//获取下一页内容
				self.option.scrollCount += self.getData(self.option.pageNum + 1) ? 1 : 0;
			}
		});
	}
};
//拼接字符串
var createHtmlFuncs = {
topic : function(jsonData){
	var eleText = '';
	$.each(jsonData.dataList, function(i, data){
		eleText += [
			'<li>',
				'<div class="top_num">',
					'<div class="data">',
						data.chapter > 0 ? '<span class="num">第<em>'+data.chapter+'</em>期</span>' : '',
						'<span class="time">'+data.post_date+'</span>',
					'</div>',
					'<h1><a href="'+data.url+'" target="_blank">'+data.title+'</a></h1>',
				'</div>',
				'<div class="txt">',
					'<div class="img"><a href="'+data.url+'" target="_blank"><img src="'+data.pic_url+'"  alt=""></a></div>',
					'<div class="text">',
						'<p><i></i>'+data.content.substr(0, 108,'...')+'<a class="more" href="'+data.url+'" target="_blank">【详细】</a></p>',
						'<div class="operation">',
							'<a href="'+data.url+'" target="_blank"><span><i class="comment"></i>'+data.comment_count+'</span></a>',
							'<a><span><i class="read"></i>'+data.view_count+'</span></a>',
							//'<a href="'+data.like_url+'" target="_blank"><span><i class="dislike"></i>'+data.like_count+'</span></a>',
						'</div>',
					'</div>',
				'</div>',
			'</li>'
		].join('');
	});
	return eleText;
},
news : (function(){
	var ads = [], seek = '.ad_650', getAdKey = function(len){
		var k = len;
		if( k >= ads.length ){
			k = 0;
		}
		return k;
	};
	return function(jsonData){
		var eleText = '', adKey;
		ads.length || this.option.parent.children().each(function(){
			var o=$(this);
			o.children(seek).length&&ads.push(o.prop("outerHTML"));
		});
		adKey = getAdKey(this.option.parent.find(seek).length);
		$.each(jsonData.dataList, function(i, data){
			eleText += [
				'<li>',
					'<div class="news_content newsOne">',
						'<h1 class="news_title"><a href="'+data.url+'" target="_blank">'+data.title+'</a></h1>',
						'<div class="abs">'+data.content+'</div>',
						'<div class="news_users">',
							'<a href="'+data.url+'" target="_blank"><span class="comment"></span>'+data.comment_count+'</a>',
							'<a href="javascript:;"><span class="read"></span>'+data.view_count+'</a>',
							//'<a href="'+data.like_url+'" target="_blank"><span class="like"></span>'+data.like_count+'</a>',
							'<span class="time">'+data.post_date+'</span>',
						'</div>',
					'</div>',
					'<div class="newsOne_img"><a href="'+data.url+'" target="_blank"><img src="'+data.news_pic_url+'"  alt=""></a></div>',
				'</li>'
			].join('');
			if( !((i + 1) % 5) ){
				eleText += ads[adKey];
				adKey = getAdKey(adKey + 1);
			}
		});
		return eleText;
	}
})(),
special : function(jsonData){
	var eleText = '';
	$.each(jsonData.dataList, function(i, data){
		var keywords = [];
		$.each(data.keywords, function(k, word){
			keywords.push('<a href="'+data.url+'" target="_blank">'+word+'</a>');
		});
		eleText += [
			'<li class="special_list_img">',
				'<a href="'+data.url+'" target="_blank"><img src="',data.pic_url || '/images_v4/images/special_image.jpg','" alt="'+data.title+'"></a>',
				'<div class="special_des">',
					'<p class="time">'+data.post_date+'</p>',
					'<h1><a href="'+data.url+'" target="_blank">'+data.title+'</a></h1>',
					'<p class="txt"><i></i>'+data.content+'<a href="'+data.url+'" target="_blank">【详细】</a></p>',
					'<div class="keybox">'+keywords.join('')+'</div>',
				'</div>',
			'</li>'
		].join('');
	});
	return eleText
},
view : function(jsonData){
	var resultArray = [];
	$.each(jsonData.dataList, function(i, data){
		resultArray.push([
			'<li>',
				'<div class="category_top">',
					'<span><b>'+data.type+'</b>/'+data.column+'</span>',
					'<span class="name_date"><em>',data.from||'','</em> | '+data.post_date+'</span>',
				'</div>',
				'<h2 class="newsTitle"><a href="'+data.url+'" target="_blank">'+data.title+'</a></h2>',
				'<p><a class="show_img" href="'+data.url+'" target="_blank"><img src="'+data.pic_url+'"></a></p>',
				'<p class="abs">'+data.content+'</p>',
				'<div class="operation">',
					'<div class="ico browse"><i></i>'+data.view_count+'</div>',
					'<a href="'+data.url+'#comment" target="_blank"><div class="ico msg"><i></i>'+data.comment_count+'</div></a>',
					//'<div class="ico praise"><i></i>'+data.like_count+'</div>',
				'</div>',
			'</li>'
		].join(''));
	});
	return resultArray;
}
};
$.extend({
	listPage:function(opt){
		return new topicObj(opt);
	}
});
})();