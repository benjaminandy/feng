/**
 * @author oldj
 * @blog http://oldj.net
 */

'use strict';

function NCFloat(el) {
    this.el = el;
    this.init();
}

NCFloat.prototype = {
init: function () {
        var _this = this;
        this.makeOverlay();
        var as = this.el.getElementsByTagName('a');
        var i;
        var el;
        for (i = 0; i < as.length; i ++) {
            el = as[i];
            if (el.className == 'close') {
                el.onclick = function () {
                    _this.hide();
                    return false;
                };
                break;
            }
        }
        
        var old_hideWindow;
        if (typeof hideWindow === 'function') {
            old_hideWindow = hideWindow;
            hideWindow = function () {
                old_hideWindow();
                location.reload();
            };
        }
    },
    makeOverlay: function () {
        if (this.el_overlay) return;
        var el = document.createElement('div');
        el.className = 'nc-f-overlay';
        document.body.appendChild(el);
        this.el_overlay = el;
    },
    show: function (callback) {
        this.el_overlay.style.display = 'block';
        this.el.style.display = 'block';

        callback && callback();
    },
    hide: function (keep) {
        this.el_overlay.style.display = 'none';
        this.el.style.display = 'none';
        if (!keep) {
            location.reload();
        }
    }
};
